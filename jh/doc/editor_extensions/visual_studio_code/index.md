---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# VS Code 的极狐GitLab 工作流扩展

极狐GitLab 工作流扩展将极狐GitLab 和 Visual Studio Code（VSC）进行集成。您可以减少上下文切换并在 VSC 中执行更多日常任务，例如：

- 查看议题。
- 从 VSC 命令面板运行一般命令。
- 从 VSC 直接创建和评审合并请求。
- 验证您的极狐GitLab CI/CD 配置。
- 查看流水线状态。
- 查看 CI/CD 作业输出。
- 创建和粘贴代码片段到您的编辑器，或从编辑器复制代码片段。
- 不进行克隆就可以浏览仓库。
- 接收代码建议。
- 查看安全发现。

## 下载扩展

从 VSC 下载扩展。

## 配置扩展

下载扩展后，您可以配置：

- 展示或隐藏的功能
- 自签署证书
<!--- [Code Suggestions](../../user/project/repository/code_suggestions.md).-->

## 使用扩展报告议题

您可以在 `gitlab-vscode-extension` 议题队列中报告议题、漏洞或功能请求。

<!--
## 相关主题

- [Download the extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow)
- [Extension documentation](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/README.md)
- [View source code](https://gitlab.com/gitlab-org/gitlab-vscode-extension/)
-->
