---
redirect_to: 'license_approval_policies.md'
remove_date: '2023-07-25'
---

# 许可证检查策略（已删除） **(ULTIMATE)**

此功能废弃于 15.9 版本，删除于 16.0 版本。
使用[许可证批准策略](license_approval_policies.md)代替。

<!-- This redirect file can be deleted after <2023-07-25>. -->
<!-- Redirects that point to other docs in the same project expire in three months. -->
<!-- Redirects that point to docs in a different project or site (link is not relative and starts with `https:`) expire in one year. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/redirects.html -->
