---
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# CycloneDX 文件的许可证扫描 **(ULTIMATE ALL)**

> - [引入](https://gitlab.com/gitlab-org/gitlab/-/issues/384932) 在极狐GitLab 15.9 中，适用于极狐GitLab SaaS 的两个功能标志为 `license_scanning_sbom_scanner` 和 `package_metadata_synchronization`，两个功能标志默认禁用。
> - [GA](https://gitlab.com/gitlab-org/gitlab/-/issues/385176) 在极狐GitLab 16.4 中删除功能标志 `license_scanning_sbom_scanner` 和 `package_metadata_synchronization`。

注意：
极狐GitLab 15.9 中弃用了传统的许可证合规性分析器，并在极狐GitLab 16.3 中删除。要继续使用极狐GitLab 进行许可证合规性，请从 CI/CD 流水线中删除许可证合规性模板，并添加[依赖扫描模板](../../application_security/dependency_scanning/index.md#configuration)。现在，依赖扫描模板能够收集所需的许可证信息，因此不再需要运行单独的许可证合规性作业。在验证实例已升级以支持许可证扫描的新方法之前，不应删除许可证合规性 CI/CD 模板。要快速在规模上使用依赖扫描器，可以在组级别设置[扫描执行策略](../../application_security/policies/scan-execution-policies.md)，以强制对组中所有项目执行基于 SBOM 的许可证扫描。
然后，可以从 CI/CD 配置中删除 `Jobs/License-Scanning.gitlab-ci.yml` 模板。如果希望继续使用传统的许可证合规性功能，可以通过将 `LICENSE_MANAGEMENT_VERSION CI` 变量设置为 `4` 来实现。此变量可以在[项目](../../../ci/variables/index.md#for-a-project)、[组](../../../ci/variables/index.md#for-a-group)或[实例](../../../ci/variables/index.md#for-an-instance)级别设置。

要检测正在使用的许可证，许可证合规性依赖于运行[依赖扫描 CI 作业](../../application_security/dependency_scanning/index.md)，并分析这些作业生成的[CycloneDX](https://cyclonedx.org/)软件构建材料（SBOM）。此扫描方法能够解析和识别[SPDX列表](https://spdx.org/licenses/)中定义的500多种不同类型的许可证。只要它们属于[我们支持的语言之一](#supported-languages-and-package-managers)生成CycloneDX报告，并遵循[GitLab CycloneDX属性分类法](../../../development/sec/cyclonedx_property_taxonomy.md)，就可以使用第三方扫描程序生成依赖列表。请注意，目前尚不能使用CI报告产物作为许可证信息的数据源，并且不在SPDX列表中的许可证将报告为“Unknown”。提供其他许可证的能力在[epic 10861](https://gitlab.com/groups/gitlab-org/-/epics/10861)中跟踪。

注意：
许可证扫描功能依赖于在外部数据库中收集的公开可用的软件包元数据，并与极狐GitLab 实例自动同步。该数据库是一个托管在阿里云的多区域对象存储桶。
扫描仅在极狐GitLab 实例内执行。不会将上下文信息（例如项目依赖项列表）发送到外部服务。

## 配置

要启用CycloneDX文件的许可证扫描：

- 启用[依赖扫描](../../application_security/dependency_scanning/index.md#enabling-the-analyzer)，并确保满足其先决条件。
- 仅适用于极狐GitLab自托管，您可以在极狐GitLab实例的管理区域中[选择要同步的元数据类型](../../../administration/settings/security_and_compliance.md#choose-package-registry-metadata-to-sync)。要使此数据同步起作用，必须允许从您的极狐GitLab 实例到域 `oss-cn-beijing.aliyuncs.com` 的出站网络流量。如果您的网络连接受限或没有网络连接，则请参阅文档中的[在离线环境中运行](#running-in-an-offline-environment)部分以获取进一步的指导。
- 您需要给极狐GitLab 添加自定义环境变量，确保极狐GitLab 可以从同步阿里云对象存储桶中获取到最新扫描许可证时用到的元数据。请按照[阿里云文档](https://help.aliyun.com/zh/ram/user-guide/create-an-accesskey-pair-1)创建 AccessKey ID 和 AccessKey Secret，然后把获取到的值设置到自定义环境变量 `JH_MIRROR_PACKAGE_METADATA_ACCESS_KEY_ID` 和 `JH_MIRROR_PACKAGE_METADATA_ACCESS_KEY_SECRET` 中。环境变量的设置方法请参阅[文档](https://docs.gitlab.cn/omnibus/settings/environment-variables.html)。

<a id="supported-languages-and-package-managers"></a>

## 支持的语言和包管理器

许可证扫描支持以下语言和包管理器:

<!-- markdownlint-disable MD044 -->
<table class="supported-languages">
  <thead>
    <tr>
      <th>语言</th>
      <th>包管理器</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>.NET</td>
      <td rowspan="2"><a href="https://www.nuget.org/">NuGet</a></td>
    </tr>
    <tr>
      <td>C#</td>
    </tr>
    <tr>
      <td>C</td>
      <td rowspan="2"><a href="https://conan.io/">Conan</a></td>
    </tr>
    <tr>
      <td>C++</td>
    </tr>
    <tr>
      <td>Go</td>
      <td><a href="https://go.dev/">Go</a></td>
    </tr>
    <tr>
      <td rowspan="2">Java</td>
      <td><a href="https://gradle.org/">Gradle</a></td>
    </tr>
    <tr>
      <td><a href="https://maven.apache.org/">Maven</a></td>
    </tr>
    <tr>
      <td rowspan="3">JavaScript 和 TypeScript</td>
      <td><a href="https://www.npmjs.com/">npm</a></td>
    </tr>
    <tr>
      <td><a href="https://pnpm.io/">pnpm</a></td>
    </tr>
    <tr>
      <td><a href="https://classic.yarnpkg.com/en/">yarn</a></td>
    </tr>
    <tr>
      <td>PHP</td>
      <td><a href="https://getcomposer.org/">Composer</a></td>
    </tr>
    <tr>
      <td rowspan="4">Python</td>
      <td><a href="https://setuptools.readthedocs.io/en/latest/">setuptools</a></td>
    </tr>
    <tr>
      <td><a href="https://pip.pypa.io/en/stable/">pip</a></td>
    </tr>
    <tr>
      <td><a href="https://pipenv.pypa.io/en/latest/">Pipenv</a></td>
    </tr>
    <tr>
      <td><a href="https://python-poetry.org/">Poetry</a></td>
    </tr>
    <tr>
      <td>Ruby</td>
      <td><a href="https://bundler.io/">Bundler</a></td>
    </tr>
    <tr>
      <td>Scala</td>
      <td><a href="https://www.scala-sbt.org/">sbt</a></td>
    </tr>
  </tbody>
</table>
<!-- markdownlint-disable MD044 -->

支持的文件和版本是由[依赖扫描](../../application_security/dependency_scanning/index.md#supported-languages-and-package-managers)支持的。

## 许可证表达式

CycloneDX文件的许可证扫描不支持[复合许可证](https://spdx.github.io/spdx-spec/v2-draft/SPDX-license-expressions/)。添加此功能在问题 [336878](https://gitlab.com/gitlab-org/gitlab/-/issues/336878) 中进行跟踪。

## 根据检测到的许可证阻止合并请求

用户可以通过配置[许可证批准策略](../license_approval_policies.md)要求根据检测到的许可证批准合并请求。

<a id="running-in-an-offline-environment"></a>

## 在离线环境中运行

对于在通过互联网有限、受限或不稳定网络访问外部资源的环境中运行的自托管极狐GitLab 实例，需要进行一些调整才能成功扫描 CycloneDX 报告以获取许可证。有关更多信息，请参见[快速入门指南](../../../topics/offline/quick_start_guide#%E5%90%AF%E7%94%A8%E5%8C%85%E5%85%83%E6%95%B0%E6%8D%AE%E6%95%B0%E6%8D%AE%E5%BA%93)。

## 故障排除

### 未扫描CycloneDX文件且似乎未提供任何结果

确保CycloneDX文件符合[CycloneDX JSON规范](https://cyclonedx.org/docs/latest/json)。该规范不允许[重复条目](https://cyclonedx.org/docs/latest/json/#components)。包含多个SBOM文件的项目应将每个SBOM文件报告为单独的CI报告产物，或者应确保如果将SBOM合并为CI流水线的一部分，则删除重复项。

可以通过以下方式验证CycloneDX SBOM文件是否符合`CycloneDX JSON规范`：

```shell
$ docker run -it --rm -v "$PWD:/my-cyclonedx-sboms" -w /my-cyclonedx-sboms cyclonedx/cyclonedx-cli:latest cyclonedx validate --input-version v1_4 --input-file gl-sbom-all.cdx.json

Validating JSON BOM...
BOM validated successfully.
```

如果JSON BOM未通过验证，例如因为存在重复组件：

```shell
Validation failed: Found duplicates at the following index pairs: "(A, B), (C, D)"
#/properties/components/uniqueItems
```

可以通过更新CI模板以使用[jq](https://jqlang.github.io/jq/)从`gl-sbom-*.cdx.json`报告中删除重复组件的工作定义来解决此问题。例如，以下操作会从`gemnasium-dependency_scanning`作业生成的`gl-sbom-gem-bundler.cdx.json`报告文件中删除重复的组件：

```yaml
include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

gemnasium-dependency_scanning:
  after_script:
    - apk update && apk add jq
    - jq '.components |= unique' gl-sbom-gem-bundler.cdx.json > tmp.json && mv tmp.json gl-sbom-gem-bundler.cdx.json
```

### 删除未使用的许可证数据

许可证扫描更改（在极狐GitLab 15.9中发布）需要实例上有大量额外的磁盘空间。这个问题在极狐GitLab 16.3中通过[减少磁盘上包元数据表的占用](https://gitlab.com/groups/gitlab-org/-/epics/10415)史诗得以解决。但是，如果您的实例在极狐GitLab 15.9 和 16.3 之间运行了许可证扫描，您可能希望删除不必要的数据。

要删除不必要的数据：

1. 检查[package_metadata_synchronization](https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#new-license-compliance-scanner)功能标志当前是否启用，或者以前是否启用，并在这种情况下禁用它。使用[Rails控制台](../../../administration/operations/rails_console.md)执行以下命令。

   ```ruby
   Feature.enabled?(:package_metadata_synchronization) && Feature.disable(:package_metadata_synchronization)
   ```

1. 检查数据库中是否存在废弃的数据：

   ```ruby
   PackageMetadata::PackageVersionLicense.count
   PackageMetadata::PackageVersion.count
   ```

1. 如果数据库中存在废弃的数据，请按顺序运行以下命令删除它们：

   ```ruby
   ActiveRecord::Base.connection.execute('SET statement_timeout TO 0')
   PackageMetadata::PackageVersionLicense.delete_all
   PackageMetadata::PackageVersion.delete_all
   ```
