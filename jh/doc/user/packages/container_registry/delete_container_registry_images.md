---
stage: Package
group: Container Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 从容器镜像库中删除容器镜像 **(BASIC ALL)**

您可以从容器镜像库中删除容器镜像。

WARNING:
删除容器镜像是一种破坏性操作，无法撤消。要恢复已删除的容器镜像，您必须重建并重新上传。

<a id="garbage-collection"></a>

## 垃圾收集

删除私有化部署实例上的容器镜像不会释放存储空间，只会将镜像标记为可以删除。要实际删除未引用的容器镜像并恢复存储空间，管理员必须运行[垃圾收集](../../../administration/packages/container_registry.md#container-registry-garbage-collection)。

在 JiHuLab.com 上，最新版本的容器镜像库包含自动在线垃圾收集器。<!--For more information, see [this blog post](https://about.gitlab.com/blog/2021/10/25/gitlab-com-container-registry-update/).-->
在这个新版本的容器镜像库中，以下内容在不被引用超过 24 小时后将会被自动计划删除:

- 任何镜像清单均未引用的镜像层。
- 没有标签且不被其他清单引用的镜像清单（如多架构镜像）。

在线垃圾收集器是实例范围的功能，适用于所有命名空间。

<a id="use-the-gitlab-ui"></a>

## 使用极狐GitLab UI

使用极狐GitLab UI 删除容器镜像：

1. 在左侧边栏中，选择 **搜索或转到**，然后：
   - 对于项目，选择您的项目。
   - 对于群组，选择您的群组。
1. 在左侧边栏中，选择 **部署 > 容器镜像库**。
1. 在 **容器镜像库** 页面中，您可以通过以下任一方式选择要删除的内容：

   - 通过选择红色的 **{remove}** 图标删除整个镜像及其包含的所有标签。
   - 进入镜像页面，并通过选择要删除的标签旁边的红色 **{remove}** 图标，单独或批量删除标签。

1. 在对话框中，选择 **删除标签**。

## 使用 API

您可以使用 API 自动执行删除容器镜像的过程。有关详细信息，请参阅以下端点：

- [删除镜像](../../../api/container_registry.md#delete-registry-repository)
- [删除镜像标签](../../../api/container_registry.md#delete-a-registry-repository-tag)
- [批量删除镜像标签](../../../api/container_registry.md#delete-registry-repository-tags-in-bulk)

## 使用极狐GitLab CI/CD

NOTE:
极狐GitLab CI/CD 不提供删除容器镜像的内置方法。此示例使用名为 [reg](https://github.com/genuinetools/reg) 的第三方工具与容器镜像库 API 通信。有关此第三方工具的帮助，请参阅 [reg 的议题队列](https://github.com/genuinetools/reg/issues)。

以下示例定义了两个阶段：`build` 和 `clean`。`build_image` 作业为分支构建容器镜像，而 `delete_image` 作业删除它。`reg` 可执行文件被下载，并用于删除与 `$CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG` [预定义的 CI/CD 变量](../../../ci/variables/predefined_variables.md) 匹配的容器镜像。

要使用此示例，请更改 `IMAGE_TAG` 变量来满足您的需要。

```yaml
stages:
  - build
  - clean

build_image:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  only:
    - branches
  except:
    - main

delete_image:
  before_script:
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v$REG_VERSION/reg-linux-amd64" --output ./reg
    - echo "$REG_SHA256  ./reg" | sha256sum -c -
    - chmod a+x ./reg
  image: curlimages/curl:7.86.0
  script:
    - ./reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $IMAGE_TAG
  stage: clean
  variables:
    IMAGE_TAG: $CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG
    REG_SHA256: ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
    REG_VERSION: 0.16.1
  only:
    - branches
  except:
    - main
```

NOTE:
您可以从 [发布页面](https://github.com/genuinetools/reg/releases) 下载最新的 `reg` 版本，然后通过更改 `delete_image` 作业中定义的 `REG_SHA256` 和 `REG_VERSION` 变量来更新代码示例。

## 使用清理策略

您可以为每个项目创建一个[清理策略](reduce_container_registry_storage.md#cleanup-policy)，确保定期从容器镜像库中删除旧标签和镜像。
