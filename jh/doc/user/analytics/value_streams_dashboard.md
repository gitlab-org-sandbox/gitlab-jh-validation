---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 价值流仪表盘（Beta） **(ULTIMATE ALL)**

> - 引入于 15.8 版本，作为内部 [Beta](../../policy/alpha-beta-support.md#beta) 功能，[功能标志](../../administration/feature_flags.md)为 `group_analytics_dashboards_page`。默认禁用。
> - 发布于 15.11 版本，作为 [Beta](../../policy/alpha-beta-support.md#beta) 功能，[功能标志](../../administration/feature_flags.md)为 `group_analytics_dashboards_page`。默认启用。
> - 一般可用于 16.0 版本。删除功能标志 `group_analytics_dashboards_page`。

<!--To help us improve the Value Streams Dashboard, please share feedback about your experience in this [survey](https://gitlab.fra1.qualtrics.com/jfe/form/SV_50guMGNU2HhLeT4).
有关更多信息，另请参阅[价值流管理类别方向页面](https://about.gitlab.com/direction/plan/value_stream_management/)。-->

价值流仪表盘是一个可自定义的仪表盘，可用于识别数字化转型改进的趋势、模式和机会。
价值流仪表盘中的集中式 UI 作为单一事实来源 (SSOT)，所有利益相关者都可以访问和查看与组织相关的同一组指标。

价值流仪表盘包括以下指标：
- [DORA 指标](dora_metrics.md)
- [价值流分析（VSA） - 流指标](../group/value_stream_analytics/index.md)
- 漏洞<!--[漏洞](https://gitlab.com/gitlab-org/gitlab/-/security/vulnerability_report)-->指标

您可以使用价值流仪表盘：

- 跟踪并比较一段时间内的上述指标。
- 尽早识别下降趋势。
- 了解安全风险。
- 深入研究各个项目或指标，采取改进措施。

## DevSecOps 指标比较面板

DevSecOps 指标比较显示一个群组或项目，在本月至今、上个月、前一个月和过去 180 天内的 DORA4、漏洞和流指标。

此可视化可帮助您获得对多个 DevOps 指标的高级自定义视图，并了解它们是否逐月改进。您可以一目了然地比较群组、项目和团队之间的表现，可帮助您确定价值贡献最大、表现出色或表现不佳的团队和项目。

![DevOps metrics comparison](img/devops_metrics_comparison_v15_8.png)

您还可以向下钻取指标以进行进一步分析。
当您将鼠标悬停在某个指标上时，工具提示会显示该指标的说明以及指向相关文档页面的链接。

## DORA Performer 评分面板

> 引入于极狐GitLab 16.2，[功能标志](../../administration/feature_flags.md)为 `dora_performers_score_panel`。默认禁用。

FLAG:
默认情况下此功能不可用。要使其可用，管理员可以启用名为 `dora_performers_score_panel` 的[功能标志](../../administration/feature_flags.md)。

[DORA 指标](dora_metrics.md) Performer 评分面板是一个条形图，可直观地显示组织在不同项目中的 DevOps 绩效水平的状态。

该图表是项目 DORA 分数的细分，分为高、中和低。
它聚合了群组中的所有子项目。

图表上的每个条形显示每个分数类别的项目总数，每月计算一次。
要从图表中排除数据（例如“不包括”），请在图例中选择要排除的系列。
将鼠标悬停在每个条上会显示一个解释分数定义的对话框。

例如，如果项目的部署频率（速度）部分得分较高，则意味着该项目每天至少部署到生产环境一次。

| 指标     | 描述                     |高|中|低|
|--------|------------------------|------|-------|-----|
| 部署频率   | 每天部署到生产环境的数量           | ≥30 | 1-29 | ≤1 |
| 变更准备时间 | 从代码提交到代码在生产中成功运行所需的天数  | ≤7 | 8-29 | ≥30 |
| 恢复服务时间 | 发生影响用户的服务事件或缺陷时恢复服务的天数 | ≤1 | 2-6 | ≥7 |
| 变更失败率  | 生产变化导致服务质量下降的百分比       |≤15%   | 16%-44% | ≥45% |

这些评分基于 Google 在 [DORA 2022 DevOps 加速状态报告](https://cloud.google.com/blog/products/devops-sre/dora-2022-accelerate-state-of-devops-report-now-out)中的分类。

## 查看价值流仪表盘

先决条件：

- 要查看某个群组的价值流仪表盘，您必须至少具有该群组的报告者角色。

查看价值流仪表盘：

- 从分析仪表盘：

    1. 在群组的左侧边栏上，选择 **搜索或转到** 并找到您的群组。
    1. 选择 **分析 > 分析仪表盘**。

- 从价值流分析：

    1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目或群组。
    1. 选择 **分析 > 价值流分析**。
    1. 在 **筛选结果** 文本框下，在 **生命周期s** 行，选择 **价值流仪表盘 / DORA**。
    1. 可选。要打开新页面，将 `/analytics/dashboards/value_streams_dashboard` 附加到群组 URL（例如 `https://gitlab.com/groups/gitlab-org/-/analytics/dashboards/value_streams_dashboard`）。

## 自定义仪表盘面板

您可以自定义价值流仪表盘并配置要包含在页面中的子组和项目。

一个视图最多可以显示四个子组或项目。

### 使用查询参数

要显示多个子组和项目，请将它们的路径指定为 URL 参数。

例如，参数 `query=gitlab-org/gitlab-ui,gitlab-org/plan-stage` 显示三个独立的面板，分别用于：

- `gitlab-org` 群组
- `gitlab-ui` 项目
- `gitlab-org/plan-stage` 子群组

### 使用 YAML 配置

要更改页面的默认内容，您需要在您选择的项目中创建一个 YAML 配置文件。查询参数仍可用于覆盖 YAML 配置。

首先，您需要设置项目。

先决条件：

- 您必须至少具有该群组的维护者角色。

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的群组。
1. 选择 **设置 > 通用**。
1. 展开 **分析**。
1. 选择您要存储 YAML 配置文件的项目。
1. 选择 **保存更改**。

设置项目后，设置配置文件：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在默认分支中，创建配置文件：`.gitlab/analytics/dashboards/value_streams/value_streams.yaml`。
1. 在 `value_streams.yaml` 配置文件中，填写配置选项：

```yaml
# title - Change the title of the Value Streams Dashboard. [optional]
title: 'Custom Dashboard title'

# description - Change the description of the Value Streams Dashboard. [optional]
description: 'Custom description'

# panels - List of panels that contain panel settings.
#   title - Change the title of the panel. [optional]
#   data.namespace - The Group or Project path to use for the chart panel.
#   data.exclude_metrics - Hide rows by metric ID from the chart panel.
#   data.filter_labels -
#     Only show results for data that matches the queried label(s). If multiple labels are provided,
#     only a single label needs to match for the data to be included in the results.
#     Compatible metrics (other metrics will be automatically excluded):
#       * lead_time
#       * cycle_time
#       * issues
#       * issues_completed
#       * merge_request_throughput
panels:
  - title: 'My Custom Project'
    data:
      namespace: group/my-custom-project
  - data:
      namespace: group/another-project
      filter_labels:
        - in_development
        - in_review
  - title: 'My Custom Group'
    data:
      namespace: group/my-custom-group
      exclude_metrics:
        - deployment_frequency
        - change_failure_rate
  - data:
      namespace: group/another-group
```

  以下示例为 `my-group` 命名空间的面板提供了一个选项配置：

  ```yaml
  panels:
    - data:
        namespace: my-group
  ```

## 仪表盘指标和下钻报告

| 指标        | 描述                                  | 下钻报告                                                                                                                      | 文档页面                                                                           | ID  |
|-----------|-------------------------------------|---------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------|-----|
| 部署频率      | 每天平均部署到生产的次数。指标衡量向最终用户交付价值的频率。      | [部署频率选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=deployment-frequency)                                | [部署频率](dora_metrics.md#deployment-frequency)                                   | `deployment_frequency` |
| 变更的前置时间   | 将提交成功交付到生产环境的时间。该指标反映了 CI/CD 流水线的效率。 | [前置时间选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=lead-time)                                           | [变更的前置时间](dora_metrics.md#lead-time-for-changes)                               | `lead_time_for_changes` |
| 恢复服务时间    | 组织从生产故障中恢复所需的时间。                    | [恢复服务时间选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=time-to-restore-service)                           | [恢复服务时间](dora_metrics.md#time-to-restore-service)                              | `time_to_restore_service` |
| 变更失败率     | 在生产中导致事件的部署百分比。                     | [变更失败率选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=change-failure-rate)                                | [变更失败率](dora_metrics.md#change-failure-rate)                                   |`change_failure_rate` |
| 前置时间      | 从议题创建到议题关闭的中位时间。                    | [价值流分析](https://jihulab.com/groups/gitlab-cn/-/analytics/value_stream_analytics)                                          | [查看议题的前置时间和周期时间](../group/value_stream_analytics/index.md#key-metrics)         |  `lead_time` |
| 周期时间      | 从最早提交关联议题的合并请求，到该议题关闭的中位时间。         | [VSA 概览](https://jihulab.com/groups/gitlab-cn/-/analytics/value_stream_analytics)                                         | [查看议题的前置时间和周期时间](../group/value_stream_analytics/index.md#key-metrics)         | `cycle_time` |
| 新议题       | 创建的新议题数。                            | [议题分析](https://jihulab.com/groups/gitlab-cn/-/issues_analytics)                                                           | [项目](issue_analytics.md)和[群组](../../user/group/issues_analytics/index.md)的议题分析 | `issues`  |
| 关闭的议题     | 按月关闭的议题的数量                          | [价值流分析](https://jihulab.com/groups/gitlab-cn/-/analytics/value_stream_analytics)                                          | [价值流分析](../group/value_stream_analytics/index.md)                              | `issues_completed`|
| 部署次数      | 部署到生产环境的总数。                         | [合并请求分析](https://jihulab.com/gitlab-cn/gitlab/-/analytics/merge_request_analytics)                                        | [合并请求分析](merge_request_analytics.md)                                           | `deploys` |
| 合并请求吞吐量   | 按月合并的合并请求的数量                        | [群组生产力分析](productivity_analytics.md)、[项目合并请求分析](https://jihulab.com/gitlab-cn/gitlab/-/analytics/merge_request_analytics) | [群组生产力分析](productivity_analytics.md) [项目合并请求分析](merge_request_analytics.md)    | `merge_request_throughput`|
| 严重漏洞      | 项目或群组中随时间推移的严重漏洞                    | [漏洞报告](https://jihulab.com/gitlab-cn/gitlab/-/security/vulnerability_report)                                              | [漏洞报告](../application_security/vulnerability_report/index.md)                  | `vulnerability_critical` |
| 高危漏洞      | 项目或群组中随时间推移的高危漏洞                    | [漏洞报告](https://jihulab.com/gitlab-cn/gitlab/-/security/vulnerability_report)                                              | [漏洞报告](../application_security/vulnerability_report/index.md)                  | `vulnerability_high`|

## 使用 Jira 的价值流仪表盘指标

以下指标不依赖于使用 Jira：

- DORA 部署频率
- DORA 变更前置时间
- 部署数量
- 合并请求吞吐量
- 漏洞
