---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 目标和关键结果（OKR） **(ULTIMATE ALL)**

> 引入于 15.6 版本，[功能标志](../administration/feature_flags.md)为 `okrs_mvc`。默认禁用。

OKR 是[实验](../policy/experiment-beta-support.md#experiment)性功能。

FLAG:
在私有化部署版上，此功能默认不可用。如想启用该项目，管理员可以[启用功能标志](../administration/feature_flags.md) `okrs_mvc`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

目标和关键结果（OKR）是用于设置和跟踪与组织的总体战略和愿景保持一致的目标的框架。

极狐GitLab 中的目标和关键结果的很多特性功能相近。在文档中，术语 **OKR** 指的是目标和关键结果。

OKR 是一种工作项。
<!--For the roadmap of migrating [issues](project/issues/index.md) and [epics](group/epics/index.md)
to work items and adding custom work item types, see
[epic 6033](https://gitlab.com/groups/gitlab-org/-/epics/6033) or the
[Plan direction page](https://about.gitlab.com/direction/plan/).-->

## 设计有效的 OKR

使用目标和关键结果使您的员工朝着共同目标迈进并跟踪进度。
设定一个大目标为 O，并使用[子目标和关键结果](#child-objectives-and-key-results)来衡量大目标的完成情况。

**O** 是要实现的理想目标，并定义**您打算做什么**，通过将个人、团队或部门的工作与公司整体战略联系起来，展示他们的工作如何影响组织的总体方向。

**KR** 是针对一致目标的进展衡量标准，表达了**您如何知道是否达到了目标**（O）。
通过实现特定结果（KR），您可以为实现相关 O 取得进展。

您可以用这句话确定您的 OKR 是否有意义：

<!-- vale gitlab.FutureTense = NO -->
> 我/我们将通过获得和实现以下指标（KR），在（日期）之前完成（O）。
<!-- vale gitlab.FutureTense = YES -->

<!--
To learn how to create better OKRs and how we use them at GitLab, see the
[Objectives and Key Results handbook page](https://about.gitlab.com/company/okrs/).
-->

## 创建目标

先决条件：

- 您必须至少具有该项目的访客角色。

创建目标：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **计划 > 议题**。
1. 在右上角的 **新建议题** 旁边，选择向下箭头 **{chevron-lg-down}**，然后选择 **新建目标**。
1. 再次选择 **新建目标**。
1. 输入目标名称。
1. 选择 **创建目标**。

要创建关键结果，请将其[添加为现有目标的子项](#add-a-child-key-result)。

<a id="view-an-objective"></a>

## 查看目标

先决条件：

- 您必须至少具有该项目的访客角色。

查看目标：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **计划 > 议题**。
1. 使用 `Type = objective` [过滤议题列表](project/issues/managing_issues.md#filter-the-list-of-issues)。
1. 从列表中选择一个目标的标题。

<a id="view-a-key-result"></a>

## 查看关键结果

先决条件：

- 您必须至少具有该项目的访客角色。

查看关键结果：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **计划 > 议题**。
1. 使用 `Type = key_result` [筛选议题列表](project/issues/managing_issues.md#filter-the-list-of-issues)。
1. 从列表中选择关键结果的标题。

或者，您可以从其父级目标中的 **子目标和关键结果** 部分访问关键结果。

## 编辑标题和描述

先决条件：

- 您必须至少具有该项目的报告者角色。

要编辑 OKR：

1. [打开您要编辑的目标](#view-an-objective)或[关键结果](#view-a-key-result)。
1. 可选。要编辑标题，请选择它，进行更改，然后选择标题文本框之外的任何区域。
1. 可选。要编辑说明，请选择编辑图标 (**{pencil}**)，进行更改，然后选择 **保存**。

## 查看 OKR 系统备注

> - 引入于 15.7 版本，[功能标志](../administration/feature_flags.md)为 `work_items_mvc_2`。默认禁用。
> - 功能标志变更于 15.8 版本，变更为 `work_items_mvc`。默认禁用。
> - 更改活动排序顺序功能引入于 15.8 版本。
> - 过滤活动功能引入于 15.10 版本。
> - 在 SaaS 和私有化部署版上默认启用于 15.10 版本。

先决条件：

- 您必须至少具有该项目的报告者角色。

您可以查看与该任务相关的所有系统备注。默认情况下，它们按 **最旧的优先** 排序。
您始终可以将排序顺序更改为 **最新的优先**，支持跨会话生效。

## 评论和主题

您可以在任务中添加[评论](discussions/index.md)和回复主题。

## 指派用户

要显示谁负责 OKR，您可以将用户分配给它。

基础版的用户可以为每个 OKR 分配一个用户。
专业版及更高版本的用户可以将多个用户分配给一个 OKR。
另请参阅[议题的多个指派人](project/issues/multiple_assignees_for_issues.md)。

先决条件：

- 您必须至少具有该项目的报告者角色。

要更改 OKR 的指派人：

1. [打开您要编辑的目标](#view-an-objective)或[关键结果](#view-a-key-result)。
1. 在 **指派人** 旁边，选择 **添加指派人**。
1. 从下拉列表中选择要添加为指派人的用户。
1. 选择下拉列表之外的任何区域。

## 分配标记

先决条件：

- 您必须至少具有该项目的报告者角色。

使用[标记](project/labels.md)，在团队之间组织 OKR。

为 OKR 添加标记：

1. [打开您要编辑的目标](okrs.md#view-an-objective)或[关键结果](#view-a-key-result)。
1. 在 **标记** 旁边，选择 **添加标记**。
1. 从下拉列表中选择要添加的标记。
1. 选择下拉列表之外的任何区域。

## 添加目标到里程碑

> 引入于 15.7 版本。

您可以将目标添加到[里程碑](project/milestones/index.md)。
查看目标时，您可以看到里程碑标题。

先决条件：

- 您必须至少具有该项目的报告者角色。

向里程碑添加目标：

1. [打开您要编辑的目标](#view-an-objective)。
1. 在 **里程碑** 旁边，选择 **添加到里程碑**。如果目标已经属于里程碑，则下拉列表会显示当前里程碑。
1. 从下拉列表中选择要与目标关联的里程碑。

## 设置目标进度

显示完成目标所需的工作量。

您只能在目标上手动设置进度，不支持从子目标或关键结果中自动汇总。

先决条件：

- 您必须至少具有该项目的报告者角色。

设定目标的进度：

1. [打开您要编辑的目标](#view-an-objective)。
1. 选择 **进度** 旁边的文本框。
1. 输入 0 到 100 之间的数字。

## 设置健康状态

> 引入于 15.7 版本。

为了更好地跟踪实现目标的风险，您可以为每个目标和关键结果分配一个[健康状态](project/issues/managing_issues.md#health-status)。
您可以使用健康状态向组织中的其他人发出信号，表明 OKR 是按计划进行还是需要注意保持进度。

先决条件：

- 您必须至少具有该项目的报告者角色。

设置 OKR 的健康状态：

1. [打开要编辑的关键结果](#view-a-key-result)。
1. 在 **健康状态** 旁边，选择下拉列表并选择所需的健康状态。

## 将关键结果升级为目标

> - 引入于极狐GitLab 16.0。
> - 快速操作 `/promote_to` 引入于极狐GitLab 16.1。

先决条件：

- 您必须至少具有该项目的报告者角色。

升级关键结果：

1. [打开关键结果](#view-a-key-result)。
1. 在右上角，选择垂直省略号 (**{ellipsis_v}**)..。
1. 选择 **升级为目标**。

另外，您也可以使用 `/promote_to objective` [快速操作](../user/project/quick_actions.md)。

## 复制目标或关键结果引用

> 引入于极狐GitLab 16.1。

要在极狐GitLab 的其他地方引用目标或关键结果，您可以使用其完整 URL 或简短引用，类似 `namespace/project-name#123`，其中 `namespace` 是群组或用户名。

要将目标或关键结果参考复制到剪贴板：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **计划 > 议题**，然后选择您的目标或关键结果进行查看。
1. 在右上角，选择垂直省略号 (**{ellipsis_v}**)，然后选择 **复制引用**。

您现在可以引用粘贴到其他描述或评论中。

在[极狐GitLab Flavored Markdown](markdown.md#gitlab-specific-references) 中了解有关目标或关键结果参考的更多信息。

## 复制目标或关键结果电子邮件地址

> 引入于极狐GitLab 16.1。

您可以通过发送电子邮件在目标或关键结果中创建评论。
向此地址发送电子邮件会创建包含电子邮件正文的评论。

有关通过发送电子邮件创建评论和必要配置的更多信息，请参阅[通过发送电子邮件回复评论](discussions/index.md#reply-to-a-comment-by-sending-email)。

要复制目标或关键结果的电子邮件地址：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **计划 > 议题**，然后选择您的议题进行查看。
1. 在右上角，选择垂直省略号 (**{ellipsis_v}**)，然后选择 **复制目标电子邮件地址** 或 **复制关键结果电子邮件地址**。

## 关闭 OKR

完成 OKR 后，您可以关闭它。
OKR 标记为已关闭但未删除。

先决条件：

- 您必须至少具有该项目的报告者角色。

关闭 OKR：

1. [打开您要编辑的目标](#view-an-objective)。
1. 在 **状态** 旁边，选择 **已关闭**。

您可以用同样的方式重新打开一个关闭的 OKR。

<a id="child-objectives-and-key-results"></a>

## 子目标和关键结果

在极狐GitLab 中，O 与 KR 类似。
在您的工作流程中，使用 KR 来衡量 O 中描述的目标。

您可以将子目标添加到总共 9 个级别。一个目标最多可以有 100 个子 OKR。
关键结果是目标的子项，关键结果本身不能有子项。

目标描述下方的 **子目标和关键结果** 部分，提供了子目标和关键结果。

### 添加子目标

先决条件：

- 您必须至少具有该项目的访客角色。

向目标添加新的子目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **新建目标**。
1. 输入新目标的标题。
1. 选择 **创建目标**。

将现有目标添加到其它目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **现有目标**。
1. 通过输入部分标题，搜索所需的目标，然后选择所需的匹配项。

    要添加多个目标，请重复此步骤。

1. 选择 **添加目标**。

### 添加子关键结果

先决条件：

- 您必须至少具有该项目的访客角色。

要向目标添加新的关键结果：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **新建关键结果**。
1. 输入新关键结果的标题。
1. 选择 **创建关键结果**。

要将现有关键结果添加到目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **现有关键结果**。
1. 通过输入部分标题，搜索所需的 OKR，然后选择所需的匹配项。

    要添加多个目标，请重复此步骤。

1. 选择 **添加关键结果**。

### 重新排序子目标和关键结果

> 引入于极狐itLab 16.0。

先决条件：

- 您必须至少具有该项目的报告者角色。

默认情况下，子 OKR 按创建日期排序。
如要重新排序，请进行拖动。

### 计划 OKR check-in 提醒

> 引入于极狐GitLab 16.4，[功能标志](../administration/feature_flags.md)为 `okr_checkin_reminders`。默认禁用。

FLAG:
在私有化部署版本中，默认情况下此功能不可用。要启用该功能，管理员可以启用名为 `okr_checkin_reminders` 的[功能标志](../administration/feature_flags.md)。
在 JihuLab.com 上，此功能不可用。
此功能尚未准备好用于生产用途。

安排 check-in 提醒，提醒您的团队提供您关心的关键结果的状态更新。
提醒会以电子邮件通知和待办事项的形式发送给后代对象和关键结果的所有指派人。
用户无法取消订阅电子邮件通知，但可以关闭 check-in 提醒。
提醒会在周二进行发送。

先决条件：

- 您必须至少具有该项目的报告者角色。
- 项目中必须有至少一个目标和至少一个关键成果。
- 您可以仅为顶级目标安排提醒。
  为子目标安排 check-in 提醒没有效果。
  顶级目标的设置会继承到所有子目标。

要为某个目标安排定期提醒，请在新评论中使用 `/checkin_reminder <cadence>` [快速操作](project/quick_actions.md#work-items)。
`<cadence>` 的选项有：

- `weekly`
- `twice-monthly`
- `monthly`
- `never`（默认）

例如，安排每周的 check-in 提醒，请输入：

```plaintext
/checkin_reminder weekly
```

要关闭 check-in 提醒，请输入：

```plaintext
/checkin_reminder never
```

## 私密 OKR

> 引入于极狐GitLab 15.3。

私密 OKR 是仅具有[足够权限](#who-can-see-confidential-tasks)的项目成员可见的 OKR。
您可以使用私密 OKR 来保护安全漏洞的私密性或防止意外泄露。

### 将 OKR 设置为私密

默认情况下，OKR 是公开的。
您可以在创建或编辑 OKR 时将其设为私密。

先决条件：

- 您必须至少具有该项目的报告者角色。
- **私密目标**只能有私密[子目标或关键结果](#child-objectives-and-key-results)：
    - 要将目标设置为私密：如果它有任何子目标或关键结果，您必须首先将子目标或关键结果全部设置为私密或将其删除。
    - 要将目标设置为非私密：如果它有任何子目标或关键结果，您必须首先将子目标或关键结果全部设为非私密或将其删除。
    - 要将子目标或关键结果添加到私密目标中，您必须首先将子目标或关键结果设为私密。

#### 在新 OKR 中

创建新目标时，文本区域正下方的复选框可用于将 OKR 标记为私密。

选中该框并选择 **创建目标** 或 **创建关键结果** 以创建 OKR。

#### 在现有 OKR 中

要更改现有 OKR 的私密性：

1. [打开目标](#view-an-objective)或[关键结果](#view-a-key-result)。
1. 在右上角，选择垂直省略号 (**{ellipsis_v}**)。
1. 选择 **开启私密**。

<a id="who-can-see-confidential-tasks"></a>

### 谁可以查看私密 OKR

当 OKR 被设置为私密时，只有至少具有项目报告者角色的用户才可以访问 OKR。

具有访客或[最小访问权限](permissions.md#users-with-minimal-access)的用户无法访问该 OKR，即使他们在 OKR 被更改为私密之前积极参与此 OKR。

但是，具有**访客角色**的用户可以创建私密 OKR，但只能查看他们自己创建的 OKR。

如果具有访客角色的用户或非成员用户被分配给该 OKR，则他们可以查看该私密 OKR。
当具有访客角色的用户或非成员用户被取消分配私密 OKR 后，他们将无法再查看该 OKR。

没有权限的用户在搜索时，私密 OKR 不会展示在搜索结果中。

### 私密 OKR 标识

私密 OKR 在视觉上与常规任务在一些方面有所不同。
无论在何处列出 OKR，您都可以在标记为私密的 OKR 旁边看到私密(**{eye-slash}**)图标。

如果您没有[足够的权限](#who-can-see-confidential-tasks)，您将无法看到私密 OKR。

同样，在 OKR 内部，您可以在面包屑旁边看到私密(**{eye-slash}**)图标。

从常规到私密和从私密到常规的每次更改，都由 OKR 评论中的系统注释进行标识，例如：

> - **{eye-slash}** Jo Garcia 在五分钟前将议题标记为私密
> - **{eye}** Jo Garcia 刚刚将议题标记为公开

## 两列布局

> 引入于GitLab 16.2，[功能标志](../administration/feature_flags.md)为 `work_items_mvc_2`。默认禁用。

FLAG:
在私有化部署版本中，默认情况下此功能不可用。要使其可用，管理员可以启用名为 `work_items_mvc_2` 的[功能标志](../administration/feature_flags.md)。
在 JihuLab.com 上，此功能不可用。
此功能尚未准备好用于生产用途。

启用后，OKR 使用两列布局，类似于议题。
描述和主题位于左侧，属性（例如标签或指派人）位于右侧。

![OKR two column view](img/objective_two_column_view_v16_2.png)

## OKR 中的关联项

> 引入于极狐GitLab 16.5，[功能标志](../administration/feature_flags.md)为 `linked_work_items`。默认禁用。

FLAG:
在私有化部署版本中，默认情况下此功能不可用。要使其可用，管理员可以启用名为 `linked_work_items` 的[功能标志](../administration/feature_flags.md)。
在 JihuLab.com 上，此功能不可用。
此功能尚未准备好用于生产用途。

关联的项目是双向的，并显示在子目标和关键结果下方的部分中。您可以关联同一项目中的目标、关键结果或任务。

仅当用户可以看到这两个项目时，该关系才会显示在 UI 中。

### 添加关联项

先决条件：

- 您必须至少具有该项目的访客角色。

要将项目关联到目标和关键结果：

1. 在目标和关键结果的 **关联项** 部分中，选择 **添加**。
1. 选择两个项目之间的关系：
    - **关联**
    - **阻塞**
    - **被阻塞**
1. 输入该项目的搜索文本。
1. 添加所有要关联的项目后，选择搜索框下方的 **添加**。

添加完所有关联的项目后，您可以看到它们已分类，以便可以更加直观地理解它们之间的关系。

![Linked items block](img/linked_items_list_v16_5.png)

### 移除关联项

先决条件：

- 您必须至少具有该项目的访客角色。

在目标和关键结果的 **关联项** 部分中，在每个项目旁边，选择垂直省略号 (**{ellipsis_v}**)，然后选择 **删除**。

由于双向关系，该关系不再出现在任一项目中。
