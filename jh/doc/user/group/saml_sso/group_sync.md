---
type: reference, howto
stage: Govern
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# SAML 群组同步 **(PREMIUM ALL)**

> 私有化部署实例中此功能引入于极狐GitLab 15.1。

WARNING:
添加或更改群组同步配置可以从映射的极狐GitLab 群组中移除用户。
如果群组名称与 SAML 响应中的 `groups` 列表不匹配，则会进行移除。
在进行更改之前，您需要确保 SAML 响应包含 `groups` 属性并且 `AttributeValue` 的值与极狐GitLab 中的 **SAML 群组名称**匹配，或者您确保从极狐GitLab 中移除所有群组以禁用群组同步。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo of Group Sync using Azure, see [Demo: SAML Group Sync](https://youtu.be/Iqvo2tJfXjg).
-->

## 配置 SAML 群组同步

NOTE:
如果您使用 SAML 群组同步并拥有多个极狐GitLab 节点（例如在分布式或高可用架构中），则除了 Rails 应用程序节点之外，您还必须在所有 Sidekiq 节点上包含 SAML 配置块。

WARNING:
为了防止用户意外从极狐GitLab 群组中移除，请在极狐GitLab 中启用群组同步之前严格遵循以下要求：

要为私有化部署的极狐GitLab 实例配置 SAML 群组同步：

1. 配置 [SAML OmniAuth Provider](../../../integration/saml.md)。
1. 确保您的 SAML 身份提供商发送与 `groups_attribute` 设置的值同名的属性语句。请参阅以下属性声明示例以供参考：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "saml",
       label: "Provider name", # optional label for login button, defaults to "Saml",
       groups_attribute: 'Groups',
       args: {
         assertion_consumer_service_url: "https://gitlab.example.com/users/auth/saml/callback",
         idp_cert_fingerprint: "43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8",
         idp_sso_target_url: "https://login.example.com/idp",
         issuer: "https://gitlab.example.com",
         name_identifier_format: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
       }
     }
   ]
   ```

要为 JihuLab.com 实例配置 SAML 群组同步：

1. 请参阅 [JihuLab.com 群组的 SAML SSO](index.md)。
1. 确保您的 SAML 身份提供商发送名为 `Groups` 或 `groups` 的属性语句。

NOTE:
SAML 响应中的 `Groups` 或 `groups` 值可以是群组名称或 ID。
例如，Azure AD 发送 Azure 群组对象 ID 而不是名称。配置 [SAML 群组链接](#configure-saml-group-links) 时使用 ID 值。

```xml
<saml:AttributeStatement>
  <saml:Attribute Name="Groups">
    <saml:AttributeValue xsi:type="xs:string">Developers</saml:AttributeValue>
    <saml:AttributeValue xsi:type="xs:string">Product Managers</saml:AttributeValue>
  </saml:Attribute>
</saml:AttributeStatement>
```

其他属性名称（例如 `http://schemas.microsoft.com/ws/2008/06/identity/claims/groups`）不可作为群组源。

<!-- 有关在 SAML 身份提供商的设置中配置所需属性名称的更多信息，请参阅[示例群组 SAML 和 SCIM 配置](../../../user/group/saml_sso/example_saml_config.md)。-->

<a id="configure-saml-group-links"></a>

## 配置 SAML 群组链接

启用 SAML 后，具有维护者或所有者角色的用户会在群组的 **设置 > SAML 群组链接** 中看到一个新菜单项。您可以配置一个或多个 **SAML 群组链接** 以将 SAML 身份提供商群组名称映射到极狐GitLab 角色中。可以对顶级群组或任何子群组执行此操作。

要链接 SAML 群组：

1. 在 **SAML 群组名称** 中，输入相关 `saml:AttributeValue` 的值。此处输入的值必须与 SAML 响应中发送的值完全匹配。对于某些 IdP，这可能是群组 ID 或对象 ID (Azure AD)，而不是群组名称。
1. 在 **访问级别** 中选择角色。
1. 选择 **保存**。
1. 如果需要，请重复添加其他群组链接。

![SAML Group Links](img/saml_group_links_v13_9.png)

如果用户是映射到同一极狐GitLab 群组的多个 SAML 群组的成员，则该用户将获得群组中的最高角色。例如，如果在一个群组链接为访客角色，另一个群组链接为维护者角色，则同时属于这两个群组的用户将获得维护者角色。

授予角色的用户：

- 具有群组[直接成员资格](../../project/members/index.md#display-direct-members)的用户展示为群组同步的更高角色。
- 具有群组[继承成员资格](../../project/members/index.md#display-inherited-members)的用户展示为群组同步更低或平行的角色。

每次用户登录时都会评估 SAML 群组成员资格。

### 使用 API

> 引入于极狐GitLab 15.3。

您可以使用极狐GitLab API [列举、添加和删除](../../../api/groups.md#saml-group-links) SAML 群组链接。

<a id="microsoft-azure-active-directory-integration"></a>

## Microsoft Azure Active Directory 集成

> 引入于极狐GitLab 16.3。

NOTE:
Azure AD 在群组声明中发送最多 150 个群组。当用户是超过 150 个群组的成员时，Azure AD 会在 SAML 响应中发送群组超额声明属性。然后必须使用 Microsoft Graph API 获取群组成员身份。

要集成 Microsoft Azure AD，您需要：

- 配置 Azure AD 以使极狐GitLab 能够与 Microsoft Graph API 进行通信。
- 配置极狐GitLab。

### 极狐GitLab 设置为 Azure AD 字段


| 极狐GitLab 设置 | Azure 字段                              |
| ============== | ========================================== |
| 租户 ID      | Directory (tenant) ID                      |
| 客户端 ID      | Application (client) ID                    |
| 客户端 Secret  | Value (on **Certificates & secrets** page) |

### 配置 Azure AD

<!-- vale gitlab.SentenceSpacing = NO -->

1. 在 [Azure 门户](https://portal.azure.com) 中，转到 **Azure Active Directory > App registrations > All applications**，然后选择您的极狐GitLab SAML 应用程序。
1. 在 **Essentials** 下，显示 **Application (client) ID** 和 **Directory (tenant) ID** 值。复制这些值，因为您需要它们来进行极狐GitLab 配置。
1. 在左侧导航中，选择 **Certificates & secrets**。
1. 在 **Client secrets** 选项卡上，选择 **New client secret**。
    1. 在 **Description** 文本框中，添加描述。
    1. 在 **Expires** 下拉列表中，设置凭证的过期日期。如果 secret 过期，极狐GitLab 集成在凭证更新之前都不再起作用。
    1. 要生成凭据，请选择 ***Add**。
    1. 复制凭证的 **Value**。该值仅显示一次，您需要它来进行极狐GitLab 配置。
1. 在左侧导航中，选择 **API permissions**。
1. 选择 **Microsoft Graph > Application permissions**。
1. 选择复选框 **GroupMember.Read.All** 和 **User.Read.All**。
1. 选择 **Add permissions** 进行保存。
1. 选择 **Grant admin consent for `<application_name>`**，然后在确认对话框中选择 **Yes**。两个权限的 **Status** 列应更改为带有 **Granted for `<application_name>`** 的绿色复选标记。

<!-- vale gitlab.SentenceSpacing = YES -->

### 配置极狐GitLab

为 JihuLab.com 群组配置：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的顶级群组。
1. 选择 **设置 > SAML SSO**。
1. 配置[群组的SAML SSO](../../../user/group/saml_sso/index.md)。
1. 在 **Microsoft Azure 集成** 部分中，选中 **为此群组启用 Microsoft Azure 集成** 复选框。
   仅当为群组配置并启用 SAML SSO 时，此部分才可见。
1. 输入之前在 Azure 门户中配置 Azure Active Directory 时获取的 **租户 ID**、**客户端 ID** 和 **客户端 secret**。
1. 可选。如果使用 Azure AD China，请输入相应的 **登录 API 端点** 和 **Graph API 端点**。默认值适用于大多数组织。
1. 选择 **保存更改**。

为私有化部署配置：

1. 配置[实例的 SAML SSO](../../../integration/saml.md)。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 在 **Microsoft Azure 集成** 部分中，选中 **为此群组启用 Microsoft Azure 集成** 复选框。
1. 输入之前在 Azure 门户中配置 Azure Active Directory 时获取的 **租户 ID**、**客户端 ID** 和 **客户端 secret**。
1. 可选。如果使用 Azure AD China，请输入相应的 **登录 API 端点** 和 **Graph API 端点**。默认值适用于大多数组织。
1. 选择 **保存更改**。

使用此配置，如果用户使用 SAML 登录并且 Azure 在响应中发送群组超额声明，极狐GitLab 会启动群组同步作业来调用 Microsoft Graph API 并检索用户的群组成员身份。
然后，极狐GitLab 群组成员身份将根据 SAML 群组链接进行更新。

## 全局 SAML 群组成员身份锁定 **(PREMIUM SELF)**

> 引入于极狐GitLab 15.10。

极狐GitLab 管理员可以使用全局 SAML 群组成员资格锁定功能来防止群组成员邀请新成员加入其成员资格与 SAML 群组链接同步的子群组。

全局群组成员身份锁定仅适用于配置了 SAML 群组链接同步的顶级群组的子群组。任何用户都不能修改为 SAML 群组链接同步配置的顶级群组的成员身份。

启用全局群组成员身份锁定时：

- 只有管理员可以管理任何群组的成员身份，包括访问级别。
- 用户不能：
    - 与其他群组共享项目。
    - 邀请成员加入在群组中创建的项目。

要启用全局群组成员身份锁定：

1. 为您的私有化部署极狐GitLab 实例[配置 SAML](../../../integration/saml.md)。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 确保选中 **将成员身份锁定到 SAML 同步** 复选框。

## 自动成员移除

群组同步后，不是映射的 SAML 群组成员的用户将从该群组中移除。
在 JihuLab.com 上，顶级群组中的用户不会被移除，而是会被分配默认成员角色。

例如，在下图中：

- Alex Garcia 登录极狐GitLab 并从极狐GitLab 群组 C 中移除，因为他们不属于 SAML 群组 C。
- Sidney Jones 属于 SAML 群组 C，但未添加到极狐GitLab 群组 C 中，因为他们尚未登录。

```mermaid
graph TB
   subgraph SAML users
      SAMLUserA[Sidney Jones]
      SAMLUserB[Zhang Wei]
      SAMLUserC[Alex Garcia]
      SAMLUserD[Charlie Smith]
   end

   subgraph SAML groups
      SAMLGroupA["Group A"] --> SAMLGroupB["Group B"]
      SAMLGroupA --> SAMLGroupC["Group C"]
      SAMLGroupA --> SAMLGroupD["Group D"]
   end

   SAMLGroupB --> |Member|SAMLUserA
   SAMLGroupB --> |Member|SAMLUserB

   SAMLGroupC --> |Member|SAMLUserA
   SAMLGroupC --> |Member|SAMLUserB

   SAMLGroupD --> |Member|SAMLUserD
   SAMLGroupD --> |Member|SAMLUserC
```

```mermaid
graph TB
    subgraph GitLab users
      GitLabUserA[Sidney Jones]
      GitLabUserB[Zhang Wei]
      GitLabUserC[Alex Garcia]
      GitLabUserD[Charlie Smith]
    end

   subgraph GitLab groups
      GitLabGroupA["Group A (SAML configured)"] --> GitLabGroupB["Group B (SAML Group Link not configured)"]
      GitLabGroupA --> GitLabGroupC["Group C (SAML Group Link configured)"]
      GitLabGroupA --> GitLabGroupD["Group D (SAML Group Link configured)"]
   end

   GitLabGroupB --> |Member|GitLabUserA

   GitLabGroupC --> |Member|GitLabUserB
   GitLabGroupC --> |Member|GitLabUserC

   GitLabGroupD --> |Member|GitLabUserC
   GitLabGroupD --> |Member|GitLabUserD
```

```mermaid
graph TB
   subgraph GitLab users
      GitLabUserA[Sidney Jones]
      GitLabUserB[Zhang Wei]
      GitLabUserC[Alex Garcia]
      GitLabUserD[Charlie Smith]
   end

   subgraph GitLab groups after Alex Garcia signs in
      GitLabGroupA[Group A]
      GitLabGroupA["Group A (SAML configured)"] --> GitLabGroupB["Group B (SAML Group Link not configured)"]
      GitLabGroupA --> GitLabGroupC["Group C (SAML Group Link configured)"]
      GitLabGroupA --> GitLabGroupD["Group D (SAML Group Link configured)"]
   end

   GitLabGroupB --> |Member|GitLabUserA
   GitLabGroupC --> |Member|GitLabUserB
   GitLabGroupD --> |Member|GitLabUserC
   GitLabGroupD --> |Member|GitLabUserD
```

### 属于多个 SAML 群组的用户自动从极狐GitLab 群组中移除

将 Azure AD 与 SAML 结合使用时，如果组织中的用户是 150 多个群组的成员，并且您使用 SAML 群组同步，则该用户可能会失去其群组成员身份。
有关更多信息，请参阅 [Microsoft 群组超额](https://learn.microsoft.com/en-us/security/zero-trust/develop/configure-tokens-group-claims-app-roles#group-overages)的相关内容。

极狐GitLab 具有 [Microsoft Azure Active Directory 集成](#microsoft-azure-active-directory-integration) 功能，可为拥有 150 多个群组成员身份的用户的组织启用 SAML 群组同步。此集成使用 Microsoft Graph API 来获取所有用户成员资格，且不限于 150 个群组。

否则，您可以通过更改[群组声明](https://learn.microsoft.com/en-us/azure/active-directory/hybrid/connect/how-to-connect-fed-group-claims#configure-the-azure-ad-application-registration-for-group-attributes)以使用 `Groups assigned to the application` 选项来解决这个问题。

![Manage Group Claims](img/Azure-manage-group-claims.png).
