---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 对 Terraform 与极狐GitLab 的集成进行故障排除

当您使用与 Terraform 和极狐GitLab 的集成时，您可能会遇到需要解决的问题。

## 刷新子组状态时未检测到 `gitlab_group_share_group` 资源

极狐GitLab Terraform 提供程序可能无法检测到现有的 `gitlab_group_share_group` 资源。这会在运行 `terraform apply` 时导致错误，因为 Terraform 会尝试重新创建现有资源。

例如，考虑以下群组/子组配置：

```plaintext
parent-group
├── subgroup-A
└── subgroup-B
```

其中：

- 用户 `user-1` 创建 `parent-group`、`subgroup-A` 和 `subgroup-B`。
- `subgroup-A` 与 `subgroup-B` 共享。
- 用户 `terraform-user` 是 `parent-group` 的成员，继承了对两个子组的 `owner` 访问权限。

刷新 Terraform 状态时，提供者发出的 API 查询 `GET /groups/:subgroup-A_id` 不会返回 `shared_with_groups` 数组中的 `subgroup-B` 的详细信息。这会导致错误。

要解决此问题，请确保应用以下条件之一：

1. `terraform-user` 创建所有子组资源。
1. 将维护者或所有者角色授予 `subgroup-B` 上的 `terraform-user` 用户。
1. `terraform-user` 继承的对 `subgroup-B` 和 `subgroup-B` 的访问权限包含至少一个项目。

### 使用基本模板时出现无效的 CI/CD 语法错误

使用 Terraform 模板时，您可能会遇到 CI/CD 语法错误：

- 在 14.2 及更高版本上，使用 `latest` 模板。
- 在 15.0 及更高版本上，使用任何版本的模板。

例如：

```yaml
include:
  # On 14.2 and later, when using either of the following:
  - template: Terraform/Base.latest.gitlab-ci.yml
  - template: Terraform.latest.gitlab-ci.yml
  # On 15.0 and later, the following templates have also been updated:
  - template: Terraform/Base.gitlab-ci.yml
  - template: Terraform.gitlab-ci.yml

my-terraform-job:
  extends: .apply
```

错误有三种不同的原因：

- 在 `.init` 的情况下，发生错误是因为初始化阶段和作业已从模板中删除，因为它们不再需要。要解决语法错误，您可以安全地删除任何扩展 `.init` 的作业。
- 对于所有其他作业，失败的原因是基础作业已重命名：每个作业名称都添加了 `.terraform:` 前缀。例如，`.apply` 变成了 `.terraform:apply`。要修复此错误，您可以更新基本作业名称。例如：

  ```diff
    my-terraform-job:
  -   extends: .apply
  +   extends: .terraform:apply
  ```

- 在 15.0 版本中，模板使用 [`rules`](../../../ci/yaml/index.md#rules) 语法而不是 [`only/except`](../../../ci/yaml/index.md#only--except)。确保 `.gitlab-ci.yml` 文件中的语法不包含两者。

<a id="use-an-older-version-of-the-template"></a>

#### 使用旧版本的模板

在主要版本期间可能会发生重大更改。如果您遇到重大更改或想要使用旧版本的模板，您可以更新 `.gitlab-ci.yml` 以引用旧版本。例如：

```yaml
include:
  remote: https://gitlab.com/gitlab-org/configure/template-archive/-/raw/main/14-10/Terraform.gitlab-ci.yml
```

<!--
View the [template-archive](https://gitlab.com/gitlab-org/configure/template-archive) to see which templates are available.
-->

## 故障排除

<a id="unable-to-lock-terraform-state-files-in-ci-jobs-for-terraform-apply-using-a-plan-created-in-a-previous-job"></a>

### 无法使用在先前作业中创建的计划为 `terraform apply` 锁定 CI 作业中的 Terraform 状态文件

当将 `-backend-config=` 传递给 `terraform init` 时，Terraform 会将这些值保存在计划缓存文件中，包括 `password` 值。

因此，要创建一个计划并稍后在另一个 CI 作业中使用相同的计划，您可能会在使用 `-backend-config=password=$CI_JOB_TOKEN` 时收到错误 `Error: Error acquire the state lock` 错误。
发生这种情况是因为 `$CI_JOB_TOKEN` 的值仅在当前作业期间有效。

作为一种解决方法，在您的 CI 作业中使用 [http 后端配置变量](https://www.terraform.io/language/settings/backends/http.html#configuration-variables)，这是遵循[开始使用极狐GitLab CI](terraform_state.md#initialize-a-terraform-state-as-a-backend-by-using-gitlab-cicd) 说明时，后台发生的事情。

### Error: "address": required field is not set

默认情况下，我们将 `TF_ADDRESS` 设置为 `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}`。
如果您未在作业中设置 `TF_STATE_NAME` 或 `TF_ADDRESS`，则作业将失败并显示错误消息 `Error: "address": required field is not set`。

要解决此问题，请确保在返回错误的作业中可以访问 `TF_ADDRESS` 或 `TF_STATE_NAME`：

1. 为作业配置 [CI/CD 环境范围](../../../ci/variables/index.md#add-a-cicd-variable-to-a-project)。
1. 设置作业的[环境](../../../ci/yaml/index.md#environment)，与上一步中的环境范围相匹配。

### Error refreshing state: HTTP remote state endpoint requires auth

要解决此问题，请确保：

- 您使用的访问令牌具有 `api` 范围。
- 如果您设置了 `TF_HTTP_PASSWORD` CI/CD 变量，请确保您：
  - 设置与 `TF_PASSWORD` 相同的值。
  - 如果您的 CI/CD 作业没有明确使用它，请删除 `TF_HTTP_PASSWORD` 变量。
