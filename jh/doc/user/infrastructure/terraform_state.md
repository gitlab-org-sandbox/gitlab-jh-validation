---
redirect_to: 'iac/terraform_state.md'
remove_date: '2022-01-26'
---

此文档已移动到[其它位置](iac/terraform_state.md)。

<!-- This redirect file can be deleted after <2022-01-26>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->