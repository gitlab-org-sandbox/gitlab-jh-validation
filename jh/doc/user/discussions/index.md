---
stage: Create
group: Code Review
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 评论和主题 **(BASIC ALL)**

> - 分页合并请求讨论引入于极狐GitLab 15.1，[功能标志](../../administration/feature_flags.md)为 `paginated_mr_discussions`。默认禁用。
> - 分页合并请求讨论在 JihuLab.com 上启用于极狐GitLab 15.2。
> - 分页合并请求讨论在私有化部署版本中启用于极狐GitLab 15.3。
> - 分页合并请求讨论普遍可用于极狐GitLab 15.8。移除功能标志 `paginated_mr_discussions`。

极狐GitLab 鼓励通过评论、主题和[代码建议](../project/merge_requests/reviews/suggestions.md)进行交流。

有两种类型的评论：

- 标准评论。
- 用户可以解决的主题中的评论。

在评论中，您可以输入 Markdown <!--[Markdown](../markdown.md)-->并使用[快速操作](../project/quick_actions.md)。

您可以在提交差异评论中[建议代码更改](../project/merge_requests/reviews/suggestions.md)，用户可以通过用户界面接受。

## 可以添加评论的地方

您可以在以下位置创建评论：

- 提交差异
- 提交
- 设计
- 史诗
- 议题
- 合并请求
- 代码片段
- 任务
- OKR

每个对象最多可以有 5,000 条评论。

## 提及

您可以在实例中使用 `@username` 或 `@groupname` 提及用户或群组（包括子组）。所有提到的用户都会收到待办事项和电子邮件的通知。
用户可以在[通知设置](../profile/notifications.md)中自行更改此设置。

您可以快速查看哪些评论涉及您，因为您自己（当前登录的用户）的提及以不同颜色突出显示。

### 提及所有成员

> 功能标志 `disable_all_mention` 引入于极狐GitLab 16.1。默认禁用。在 JihuLab.com 上启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下未启用此标志。要使其可用，管理员可以启用名为 `disable_all_mention` 的[功能标志](../../administration/feature_flags.md)。
在 JihuLab.com 上，此标志启用。

启用此功能标志后，在评论和描述中键入 `@all` 会生成纯文本而不会提及所有人。
当您禁用此功能时，Markdown 文本中现有的 `@all` 不会受到影响并会保留为链接。只有启用功能标志之后的 `@all` 提及才会显示为纯文本。

避免在评论和描述中提及 `@all`。
当您这样做时，您不仅会提及项目、议题或合并请求的参与者，还会提及该项目父组的所有成员。
所有这些用户都会收到电子邮件通知和待办事项。这些通知也可能会被当作垃圾邮件。

可以在[群组设置](../group/manage.md#disable-email-notifications)中禁用通知和提及。

可以在[群组设置](../group/manage.md#disable-email-notifications)中禁用通知和提及。

### 在议题或合并请求中提及群组

当您在评论中提及某个群组时，该群组的每个成员都会将一个待办事项添加到他们的待办事项列表中。

1. 打开 MR 或议题。
1. 在评论中，键入 `@`，后跟用户、群组或子组命名空间。例如，`@alex`、`@alex-team` 或 `@alex-team/marketing`。
1. 选择 **评论**。

为所有群组和子组成员创建一个待办事项。

## 向合并请求差异添加评论

您可以向合并请求差异添加评论。即使您进行以下操作，这些评论仍然会存在：

- 变基后强制推送。
- 修改提交。

添加提交差异评论：

1. 要选择特定提交，在合并请求上，选择 **提交** 选项卡，选择提交消息。要查看最新提交，请选择 **变更** 选项卡。
1. 通过您要评论的行，将鼠标悬停在行号上并选择 **评论** (**{comment}**)。您可以通过拖动 **评论** (**{comment}**) 图标来选择多行。
1. 输入您的评论并选择 **启动评审** 或 **立即添加评论**。

评论显示在合并请求的 **讨论** 选项卡上。

该评论不会显示在您项目的 **代码 > 提交** 页面上。

NOTE:
当您的评论包含对合并请求中包含的提交的引用时，它会自动转换为当前合并请求上下文中的链接。例如，`28719b171a056960dfdc0012b625d0b47b123196` 变成 `https://gitlab.example.com/example-group/example-project/-/merge_requests/12345/diffs?commit_id=28719b171a056960dfdc0012b625d0b47b123196`。

<a id="reply-to-a-comment-by-sending-email"></a>

## 通过发送邮件回复评论

如果您配置了"通过电子邮件回复"<!--["reply by email"](../../administration/reply_by_email.md)-->，您可以通过发送电子邮件来回复评论。

- 当您回复标准评论时，会创建另一个标准评论。
- 当您回复主题评论时，会在主题中创建回复。
- 当您向议题电子邮件地址发送电子邮件<!--[向议题电子邮件地址发送电子邮件](../project/issues/managing_issues.md#copy-issue-email-address) -->时，则会创建标准评论。

您可以在电子邮件回复中使用 Markdown<!--[Markdown](../markdown.md)--> 和 [快速操作](../project/quick_actions.md)。

## 编辑评论

您可以随时编辑自己的评论。
具有维护者角色或更高级别的任何人也可以编辑其他人发表的评论。

编辑评论：

1. 在评论上，选择 **编辑评论** (**{pencil}**)。
1. 进行编辑。
1. 选择 **保存修改**。

<a id="editing-a-comment-to-add-a-mention"></a>

### 编辑评论以添加提及

默认情况下，当您提及用户时，系统会为他们[创建一个待办事项](../todos.md#actions-that-create-to-do-items)，并向他们发送一封[通知电子邮件](../profile/notifications.md)。

如果您编辑现有评论，添加以前不存在的用户提及，极狐GitLab：

- 为提到的用户创建一个待办事项。
- 不发送通知电子邮件。

<a id="prevent-comments-by-locking-the-discussion"></a>

## 通过锁定讨论来阻止评论

您可以在议题或合并请求中阻止公开评论。当您这样做时，只有项目成员可以添加和编辑评论。

先决条件：

- 在合并请求中，您必须至少具有开发者角色。
- 在议题中，您必须至少具有报告者角色。

1. 在右侧边栏的 **锁定议题** 或 **锁定合并请求** 旁边，选择 **编辑**。
1. 在确认对话框中，选择 **锁定**。

评论被添加到页面详细信息中。

如果议题或合并请求被锁定并关闭，您将无法重新打开它。

## 添加内部备注

> - 引入于 13.9 版本，在名为 `confidential_notes` 的功能标志后默认禁用。
> - 变更于 14.10 版本：您只能将议题和史诗中的评论标记为机密。以前，也可以为在合并请求和片段中的评论这么做。
> - 于 15.0 版本，从“私密评论”重命名为“内部备注”。
> - 于 15.0 版本，启用于 SaaS 版和私有化部署版。
> - 功能标志 `confidential_notes` 删除于 15.2 版本。
> - 将权限更改为至少具有报告者角色于极狐GitLab 15.6。在 15.5 及更早版本中，议题和史诗作者及指派人也可以读取并创建内部备注。

您可以将内部备注**添加到议题或史诗**。它只对至少具有报告者角色的项目成员可见。

记住：

- 对内部备注的回复也是内部的。
- 您不能将内部备注变成普通备注。

先决条件：

- 您必须至少具有项目的报告者角色。

要添加内部备注：

1. 开始添加新评论。
1. 在评论下方，选中 **将此作为内部备注** 复选框。
1. 选择 **添加内部备注**。

![Internal notes](img/add_internal_note_v15_0.png)

您还可以将[议题标记为私密](../project/issues/confidential_issues.md)。

## 只显示评论

在包含许多评论的讨论中，过滤讨论以仅显示更改[系统注释](../project/system_notes.md)的评论或历史记录。系统备注包括对描述的更改、其他极狐GitLab 对象中的提及或对标签、指派人和里程碑的更改。
极狐GitLab 会保存您的个人设置，并将其应用于您查看的每个议题、合并请求或史诗。

1. 打开合并请求、史诗或议题的 **概览** 选项卡。
1. 在页面右侧，从过滤器中选择：
    - **显示所有活动**：显示所有用户评论和系统注释（议题更新、来自其他议题的提及、对描述的更改等）。
    - **仅显示评论**：仅显示用户评论。
    - **仅显示历史记录**：仅显示活动记录。

## 更改活动排序顺序

您可以颠倒默认顺序，并与按最近项目排序的活动提要进行交互。您的偏好设置保存在本地存储中，并自动应用于您查看的每个议题、合并请求或史诗。

要更改活动排序顺序：

1. 打开合并请求、议题或史诗中的 **概览** 选项卡。
1. 在页面右侧的 **排序或过滤** 下拉列表中，选择 **最新在前** 或 **最早在前**（默认）。

<a id="view-description-change-history"></a>

## 查看描述更改历史记录 **(PREMIUM ALL)**

您可以看到对历史记录中列出的描述所做的更改。

要比较更改，请选择**与以前的版本比较**。

## 将议题指派给评论用户

您可以将议题指派给发表评论的用户。

1. 在评论中，选择 **更多操作** (**{ellipsis_v}**) 菜单。
1. 选择 **分配给评论用户**：
   ![Assign to commenting user](img/quickly_assign_commenter_v13_1.png)
1. 要取消分配评论者，请再次选择该按钮。

<a id="create-a-thread-by-replying-to-a-standard-comment"></a>

## 通过回复标准评论创建主题

当您回复标准评论时，您就创建了一个主题。

先决条件：

- 您必须至少具有访客角色。
- 您必须处于议题、合并请求或史诗中。不支持提交和代码片段中的主题。

要通过回复评论来创建主题：

1. 在评论的右上角，选择 **回复评论** (**{comment}**)。

   ![Reply to comment button](img/reply_to_comment_button.png)

   显示回复区域。

1. 输入您的回复。
1. 选择 **回复** 或 **立即添加评论**（取决于您在 UI 中回复的位置）。

置顶评论将转换为主题。

## 创建主题而不回复评论

您可以在不回复标准评论的情况下创建主题。

先决条件：

- 您必须至少具有访客角色。
- 您必须处于议题、合并请求、提交或代码片段中。

创建主题：

1. 输入评论。
1. 在评论下方的 **评论** 按钮右侧，选择向下箭头 (**{chevron-down}**)。
1. 从列表中选择 **开启主题**。
1. 再次选择 **开启主题**。

这样就创建了主题评论。

![Thread comment](img/discussion_comment.png)

## 解决主题

> - 解决议题主题引入于极狐GitLab 16.3，[功能标志](../../administration/feature_flags.md)为 `resolvable_issue_threads`。默认禁用。
> - 可解决的议题主题在 JihuLab.com 和私有化部署版本上启用于极狐GitLab 16.4。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 *可解决的议题主题* 可用。
要隐藏该功能，管理员可以禁用名为 `resolvable_issue_threads` 的[功能标志](../../administration/feature_flags.md)。
在 JihuLab.com 上，可以使用此功能。

您可以在想要结束对话时解决主题。

先决条件：

- 您必须处于议题或合并请求中。
- 您必须在议题或合并请求中至少具有开发者角色或是作者。

要解决主题：

1. 转到主题。
1. 执行以下操作之一：
    - 在原评论的右上角，选择 **解决主题** (**{check-circle}**) 图标。
    - 在最后一条回复下方的 **回复** 字段中，选择 **解决主题**。
    - 在最后一条回复下方的 **回复** 字段中，输入文本，选中 **解决主题** 复选框，然后选择 **立即添加评论**。
