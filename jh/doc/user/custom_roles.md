---
stage: Govern
group: Authentication
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 自定义角色 **(ULTIMATE ALL)**

> - 引入于极狐GitLab 15.7，[功能标志](../administration/feature_flags.md)为 `customizable_roles`。
> - 默认启用于极狐GitLab 15.9。
> - 功能标志移除于极狐GitLab 15.10。
> - 自定义角色查看漏洞报告的功能引入于极狐GitLab 16.1，[功能标志](../administration/feature_flags.md)为 `custom_roles_vulnerability`。
> - 查看漏洞报告的功能默认引入于极狐GitLab 16.1。
> - 功能标志 `custom_roles_vulnerability` 移除于极狐GitLab 16.2。
> - 使用 UI 创建和移除自定义角色的功能引入于极狐GitLab 16.4。
> - 管理群组成员的功能引入于极狐GitLab 16.5，[功能标志](../administration/feature_flags.md)为 `admin_group_member`。
> - 管理项目访问令牌的功能引入于极狐GitLab 16.5，[功能标志](../administration/feature_flags.md)为 `manage_project_access_tokens`。

自定义角色允许拥有所有者角色的群组成员创建特定于其组织需求的角色。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo of the custom roles feature, see [[Demo] Ultimate Guest can view code on private repositories via custom role](https://www.youtube.com/watch?v=46cp_-Rtxps).
-->

可以使用以下细粒度权限。您可以将这些权限添加到任何基本角色中，并将它们相互组合添加以创建自定义角色：

- Guest+1 角色，允许具有访客角色的用户查看代码。
- 在 16.1 及更高版本中，您可以创建能够查看漏洞报告并更改漏洞状态的自定义角色。
- 在 16.3 及更高版本中，您可以创建能够查看依赖项列表的自定义角色。
- 在 16.4 及更高版本中，您可以创建能够批准合并请求的自定义角色。
- 在 16.5 及更高版本中，您可以创建能够管理群组成员的自定义角色。

当您为具有访客角色的用户启用自定义角色时，该用户可以拥有更高的权限，因此：

- 在私有化部署的极狐GitLab 上被视为[计费用户](../subscriptions/self_management/index.md#billable-users)。
- 在 JihuLab.com 上[占用席位](../subscriptions/jihulab_com/index.md#how-seat-usage-is-determined)。

这种情况不适用于 Guest+1，因为 Guest+1 是一个仅启用 `read_code` 权限的访客自定义角色。具有该特定自定义角色的用户不被视为计费用户，并且不占用席位。

## 创建自定义角色

先决条件：

- 您必须是私有化部署实例的管理员，或者在您要在其中创建自定义角色的群组中具有所有者角色。
- 该团队的订阅级别必须是旗舰版。
- 您必须拥有：
    - 至少一个私有项目，以便您可以看到为具有访客角色的用户提供自定义角色的效果。该项目可以在该群组或该群组的子组中。
    - [具有 API 范围的个人访问令牌](profile/personal_access_tokens.md#create-a-personal-access-token)。

### 极狐GitLab SaaS

先决条件：

- 您必须在要在其中创建自定义角色的群组中具有所有者角色。

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的群组。
1. 选择 **设置 > 角色和权限**。
1. 选择 **添加新角色**。
1. 在 **用作模板的基本角色** 中，选择 **访客**。
1. 在 **角色名称** 中，输入自定义角色的标题。
1. 选择新自定义角色的 **权限**。
1. 选择 **创建新角色**。

### 私有化部署的极狐GitLab 实例

先决条件：

- 您必须是要在其中创建自定义角色的私有化部署实例的管理员。

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 角色和权限**。
1. 从顶部下拉列表中，选择您要在其中创建自定义角色的群组。
1. 选择 **添加新角色**。
1. 在 **用作模板的基本角色** 中，选择 **访客**。
1. 在 **角色名称** 中，输入自定义角色的标题。
1. 选择新自定义角色的 **权限**。
1. 选择 **创建新角色**。

您也可以[使用 API](../api/member_roles.md#add-a-member-role-to-a-group) 创建自定义角色。

### 自定义角色要求

系统为每种功能都定义了最低访问级别。为了能够创建启用某种功能的自定义角色，`member_roles` 表中必须记录相关的最低访问级别。对于所有功能来说，最低访问级别是访客。只有至少具有访客角色的用户才能被分配自定义角色。

某些角色和能力需要启用其他功能。例如，如果已经启用了读取漏洞 (`read_vulnerability`)功能，则自定义角色只能启用漏洞管理 (`admin_vulnerability`)功能。

您可以在下表中查看功能要求。

| 功能  | 必须功能 |
| -- | -- |
| `read_code` | - |
| `read_dependency` | - |
| `read_vulnerability` | - |
| `admin_merge_request` | - |
| `admin_vulnerability` | `read_vulnerability` |
| `admin_group_member` | - |
| `manage_project_access_tokens` | - |

## 关联自定义角色和现有群组成员

要将自定义角色与现有群组成员（具有所有者角色的群组成员）关联：

1. 邀请用户作为直接成员加入根群组或根群组层次结构中的任何子组或项目（以访客身份）。此时，该访客用户无法看到群组或子组中项目的任何代码。
1. 可选。如果所有者不知道接收自定义角色的访客用户的 `id`，请通过 [API 请求](../api/member_roles.md#list-all-member-roles-of-a-group)查看 `id`。

1. 使用[群组和项目成员 API 端点](../api/members.md#edit-a-member-of-a-group-or-project)将成员与 Guest+1 角色相关联。

   ```shell
   # to update a project membership
   curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" --data '{"member_role_id": '<member_role_id>', "access_level": 10}' "https://gitlab.example.com/api/v4/projects/<project_id>/members/<user_id>"

   # to update a group membership
   curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" --data '{"member_role_id": '<member_role_id>', "access_level": 10}' "https://gitlab.example.com/api/v4/groups/<group_id>/members/<user_id>"
   ```

   其中：

    - `<project_id` 和 `<group_id>`：与接收自定义角色的成员资格相关联的 `id` 或 [URL 编码的项目或群组路径](../api/rest/index.md#namespaced-path-encoding)。
    - `<member_role_id>`：上一节中创建的成员角色的 `id`。
    - `<user_id>`：接收自定义角色的用户的 `id`。

   现在，Guest+1 用户可以查看与此成员资格相关联的所有项目的代码。

## 移除自定义角色

先决条件：

- 您必须是管理员或在要从中移除自定义角色的群组中具有所有者角色。

仅当没有群组成员拥有该角色时，您才可以从群组中移除该角色。

您可以从具有该自定义角色的所有群组成员中移除该自定义角色，或者从群组中移除拥有该自定义角色的所有成员。

### 从群组成员中移除自定义角色

要从群组成员中移除自定义角色，请使用[群组和项目成员 API 端点](../api/members.md#edit-a-member-of-a-group-or-project)并传递一个空的 `member_role_id` 值。

```shell
# to update a project membership
curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" --data '{"member_role_id": "", "access_level": 10}' "https://gitlab.example.com/api/v4/projects/<project_id>/members/<user_id>"

# to update a group membership
curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" --data '{"member_role_id": "", "access_level": 10}' "https://gitlab.example.com/api/v4/groups/<group_id>/members/<user_id>"
```

### 从群组中移除具有自定义角色的群组成员

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的群组。
1. 选择 **管理 > 成员**。
1. 在要移除的成员行上，选择垂直省略号(**{ellipsis_v}**) 并选择 **删除成员**。
1. 在 **删除成员** 确认对话框中，不要选中任何复选框。
1. 选择 **删除成员**。

### 删除自定义角色

在确保没有群组成员拥有该自定义角色后，您可以删除该自定义角色。

1. 在左侧边栏中，选择 **搜索或转到**。
1. 仅限 JihuLab.com。选择 **管理中心**。
1. 选择 **设置 > 角色和权限**。
1. 选择 **自定义角色**。
1. 在 **操作** 列中，选择 **删除角色** (**{remove}**) 并确认。

您还可以使用 API 删除自定义角色。
要使用 API，您必须知道自定义角色的 `id`。如果您不知道 `id`，您可以通过 [API 请求](../api/member_roles.md#list-all-member-roles-of-a-group)进行查找。

## 已知问题

- 如果具有自定义角色的用户与群组或项目共享，则他们的自定义角色不会随他们一起转移。用户在新的群组或项目中拥有常规的访客角色。
- 您不能将[审计员用户](../administration/auditor_users.md)用作自定义角色的模板。
