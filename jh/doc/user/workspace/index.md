---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 工作区 (Beta) **(PREMIUM ALL)**

> - 引入于 15.11 版本，[功能标志](../../administration/feature_flags.md)为 `remote_development_feature_flag`。默认禁用。
> - 在 SaaS 和私有化部署版上启用于 16.0 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `remote_development_feature_flag`。在 SaaS 版上，此功能可用。此功能尚未准备好用于生产。

WARNING:
此功能处于 [Beta](../../policy/alpha-beta-support.md#beta) 状态，如有更改，恕不另行通知。

工作区是极狐GitLab 中代码的虚拟沙箱环境。您可以使用工作区为您的极狐GitLab 项目创建和管理隔离的开发环境。这些环境确保不同的项目不会相互干扰。

每个工作区都包含自己的一组依赖项、库和工具，您可以对其进行自定义以满足每个项目的特定需求。工作区使用 AMD64 架构。

## 工作区和项目

工作区的范围仅限于项目。
当您[创建工作区](configuration.md#set-up-a-workspace)时，您必须：

- 将工作区分配给特定项目。
- 选择带有 [`.devfile.yaml`](#devfile) 文件的项目。

工作区可以与极狐GitLab API 交互，访问级别由当前用户权限定义。
即使用户权限稍后被撤销，正在运行的工作区仍然可以访问。

### 从项目中打开和管理工作区

> 引入于极狐GitLab 16.2。

要从文件或仓库文件列表打开工作区：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在右上角，选择 **编辑**。
1. 从下拉列表中的 **您的工作区** 下，选择工作区。

从下拉列表中，您还可以：

- 重新启动、暂停或终止现有工作区。
- 创建一个新的工作区。

### 删除与工作区关联的数据

当您删除与工作区关联的项目、代理、用户或令牌时：

- 工作区已从用户界面中删除。
- 在 Kubernetes 集群中，正在运行的工作区资源会成为孤立资源，不会自动删除。

要清理孤立资源，管理员必须手动删除 Kubernetes 中的工作区。

<!--[Issue 414384](https://gitlab.com/gitlab-org/gitlab/-/issues/414384) proposes to change this behavior.-->

<a id="devfile"></a>

## Devfile

devfile 是一个文件，它通过为极狐GitLab 项目指定必要的工具、语言、运行时和其他组件来定义开发环境。

工作区内置了对开发文件的支持。您可以在极狐GitLab 配置文件中为您的项目指定一个 devfile。devfile 用于根据定义的规范自动配置开发环境。

这样，无论您使用何种机器或平台，您都可以创建一致且可重现的开发环境。

### 相关 schema 属性

极狐GitLab 仅支持 [devfile 2.2.0](https://devfile.io/docs/2.2.0/devfile-schema) 中的 `container` 和 `volume` 组件。
使用 `container` 组件将容器镜像定义为 devfile 工作区的执行环境。
您可以指定基础镜像、依赖项和其他设置。

只有以下属性与 `container` 组件的极狐GitLab 实现相关：

| 属性     | 定义                                                                        |
|----------------| --------------------------------------------------- |
| `image`        | 用于工作区的容器镜像的名称。                             |
| `memoryRequest`| 容器可以使用的最小内存量。                                  |
| `memoryLimit`  | 容器可以使用的最大内存量。                                   |
| `cpuRequest`   | 容器可以使用的最小 CPU 数量。                                      |
| `cpuLimit`     | 容器可以使用的最大 CPU 数量。                                      |
| `env`          | 在容器中使用的环境变量。                                    |
| `endpoints`    | 容器暴露的端口映射。                                       |
| `volumeMounts` | 要挂载在容器中的存储卷。                                         |

<a id="example-configurations"></a>

### 配置示例

以下是 devfile 配置示例：

```yaml
schemaVersion: 2.2.0
components:
  - name: tooling-container
    attributes:
      gl/inject-editor: true
    container:
      image: registry.gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/debian-bullseye-ruby-3.2-node-18.12:rubygems-3.4-git-2.33-lfs-2.9-yarn-1.22-graphicsmagick-1.3.36-gitlab-workspaces
      env:
        - name: KEY
          value: VALUE
      endpoints:
        - name: http-3000
          targetPort: 3000
```

有关更多信息，请参阅 [devfile 文档](https://devfile.io/docs/2.2.0/devfile-schema)。 
<!--有关其他示例，请参阅 [`examples` 项目](https://gitlab.com/gitlab-org/remote-development/examples)。-->

该容器镜像仅用于演示目的。
要使用您自己的容器镜像，请参阅[任意用户 ID](#arbitrary-user-ids)。

## Web IDE

默认情况下，工作区与 Web IDE 捆绑在一起。Web IDE 是唯一可用于工作区的代码编辑器。

Web IDE 由 [VS Code 派生项目](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork)提供支持。有关详细信息，请参阅 [Web IDE](../project/web_ide/index.md)。

## 个人访问令牌

> 引入于极狐GitLab 16.4。

当您[创建工作区](configuration.md#set-up-a-workspace)时，您将获得具有 `write_repository` 权限的个人访问令牌。
该令牌用于在启动工作区时初始克隆项目。

您在工作区中执行的任何 Git 操作都使用此令牌进行身份验证和授权。
当您终止工作区时，令牌将被撤销。

## 集群中的 Pod 交互

工作区在 Kubernetes 集群中作为 Pod 运行。极狐GitLab 不对 Pod 相互交互的方式施加任何限制。

由于此要求，您可能希望将此功能与集群中的其他容器隔离开来。

## 网络访问和工作区授权

限制对 Kubernetes 控制平面的网络访问是客户端的责任，因为极狐GitLab 无法控制 API。

只有工作区创建者可以访问工作区和该工作区中公开的任何端点。工作区创建者仅在使用 OAuth 进行用户身份验证后才有权访问工作区。

## 计算资源和卷存储

当您停止工作区时，该工作区的计算资源将缩减为零。但是，为工作区配置的卷仍然存在。

要删除配置的卷，您必须终止工作区。

<a id="arbitrary-user-ids"></a>

## 任意用户 ID

您可以提供自己的容器镜像，它可以作为任何 Linux 用户 ID 运行。

极狐GitLab 无法预测容器镜像的 Linux 用户 ID。极狐GitLab 使用 Linux 根群组 ID 权限来创建、更新或删除容器中的文件。Kubernetes 集群使用的容器运行时必须确保所有容器的默认 Linux 群组 ID 为 `0`。

如果您的容器镜像不支持任意用户 ID，则无法在工作区中创建、更新或删除文件。<!--To create a container image that supports arbitrary user IDs,
see [Create a custom workspace image that supports arbitrary user IDs](../workspace/create_image.md).-->

详情请参见 [OpenShift 文档](https://docs.openshift.com/container-platform/4.12/openshift_images/create-images.html#use-uid_create-images)。

<!--
## 相关主题

- [极狐GitLab 工作区 Demo](https://go.gitlab.com/qtu66q)
-->
