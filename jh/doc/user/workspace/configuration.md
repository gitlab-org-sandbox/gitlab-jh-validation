---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 工作区配置 (Beta) **(PREMIUM ALL)**

> - 引入于极狐GitLab 15.11，[功能标志](../../administration/feature_flags.md)为 `remote_development_feature_flag`。默认禁用。
> - [在 JihuLab.com 和私有化部署上启用](https://gitlab.com/gitlab-org/gitlab/-/issues/391543)于极狐GitLab 16.0。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。
要隐藏该功能，管理员可以禁用名为 `remote_development_feature_flag` 的[功能标志](../../administration/feature_flags.md)。
在 JihuLab.com 上，可以使用此功能。
该功能尚未准备好用于生产使用。

WARNING:
此功能处于 [Beta](../../policy/experiment-beta-support.md#beta) 阶段。如有变更，恕不另行通知。
<!--To leave feedback, see the [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/410031).-->

您可以使用[工作区](index.md)为您的极狐GitLab 项目创建和管理隔离的开发环境。
每个工作区都包含自己的一组依赖项、库和工具，您可以对其进行自定义以满足每个项目的特定需求。

<a id="set-up-a-workspace"></a>

## 设置工作区

> 对私有项目的支持引入于极狐GitLab 16.4。

### 先决条件

- 设置适用于 Kubernetes 的极狐GitLab 代理支持的 Kubernetes 集群。
  请参阅[支持的 Kubernetes 版本](../clusters/agent/index.md#supported-kubernetes-versions-for-gitlab-features)。
- 确保启用 Kubernetes 集群的弹性伸缩。
- 在 Kubernetes 集群中，验证是否定义了[默认存储类](https://kubernetes.io/docs/concepts/storage/storage-classes/)，以便可以为每个工作区动态配置卷。
- 在 Kubernetes 集群中，安装您选择的 Ingress 控制器（例如 `ingress-nginx`），并使该控制器可以通过域访问。例如，将 `*.workspaces.example.dev` 和 `workspaces.example.dev` 指向 Ingress 控制器公开的负载均衡器。
- 在 Kubernetes 集群中，[安装 `gitlab-workspaces-proxy`](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy#installation-instructions)。
- 在 Kubernetes 集群中，[安装 Kubernetes 的极狐GitLab 代理](../clusters/agent/install/index.md)。
- 使用此代码片段为极狐GitLab 代理配置远程开发设置，并根据需要更新 `dns_zone`：

  ```yaml
  remote_development:
    enabled: true
    dns_zone: "workspaces.example.dev"
  ```

您可以使用项目根群组下定义的任何代理，前提是为该代理正确配置了远程开发。
- 您必须至少在根群组中具有开发人员角色。
- 在您想要使用此功能的每个公共项目中，创建一个 [devfile](index.md#devfile)：
    1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
    1. 在项目的根目录中，创建一个名为 `.devfile.yaml` 的文件。
       您可以使用[示例配置](index.md#example-configurations)之一。
- 确保 devfile 中使用的容器镜像支持[任意用户 ID](index.md#arbitrary-user-ids)。

### 创建工作区

创建工作区：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **您的工作**。
1. 选择 **工作区**。
1. 选择 **新建工作区**。
1. 从 **选择项目** 下拉列表中，[选择带有 `.devfile.yaml` 文件的项目](#preventions)。您只能为公共项目创建工作区。
1. 从 **选择集群代理** 下拉列表中，选择项目所属群组拥有的集群代理。
1. 在 **自动终止前的时间** 中，输入工作区自动终止之前的小时数。
   此超时是一种安全措施，可防止工作区消耗过多资源或无限期运行。
1. 选择 **创建工作区**。

工作区可能需要几分钟才能启动。
要打开工作区，请在 **预览** 下选择工作区。
您还可以访问终端并安装必要的依赖项。

## 使用 SSH 连接到工作区

> 引入于极狐GitLab 16.3。

先决条件：

- 必须为工作区启用 SSH。
- 您必须有一个指向 [`gitlab-workspaces-proxy`](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy) 的 TCP 负载均衡器。

要使用 SSH 客户端连接到工作区：

1. 运行命令：

   ```shell
   ssh <workspace_name>@<ssh_proxy>
   ```

1. 对于密码，请输入至少具有 `read_api` 范围的个人访问令牌。

当您通过 TCP 负载均衡器连接到 `gitlab-workspaces-proxy` 时，`gitlab-workspaces-proxy` 会检查用户名（工作区名称）并与极狐GitLab 交互以验证：

- 个人访问令牌
- 用户对工作区的访问

### 设置 `gitlab-workspaces-proxy` 进行 SSH 连接

先决条件：

- 您必须拥有用于客户端验证的 SSH 主机 secret。

现在，[`gitlab-workspaces-proxy`](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy) 中默认启用 SSH。
要使用极狐GitLab Helm chart 设置 `gitlab-workspaces-proxy`：

1. 运行命令：

   ```shell
   ssh-keygen -f ssh-host-key -N '' -t rsa
   export SSH_HOST_KEY=$(pwd)/ssh-host-key
   ```

1. 使用生成的 SSH 主机 secret 安装 `gitlab-workspaces-proxy`：

   ```shell
   helm upgrade --install gitlab-workspaces-proxy \
         gitlab-workspaces-proxy/gitlab-workspaces-proxy \
         --version 0.1.8 \
         --namespace=gitlab-workspaces \
         --create-namespace \
         --set="auth.client_id=${CLIENT_ID}" \
         --set="auth.client_secret=${CLIENT_SECRET}" \
         --set="auth.host=${GITLAB_URL}" \
         --set="auth.redirect_uri=${REDIRECT_URI}" \
         --set="auth.signing_key=${SIGNING_KEY}" \
         --set="ingress.host.workspaceDomain=${GITLAB_WORKSPACES_PROXY_DOMAIN}" \
         --set="ingress.host.wildcardDomain=${GITLAB_WORKSPACES_WILDCARD_DOMAIN}" \
         --set="ingress.tls.workspaceDomainCert=$(cat ${WORKSPACES_DOMAIN_CERT})" \
         --set="ingress.tls.workspaceDomainKey=$(cat ${WORKSPACES_DOMAIN_KEY})" \
         --set="ingress.tls.wildcardDomainCert=$(cat ${WILDCARD_DOMAIN_CERT})" \
         --set="ingress.tls.wildcardDomainKey=$(cat ${WILDCARD_DOMAIN_KEY})" \
         --set="ssh.host_key=$(cat ${SSH_HOST_KEY})" \
         --set="ingress.className=nginx"
   ```

### 更新您的运行时镜像

要更新 SSH 连接的运行时镜像：

1. 在运行时镜像中安装 [`sshd`](https://man.openbsd.org/sshd.8)。
1. 创建名为 `gitlab-workspaces` 的用户以允许无密码访问您的容器。

```Dockerfile
FROM golang:1.20.5-bullseye

# Install `openssh-server` and other dependencies
RUN apt update \
    && apt upgrade -y \
    && apt install  openssh-server sudo curl git wget software-properties-common apt-transport-https --yes \
    && rm -rf /var/lib/apt/lists/*

# Permit empty passwords
RUN sed -i 's/nullok_secure/nullok/' /etc/pam.d/common-auth
RUN echo "PermitEmptyPasswords yes" >> /etc/ssh/sshd_config

# Generate a workspace host key
RUN ssh-keygen -A
RUN chmod 775 /etc/ssh/ssh_host_rsa_key && \
    chmod 775 /etc/ssh/ssh_host_ecdsa_key && \
    chmod 775 /etc/ssh/ssh_host_ed25519_key

# Create a `gitlab-workspaces` user
RUN useradd -l -u 5001 -G sudo -md /home/gitlab-workspaces -s /bin/bash gitlab-workspaces
RUN passwd -d gitlab-workspaces
ENV HOME=/home/gitlab-workspaces
WORKDIR $HOME
RUN mkdir -p /home/gitlab-workspaces && chgrp -R 0 /home && chmod -R g=u /etc/passwd /etc/group /home

# Allow sign-in access to `/etc/shadow`
RUN chmod 775 /etc/shadow

USER gitlab-workspaces
```

## 在 Kubernetes 的极狐GitLab 代理中禁用远程开发

您可以阻止 Kubernetes 的极狐GitLab 代理的 `remote_development` 模块与极狐GitLab 进行通信。
要在极狐GitLab 代理配置中禁用远程开发，请设置此参数：

```yaml
remote_development:
  enabled: false
```

如果您已经有正在运行的工作区，管理员必须在 Kubernetes 中手动删除这些工作区。

## 相关主题

- [极狐GitLab 远程开发工作区快速入门指南](https://go.gitlab.com/AVKFvy)
- [在极狐GitLab 中为按需、基于云的开发环境设置基础架构](https://go.gitlab.com/dp75xo)

## 故障排查

### 创建工作区时出现 `Failed to renew lease`

由于 Kubernetes 的极狐GitLab 代理中的已知问题，您可能无法创建工作区。
代理日志中可能会出现以下错误消息：

```plaintext
{"level":"info","time":"2023-01-01T00:00:00.000Z","msg":"failed to renew lease gitlab-agent-remote-dev-dev/agent-123XX-lock: timed out waiting for the condition\n","agent_id":XXXX}
```

当代理实例无法续订其 leadership lease 时会出现此问题，从而导致仅 leader 模块（包括 `remote_development` 模块）关闭。
要解决此问题，请重新启动代理实例。
