---
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 高级搜索语法 **(PREMIUM)**

使用[高级搜索](../advanced_search.md)，您可以对整个极狐GitLab 实例进行彻底搜索。

高级搜索语法支持带有前缀、布尔运算符等的模糊或精确搜索查询。高级搜索使用 [Elasticsearch 的语法](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html#simple-query-string-syntax)。

WARNING:
高级搜索仅搜索默认项目分支。

## 一般搜索

<!-- markdownlint-disable -->

| 使用 | 描述 | 示例                                                                                                                                        |
|-----|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| `"` | 精确搜索 | `"gem sidekiq"`                   |
| <code>&#124;</code> | 或 | <code>display &#124; banner</code>          |
| `+` | 与          | `display +banner` |
| `-` | 除外      | [`display -banner`](https://gitlab.com/search?group_id=9970&project_id=278964&scope=blobs&search=display+-banner)                              |
| `*` | 部分      | `bug error 50*`       |
| `\` | 转义       | `\*md`         |

## 代码搜索

| 使用          | 描述                    | 示例                                                                                                                                              |
|--------------|---------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| `filename:`  | 文件名                        | `filename:*spec.rb`    |
| `path:`      | 仓库位置             | `path:spec/workers/` |
| `extension:` | 文件扩展名，不包含 `.` | `extension:js`              |
| `blob:`      | Git 对象 ID                   | `blob:998707*`                           |

`extension` 和 `blob` 只返回完全匹配。

## 示例

| 示例                                                                                                                                                                             | 描述                                                          |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
| `rails -filename:gemfile.lock`              | 在除 *`gemfile.lock`* 文件之外的所有文件中显示 *rails*。          |
| `RSpec.describe Resolvers -*builder`                              | 显示所有不以 *builder* 开头的 *RSpec.describe Resolvers*。 |
| <code>bug &#124; (display +banner)</code>  | 显示 *bug*，**或** *display* **和** *banner*。                        |

<!-- markdownlint-enable -->
