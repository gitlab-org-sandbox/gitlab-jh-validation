---
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 在极狐GitLab 中搜索 **(BASIC ALL)**

极狐GitLab 有两种可用的搜索类型：*基本*和*高级*。

两种类型的搜索都是相同的，除非您通过代码进行搜索。

- 当您使用基本搜索来搜索代码时，您的搜索一次包括一个项目。
- 当您使用[高级搜索](advanced_search.md)来搜索代码时，您的搜索会同时包含所有项目。

## 全局搜索范围 **(BASIC SELF)**

> 引入于 14.3 版本。

要提高实例全局搜索的性能，管理员可以通过禁用 `ops` 功能标志<!--[`ops` 功能标志](../../development/feature_flags/index.md#ops-type)-->来限制全局搜索范围。

| 范围   | 功能标志                   | 描述                                                                  |
|------|------------------------|---------------------------------------------------------------------|
| 代码   | `global_search_code_tab` | 启用后，全局搜索会将代码作为搜索的一部分。                                               |
| 提交   | `global_search_commits_tab` | 启用后，全局搜索会将提交作为搜索的一部分。                                               |
| 史诗   | `global_search_epics_tab`          | 全局搜索会将史诗作为搜索的一部分。                                                   |
| 议题   | `global_search_issues_tab` | 启用后，全局搜索会将议题作为搜索的一部分。                                               |
| 合并请求 | `global_search_merge_requests_tab` | 启用后，全局搜索将合并请求作为搜索的一部分。                                              |
| 用户   | `global_search_users_tab` | 启用后，全局搜索会将用户作为搜索的一部分。                                               |
| Wiki | `global_search_wiki_tab` | 启用后，全局搜索会将 wiki 作为搜索的一部分。[群组 Wiki](../project/wiki/group.md) 不包括在内。 |

全局搜索在私有化部署实例中默认启用所有范围。

## 全局搜索验证

> - 对议题搜索中部分匹配的支持引入于极狐GitLab 14.9，[功能标志](../../administration/feature_flags.md)为 `issues_full_text_search`。默认禁用。
> - 普遍可用于极狐GitLab 16.2。移除 `issues_full_text_search` 功能标志。

全局搜索会忽略包含以下内容的搜索并将其记录为滥用：

- 少于 2 个字符。
- 超过 100 个字符的词（URL 搜索词最多包含 200 个字符）。
- 使用停止词作为唯一搜索词进行搜索（例如，`the`、`and` 或 `if` 等）。
- 使用不完全数字的 `group_id` 或 `project_id` 参数进行搜索。
- 使用包含 [Git refname](https://git-scm.com/docs/git-check-ref-format) 不允许的特殊字符的 `repository_ref` 或 `project_ref` 参数进行搜索。
- 使用未知的 `scope` 进行搜索。

全局搜索仅对包含以下内容的搜索标记错误：

- 搜索超过 4096 个字符。
- 搜索超过 64 个字词。

议题搜索不支持部分匹配。例如，当您输入 `play` 搜索议题时，系统不会返回包含 `play` 的议题。但是，查询会匹配字符串的所有可能变体（例如 `plays`）。

<a id="autocomplete-suggestions"></a>

## 自动完成建议

在搜索栏中，您可以查看以下自动完成建议：

- [项目](#search-for-projects-by-full-path)和群组
- 用户
- 帮助页面
- 项目功能（例如，里程碑）
- 设置（例如，用户设置）
- 最近查看的议题和史诗
- 最近查看的合并请求
- 用于项目中议题的[极狐GitLab Flavored Markdown](../markdown.md#gitlab-specific-references)

## 在所有极狐GitLab 中搜索

要在所有极狐GitLab 中搜索：

1. 在左侧边栏的顶部，选择 **搜索或转到**。
1. 输入您的搜索查询。您必须输入至少两个字符。
1. 按 <kbd>Enter</kbd> 进行搜索，或从列表中选择。

显示结果。要过滤结果，请在左侧边栏中选择一个过滤器。

## 在项目中搜索

要在项目中搜索：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 再次选择 **搜索或转到**，然后输入要搜索的字符串。
1. 按 <kbd>Enter</kbd> 进行搜索，或从列表中选择。

显示结果。要过滤结果，请在左侧边栏中选择一个过滤器。

<a id="search-for-projects-by-full-path"></a>

## 使用完整路径搜索项目

> - 引入于极狐GitLab 15.9，[功能标志](../../administration/feature_flags.md)为 `full_path_project_search`。默认禁用。
> - 普遍可用于极狐GitLab 15.11。移除 `full_path_project_search` 功能标志。

您可以通过在搜索框中输入项目的完整路径（包括其所属的命名空间）来搜索项目。
当您键入项目路径时，将显示[补全建议](#autocomplete-suggestions)。

例如：

- `gitlab-org/gitlab` 在 `gitlab-org` 命名空间中搜索 `gitlab` 项目。
- `gitlab-org/` 显示属于 `gitlab-org` 命名空间的项目的补全建议。

## 搜索结果中包含存档项目

> - 引入于极狐GitLab 16.1，[功能标志](../../administration/feature_flags.md)为 `search_projects_hide_archived`。默认禁用。
> - 普遍可用于极狐GitLab 16.3。移除 `search_projects_hide_archived` 功能标志。

默认情况下，存档项目不包含在搜索结果中。

要包含存档项目：

1. 在项目搜索页面的左侧边栏中，勾选 **包括存档** 复选框。
1. 在左侧边栏中，选择 **应用**。

## 从搜索结果中排除存档项目中的议题

> 引入于极狐GitLab 16.2，[功能标志](../../administration/feature_flags.md)为 `search_issues_hide_archived_projects`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，管理员可以启用名为 `search_issues_hide_archived_projects` 的[功能标志](../../administration/feature_flags.md)。在 JihuLab.com 上，此功能不可用。

默认情况下，存档项目中的议题包含在搜索结果中。
要排除存档项目中的议题，请确保启用 `search_issues_hide_archived_projects` 功能标志。

要在启用了 `search_issues_hide_archived_projects` 的存档项目中包含议题，您必须将参数 `include_archived=true` 添加到 URL 中。

## 搜素代码

在项目中搜索代码或其他文档：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 再次选择 **搜索或转到**，键入要搜索的字符串。
1. 按 <kbd>Enter</kbd> 或从列表中选择。

代码搜索仅显示文件中的第一个结果。
要搜索整个极狐GitLab，请让您的管理员启用[高级搜索](advanced_search.md)。

### 从代码搜索中查看 Git blame

> 引入于 14.7 版本。

找到搜索结果后，您可以查看对结果行进行最后更改的人员。

1. 在代码搜索结果中，将鼠标悬停在行号上。
1. 在左侧，选择 **查看 blame**。

### 按语言过滤代码搜索结果

> 引入于极狐GitLab 15.10。

要按一种或多种语言过滤代码搜索结果：

1. 在代码搜索页面的左侧边栏中，选择一种或多种语言。
1. 在左侧边栏中，选择 **应用**。

## 搜索提交 SHA

搜索提交 SHA：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 再次选择 **搜索或转到**，然后键入要搜索的提交 SHA。
1. 按 <kbd>Enter</kbd> 进行搜索，或从列表中选择。

如果返回单个结果，极狐GitLab 将重定向到提交结果，并为您提供返回搜索结果页面的选项。

## 从历史记录中搜索

您可以从历史记录中搜索议题和合并请求。搜索历史记录存储在本地浏览器中。要从历史记录中运行搜索：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 查看最近的搜索：

    - 对于议题，请在左侧边栏中选择 **计划 > 议题**。在列表上方搜索框左侧，选择 (**{history}**)。
    - 对于合并请求，在左侧边栏中，选择 **代码 > 合并请求**。在列表上方搜索框左侧，选择 **最近搜索**。

1. 从下拉列表中选择搜索。

