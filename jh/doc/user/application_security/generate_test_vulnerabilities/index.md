---
type: reference, howto
stage: Secure
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 生成测试漏洞

您可以在处理[漏洞报告](../vulnerability_report/index.md)时生成测试漏洞，在不运行流水线的情况下测试极狐GitLab 漏洞管理功能。

1. 登录极狐GitLab。
1. 进入 `/-/profile/personal_access_tokens`，生成具有 `api` 权限的个人访问令牌。
1. 进入你的项目页面，找到项目 ID。您可以在项目标题下方找到项目 ID。
1. [克隆极狐GitLab 仓库](../../../gitlab-basics/start-using-git.md#clone-a-repository)到您的本地计算机。
1. 打开终端并转到 `gitlab/qa` 目录。
1. 运行 `bundle install`。
1. 运行以下命令：

```shell
GITLAB_QA_ACCESS_TOKEN=<your_personal_access_token> GITLAB_URL="<address:port>" bundle exec rake vulnerabilities:setup\[<your_project_id>,<vulnerability_count>\] --trace
```

确保执行以下操作：

- 将 `<your_personal_access_token>` 替换为您在第一步中生成的令牌。
- 仔细检查 `GITLAB_URL`。它应该指向极狐GitLab 实例的地址和端口，例如您正在运行 GDK 时，指向 `http://localhost:3000`。
- 将 `<your_project_id>` 替换为您在第三步中获得的 ID。
- 将 `<vulnerability_count>` 替换为您想要生成的漏洞数量。

该脚本会在项目中创建指定数量的 placeholder 漏洞。
