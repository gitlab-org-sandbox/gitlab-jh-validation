---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 依赖扫描分析器 **(ULTIMATE)**

依赖扫描支持以下官方分析器：

- `gemnasium`
- `gemnasium-maven`
- `gemnasium-python`

分析器以 Docker 镜像的形式发布，依赖扫描使用这些镜像为每次分析启动专用容器。

依赖扫描预先配置了一组由极狐GitLab 维护的**默认镜像**，但用户也可以集成他们自己的**自定义镜像**。

`bundler-audit` 和 `retire.js` 分析器在 14.8 版本中已弃用，并在 15.0 版本中删除。使用 Gemnasium 代替。

## 官方默认分析器

对官方分析器的任何自定义更改都可以通过在 `.gitlab-ci.yml` 中使用 [CI/CD 变量](index.md#customizing-the-dependency-scanning-settings)来实现。

### 使用自定义 Docker 镜像

您可以切换到自定义 Docker 镜像库，该镜像库以不同的前缀提供官方分析器镜像。例如，下面的示例指示依赖扫描拉取 `my-docker-registry/gl-images/gemnasium`。在 `.gitlab-ci.yml` 中定义：

```yaml
include:
  template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  SECURE_ANALYZERS_PREFIX: my-docker-registry/gl-images
```

此配置要求您的自定义镜像库为所有官方分析器提供镜像。

### 禁用特定分析器

您可以选择不想运行的官方分析器。以下示例展示如何禁用 `gemnasium` 分析器。
在 `.gitlab-ci.yml` 中定义：

```yaml
include:
  template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  DS_EXCLUDED_ANALYZERS: "gemnasium"
```

### 禁用默认分析器

将 `DS_EXCLUDED_ANALYZERS` 设置为官方分析器列表会禁用它们。
在 `.gitlab-ci.yml` 中定义：

```yaml
include:
  template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  DS_EXCLUDED_ANALYZERS: "gemnasium, gemnasium-maven, gemnasium-python"
```

当完全依赖[自定义分析器](#custom-analyzers)时使用此选项。

<a id="custom-analyzers"></a>

## 自定义分析器

您可以通过在 CI 配置中定义 CI 作业来提供自己的分析器。为保持一致性，您应该在自定义依赖扫描作业后加上 `-dependency_scanning`。下面介绍如何添加基于 Docker 镜像 `my-docker-registry/analyzers/nuget` 的扫描作业，并在执行 `/analyzer run` 时生成依赖扫描报告 `gl-dependency-scanning-report.json`。在 `.gitlab-ci.yml` 中定义以下内容：

```yaml
nuget-dependency_scanning:
  image:
    name: "my-docker-registry/analyzers/nuget"
  script:
    - /analyzer run
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
```

<!--
The [Security Scanner Integration](../../../development/integrations/secure.md) documentation explains how to integrate custom security scanners into GitLab.
-->

## 分析器数据

下表列出了每个 Gemnasium 分析器可用的数据。

| Property \ Tool                       |      Gemnasium     |
|---------------------------------------|:------------------:|
| Severity                              | 𐄂                  |
| Title                                 | ✓                  |
| File                                  | ✓                  |
| Start line                            | 𐄂                  |
| End line                              | 𐄂                  |
| External ID (for example, CVE)        | ✓                  |
| URLs                                  | ✓                  |
| Internal doc/explanation              | ✓                  |
| Solution                              | ✓                  |
| Confidence                            | 𐄂                  |
| Affected item (for example, class or package) | ✓                  |
| Source code extract                   | 𐄂                  |
| Internal ID                           | ✓                  |
| Date                                  | ✓                  |
| Credits                               | ✓                  |

- ✓ => 我们有这些数据
- ⚠ => 我们有这些数据，但它是部分可靠的，或者我们需要从非结构化内容中提取该数据
- 𐄂 => 我们没有这些数据，或者需要开发特定或低效/不可靠的逻辑来获取它。

这些工具提供的值是异构的，因此它们有时会被标准化为通用值（例如，`severity`、`confidence` 等）。
