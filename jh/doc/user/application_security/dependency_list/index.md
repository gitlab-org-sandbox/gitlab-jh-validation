---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 依赖项列表 **(ULTIMATE ALL)**

> - 系统依赖项引入于极狐GitLab 14.6。
> - 群组级别的依赖项列表引入于极狐GitLab 16.2，[功能标志](../../../administration/feature_flags.md)为 `group_level_dependencies`。默认禁用。
> - 在 JihuLab.com 和私有化部署实例上启用群组级别的依赖项列表于极狐GitLab 16.4。

使用依赖项列表查看项目或群组的依赖项和有关这些依赖项的关键详细信息，包括它们的已知漏洞。它是项目中依赖项的集合，包括现有的和新的发现。该信息有时被称为软件物料清单、SBOM 或 BOM。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Project Dependency](https://www.youtube.com/watch?v=ckqkn9Tnbw4).
-->

## 先决条件

要查看项目的依赖项，请确保满足以下要求：

- 必须为您的项目配置[依赖扫描](../dependency_scanning/index.md)或[容器扫描](../container_scanning/index.md) CI 作业。
- 您的项目至少使用 Gemnasium 支持的[语言和包管理器](../dependency_scanning/index.md#supported-languages-and-package-managers)之一。
- 在默认分支上运行了一条成功的流水线。您不应更改允许[应用程序安全作业](../../application_security/index.md#application-coverage)失败的默认行为。

## 查看项目的依赖项

查看项目中的依赖项或群组中所有项目的依赖项：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目或群组。
1. 选择 **安全 > 依赖项列表**。

列出了每个依赖项的详细信息，并按漏洞严重性（如果有）的递减顺序排序。您可以按组件名称或打包程序对列表进行排序。

| 字段    | 描述                                                                                                           |
|-----------|--------------------------------------------------------------------------------------------------------------|
| Component | 依赖项的名称和版本。                                                                                                   |
| Packager  | 用于安装依赖项的打包程序。                                                                                                |
| Location  | 对于系统依赖项，会列出已扫描的镜像。对于应用程序依赖项，显示一个链接，指向您的项目中声明依赖项的特定于打包程序的锁定文件。如果存在并且支持的话，还显示顶级依赖项的[依赖项路径](#dependency-paths)。 |
| License   | 链接到依赖项的软件许可证。警告标志，包含在依赖项中检测到的漏洞数量。                                                                           |
| Projects<sup>2</sup> | 具有依赖项的项目链接。如果多个项目拥有相同的依赖项，则会显示这些项目的总数。要转到具有此依赖项的项目，请选择 **项目** 编号，然后搜索并选择其名称。仅在群组层次结构中最多出现 600 次的群组才支持项目搜索功能。 |

<html>
<small>脚注：
  <ol>
    <li>仅限项目级别。</li>
    <li>仅限群组级别。</li>
  </ol>
</small>
</html>

![Dependency list](img/dependency_list_v16_3.png)

### 漏洞

如果依赖项存在已知漏洞，请通过单击依赖项名称旁边的箭头或指示存在多少已知漏洞的标记来查看它们。对于每个漏洞，其严重性和描述显示在其下方。要查看漏洞的更多详细信息，请选择漏洞的描述，然后[漏洞详情](../vulnerabilities)页面打开。

### 依赖路径

依赖项列表显示了依赖项和它所连接的顶级依赖项之间的路径（如果有）。多个路径可以将瞬态依赖项连接到顶层依赖项，但用户界面仅显示最短路径之一。

NOTE:
仅显示存在漏洞的依赖项的依赖项路径。

![Dependency path](img/yarn_dependency_path_v13_6.png)

以下包管理器支持依赖路径：

- [NuGet](https://www.nuget.org/)
- [Yarn 1.x](https://classic.yarnpkg.com/lang/en/)
- [sbt](https://www.scala-sbt.org)
- [Conan](https://conan.io)

## 许可证

如果配置了[依赖项扫描](../../application_security/dependency_scanning/index.md) CI 作业，[已发现的许可证](../../compliance/license_scanning_of_cyclonedx_files/index.md)显示在此页面上。

## 下载依赖项列表

您可以下载 JSON 格式的完整依赖项列表及其详细信息。依赖项列表仅显示在默认分支上运行的最后一个成功流水线的结果。

下载依赖项列表：

1. 在左侧边栏中，选择 **搜索或转到** 并查找您的项目或群组。
1. 选择 **安全 > 依赖项列表**。
1. 选择 **导出**。
