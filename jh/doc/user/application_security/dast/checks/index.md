---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 基于 DAST 浏览器的爬虫漏洞检查 **(ULTIMATE)**

[基于 DAST 浏览器的爬虫](../browser_based.md)提供了许多漏洞检查，用于扫描被测站点中的漏洞。

## 被动检查

| ID       | 检查                                               | 严重程度   |类型 |
|:---------|:-------------------------------------------------|:-------|:-----|
| 1004.1   | 没有 HttpOnly 属性的敏感 cookie                         | Low      |被动|
| 16.1     | 缺少内容类型标头                                         | Low      |被动|
| 16.10    | 违反内容安全政策                                         | Info     |被动|
| 16.2     | 服务器标头公开版本信息                                      | Low      |被动|
| 16.3     | X-Powered-By 标头公开版本信息                            | Low      |被动|
| 16.4     | X-Backend-Server 标头公开服务器信息                       | Info     |被动|
| 16.5     | AspNet 标头公开版本信息                                  | Low      |被动|
| 16.6     | AspNetMvc 标头公开版本信息                               | Low      |被动|
| 16.7     | Strict-Transport-Security 标头丢失或无效                | Low      |被动|
| 16.8     | Content-Security-Policy 分析                       | Info     |被动|
| 16.9     | Content-Security-Policy-Report-Only 分析           | Info     |被动|
| 200.1    | 将敏感信息暴露给未经授权的 actor（私有 IP 地址）                    | Low      |被动|
| 209.1    | 生成包含敏感信息的错误消息                                    | Low      |被动|
| 209.2    | 生成包含敏感信息的数据库错误消息                                 | Low      |被动|
| 287.1    | HTTP 上的不安全身份验证（基本身份验证）                           | Medium     |被动|
| 287.2    | HTTP 上的不安全身份验证（摘要式身份验证）                          | Low      |被动|
| 319.1    | 混合内容                                             | Info     |被动|
| 352.1    | 缺乏反 CSRF 令牌                                      | Medium     |被动|
| 359.1    | 向未经授权的 actor（信用卡）公开私人信息 (PII)                    | Medium     |被动|
| 359.2    | 向未经授权的 actor（美国社会安全号码）公开私人信息 (PII)               | Medium     |被动|
| 548.1    | 通过目录列表公开信息                                       | Low      |被动|
| 598.1    | 使用带有敏感查询字符串（会话 ID）的 GET 请求方法                     | Medium     |被动|
| 598.2    | 使用带有敏感查询字符串（密码）的 GET 请求方法                        | Medium     |被动|
| 598.3    | 使用带有敏感查询字符串（授权标头详细信息）的 GET 请求方法                  | Medium     |被动|
| 601.1    | URL 重定向到不受信任的站点（'open redirect'）                 | Low      |被动|
| 614.1    | 没有安全属性的敏感 cookie                                 | Low      |被动|
| 693.1    | 缺少 X-Content-Type-Options：nosniff                | Low      |被动|
| 798.1    | 公开私密 secret 或令牌 Adafruit API key                 | High      |被动|
| 798.2    | 公开私密 secret 或令牌 Adobe 客户端 ID (OAuth Web)         | High   |被动|
| 798.3    | 公开私密 secret 或令牌 Adobe 客户端 key                    | High   |被动|
| 798.4    | 公开私密 secret 或令牌 Age secret key                   | High   |被动|
| 798.5    | 公开私密 secret 或令牌 Airtable API Key                 | High   |被动|
| 798.6    | 公开私密 secret 或令牌 Algolia API key                  | High   |被动|
| 798.7    | 公开私密 secret 或令牌 Alibaba AccessKey ID             | High   |被动|
| 798.8    | 公开私密 secret 或令牌 Alibaba Secret Key               | High   |被动|
| 798.9    | 公开私密 secret 或令牌 Asana 客户端 ID                     | High   |被动|
| 798.10   | 公开私密 secret 或令牌 Asana 客户端 Secret                 | High   |被动|
| 798.11   | 公开私密 secret 或令牌 Atlassian API 令牌                 | High   |被动|
| 798.12   | 公开私密 secret 或令牌 AWS                              | High      |被动|
| 798.13   | 公开私密 secret 或令牌 Bitbucket 客户端 ID                 | High      |被动|
| 798.14   | 公开私密 secret 或令牌 Bitbucket 客户端 Secret             | High      |被动|
| 798.15   | 公开私密 secret 或令牌 Bittrex 访问 Key                   | High      |被动|
| 798.16   | 公开私密 secret 或令牌 Bittrex Secret Key               | High      |被动|
| 798.17   | 公开私密 secret 或令牌 Beamer API 令牌                    | High      |被动|
| 798.18   | 公开私密 secret 或令牌 Codecov 访问令牌                     | High      |被动|
| 798.19   | 公开私密 secret 或令牌 Coinbase 访问令牌                    | High      |被动|
| 798.20   | 公开私密 secret 或令牌 Clojars API 令牌                   | High      |被动|
| 798.21   | 公开私密 secret 或令牌 Confluent 访问令牌                   | High      |被动|
| 798.22   | 公开私密 secret 或令牌 Confluent Secret Key             | High      |被动|
| 798.23   | 公开私密 secret 或令牌 Contentful 交付 API 令牌             | High      |被动|
| 798.24   | 公开私密 secret 或令牌 Databricks API 令牌                | High      |被动|
| 798.25   | 公开私密 secret 或令牌 Datadog 访问令牌                     | High      |被动|
| 798.26   | 公开私密 secret 或令牌 Discord API key                  | High      |被动|
| 798.27   | 公开私密 secret 或令牌 Discord 客户端 ID                   | High      |被动|
| 798.28   | 公开私密 secret 或令牌 Discord 客户端 secret               | High   | 被动 |
| 798.29   | 公开私密 secret 或令牌 Doppler API 令牌                   | High   | 被动 |
| 798.30   | 公开私密 secret 或令牌 Dropbox API secret               | High   | 被动 |
| 798.31   | 公开私密 secret 或令牌 Dropbox 长期 API 令牌                | High   | 被动 |
| 798.32   | 公开私密 secret 或令牌 Dropbox 短期 API 令牌                | High   | 被动 |
| 798.33   | 公开私密 secret 或令牌 Drone CI 访问令牌                    | High   | 被动 |
| 798.34   | 公开私密 secret 或令牌 Duffel API 令牌                    | High   | 被动 |
| 798.35   | 公开私密 secret 或令牌 Dynatrace API 令牌                 | High   | 被动 |
| 798.36   | 公开私密 secret 或令牌 EasyPost API 令牌                  | High   | 被动 |
| 798.37   | 公开私密 secret 或令牌 EasyPost 测试 API 令牌               | High   | 被动 |
| 798.38   | 公开私密 secret 或令牌 Etsy 访问令牌                        | High   | 被动 |
| 798.39   | 公开私密 secret 或令牌 Facebook                         | High   | 被动 |
| 798.40   | 公开私密 secret 或令牌 Fastly API key                   | High   | 被动 |
| 798.41   | 公开私密 secret 或令牌 Finicity 客户端 Secret              | High   | 被动 |
| 798.42   | 公开私密 secret 或令牌 Finicity API 令牌                  | High   | 被动 |
| 798.43   | 公开私密 secret 或令牌 Flickr 访问令牌                      | High   | 被动 |
| 798.44   | 公开私密 secret 或令牌 Finnhub 访问令牌                     | High   | 被动 |
| 798.46   | 公开私密 secret 或令牌 Flutterwave Secret Key           | High   | 被动 |
| 798.47   | 公开私密 secret 或令牌 Flutterwave 加密 Key               | High   | 被动 |
| 798.48   | 公开私密 secret 或令牌 Frame.io API 令牌                  | High   | 被动 |
| 798.49   | 公开私密 secret 或令牌 FreshBooks 访问令牌                  | High   | 被动 |
| 798.50   | 公开私密 secret 或令牌 GoCardless API 令牌                | High   | 被动 |
| 798.52   | 公开私密 secret 或令牌 GitHub 个人访问令牌                    | High   | 被动 |
| 798.53   | 公开私密 secret 或令牌 GitHub OAuth 访问令牌                | High   | 被动 |
| 798.54   | 公开私密 secret 或令牌 GitHub App 令牌                    | High   | 被动 |
| 798.55   | 公开私密 secret 或令牌 GitHub 刷新令牌                      | High   | 被动 |
| 798.56   | 公开私密 secret 或令牌极狐GitLab 个人访问令牌                   | High   | 被动 |
| 798.57   | 公开私密 secret 或令牌 Gitter 访问令牌                      | High   | 被动 |
| 798.58   | 公开私密 secret 或令牌 HashiCorp Terraform 用户/组织 API 令牌 | High   | 被动 |
| 798.59   | 公开私密 secret 或令牌 Heroku API Key                   | High   | 被动 |
| 798.60   | 公开私密 secret 或令牌 HubSpot API 令牌                   | High   | 被动 |
| 798.61   | 公开私密 secret 或令牌 Intercom API 令牌                  | High   | 被动 |
| 798.62   | 公开私密 secret 或令牌 Kraken 访问令牌                      | High   | 被动 |
| 798.63   | 公开私密 secret 或令牌 Kucoin 访问令牌                      | High   | 被动 |
| 798.64   | 公开私密 secret 或令牌 Kucoin Secret Key                | High   | 被动 |
| 798.65   | 公开私密 secret 或令牌 LaunchDarkly 访问令牌                | High   | 被动 |
| 798.66   | 公开私密 secret 或令牌 Linear API 令牌                    | High   | 被动 |
| 798.67   | 公开私密 secret 或令牌 Linear 客户端 Secret                | High   | 被动 |
| 798.68   | 公开私密 secret 或令牌 LinkedIn 客户端 ID                  | High   | 被动 |
| 798.69   | 公开私密 secret 或令牌 LinkedIn 客户端 secret              | High   | 被动 |
| 798.70   | 公开私密 secret 或令牌 Lob API Key                      | High   | 被动 |
| 798.72   | 公开私密 secret 或令牌 Mailchimp API key                | High   | 被动 |
| 798.74   | 公开私密 secret 或令牌 Mailgun 个人 API 令牌                | High   | 被动 |
| 798.75   | 公开私密 secret 或令牌 Mailgun Webhook 签名 key           | High   | 被动 |
| 798.77   | 公开私密 secret 或令牌 Mattermost 访问令牌                  | High   | 被动 |
| 798.78   | 公开私密 secret 或令牌 MessageBird API 令牌               | High   | 被动 |
| 798.80   | 公开私密 secret 或令牌 Netlify 访问令牌                     | High   | 被动 |
| 798.81   | 公开私密 secret 或令牌 New Relic 用户 API Key             | High   | 被动 |
| 798.82   | 公开私密 secret 或令牌 New Relic 用户 API ID              | High   | 被动 |
| 798.83   | 公开私密 secret 或令牌 New Relic ingest 浏览器 API 令牌      | High   | 被动 |
| 798.84   | 公开私密 secret 或令牌 npm 访问令牌                         | High   | 被动 |
| 798.86   | 公开私密 secret 或令牌 Okta 访问令牌                        | High   | 被动 |
| 798.87   | 公开私密 secret 或令牌 Plaid 客户端 ID                     | High   | 被动 |
| 798.88   | 公开私密 secret 或令牌 Plaid Secret key                 | High   | 被动 |
| 798.89   | 公开私密 secret 或令牌 Plaid API 令牌                     | High   | 被动 |
| 798.90   | 公开私密 secret 或令牌 PlanetScale 密码                   | High   | 被动 |
| 798.91   | 公开私密 secret 或令牌 PlanetScale API 令牌               | High   | 被动 |
| 798.92   | 公开私密 secret 或令牌 PlanetScale OAuth 令牌             | High   | 被动 |
| 798.93   | 公开私密 secret 或令牌 Postman API 令牌                   | High   | 被动 |
| 798.94   | 公开私密 secret 或令牌 Private Key                      | High   | 被动 |
| 798.95   | 公开私密 secret 或令牌 Pulumi API 令牌                    | High   | 被动 |
| 798.96   | 公开私密 secret 或令牌 PyPI 上传令牌                        | High   | 被动 |
| 798.97   | 公开私密 secret 或令牌 RubyGems API 令牌                  | High   | 被动 |
| 798.98   | 公开私密 secret 或令牌 RapidAPI 访问令牌                    | High   | 被动 |
| 798.99   | 公开私密 secret 或令牌 Sendbird 访问 ID                   | High   | 被动 |
| 798.100  | 公开私密 secret 或令牌 Sendbird 访问令牌                    | High   | 被动 |
| 798.101  | 公开私密 secret 或令牌 SendGrid API 令牌                  | High   | 被动 |
| 798.102  | 公开私密 secret 或令牌 Sendinblue API 令牌                | High   | 被动 |
| 798.103  | 公开私密 secret 或令牌 Sentry 访问令牌                      | High   | 被动 |
| 798.104  | 公开私密 secret 或令牌 Shippo API 令牌                    | High   | 被动 |
| 798.105  | 公开私密 secret 或令牌 Shopify 访问令牌                     | High   | 被动 |
| 798.106  | 公开私密 secret 或令牌 Shopify 自定义访问令牌                  | High   | 被动 |
| 798.107  | 公开私密 secret 或令牌 Shopify 个人应用访问令牌                 | High   | 被动 |
| 798.108  | 公开私密 secret 或令牌 Shopify shared secret            | High   | 被动 |
| 798.109  | 公开私密 secret 或令牌 Slack 令牌                         | High   | 被动 |
| 798.110  | 公开私密 secret 或令牌 Slack Webhook                    | High   | 被动 |
| 798.111  | 公开私密 secret 或令牌 Stripe                           | High   | 被动 |
| 798.112  | 公开私密 secret 或令牌 Square 访问令牌                      | High   | 被动 |
| 798.113  | 公开私密 secret 或令牌 Squarespace 访问令牌                 | High   | 被动 |
| 798.114  | 公开私密 secret 或令牌 SumoLogic 访问 ID                  | High   | 被动 |
| 798.115  | 公开私密 secret 或令牌 SumoLogic 访问令牌                   | High   | 被动 |
| 798.116  | 公开私密 secret 或令牌 Travis CI 访问令牌                   | High   | 被动 |
| 798.117  | 公开私密 secret 或令牌 Twilio API Key                   | High   | 被动 |
| 798.118  | 公开私密 secret 或令牌 Twitch API 令牌                    | High   | 被动 |
| 798.119  | 公开私密 secret 或令牌 Twitter API Key                  | High   | 被动 |
| 798.120  | 公开私密 secret 或令牌 Twitter API Secret               | High   | 被动 |
| 798.121  | 公开私密 secret 或令牌 Twitter 访问令牌                     | High   | 被动 |
| 798.122  | 公开私密 secret 或令牌 Twitter 访问 Secret                | High   | 被动 |
| 798.123  | 公开私密 secret 或令牌 Twitter Bearer 令牌                | High   | 被动 |
| 798.124  | 公开私密 secret 或令牌 Typeform API 令牌                  | High   | 被动 |
| 798.125  | 公开私密 secret 或令牌 Yandex API Key                   | High   | 被动 |
| 798.126  | 公开私密 secret 或令牌 Yandex AWS 访问令牌                  | High   | 被动 |
| 798.127  | 公开私密 secret 或令牌 Yandex 访问令牌                      | High   | 被动 |
| 798.128  | 公开私密 secret 或令牌 Zendesk Secret Key               | High   | 被动 |
| 829.1    | 包含来自不可信控制领域的功能                                   | Low      |被动|
| 829.2    | 检测到无效的子资源完整性值                                    | Medium     |被动|

## 主动检查

| ID              |检查 |严重程度 | 类型  |
|:-----------------|:------|:---------|:----|
| 113.1 | HTTP 标头中 CRLF 序列的不正确中和 | High | 主动  |
| 22.1   |对受限制目录的路径名限制不当（路径遍历）|High| 主动  |
| 611.1 |外部 XML 实体注入 (XXE) |High| 主动  |
| 94.4  |服务器端代码注入（NodeJS）|High| 主动  |
