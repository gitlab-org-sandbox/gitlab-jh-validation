---
type: reference, howto
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 离线部署 **(ULTIMATE SELF)**

在未连接到 Internet 时，可以运行大多数极狐GitLab 安全扫描程序。

本文档介绍如何在离线环境中操作安全类别（即扫描仪类型）。这些说明也适用于受保护、具有安全策略（例如防火墙策略）或以其他方式限制访问完整 Internet 的私有化部署安装实例。系统将这些环境称为离线环境。其他常见名称包括：

- 隔离环境
- 有限的连接环境
- 局域网 (LAN) 环境
- 内联网环境

这些环境具有阻止或限制互联网访问的物理屏障或安全策略（例如防火墙）。这些说明是为物理断开的网络设计的，但也可以在这些其他用例中遵循。

## 定义离线环境

在离线环境中，极狐GitLab 实例可以是一个或多个服务器和服务，它们可以在本地网络上进行通信，但不能或非常有限地访问互联网。假设极狐GitLab 实例和支持基础设施（例如，私有 Maven 仓库）中的任何内容都可以通过本地网络连接进行访问。假设来自 Internet 的任何文件都必须通过物理媒体（USB 驱动器、硬盘驱动器、可写 DVD 等）进入。

## 概览

极狐GitLab 扫描器通常连接到互联网以下载最新的签名、规则和补丁集。通过使用本地网络上可用的资源来配置工具以正常运行需要一些额外的步骤。

### 容器镜像库和软件包库

在高级别上，安全分析器作为 Docker 镜像提供，并且可以利用各种软件包库。当您在连接 Internet 的极狐GitLab 安装实例上运行作业时，极狐GitLab 会检查您是否拥有这些 Docker 镜像的最新版本，并可能连接到软件包库以安装必要的依赖项。

在离线环境中，必须禁用这些检查。您必须更新每个扫描器，引用不同的内部托管镜像库或提供对单个扫描器镜像的访问权限。

您还必须确保您的应用程序可以访问未托管在 SaaS 上的通用包仓库，例如 npm、yarn 或 Ruby gems。可以通过临时连接到网络或通过在您自己的离线网络中镜像包来获取这些仓库中的包。

### 与漏洞交互

一旦发现漏洞，您就可以与之交互。阅读有关如何[解决漏洞](../vulnerabilities/index.md)的更多信息。

在某些情况下，报告的漏洞提供的元数据可以包含在 UI 中公开的外部链接。在离线环境中可能无法访问这些链接。

### 解决漏洞

[解决漏洞](../vulnerabilities/index.md#resolve-a-vulnerability)功能可用于离线依赖扫描和容器扫描，但根据您的实例配置可能无法正常工作。当我们能够访问托管该依赖项或镜像库的最新版本的仓库服务时，我们只能建议解决方案，这些解决方案通常是已修补的更新版本。

### 扫描器签名和规则更新

当连接到互联网时，一些扫描器会参考公共数据库以获取最新的签名和规则集进行检查。当没有连接时，这是不可能的。因此，根据扫描器的不同，您必须禁用这些自动更新检查，并使用它们附带的数据库并手动更新这些数据库，或者提供对您自己在网络中托管的副本的访问权限。

<!--
## 特定的扫描器说明

每个单独的扫描器可能与上述步骤略有不同。您可以在以下每个页面中找到更多信息：

- [Container scanning offline directions](../container_scanning/index.md#running-container-scanning-in-an-offline-environment)
- [SAST offline directions](../sast/index.md#running-sast-in-an-offline-environment)
- [Secret Detection offline directions](../secret_detection/#running-secret-detection-in-an-offline-environment)
- [DAST offline directions](../dast/run_dast_offline.md#run-dast-in-an-offline-environment)
- [API Fuzzing offline directions](../api_fuzzing/#running-api-fuzzing-in-an-offline-environment)
- [License Compliance offline directions](../../compliance/license_compliance/index.md#running-license-compliance-in-an-offline-environment)
- [Dependency Scanning offline directions](../dependency_scanning/index.md#running-dependency-scanning-in-an-offline-environment)
-->

<a id="loading-docker-images-onto-your-offline-host"></a>

## 将 Docker 镜像加载到离线主机上

要使用许多极狐GitLab 功能，包括安全扫描和 [Auto DevOps](../../../topics/autodevops/index.md)，runner 必须能够获取相关的 Docker 镜像。

在不直接访问公共互联网的情况下，要使这些镜像可用，可以下载镜像，然后打包并将它们传输到离线主机。以下是此类转移的示例：

1. 从公共互联网下载 Docker 镜像。
1. 将 Docker 镜像打包为 tar archives。
1. 将镜像传输到离线环境。
1. 将传输的镜像加载到离线 Docker 镜像库中。

<a id="using-the-official-gitlab-template"></a>

### 使用官方模板

极狐GitLab 提供了一个[模板](../../../ci/yaml/index.md#includetemplate)来简化这个过程。

这个模板应该用在一个新的空项目中，带有一个 `.gitlab-ci.yml` 文件，其中包含：

```yaml
include:
  - template: Security/Secure-Binaries.gitlab-ci.yml
```

流水线下载安全扫描程序所需的 Docker 镜像并将它们保存为[作业产物](../../../ci/jobs/job_artifacts.md)，或将它们推送到执行流水线所在项目的[容器镜像库](../../packages/container_registry/index.md)。这些 archives 可以转移到另一个位置并在 Docker daemon 中[加载](https://docs.docker.com/engine/reference/commandline/load/)。
此方法需要 runner 同时访问 `jihulab.com`（包括 `registry.gitlab.cn`）和本地离线实例。此 runner 必须在[特权模式](https://docs.gitlab.cn/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)下运行才能使用 `docker` 作业内的命令。此 runner 可以安装在 DMZ 或堡垒上，并且仅用于此特定项目。

WARNING:
此模板不包括容器扫描分析器的更新。<!--Please see
[Container scanning offline directions](../container_scanning/index.md#running-container-scanning-in-an-offline-environment).-->

#### 计划更新

默认情况下，当 `.gitlab-ci.yml` 添加到仓库时，此项目的流水线仅运行一次。要更新极狐GitLab 安全扫描器和签名，有必要定期运行此流水线。极狐GitLab 提供了一种[计划流水线](../../../ci/pipelines/schedules.md)的方法。例如，您可以将其设置为每周下载和存储 Docker 镜像。

#### 使用创建的安全包

使用 `Secure-Binaries.gitlab-ci.yml` 模板的项目现在应该托管运行极狐GitLab 安全功能所需的所有必需镜像和资源。

接下来，您必须告诉离线实例使用这些资源，而不是 SaaS 上的默认资源。为此，请使用项目[容器镜像库](../../packages/container_registry/index.md) 的 URL 设置 CI/CD 变量 `SECURE_ANALYZERS_PREFIX`。

您可以在项目的 `.gitlab-ci.yml` 中或在项目或组级别的 UI 中设置此变量。有关详细信息，请参阅[极狐GitLab CI/CD 变量页面](../../../ci/variables/index.md#define-a-cicd-variable-in-the-ui)。

#### 变量

下表显示了哪些 CI/CD 变量可以与 `Secure-Binaries.gitlab-ci.yml` 模板一起使用：

| CI/CD 变量                            | 描述                                   | 默认值                     |
|-------------------------------------------|-----------------------------------------------|-----------------------------------|
| `SECURE_BINARIES_ANALYZERS`               | 要下载的分析器的逗号分隔列表 | `"bandit, brakeman, gosec, and so on..."` |
| `SECURE_BINARIES_DOWNLOAD_IMAGES`         | 用于禁用作业                          | `"true"`                          |
| `SECURE_BINARIES_PUSH_IMAGES`             | 将文件推送到项目仓库            | `"true"`                          |
| `SECURE_BINARIES_SAVE_ARTIFACTS`          | 将镜像存档另存为产物         | `"false"`                         |
| `SECURE_BINARIES_ANALYZER_VERSION`        | 默认分析器版本（Docker 标签）         | `"2"`                             |

### 没有官方模板的替代方式

如果无法按照上述方法进行，则可以手动传输镜像：

#### 示例镜像打包程序脚本

```shell
#!/bin/bash
set -ux

# Specify needed analyzer images
analyzers=${SAST_ANALYZERS:-"bandit eslint gosec"}
gitlab=registry.gitlab.cn/security-products/

for i in "${analyzers[@]}"
do
  tarname="${i}_2.tar"
  docker pull $gitlab$i:2
  docker save $gitlab$i:2 -o ./analyzers/${tarname}
  chmod +r ./analyzers/${tarname}
done
```

#### 示例镜像加载器脚本

此示例将镜像从堡垒主机加载到脱机主机。在某些配置中，此类传输可能需要物理介质：

```shell
#!/bin/bash
set -ux

# Specify needed analyzer images
analyzers=${SAST_ANALYZERS:-"bandit eslint gosec"}
registry=$GITLAB_HOST:4567

for i in "${analyzers[@]}"
do
  tarname="${i}_2.tar"
  scp ./analyzers/${tarname} ${GITLAB_HOST}:~/${tarname}
  ssh $GITLAB_HOST "sudo docker load -i ${tarname}"
  ssh $GITLAB_HOST "sudo docker tag $(sudo docker images | grep $i | awk '{print $3}') ${registry}/analyzers/${i}:2"
  ssh $GITLAB_HOST "sudo docker push ${registry}/analyzers/${i}:2"
done
```

### 在离线环境中使用带有 AutoDevOps 的极狐GitLab 安全功能

您可以在离线环境中使用 AutoDevOps 进行安全扫描。但是，您必须首先执行以下步骤：

1. 将容器镜像加载到本地镜像库中。极狐GitLab 安全功能利用分析器容器镜像进行各种扫描。这些镜像必须作为运行 AutoDevOps 的一部分提供。在运行 AutoDevOps 之前，请按照[上述步骤](#using-the-official-gitlab-template)，将这些容器镜像加载到本地容器镜像库中。

1. 设置 CI/CD 变量来确保 AutoDevOps 在正确的位置查找这些镜像。AutoDevOps 模板利用 `SECURE_ANALYZERS_PREFIX` 变量来识别分析器镜像的位置。确保将此变量设置为加载分析器镜像的位置的正确值。您可以考虑使用项目 CI/CD 变量或通过[修改](../../../topics/autodevops/customize.md#customize-gitlab-ciyml) `.gitlab-ci.yml` 直接存档。

完成这些步骤后，极狐GitLab 将拥有安全分析器的本地副本，并设置为使用它们而不是 Internet 托管的容器镜像。允许您在离线环境中在 AutoDevOps 中运行安全功能。

请注意，这些步骤特定于极狐GitLab 安全功能。使用 AutoDevOps 的其他阶段可能需要 [Auto DevOps 文档](../../../topics/autodevops/index.md)中介绍的其他步骤。
