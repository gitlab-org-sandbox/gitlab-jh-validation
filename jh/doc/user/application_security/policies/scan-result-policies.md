---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 扫描结果策略 **(ULTIMATE ALL)**

> 群组级扫描结果策略引入于 15.6 版本。

您可以使用扫描结果策略根据扫描结果采取行动。例如，一种类型的扫描结果策略是允许根据一个或多个安全扫描作业的 findings，要求批准的安全批准策略。在 CI 扫描作业完全执行后评估扫描结果策略。

NOTE:
扫描结果策略仅适用于[受保护](../../project/protected_branches.md)的目标分支。

NOTE:
创建或删除受保护分支时，会同步策略批准规则，有一分钟的延迟。

## 具有多个流水线的合并请求

> - 引入于极狐GitLab 16.2，[功能标志](../../../administration/feature_flags.md)为 `multi_pipeline_scan_result_policies`。默认禁用。
> - 在 JihuLab.com 和私有化部署上启用于极狐GitLab 16.3。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。要隐藏该功能，管理员可以禁用名为 `multi_pipeline_scan_result_policies` 的[功能标志](../../../administration/feature_flags.md)。在 JihuLab.com 上，可以使用此功能。

一个项目可以配置多个流水线类型。一次提交可以启动多个流水线，每个流水线都可能包含安全扫描。

- 在 16.3 及更高版本中，将评估 MR 源分支和目标分支中最新提交的所有已完成流水线的结果，并用以执行扫描结果策略。
  不考虑父子流水线和按需 DAST 流水线。
- 在 16.2 及更早版本中，执行扫描结果策略时，仅评估最新完成的流水线的结果。

<a id="scan-result-policy-editor"></a>

## 扫描结果策略编辑器

> 引入于极狐GitLab 14.8。
> 默认启用于极狐GitLab 15.6。

NOTE:
只有项目所有者有权选择安全策略项目。

完成策略后，通过选择编辑器底部的 **使用合并请求进行配置** 来保存，将您重定向到项目的已配置安全策略项目的合并请求。
如果安全策略项目未关联到您的项目，系统会为您创建项目。
通过选择编辑器底部的 **删除策略**，也可以从编辑器界面中删除现有策略。

大多数策略更改会在合并请求合并后立即生效。未通过合并请求并直接提交到默认分支的任何更改，可能需要长达 10 分钟才能使策略更改生效。

策略编辑器支持 YAML 模式和规则模式。

NOTE:
对于具有大量项目的群组创建的扫描结果策略，其传播将需要一段时间才能完成。

## 扫描结果策略 schema（Scan result policies schema）

带有扫描结果策略的 YAML 文件由一组与嵌套在 `scan_result_policy` key 下的扫描结果策略模式匹配的对象组成。您可以在 `scan_result_policy` key 下配置最多五个策略。

当您保存新策略时，极狐GitLab 会根据[此 JSON 模式](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/validators/json_schemas/security_orchestration_policy.json) 验证其内容。
如果您不熟悉如何阅读 [JSON 模式](https://json-schema.org/)，以下部分和表格提供另一种选择。

| 字段 | 类型 | 是否必需 | 可能的值             | 描述 |
|-------|------|------|------------------|-------------|
| `scan_result_policy` | 扫描结果策略的 `array` | 是    | 扫描结果策略列表（最多 5 个） |

## 扫描结果策略 schema（Scan result policy schema）

> `approval_settings` 字段引入于极狐GitLab 16.4，[功能标志](../../../administration/feature_flags.md)为 `scan_result_policy_settings`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，管理员可以启用名为 `scan_result_policy_settings` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能不可用。

| 字段                | 类型              | 是否必需 | 可能的值                                                                                                                                                                 | 描述                                                 |
|-------------------|-----------------|------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|
| `name`            | `string`   | 是    |                                                                                                                                                                      | 策略名称。最多 255 个字符。                                   |
| `description`（可选） | `string`   | 是    |                                                                                                                                                                      | 策略的描述。                                             |
| `enabled`         | `boolean`| 是    | `true`、`false`                                                                                                             | 启用 (`true`) 或禁用 (`false`) 策略的标志。                   |
| `rules`           |  规则的 `array`  | 是    |                                                                                                                                                                      | 策略应用的规则列表。                                         |
| `actions`         |  操作的 `array`   | 否    |                                                                                                                                                                      | 策略执行的操作列表。                                         |
| `approval_settings` | `object` | 否    | `{prevent_approval_by_author: boolean, prevent_approval_by_commit_author: boolean, remove_approvals_with_new_commit: boolean, require_password_to_approve: boolean}` | 策略覆盖的项目设置 |

## `scan_finding` 规则类型

> - 扫描结果策略字段 `vulnerability_attributes` 引入于极狐GitLab 16.2，[功能标志](../../../administration/feature_flags.md)为 `enforce_vulnerability_attributes_rules`。默认禁用。
> - 在 JihuLab.com 和私有化部署上启用于极狐GitLab 16.3。
> - 扫描结果策略字段 `vulnerability_age` 引入于极狐GitLab 16.2。
> - `branch_exceptions` 字段引入于极狐GitLab 16.3，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_exceptions`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `vulnerability_attributes` 字段可用。要隐藏该功能，管理员可以禁用名为 `enforce_vulnerability_attributes_rules` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `branch_exceptions` 字段可用。要隐藏该功能，管理员可以禁用名为 `security_policies_branch_exceptions` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

此规则根据安全扫描结果执行定义的操作。

| 字段 | 类型                 | 是否必需                         | 可能的值                                                                                                         | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|------------|--------------------|------------------------------|--------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`     | `string`           | 是                            | `scan_finding`                                                                                               | 规则的类型                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `branches` | `string` 的 `array` | 如果 `branch_type` 字段不存在，则为"是" | `[]` 或分支名称                                                                                                   | 仅适用于受保护的目标分支。一个空数组 `[]` 可以将规则应用于所有受保护的目标分支。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `branch_type` | `string`           | 如果 `branches` 字段不存在，则为"是"    | `default` 或 `protected`                                                                                      | 应用特定策略的分支类型。不能与 `branches` 字段一起使用。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `branch_exceptions` | `string` 的 `array` | 否                            | 分支名称                                                                                                         | 规则不包括的分支。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `scanners`  | `string` 的 `array` | 是                            | `sast`、`secret_detection`、`dependency_scanning`、`container_scanning`、`dast`、`coverage_fuzzing`、`api_fuzzing` | 此规则要考虑的安全扫描程序。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `vulnerabilities_allowed`  | `integer`          | 是                            | 大于或等于零                                                                                                       | 在考虑此规则之前允许的漏洞数。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `severity_levels`  | `string` 的 `array` | 是                            | `info`、`unknown`、`low`、`medium`、`high`、`critical`                                                            | 此规则要考虑的严重性级别。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `vulnerability_states`  | `string` 的 `array` | 是                            | `newly_detected`、`detected`、`confirmed`、`resolved`、`dismissed`、`new_needs_triage`、`new_dismissed`            | 所有漏洞分为两类：<br><br>**新检测到的漏洞** - `newly_detected` 策略选项涵盖合并请求本身中的 `Detected` 或 `Dismissed` 漏洞，这些漏洞目前在默认分支上不存在。此策略选项要求流水线在按规则评估之前完成，以便它知道是否新检测到漏洞。合并请求在流水线之前被阻止，并且必要的安全扫描已完成。<br><br> • `Detected`<br> • Dismissed<br><br> `new_needs_triage` 选项可选状态<br><br> • Detected<br><br> `new_dismissed` 选项可选状态<br><br>  • `Dismissed`<br><br>**预先存在的漏洞** - 这些策略选项会立即进行评估，并且不需要完整的流水线，因为它们只考虑先前在默认分支中检测到的漏洞。<br><br> • `Detected` - 该策略寻找在 `Detected` 状态下的漏洞。<br> • `Confirmed` - 该策略寻找在 `Confirmed` 状态下的漏洞。<br> • `Dismissed` - 该策略寻找在 `Dismissed` 状态下的漏洞。<br> • `Resolved` - 该策略寻找在 `Resolved` 状态下的漏洞。 |
| `vulnerability_attributes` | `object`           | 否                            | `{false_positive: boolean, fix_available: boolean}`                                                          | 默认情况下会考虑所有漏洞发现。但可以对参数应用过滤器以仅考虑漏洞发现：<br><br> • 有可用修复 (`fix_available: true`)<br><br> • 无可用修复 (`fix_available: false`)< br> • 误报 (`false_positive: true`)<br> • 非误报 (`false_positive: false`)<br> • 或两者的组合。例如 (`fix_available: true, false_positive: false`)                                                                                                                                                                                                                                                                                                                                  |
| `vulnerability_age` | `object`           | 否                            | N/A                                                                                                          | 按存在时间过滤预先存在的漏洞发现结果。漏洞的存在时间计算为自项目中检测到该漏洞以来的时间。标准为 `operator`、`value` 和  `interval`。<br>-`operator` 标准指定所使用的年龄比较是大（`greater_than`）还是小（`less_than`）。<br >- `value` 条件指定代表漏洞存在时间的数值。<br>- `interval` 条件指定漏洞存在时间的计量单位：`day`、`week`、`month` 或 `year`。<br><br>示例：`operator: greater_than`、`value: 30`、`interval: day`。                                                                                                                                                                                                                                                                             |

## `license_finding` 规则类型

> - 引入于极狐GitLab 15.9，[功能标志](../../../administration/feature_flags.md)为 `license_scanning_policies`。
> - 普遍可用于极狐GitLab 15.11。移除功能标志 `license_scanning_policies`。
> - `branch_exceptions` 字段引入于极狐GitLab 16.3，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_exceptions`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `branch_exceptions` 字段可用。要隐藏该功能，管理员可以禁用名为 `security_policies_branch_exceptions` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

此规则根据许可证结果执行定义的操作。

| 字段 | 类型                 | 是否必需                         | 可能的值                        | 描述                                      |
|------------|--------------------|------------------------------|-----------------------------|-----------------------------------------|
| `type` | `string`           | 是                            | `license_finding`           | 规则的类型                                   |
| `branches` | `string` 的 `array` | 如果 `branch_type` 字段不存在，则为"是" | `[]` 或分支的名称                 | 仅适用于受保护的目标分支。空数组 `[]` 将规则应用于所有受保护的目标分支。 |
| `branch_type` | `string`           | 如果 `branches` 字段不存在，则为"是"     | `default` 或 `protected`     | 应用特定策略的分支类型。无法与 `branches` 字段一起使用。      |
| `branch_exceptions` | `string` 的 `array`     | 否                            | 分支名称                        | 规则不包括的分支。                               |
| `match_on_inclusion` | `boolean`          | 是                            | `true`、`false`              | 该规则是否匹配包含或排除 `license_types` 中列出的许可证。   |
| `license_types` | `string` 的 `array` | 是                            | 许可证类型                       | 要匹配的许可证类型，例如 `BSD` 或 `MIT`。             |
| `license_states` | `string` 的 `array` | 是                            | `newly_detected`、`detected` | 是否匹配新检测到的和/或以前检测到的许可证。                  |

## `any_merge_request` 规则类型

> - 引入于极狐GitLab 16.4。
> - `branch_exceptions` 字段引入于极狐GitLab 16.3，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_exceptions`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `branch_exceptions` 字段可用。要隐藏该功能，管理员可以禁用名为 `security_policies_branch_exceptions` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

此规则根据提交签名为合并请求执行定义的操作。

| 字段                  | 类型                 | 是否必需                         | 可能的值                    | 描述                                                                       |
|---------------------|--------------------|------------------------------|-------------------------|--------------------------------------------------------------------------|
| `type`              | `string`           | 是                            | `any_merge_request`     | 规则类型。                                                                    |
| `branches`          | `string` 的 `array` | 如果 `branch_type` 字段不存在，则为"是" | `[]` 或分支的名称             | 仅适用于受保护的目标分支。空数组 `[]` 将规则应用于所有受保护的目标分支。不能与 `branch_type` 字段一起使用。         |
| `branch_type`       | `string`           | 如果 `branches` 字段不存在则为"是"     | `default` 或 `protected` | 应用特定策略的分支类型。无法与 `branches` 字段一起使用。                                       |
| `branch_exceptions` | `string` 的 `array`        | 否                            | 分支名称                    | 规则不包括的分支。                                                                |
| `commits`           | `string`           | 是                            | `any`、`unsigned`        | 规则是否匹配任何提交，或者仅在合并请求中检测到未签名的提交时匹配。 |

## `require_approval` 操作类型

此操作设置在满足已定义策略中的至少一个规则的条件时，需要的批准规则。

| 字段 | 类型                  | 是否必需 | 可能的值 | 描述 |
|-------|---------------------|------|-----------------|-------------|
| `type` | `string`            | 是    | `require_approval` | 操作的类型。 |
| `approvals_required` | `integer`           | 是    | 大于或等于零 | 所需的 MR 批准数。 |
| `user_approvers` | `string` 的 `array`  | 否    | 一位或多位用户的用户名 | 被视为核准人的用户。用户必须有权访问该项目才有资格进行批准。 |
| `user_approvers_ids` | `integer` 的 `array` | 否    | 一个或多个用户的 ID | 被视为核准人的用户的 ID。用户必须有权访问该项目才有资格进行批准。 |
| `group_approvers` | `string` 的 `array`  | 否    | 一个或多个群组的路径 | 被视为核准人的群组。具有[群组中直接成员资格](../../project/merge_requests/approvals/rules.md#group-approvers)的用户有资格进行批准。 |
| `group_approvers_ids` | `integer` 的 `array` | 否    | 一个或多个群组的 ID | 被视为核准人的群组的 ID。具有[群组中直接成员资格](../../project/merge_requests/approvals/rules.md#group-approvers)的用户有资格进行批准。 |
| `role_approvers` | `string` 的 `array`         | 否    | 一个或多个[角色](../../../user/permissions.md#roles)（例如：`owner`、`maintainer`）  | 作为有资格批准的批准者的角色。 |

要求和限制：

- 您必须添加相应的安全扫描工具<!--[安全扫描工具](../index.md#security-scanning-tools)-->。否则，扫描结果策略将不起作用。
- 策略的最大数量为五个。
- 每个策略最多可以有五个规则。
- 所有配置的扫描器都必须存在于合并请求的最新流水线中。否则，即使未满足某些漏洞标准，也需要批准。

<a id="example-security-scan-result-policies-project"></a>

## 安全扫描结果策略项目示例

您可以在 `.gitlab/security-policies/policy.yml` 中使用此示例，如[安全策略项目](index.md#security-policy-project)中所述：

```yaml
---
scan_result_policy:
  - name: critical vulnerability CS approvals
    description: critical severity level only for container scanning
    enabled: true
    rules:
      - type: scan_finding
        branches:
          - main
        scanners:
          - container_scanning
        vulnerabilities_allowed: 0
        severity_levels:
          - critical
        vulnerability_states:
          - newly_detected
        vulnerability_attributes:
          false_positive: true
          fix_available: true
    actions:
      - type: require_approval
        approvals_required: 1
        user_approvers:
          - adalberto.dare
  - name: secondary CS approvals
    description: secondary only for container scanning
    enabled: true
    rules:
      - type: scan_finding
        branches:
          - main
        scanners:
          - container_scanning
        vulnerabilities_allowed: 1
        severity_levels:
          - low
          - unknown
        vulnerability_states:
          - detected
        vulnerability_age:
          operator: greater_than
          value: 30
          interval: day
    actions:
      - type: require_approval
        approvals_required: 1
        role_approvers:
          - owner
```

在此例中：

- 每个包含由容器扫描识别的新 `critical` 级别的漏洞的 MR，都需要 `alberto.dare` 的批准。
- 每个包含多个通过容器扫描发现的超过 30 天的预先存在的 `low` 或 `unknown` 漏洞的 MR 都需要获得具有所有者角色的项目成员的批准。

## 扫描结果策略编辑器示例

您可以在[扫描结果策略编辑器](#scan-result-policy-editor)的 YAML 模式下使用此示例。
它对应于上一个示例中的单个对象：

```yaml
- name: critical vulnerability CS approvals
  description: critical severity level only for container scanning
  enabled: true
  rules:
    - type: scan_finding
      branches:
        - main
      scanners:
        - container_scanning
      vulnerabilities_allowed: 1
      severity_levels:
        - critical
      vulnerability_states:
        - newly_detected
  actions:
    - type: require_approval
      approvals_required: 1
      user_approvers:
        - adalberto.dare
```

## 扫描结果策略需要额外批准的示例情况

在几种情况下，扫描结果策略需要额外的批准步骤。例如：

- 工作分支中的安全作业数量减少，并且不再与目标分支中的安全作业数量匹配。用户无法通过从 CI/CD 配置中移除扫描作业来跳过扫描结果策略。仅检查扫描结果策略规则中配置的安全扫描是否删除。

  例如，考虑默认分支流水线有四个安全扫描的情况：`sast`、`secret_detection`、`container_scanning` 和 `dependency_scanning`。扫描结果策略执行两个扫描程序：`container_scanning` 和 `dependency_scanning`。如果 MR 删除在扫描结果策略（例如 `container_scanning`）中配置的扫描，则需要额外的批准。
- 有人停止了流水线安全作业，用户无法跳过安全扫描。
- 合并请求中的作业失败并配置为 `allow_failure: false`，结果流水线处于阻塞状态。
- 流水线有一个手动作业，必须成功运行才能使整个流水线通过。
