---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 扫描执行策略 **(ULTIMATE ALL)**

> - 群组级别的安全策略引入于极狐GitLab 15.2。
> - 群组级别的安全策略在 JihuLab.com 上启用于极狐GitLab 15.4。
> - 操作容器扫描引入于极狐GitLab 15.5。
> - 扫描执行策略编辑器中支持自定义 CI 变量引入于极狐GitLab 16.2。
> - 对具有现有极狐GitLab CI/CD 配置的项目执行扫描执行策略引入于极狐GitLab 16.2，[功能标志](../../../administration/feature_flags.md)为 `scan_execution_policy_pipelines`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，此功能默认启用。如果想禁用此功能，管理员可以禁用名为 `scan_execution_policy_pipelines` 的[功能标志](../../../administration/feature_flags.md)。JihuLab.com 上启用了此功能。

群组、子群组或项目所有者可以使用扫描执行策略来要求安全扫描按指定的计划运行或与项目流水线一起运行。如果您在群组或子群组级别定义策略，安全扫描将与多个项目流水线一起运行。极狐GitLab 将所需的扫描作为新作业注入 CI/CD 流水线中。

对所有适用的项目执行扫描执行策略，即使是那些没有极狐GitLab CI/CD 配置文件或禁用 AutoDevOps 的项目。安全策略隐式创建该文件，以便可以执行策略。这确保了能够执行 secret 检测、静态分析或其他不需要在项目中构建的扫描程序的策略仍然能够被执行。

如果发生作业名称冲突，极狐GitLab 会在作业名称后附加连字符和数字。极狐GitLab 会递增该数字，直到名称不再与现有作业名称冲突。如果您在群组级别创建策略，它将应用于每个子项目或子群组。您无法从子项目或子群组编辑群组级策略。

此功能与[合规框架流水线](../../group/compliance_frameworks.md#compliance-pipelines)有一些重叠，因为我们尚未将这两个功能的用户体验进行统一。<!--有关这些功能之间的异同的详细信息，请参阅[强制扫描执行](../index.md#enforce-scan-execution)。-->

NOTE:
除 DAST 扫描之外的其他扫描的策略作业是在流水线的 `test` 阶段创建的。如果修改默认流水线 [`stages`](../../../ci/yaml/index.md#stages)，以删除 `test` 阶段，作业将在 `scan-policies` 阶段中运行。如果它不存在，则此阶段会在评估时注入到 CI 流水线中。如果 `build` 阶段存在，则会在 `build` 阶段之后注入。如果 `build` 阶段不存在，则会在流水线的开头注入。DAST 扫描始终在 `dast` 阶段运行。如果该阶段不存在，则会在流水线末尾注入一个 `dast` 阶段。

<a id="scan-execution-policy-editor"></a>

## 扫描执行策略编辑器

NOTE:
只有群组、子组或项目所有者拥有选择安全策略项目的权限。

完成策略后，通过选择编辑器底部的 **通过合并请求创建** 来保存。您将被重定向到项目的已配置安全策略项目的合并请求。如果没有关联到您的项目，则会自动创建一个安全策略项目。通过选择编辑器底部的 **删除策略**，也可以从编辑器界面中删除现有策略。

大多数策略更改会在合并请求合并后立即生效。未通过合并请求并直接提交到默认分支的任何更改，可能需要长达 10 分钟才能使策略更改生效。

![Scan Execution Policy Editor YAML Mode](img/scan_execution_policy_rule_mode_v15_11.png)

NOTE:
对站点和使用 DAST 执行策略规则模式编辑器的扫描器配置文件的选择是不同的，具体取决于策略是在项目级别还是群组级别创建的。对于项目级别的策略，规则模式编辑器会显示一个配置文件列表，供您从项目中已定义的配置文件中进行选择。对于群组级别的策略，您需要输入要使用的配置文件的名称，并且为了防止流水线错误，匹配名称的配置文件必须在所有群组的项目中。

<a id="scan-execution-policies-schema"></a>

<a id="scan-execution-policies-schema"></a>

## 扫描执行策略 schema

带有扫描执行策略的 YAML 文件由一组与扫描执行策略模式匹配的对象组成，这些对象嵌套在 `scan_execution_policy` 键下。您可以在 `scan_execution_policy` 键下配置最多 5 个策略。前 5 个之后配置的任何其他策略都不会应用。

当您保存新策略时，系统会根据[此 JSON 模式](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/validators/json_schemas/security_orchestration_policy.json)验证其内容。
如果您不熟悉如何阅读[JSON 模式](https://json-schema.org/)，以下部分和表格提供另一种选择。

| 字段 | 类型 | 是否必需 | 可能的值 | 描述                |
|-------|------|------|-----------------|-------------------|
| `scan_execution_policy` | 扫描执行策略的 `array` | 是    |  | 扫描执行策略列表（最多 5 个）。 |

## 扫描执行策略 schema

| 字段 | 类型 | 是否必需 | 可能的值           | 描述 |
|-------|------|------|----------------|-------------|
| `name` | `string` | 是    |                | 策略名称。最多 255 个字符。 |
| `description`（可选） | `string` | 是    |                | 策略的描述。 |
| `enabled` | `boolean` | 是    | `true`、`false` | 启用 (`true`) 或禁用 (`false`) 策略的标志。 |
| `rules` | 规则的 `array` | 是    |                | 策略应用的规则列表。 |
| `actions` | 操作的 `array` | 是    |                | 策略强制执行的操作列表。 |

## `pipeline` 规则类型

> - `branch_type` 字段引入于极狐GitLab 16.1，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_type`。默认禁用。
> - 普遍可用于极狐GitLab 16.2。移除功能标志 `security_policies_branch_type`。
> - `branch_exceptions` 字段引入于极狐GitLab 16.3，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_exceptions`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `branch_exceptions` 字段可用。要隐藏该功能，管理员可以禁用名为 `security_policies_branch_exceptions` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

每当流水线为选定的分支运行时，此规则就会强制执行定义的操作。

| 字段 | 类型                 | 是否必需                       | 可能的值                          | 描述                |
|-------|--------------------|----------------------------|-------------------------------|-------------------|
| `type` | `string`           | 是                          | `pipeline`                    | 规则的类型。            |
| `branches` <sup>1</sup> | `string` 的 `array` | 如果 `branch_type` 字段不存在，则为"是" | `*` 或分支名称                     | 给定策略适用的分支（支持通配符）。 |
| `branch_type` <sup>1</sup> | `string`           | 如果 `branches` 字段不存在，则为"是"    | `default`、`protected` 或 `all` | 给定策略适用的分支机构类型。    |
| `branch_exceptions` | `string` 的 `array`        | 是                          | 分支名称                          | 此规则不包括的分支。        |

1. 您只能指定 `branches` 和 `branch_type` 中的一个。

## `schedule` 规则类型

> - `branch_type` 字段引入于极狐GitLab 16.1，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_type`。默认禁用。
> - 普遍可用于极狐GitLab 16.2。移除功能标志 `security_policies_branch_type`。
> - `branch_exceptions` 字段引入于极狐GitLab 16.3，[功能标志](../../../administration/feature_flags.md)为 `security_policies_branch_exceptions`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `branch_exceptions` 字段可用。要隐藏该功能，管理员可以禁用名为 `security_policies_branch_exceptions` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

此规则安排扫描流水线，根据 `cadence` 字段中定义的计划强制执行定义的操作。计划的流水线不会运行项目的 `.gitlab-ci.yml` 文件中定义的其他作业。当项目链接到安全策略项目时，会在项目中创建安全策略机器人，并将成为任何计划流水线的作者。

| 字段                         | 类型                 | 是否必需                                      | 可能的值                          | 描述                                                                                                                                                                          |
|----------------------------|--------------------|-------------------------------------------|-------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`                     | `string`           | 是                                         | `schedule`                    | 规则的类型。                                                                                                                                                                      |
| `branches` <sup>1</sup>                | `string` 的 `array` | 如果 `branch_type` 或 `agents` 字段不存在，则为"是"   | `*` 或分支名称                     | 给定策略适用的分支（支持通配符）。如果未设置 `agents` 字段，则此字段是必需的。                                                                                                                                |
| `branch_type` <sup>1</sup> | `string`           | 如果 `branches` 或 `agents` 字段不存在，则为"是"      | `default`、`protected` 或 `all` | 给定策略适用的分支类型。                                                                                                                                                                |
| `branch_exceptions` | `string` 的 `array`        | 否                                         | 分支名称                          | 此规则不包括的分支。                                                                                                                                                                  |
| `cadence`                  | `string`           | 是                                         | CRON 表达式（例如 `0 0 * * *`）      | 一个以空格分隔的字符串，包含五个表示计划时间的字段。                                                                                                                                                  |
| `timezone`                 | `string`           | 否                                         | 时区标识符（例如 `America/New_York`)  | 应用于 cadence 的时区。值必须是 IANA 时区数据库标识符。                                                                                                                                         |
| `agents` <sup>1</sup>      | `object`           | 如果 `branch_type` 或 `branches` 字段不存在，则为"是" |                               | [集群镜像扫描](../../clusters/agent/vulnerabilities.md)将运行的[极狐GitLab 代理](../../clusters/agent/index.md)的名称。对象键是在极狐GitLab 中为您的项目配置的 Kubernetes 代理的名称。如果未设置 `branches` 字段，则此字段是必需的。 |

1. 您必须仅指定 `branches`、`branch_type` 或 `agents` 中的一个。

计划扫描流水线由安全策略机器人用户触发，该用户是项目的访客成员，该项目对 `security_policy_bot` 类型的用户具有升级的权限，因此它可以执行此任务。安全策略机器人用户会在安全策略项目链接时自动创建，并在安全策略项目取消链接时移除。

如果项目没有安全策略机器人用户，则计划扫描流水线由最后修改安全策略项目的用户触发。

极狐GitLab 支持 `cadence` 字段的以下类型的 CRON 语法：

- 在指定时间每小时一次的每日周期，例如：`0 18 * * *`
- 在指定日期和指定时间每周一次的每周周期，例如：`0 13 * * 0`

NOTE:
[CRON 语法](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)的其他元素如果受我们在实现中使用的 [cron](https://github.com/robfig/cron) 支持，则在 cadence 字段中可用。但是，极狐GitLab 并未正式测试或支持。

将 `schedule` 规则类型与 `branches` 字段结合使用时，请注意以下事项：

- 极狐GitLab Agent for Kubernetes 每 30 秒检查一次是否有适用的策略。找到策略后，将根据定义的 `cadence` 执行扫描。
- CRON 表达式使用 Kubernetes-agent pod 的系统时间进行计算。

将 `schedule` 规则类型与 `branches` 字段结合使用时，请注意以下事项：

- cron worker 每隔 15 分钟运行一次，并启动计划在前 15 分钟内运行的所有流水线。
- 根据您的规则，您可能期望计划的流水线以最多 15 分钟的时间偏移运行。
- CRON 表达式以来自极狐GitLab.com 的标准 [UTC](https://www.timeanddate.com/worldclock/timezone/utc) 时间进行评估。如果您有一个私有化部署的极狐GitLab 实例并且[更改了服务器时区](../../../administration/timezone.md)，则 CRON 表达式将使用新时区进行评估。

![CRON worker diagram](img/scheduled_scan_execution_policies_diagram.png)

### `agent` schema

使用此 schema，在 [`schedule` 规则类型](#schedule-rule-type)中定义 `agents` 对象。

| 字段 | 类型          | 是否必需 | 可能的值 | 描述 |
|--------------|-------------|------|--------------------------|-------------|
| `namespaces` | `string` 的 `array` | 是    | | 被扫描的命名空间。如果为空，将扫描所有命名空间。 |

#### 策略示例

```yaml
- name: Enforce Container Scanning in cluster connected through my-gitlab-agent for default and kube-system namespaces
  enabled: true
  rules:
    - type: schedule
      cadence: '0 10 * * *'
      agents:
        <agent-name>:
          namespaces:
            - 'default'
            - 'kube-system'
  actions:
    - scan: container_scanning
```

调度规则的 key 为：

- `cadence`（必需）：一个 [CRON 表达式](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)，表示何时运行扫描。
- `agents:<agent-name>`（必需）：用于扫描的代理名称。
- `agents:<agent-name>:namespaces`（可选）：要扫描的 Kubernetes 命名空间。如果省略，将扫描所有命名空间。

## `scan` 操作类型

当满足已定义策略中至少一条规则的条件时，此操作会使用附加参数执行选定的 `scan`。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `scan` | `string` | `dast`、`secret_detection`、`sast`、`container_scanning`、`dependency_scanning` | 操作的类型。 |
| `site_profile` | `string` | 所选 DAST 站点配置文件<!--[DAST 站点配置文件](../dast/index.md#site-profile)-->的名称。 | 用于执行 DAST 扫描的 DAST 站点配置文件。仅当 `scan` 类型为 `dast` 时才应设置此字段。 |
| `scanner_profile` | `string` 或 `null` | 所选 DAST 扫描器配置文件<!--[DAST 扫描器配置文件](../dast/index.md#scanner-profile)-->的名称。 | 用于执行 DAST 扫描的 DAST 扫描仪配置文件。仅当 `scan` 类型为 `dast` 时才应设置此字段。 |
| `variables` | `object` | | 一组 CI 变量，以 `key: value` 对数组的形式提供，用于应用和执行选定的扫描。`key` 是变量名，其 `value` 以字符串形式提供。此参数支持 GitLab CI 作业支持指定扫描的任何变量。 |
| `tags` | `string` 形式的 `array`  | | 该策略的 runner 标签列表。策略作业将由具有指定标签的 runner 运行。 |

注意：

- 您必须为分配的每个项目创建具有选定名称的站点配置文件<!--[站点配置文件](../dast/index.md#site-profile)-->和扫描器配置文件<!--[扫描器配置文件](../dast/index.md#scanner-profile)-->到选定的安全策略项目。否则，不会应用该策略，而是创建带有错误消息的作业。
- 一旦您在策略中按名称关联站点配置文件和扫描器配置文件，就无法修改或删除它们。如果要修改它们，必须首先通过将 `active` 标志设置为 `false` 来禁用策略。
- 使用计划的 DAST 扫描配置策略时，安全策略项目的仓库中提交的作者必须有权访问扫描程序和站点配置文件。否则，扫描不会成功调度。
- 对于 secret 检测扫描，仅支持具有默认规则集的规则。不支持自定义规则集<!--[自定义规则集](../secret_detection/index.md#custom-rulesets)-->。
- Secret 检测扫描在作为流水线的一部分执行时以 `default` 模式运行，当作为计划扫描的一部分执行时，以 `history` 模式运行。
- 为 `pipeline` 规则类型配置的容器扫描和集群镜像扫描会忽略 `agents` 对象中定义的集群，`agents` 对象仅适用于 `schedule` 规则类型。 必须为项目创建和配置具有在 `agents` 对象中提供的名称的代理。
- 扫描执行策略中定义的变量遵循标准的 [CI/CD 变量优先级](../../../ci/variables/index.md#cicd-variable-precedence)。

<a id="example-security-policies-project"></a>

## 示例安全策略项目

您可以在 `.gitlab/security-policies/policy.yml` 中使用此示例，如[安全策略项目](index.md#security-policy-project)中所述。

```yaml
---
scan_execution_policy:
  - name: Enforce DAST in every release pipeline
    description: This policy enforces pipeline configuration to have a job with DAST scan for release branches
    enabled: true
    rules:
      - type: pipeline
        branches:
          - release/*
    actions:
      - scan: dast
        scanner_profile: Scanner Profile A
        site_profile: Site Profile B
  - name: Enforce DAST and secret detection scans every 10 minutes
    description: This policy enforces DAST and secret detection scans to run every 10 minutes
    enabled: true
    rules:
      - type: schedule
        branches:
          - main
        cadence: "*/10 * * * *"
    actions:
      - scan: dast
        scanner_profile: Scanner Profile C
        site_profile: Site Profile D
      - scan: secret_detection
  - name: Enforce Secret Detection and Container Scanning in every default branch pipeline
    description: This policy enforces pipeline configuration to have a job with Secret Detection and Container Scanning scans for the default branch
    enabled: true
    rules:
      - type: pipeline
        branches:
          - main
    actions:
      - scan: secret_detection
      - scan: sast
        variables:
          SAST_EXCLUDED_ANALYZERS: brakeman
      - scan: container_scanning
```

在此示例中：

- 对于在匹配 `release/*` 通配符的分支上执行的每个流水线（例如，分支 `release/v1.2.1`），DAST 扫描使用 `Scanner Profile A` 和 `Site Profile B` 运行。
- DAST 和 secret 检测扫描每 10 分钟运行一次。DAST 扫描使用 `Scanner Profile C` 和 `Site Profile D`。
- Secret 检测、容器扫描和 SAST 扫描针对在 `main` 分支上执行的每个流水线运行。SAST 扫描运行时将 `SAST_EXCLUDED_ANALYZER` 变量设置为 `"brakeman"`。

## 扫描执行策略编辑器示例

您可以在[扫描执行策略编辑器](#scan-execution-policy-editor)的 YAML 模式下使用此示例。它对应于上一个示例中的单个对象。

```yaml
name: Enforce Secret Detection and Container Scanning in every default branch pipeline
description: This policy enforces pipeline configuration to have a job with Secret Detection and Container Scanning scans for the default branch
enabled: true
rules:
  - type: pipeline
    branches:
      - main
actions:
  - scan: secret_detection
  - scan: container_scanning
```

## 避免重复扫描

如果开发人员在项目的 `.gitlab-ci.yml` 文件中包含扫描作业，则扫描执行策略可能会导致同一类型的扫描器运行多次。此行为是有意为之，因为扫描仪可以使用不同的变量和设置运行多次。例如，开发人员可能希望尝试使用与安全和合规团队执行的变量不同的变量来运行 SAST 扫描。
在这种情况下，流水线中运行两个 SAST 作业，其中一个包含开发人员的变量，另一个包含安全与合规团队的变量。

如果您想避免运行重复扫描，可以从项目的 `.gitlab-ci.yml` 文件中删除扫描，或者通过设置 `SAST_DISABLED: "true"` 来禁用本地作业。以这种方式禁用作业不会阻止扫描执行策略定义的安全作业运行。
