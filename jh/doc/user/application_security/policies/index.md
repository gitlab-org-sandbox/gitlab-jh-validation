---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 策略 **(ULTIMATE ALL)**

> - 引入于 13.10 版本，功能标志为 `security_orchestration_policies_configuration`。默认禁用。
> - 在私有化部署版中启用于 14.3 版本。
> - 功能标志移除于 14.4 版本。

极狐GitLab 中的策略可以为安全团队提供一种方法，要求在项目流水线根据指定的配置运行时，运行他们选择的扫描。因此，安全团队可以确信他们设置的扫描没有被更改、替换或禁用。您可以通过导航到项目的 **安全 > 策略** 页面来访问这些内容。

极狐GitLab 支持以下安全策略：

- [扫描执行策略](scan-execution-policies.md)
- [扫描结果策略](scan-result-policies.md)

<a id="security-policy-project"></a>

## 安全策略项目

所有安全策略都作为 YAML 存储在一个单独的安全策略项目中，该项目关联到开发项目。这种关联可以是一对多的关系，允许一个安全策略项目应用于多个开发项目、群组或子组：

- 对于私有化部署版实例，关联的项目不需要与它们关联到的开发项目位于同一群组或同一子组中。
- 对于 SaaS，安全策略项目需要与开发项目在同一个顶级群组中，但是，项目不必在同一个子组中。

![Security Policy Project Linking Diagram](img/association_diagram.png)

尽管可以将一个项目关联到自身并同时作为开发项目和安全策略项目，但不建议这样做。将安全策略项目与开发项目分开，允许安全/合规团队和开发团队之间的职责完全分离。

所有安全策略都存储在关联的安全策略项目内的 `.gitlab/security-policies/policy.yml` YAML 文件中。此 YAML 的格式特定于存储在那里的策略类型。示例和架构信息可用于以下策略类型：

- [扫描执行策略](scan-execution-policies.md#example-security-policies-project)
- [扫描结果策略](scan-result-policies.md#example-security-scan-result-policies-project)

大多数策略更改会在合并请求合并后立即生效。未通过合并请求并直接提交到默认分支的任何更改，可能需要长达 10 分钟才能使策略更改生效。

### 管理关联的安全策略项目

NOTE:
只有项目所有者有权选择、编辑和取消关联安全策略项目。

作为项目所有者，请执行以下步骤来创建或编辑当前项目与您希望指定为安全策略项目的项目之间的关联：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **安全 > 策略**。
1. 选择 **编辑策略项目**，然后从下拉菜单中搜索并选择您要关联的项目。
1. 选择 **保存**。

要取消关联安全策略项目，请遵循相同的步骤，但选择垃圾桶图标。

![Security Policy Project](img/security_policy_project_v14_6.png)

### 查看关联的安全策略项目

所有有权访问项目策略页面但不是项目所有者的用户，将改为查看链接到相关安全策略项目的按钮。如果没有关联安全策略项目，则不会出现链接按钮。

## 策略管理

策略页面显示所有可用环境的已部署策略。您可以检查策略的信息（例如，描述或执行状态），并创建和编辑部署的策略：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **安全 > 策略**。

![Policies List Page](img/policies_list_v15_1.png)

<a id="policy-editor"></a>

## 策略编辑器

> 引入于极狐GitLab 13.4。

您可以使用策略编辑器来创建、编辑和删除策略：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的群组。
1. 选择 **安全 > 策略**。
    - 要创建新策略，请选择位于 **策略** 页面上方的 **新建策略**。您可以选择要创建的策略类型。
    - 要编辑现有策略，请在所选策略菜单中选择 **编辑策略**。

策略编辑器有两种模式：

- 可视*规则*模式允许您使用规则块和相关控件构建和预览策略规则。

  ![Policy Editor Rule Mode](img/policy_rule_mode_v15_9.png)

- YAML 模式允许您以 `.yaml` 格式输入策略定义，并且针对规则模式不支持的专家用户和案例。

  ![Policy Editor YAML Mode](img/policy_yaml_mode_v15_9.png)

您可以交替使用这两种模式并随时在它们之间切换。如果 YAML 资源不正确或包含规则模式不支持的数据，则会自动禁用规则模式。如果 YAML 不正确，您必须使用 YAML 模式修复您的策略，然后才能再次使用规则模式。

完成创建或编辑策略后，选择 **使用合并请求配置** 按钮保存并应用它，然后合并生成的合并请求。当您按下此按钮时，将验证策略 YAML 并显示任何产生的错误。
此外，如果您是项目所有者并且安全策略项目之前未与此项目关联，则在创建第一个策略合并请求的同时会自动创建和关联一个新项目。

## 通过脚本批量管理项目

您可以使用"漏洞检查迁移"脚本批量创建策略或关联安全策略项目和开发项目。

## 故障排除

### `Branch name 'update-policy-<timestamp>' does not follow the pattern '<branch_name_regex>'`

当您创建新的安全策略或更改现有策略时，会自动创建一个新分支，其分支名称遵循 `update-policy-<timestamp>`。例如：`update-policy-1659094451`。

如果您的群组或实例推送规则不允许包含文本 `update-policy-<timestamp>` 的分支名称形式，您将收到一条错误消息：`Branch name does not follow the pattern 'update-policy-<timestamp>'`。

解决方法是修改您的群组或实例推送规则，以允许分支遵循 `update-policy-` 后跟整数时间戳的形式。

### 对配置安全策略的常见问题进行故障排除

- 确认扫描器已正确配置并为最新分支生成结果。安全策略目标为在没有结果（没有安全报告）时要求批准，因为这确保不会引入漏洞。除非策略强制执行的扫描成功完成并经过评估，否则我们无法知道是否存在任何漏洞。
- 对于扫描结果策略，我们需要源分支和目标分支的策略中定义的每个扫描器的产物。为了确保扫描结果策略得到必要的结果，请确认已正确实施和执行了扫描。如果使用扫描执行策略，则在 `all branches` 上强制执行通常可以满足此需求。
- 在基于 SAST 操作运行扫描执行策略时，确保目标仓库包含正确的代码文件。SAST [基于 repo 中的文件类型](../sast/index.md#supported-languages-and-frameworks)运行不同的分析器，如果未找到受支持的文件，它将不会运行任何作业。有关详细信息，请参阅 [SAST CI 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml)。
- 检查任何分支配置冲突。如果您的策略配置为在 `main` 上强制执行规则，但范围内的某些项目正在使用 `master` 作为它们的默认分支，则该策略不适用于后者。您可以定义策略，以在 `default` 分支上普遍执行规则，不管此项目中或 `all protected branches` 上使用的名称以解决此问题。
- 在群组或子组级别创建的扫描结果策略可能需要一些时间，才能应用于群组中的所有合并请求。
- 计划扫描执行策略以至少 15 分钟的周期运行。了解更多[关于计划规则类型](../policies/scan-execution-policies.md#schedule-rule-type)的类型。
- 在计划流水线时，请记住 CRON 计划基于 SaaS 上的 UTC，并且基于您私有化部署实例的服务器时间。测试新策略时，流水线可能看起来运行不正常，而实际上它们是在您服务器的时区内安排的。
- 在进行扫描执行策略时，安全策略会在目标项目中创建机器人，以触发计划流水线，确保策略执行。如果机器人被删除或丢失，则不会执行目标项目的流水线。要重新创建安全策略机器人用户，请先取消关联旧的机器人，再把新的机器人关联到安全策略项目中。
- 您不应将安全策略项目关联到开发项目，以及开发项目所属的群组或子组。以这种方式关联，将导致扫描结果策略中的批准规则不适用于开发项目中的合并请求。
- 创建扫描结果策略时，[扫描发现规则](../policies/scan-result-policies.md#scan_finding-rule-type)中的 `severity_levels` 和 `vulnerability_states` 都不可置空。对于生效的规则，必须至少存在一个条目。
- 配置流水线和扫描结果策略时，很重要的一件事是要记住手动作业中的安全扫描没有进行验证，以判断是否需要 MR 批准。当您运行带有安全扫描的手动作业时，即使引入了漏洞也不一定会有批准。
<!--
If you are still experiencing issues, you can [view recent reported bugs](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Asecurity%20policies&label_name%5B%5D=type%3A%3Abug&first_page_size=20) and raise new unreported issues.
-->
