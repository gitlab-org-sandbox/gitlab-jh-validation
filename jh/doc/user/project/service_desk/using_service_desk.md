---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用服务台 **(FREE ALL)**

您可以使用服务台[创建议题](#as-an-end-user-issue-creator)或[回复议题](#as-a-responder-to-the-issue)。
<!--In these issues, you can also see our friendly neighborhood [Support Bot](configure.md#support-bot-user).-->

## 查看服务台电子邮件地址

要检查您的项目的服务台电子邮件地址：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **监控 > 服务台**。

电子邮件地址位于议题列表的顶部。

<a id="as-an-end-user-issue-creator"></a>

## 作为终端用户（议题创建者）

> 对额外电子邮件 header 的支持引入于极狐GitLab 14.6。在更早的版本中，服务台电子邮件地址必须在 "到" 字段中。

要创建服务台议题，终端用户不需要了解极狐GitLab 实例的任何信息。他们只需向指定地址发送一封电子邮件，然后收到一封确认收到的电子邮件：

![Service Desk enabled](img/service_desk_confirmation_email.png)

这也为终端用户提供了取消订阅的选项。

如果他们不选择取消订阅，则添加到该议题的任何新评论都会作为电子邮件发送：

![Service Desk reply email](img/service_desk_reply.png)

他们通过电子邮件发送的任何回复都会显示在议题中。

有关用于处理电子邮件的 header 的信息，请参阅[接收电子邮件文档](../../../administration/incoming_email.md#accepted-headers)。

<a id="as-a-responder-to-the-issue"></a>

## 作为议题响应者

对于该议题的响应者来说，一切都与其他极狐GitLab 议题一样。
极狐GitLab 显示了一个看起来熟悉的议题跟踪器，响应者可以在其中查看通过客户支持请求创建的议题，并进行过滤或与之交互。

![Service Desk Issue tracker](img/service_desk_issue_tracker.png)

来自终端用户的消息显示为来自特殊的 [Support Bot 用户](../../../subscriptions/self_management/index.md#billable-users)。
您可以像在极狐GitLab 中一样读取和编写评论：

![Service Desk issue thread](img/service_desk_thread.png)

- 项目的可见性（私有、内部、公共）不会影响服务台。
- 项目的路径（包括其群组或命名空间）显示在电子邮件中。

### 查看服务台议题

先决条件：

- 您必须至少具有该项目的报告者角色。

要查看服务台议题：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **监控 > 服务台**。

#### 重新设计的议题列表

> - 引入于极狐GitLab 16.1，[功能标志](../../../administration/feature_flags.md)为 `service_desk_vue_list`。默认禁用。
> - 在 JihuLab.com 和私有化部署版本上启用于极狐GitLab 16.5。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。要为每个项目或整个实例隐藏该功能，管理员可以禁用名为 `service_desk_vue_list` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，可以使用此功能。

启用此功能后，服务台议题列表与常规议题列表更加匹配。
可用的功能包括：

- [与议题列表](../issues/sorting_issue_lists.md)相同的排序和排序选项。
- 相同的过滤器，包括 [OR 运算符](#filter-with-the-or-operator)和[按议题 ID 过滤](#filter-issues-by-id)。

不再可以从服务台议题列表中创建新议题。
这一决定更好地反映了服务台的本质，即通过向专用电子邮件地址发送电子邮件来创建新议题。

<a id="filter-the-list-of-issues"></a>

##### 过滤议题列表

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **监控 > 服务台**。
1. 在议题列表上方，选择 **搜索或筛选结果...**。
1. 在出现的下拉列表中，选择要作为过滤依据的属性。
1. 选择或键入用于过滤属性的运算符。可用运算符为：
    - `=`: 是
    - `!=`：不是其中之一
1. 输入用于过滤属性的文本。
   您可以通过 **无** 或 **任何** 来过滤某些属性。
1. 重复此过程以按多个属性进行过滤。多个属性通过逻辑 `AND` 进行连接。

<a id="filter-with-the-or-operator"></a>

##### 使用 OR 运算符过滤

当启用使用 OR 运算符过滤时，您可以在使用以下属性[过滤议题列表](#filter-the-list-of-issues)时使用 **是其中之一 `||`**：

- 指派人
- 标签

`is one of` 表示包含 OR。例如，如果您按 `Assignee is one of Sidney Jones` 和 `Assignee is one of Zhang Wei` 进行过滤，极狐GitLab 会显示议题，其中 `Sidney` 或 `Zhang` 或二者都是指派人。

<a id="filter-issues-by-id"></a>

##### 按照 ID 过滤议题

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **监控 > 服务台**。
1. 在 **搜索** 框中，输入议题 ID。例如，输入 `#10` 以返回议题 10。

## 电子邮件内容和格式

### HTML 邮件中的特殊 HTML 格式

> - 引入于极狐GitLab 15.9，[功能标志](../../../administration/feature_flags.md)为 `service_desk_html_to_text_email_handler`。默认禁用。
> - 普遍可用于极狐GitLab 15.11。移除 `service_desk_html_to_text_email_handler` 功能标志。

HTML 电子邮件显示 HTML 格式，例如：

- 表格
- 块引用
- 图片
- 可折叠部分

### 附到内容中的文件

> - 引入于极狐GitLab 15.8，[功能标志](../../../administration/feature_flags.md)为 `service_desk_new_note_email_native_attachments`。默认禁用。
> - 在 JihuLab.com 和私有化部署版中启用于极狐GitLab 15.10。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。要为每个项目或整个实例隐藏该功能，管理员可以禁用名为 `service_desk_new_note_email_native_attachments` 的[功能标志](../../../administration/feature_flags.md)。
在 JihuLab.com 上，可以使用此功能。

如果评论包含任何附件且其总大小小于或等于 10 MB，则这些附件将作为电子邮件的一部分发送。在其他情况下，电子邮件包含附件链接。

在 15.9 及更早版本中，上传到评论的内容会作为电子邮件中的链接进行发送。

## 隐私考虑

> 更改查看创建者和参与者电子邮件的最低必须角色于极狐GitLab 15.9。

服务台议题是[私密](../issues/confidential_issues.md)的，因此它们仅对项目成员可见。项目所有者可[将议题设置为公开。
当服务台议题公开时，至少具有项目报告者角色的登录用户可以看到议题创建者和参与者的电子邮件地址。

在 15.8 及更早版本中，当服务台议题公开时，议题创建者的电子邮件地址会向所有可以查看该项目的人公开。

您项目中的任何人都可以使用服务台电子邮件地址在此项目中创建议题，**无论其在项目中的角色**。

唯一的内部电子邮件地址对至少具有极狐GitLab 实例的报告者角色的项目成员可见。
外部用户（议题创建者）无法看到信息说明中显示的内部电子邮件地址。

### 移动服务台议题

> 更改于极狐GitLab 15.7：移动服务台议题时，客户会继续收到通知

您可以像在极狐GitLab 中[移动常规议题](../issues/managing_issues.md#move-an-issue)一样移动服务台议题。

如果服务台议题转移到启用了服务台的其他项目，则创建该议题的客户将继续收到电子邮件通知。
由于移动的议题首先被关闭，然后被复制，因此客户被视为两个议题的参与者。他们将继续收到旧议题和新议题中的任何通知。
