---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
description: "自定义页面。"
---

# 自定义页面 **(PREMIUM ALL)**
> 引入于 17.2 版本。功能标志为 `ff_mr_custom_page_tab`，默认禁用。

自定义页面以 [iframe](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe) 方式嵌入到合并请求页面中。您可以启用这一功能，以在合并请求页面中显示自定义内容，例如您的自研平台。

![自定义页面](img/custom_page_example.png)

## 启用自定义页面

1. 开启功能标志，并配置[极狐独立数据库](https://docs.gitlab.cn/jh/install/jh_database.html)
2. [配置内容安全策略（CSP）](#配置内容安全策略csp)
3. 打开您要使用的群组，进入 **设置 > 通用 > 合并请求 > 合并请求自定义页面**
4. 填写[自定义页面的标题和 URL](#自定义页面-url)

![自定义页面配置](img/custom_page_settings.png)

#### 配置内容安全策略（CSP）

请按照极狐GitLab 的不同安装方式进行配置。

**Linux 软件包安装（Omnibus）：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['content_security_policy']['directives']['frame_src'] = 'https://example.com/'
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Docker：**

1. 编辑 `docker-compose.yml` 并更改 `gitlab_rails` 的值：

   ```yaml
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['content_security_policy']['directives']['frame_src'] = 'https://example.com/'
   ```

2. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   production: &base
      gitlab:
         content_security_policy:
            directives:
               frame_src: "https://example.com/"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

#### 自定义页面 URL

你可以在 URL 中使用以下关键字，它们会被动态替换为实际值：

| 关键字       | 含义  |
|-------------|----------|
| $MERGE_REQUEST_IID | 合并请求的 [IID](https://docs.gitlab.cn/jh/api/rest/#id-vs-iid) |
| $PROJECT_ID        | 项目的 ID |
| $USER_ID           | 访问合并请求的用户的 ID |
| $USERNAME          | 访问合并请求的用户的 USERNAME  |
