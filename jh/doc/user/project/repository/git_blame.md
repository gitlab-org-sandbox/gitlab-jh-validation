---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
description: "Documentation on Git file blame."
---

# Git 文件 blame **(BASIC ALL)**

[Git Blame](https://git-scm.com/docs/git-blame) 提供了关于文件中每一行的更多信息，包括最后修改时间、作者和提交哈希。 要查看文件：

1. 转到您项目的 **代码 > 仓库**。
1. 选择您要查看的文件。
1. 在右上角，选择 **Blame**。

当您选择 **Blame** 时，会显示以下信息：

![Git blame output](img/file_blame_output_v12_6.png "Blame button output")

如果您将鼠标悬停在 UI 中的某个提交上，则会显示该提交的准确日期和时间。

## Blame 以前的提交

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19299) in GitLab 12.7.
-->

要查看特定行的早期修订，请单击**查看此变更前的 blame 模式**，直到找到您有兴趣查看的更改：

![Blame previous commit](img/file_blame_previous_commit_v12_7.png "Blame previous commit")

## 关联的 `git` 命令

如果您从命令行运行 `git`，等效的命令是 `git blame <filename>`。例如，如果要查找本地目录中的 `README.md` 文件的 `blame` 信息，请运行以下命令：

```shell
git blame README.md
```

输出类似于以下内容，其中包括 UTC 格式的提交时间：

```shell
62e2353a (Achilleas Pipinellis     2019-07-11 14:52:18 +0300   1) [![build status](https://gitlab.com/gitlab-org/gitlab-docs/badges/master/build.svg)](https://gitlab.com/gitlab-com/gitlab-docs/commits/master)
fb0fc7d6 (Achilleas Pipinellis     2016-11-07 22:21:22 +0100   2)
^764ca75 (Connor Shea              2016-10-05 23:40:24 -0600   3) # GitLab Documentation
^764ca75 (Connor Shea              2016-10-05 23:40:24 -0600   4)
0e62ed6d (Mike Jang                2019-11-26 21:44:53 +0000   5) This project hosts the repository used to generate the GitLab
0e62ed6d (Mike Jang                2019-11-26 21:44:53 +0000   6) documentation website and deployed to https://docs.gitlab.com. It uses the
```

<!--
## File blame through the API

You can also get this information over the [Git file blame REST API](../../../api/repository_files.md#get-file-blame-from-repository).
-->
