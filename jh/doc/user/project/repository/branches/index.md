---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 分支 **(BASIC ALL)**

分支是项目工作树的版本。当您创建一个新的[项目](../../index.md)时，极狐GitLab 会为您的代码仓库创建一个[默认分支](default.md)（无法删除）。您可以在项目、子组、群组或实例级别配置默认分支设置。

随着项目的发展，您的团队会[创建](../web_editor.md#create-a-branch)更多分支，您最好遵循[分支命名模式](#prefix-branch-names-with-issue-numbers)。
每个分支代表一组更改，允许并行完成开发工作。一个分支的开发工作不会影响另一个分支。

分支是项目发展的基础：

1. 首先，创建一个分支并向其添加提交。
1. 当工作准备好接受审核时，创建一个[合并请求](../../merge_requests/index.md)，提议合并您分支中的更改。要简化此过程，您应该遵循[分支命名模式](#prefix-branch-names-with-issue-numbers)。
1. 使用 [review app](../../../../ci/review_apps/index.md) 预览分支中的更改。
1. 在你的分支内容合并后，[删除合并的分支](#delete-merged-branches)。

## 创建分支

先决条件：

- 您必须至少拥有项目的开发者角色。

在 UI 中创建一个新分支：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **代码 > 分支**。
1. 在右上角，选择 **新建分支**。
1. 输入一个 **分支名称**。
1. 在 **创建自** 中，选择分支的基础：现有分支、现有标签或提交 SHA。
1. 选择 **创建分支**。

### 在空白项目中创建

[空白项目](../../index.md#create-a-blank-project)不包含分支，但您可以添加分支。

先决条件：

- 您必须至少具有项目的开发者角色。
- 除非您具有维护者或所有者角色，否则[默认分支保护](../../../group/manage.md#change-the-default-branch-protection-of-a-group)必须设置为 `Partially protected` 或 `Not protected`，才能将提交推送到默认分支。

添加一个[默认分支](default.md)到一个空项目：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 滚动到 **该项目仓库当前为空** 并选择要添加的文件类型。
1. 在 Web IDE 中，对该文件进行任何所需的更改，然后选择 **创建提交**。
1. 输入提交消息，然后选择 **提交**。

极狐GitLab 创建一个默认分支并将您的文件添加到其中。

### 从议题创建

先决条件：

- 您必须至少具有项目的开发者角色。

查看议题时，您可以直接从该页面创建关联的分支。
以这种方式创建的分支使用[议题中分支名称的默认模式](#configure-default-pattern-for-branch-names-from-issues)，包括变量。

先决条件：

- 您必须在项目中具有开发者或更高的角色。

从议题创建分支：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **计划 > 议题** 并找到您的议题。
1. 在议题描述下方，找到 **创建合并请求** 下拉列表，然后选择 **{chevron-down}** 显示下拉列表。
1. 选择 **创建分支**。根据此项目的[默认样式](#configure-default-pattern-for-branch-names-from-issues)，提供默认的 **分支名称**。如果需要，您可以输入不同的 **分支名称**。
1. 选择 **创建分支**，根据您项目的[默认分支](default.md)创建分支。

## 管理和保护分支

极狐GitLab 为您提供多种方法来保护各个分支。这些方法确保您的分支从创建到删除都接受监督和质量检查：

- 项目中的[默认分支](default.md)获得额外保护。
- 配置[受保护的分支](../../protected_branches.md#protected-branches)来限制谁可以提交到一个分支，将其他分支合并到其中，或者将分支本身合并到另一个分支。
- 配置[批准规则](../../merge_requests/approvals/rules.md)来设置审核要求，包括[安全相关批准](../../merge_requests/approvals/rules.md#security-approvals)，然后分支才能合并。
- 与第三方[状态检查](../../merge_requests/status_checks.md)集成，可以确保您的分支内容符合您的质量标准。

您可以通过以下方式管理您的分支：

- 使用极狐GitLab 用户界面。
- 使用[命令行](../../../../gitlab-basics/start-using-git.md#create-a-branch)。
- 使用[分支 API](../../../../api/branches.md)。

### 查看所有分支

在极狐GitLab 用户界面中查看和管理您的分支：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **代码 > 分支**。

在此页面上，您可以：

- 查看所有分支、活动分支或陈旧分支。
- 创建新分支。
- [比较分支](#compare-branches)。
- 删除合并的分支。

### 查看具有保护配置的分支

> - 引入于 15.1 版本，功能标志为 `branch_rules`。默认禁用。
> - 在 SaaS 上启用于 15.10 版本。
> - 在私有化部署版上启用于 15.11 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需请求管理员[禁用功能分支](../../../feature_flags.md) `branch_rules`。在 SaaS 版上，此功能可用。

仓库中的分支可以通过多种方式[受保护](../../protected_branches.md)。您可以：

- 限制谁可以推送到分支。
- 限制谁可以合并分支。
- 需要批准所有更改。
- 需要外部测试才能通过。

**分支规则概览**页面显示了具有任何保护配置的所有分支及其保护方法：

![Example of a branch with configured protections](img/view_branch_protections_v15_10.png)

先决条件：

- 您必须至少在项目中具有维护者角色。

查看 **分支规则概览** 列表：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 仓库**。
1. 展开 **分支规则** 查看所有受保护的分支。
   - 为新分支添加保护：
     1. 选择 **添加分支规则**。
     1. 选择 **创建受保护的分支**。
   - 要查看有关现有分支保护的更多信息：
     1. 确定您想要了解更多信息的分支。
     1. 选择 **查看详情** 查看有关信息：
        - [分支保护](../../protected_branches.md)。
        - [批准规则](../../merge_requests/approvals/rules.md)。
        - [状态检查](../../merge_requests/status_checks.md)。

## 命名您的分支

Git 执行[分支名称规则](https://git-scm.com/docs/git-check-ref-format)，以帮助确保分支名称与其他工具保持兼容。极狐GitLab 添加了对分支名称的额外要求，并为结构良好的分支名称提供了益处。

极狐GitLab 在所有分支上执行这些附加规则：

- 分支名称中不允许有空格。
- 禁止使用 40 个十六进制字符的分支名称，因为它们类似于 Git 提交哈希值。

常见的软件包，如 Docker，可以执行[额外的分支命名限制](../../../../administration/packages/container_registry.md#docker-connection-error)。

为了与其他软件包获得最佳兼容性，请仅使用：

- 数字
- 连字符 (`-`)
- 下划线（`_`）
- ASCII 标准表中的小写字母

您可以在分支名称中使用正斜杠（`/`）和表情符号，但不能保证与其他软件包的兼容性。

具有特定格式的分支名称具有额外的好处：

- 通过[在分支名称前添加议题编号](#prefix-branch-names-with-issue-numbers)来简化合并请求工作流程。
- 根据分支名称自动执行[分支保护](../../protected_branches.md)。
- 在将分支推送到极狐GitLab 之前，使用[推送规则](../push_rules.md)测试分支名称。
- 定义要在合并请求上运行的 [CI/CD 作业](../../../../ci/jobs/index.md)。

<a id="configure-default-pattern-for-branch-names-from-issues"></a>

### 为来自议题的分支名称配置默认样式

默认情况下，极狐GitLab 在从议题创建分支时使用样式 `%{id}-%{title}`，但您可以更改此样式。

先决条件：

- 您必须至少具有该项目的维护者角色。

要更改从议题创建的分支的默认样式：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 仓库** 并展开 **分支默认值**。
1. 滚动到 **分支名称模板** 并输入一个值。该字段支持这些变量：
   - `%{id}`：议题的数字 ID。
   - `%{title}`：议题的标题，修改为仅使用 Git 分支名称中可接受的字符。
1. 选择 **保存更改**。

<a id="prefix-branch-names-with-issue-numbers"></a>

### 在分支名称前加上议题编号

要简化合并请求的创建，Git 分支名称请以议题编号开头，后跟连字符。
例如，要将分支和议题 `#123` 关联，分支名称请以 `123-` 开头。

议题和分支必须在同一个项目中。

极狐GitLab 使用议题编号将数据导入到合并请求中：

- 议题被标记为与合并请求相关。议题和合并请求显示彼此的链接。
- 分支与议题关联。
- 如果您的项目配置了[默认关闭模式](../../issues/managing_issues.md#default-closing-pattern)，则合并合并请求[也会关闭](../../issues/managing_issues.md#closing-issues-automatically)相关议题。
- 议题里程碑和标签被复制到合并请求。

## 比较分支

要比较仓库中的分支：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **代码 > 比较版本**。
1. 选择 **源** 分支来搜索您所需的分支。首先显示精确匹配。您可以使用运算符优化搜索：
    - `^` 匹配分支名称的开头：`^feat` 匹配 `feat/user-authentication`。
    - `$` 匹配分支名称的末尾：`widget$` 匹配 `feat/search-box-widget`。
    - `*` 使用通配符进行匹配：`branch*cache*` 匹配 `fix/branch-search-cache-expiration`。
    - 您可以组合运算符：`^chore/*migration$` 匹配 `chore/user-data-migration`。
1. 选择 **目标** 仓库和分支。首先显示精确匹配。
1. 在 **显示更改**下方，选择比较分支的方法：
   <!-- vale gitlab.SubstitutionWarning = NO -->
   <!-- Disable Vale so it doesn't flag "since" -->
    - **仅来自源的传入更改**（默认）显示两个分支上自最近共同提交以来与源分支的差异。
      不包括创建源分支后对目标分支所做的不相关更改。
      此方法使用 `git diff <from>...<to>` [Git 命令](https://git-scm.com/docs/git-diff#Documentation/git-diff.txt-emgitdiffemltoptionsgtltcommitgtltcommitgt--ltpathgt82308203-1)。
      为了比较分支，此方法使用合并基准而不是实际提交，因此拣选提交的更改显示为新更改。
    - **包括自创建源以来对目标的更改** 显示两个分支之间的所有差异。
      此方法使用 `git diff <from> <to>` [Git 命令](https://git-scm.com/docs/git-diff#Documentation/git-diff.txt-emgitdiffemltoptionsgt--merge-baseltcommitgtltcommitgt--ltpathgt82308203)。
   <!-- vale gitlab.SubstitutionWarning = YES -->
1. 选择 **比较** 以显示提交和更改的文件列表。
1. 可选。要回滚 **源** 和 **目标**，请选择 **交换版本** (**{substitute}**)。

## 删除合并的分支

如果合并的分支满足所有这些条件，则可以批量删除它们：

- 它们不是[受保护的分支](../../protected_branches.md)。
- 它们已合并到项目的默认分支中。

先决条件：

- 您必须至少具有该项目的开发者角色。

删除合并的分支：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **代码 > 分支**。
1. 在页面右上角，选择 **更多** **{ellipsis_v}**。
1. 选择 **删除合并的分支**。
1. 在对话框中输入 `delete` 进行确认，然后选择 **删除合并的分支**。

## 为目标分支配置规则 **(PREMIUM ALL)**

> 引入于极狐GitLab 16.4，[功能标志](../../../../administration/feature_flags.md)为 `target_branch_rules_flag`。默认禁用。

有些项目使用多个长期分支进行开发，例如 `develop` 和 `qa`。
在这些项目中，您可能希望将 `main` 保留为默认分支，但期望合并请求以 `develop` 或 `qa` 为目标分支。目标分支规则有助于确保合并请求为您的项目以合适的开发分支作为目标分支。

当您创建合并请求时，规则会检查分支的名称。如果分支名称与规则匹配，则合并请求将以您在规则中指定的分支为目标分支。如果分支名称不匹配，合并请求将以项目的默认分支为目标分支。

先决条件：

- 您必须至少具有维护者角色。

要创建目标分支规则：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **设置 > 合并请求**。
1. 选择 **添加目标分支规则**。
1. 对于 **规则名称**，提供字符串或通配符以与分支名称进行比较。
1. 选择当分支名称与 **规则名称** 匹配时要使用的 **目标分支**。
1. 选择 **保存**。

## 删除目标分支规则

当您删除目标分支规则时，现有合并请求保持不变。

先决条件：

- 您必须至少具有维护者角色。

删除目标分支规则：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **设置 > 合并请求**。
1. 在要删除的规则上选择 **删除**。

## 相关主题

- [受保护的分支](../../protected_branches.md)
- [分支 API](../../../../api/branches.md)
- [受保护的分支 API](../../../../api/protected_branches.md)
- [Git 入门](../../../../topics/git/index.md)

## 故障排除

### 包含相同提交的多个分支

在更深的技术层面上，Git 分支不是单独的实体，而是附加到一组提交 SHA 的标签。当极狐GitLab 确定分支是否已合并时，它会检查目标分支是否存在那些提交的 SHA。
当两个合并请求包含相同的提交时，可能会导致意外结果。在此示例中，分支 `B` 和 `C` 都从分支 `A` 上的相同提交（`3`）开始：

```mermaid
gitGraph
    commit id:"a"
    branch "branch A"
    commit id:"b"
    commit id:"c" type: HIGHLIGHT
    branch "branch B"
    commit id:"d"
    checkout "branch A"
    branch "branch C"
    commit id:"e"
    checkout main
    merge "branch B" id:"merges commits b, c, d"
```

如果您合并分支 `B`，分支 `A` 也会显示为已合并（您无需执行任何操作），因为来自分支 `A` 的所有提交现在都出现在目标分支 `main` 中。分支 `C` 保持未合并，因为提交 `5` 不是分支 `A` 或 `B` 的一部分。

合并请求 `A` 保持合并状态，即使您尝试将新提交推送到其分支。如果合并请求 `A` 中的任何更改仍未合并（因为它们不是合并请求 `A` 的一部分），请为它们打开一个新的合并请求。

### 错误：存在不明确的 `HEAD` 分支

在 2.16.0 之前的 Git 版本中，您可以创建一个名为 `HEAD` 的分支。
这个名为 `HEAD` 的分支与 Git 用来描述活动（检出）分支的内部引用（也称为 `HEAD`）发生冲突。这种命名冲突会阻止您更新仓库的默认分支：

```plaintext
Error: Could not set the default branch. Do you have a branch named 'HEAD' in your repository?
```

要解决此问题：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 在左侧边栏中，选择 **代码 > 分支**。
1. 搜索名为 `HEAD` 的分支。
1. 确保分支没有未提交的更改。
1. 选择 **删除分支**，然后选择 **是，删除分支**。

Git 版本 [2.16.0 及更高版本](https://github.com/git/git/commit/a625b092cc59940521789fe8a3ff69c8d6b14eb2)，阻止您使用此名称创建分支。

