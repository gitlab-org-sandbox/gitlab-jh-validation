---
type: howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通过 URL 从仓库导入项目 **(BASIC ALL)**

您可以通过提供 Git URL 来导入现有仓库：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **查看我的所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **导入项目** 选项卡。
1. 选择 **仓库（URL）**。
1. 输入 **Git 仓库 URL**。
1. 填写其他字段。
1. 选择 **创建项目**。

将显示您新创建的项目。

<!--
## 自动化群组和项目导入 **(PREMIUM ALL)**

有关自动化用户、群组和项目导入 API 调用的信息，请参阅[自动化群组和项目导入](index.md#automate-group-and-project-import)。
-->
