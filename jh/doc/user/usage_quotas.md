---
type: howto
stage: Fulfillment
group: Utilization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 存储 **(BASIC ALL)**

存储使用情况统计信息可用于项目和命名空间。您可以使用该信息来管理适用配额内的存储使用情况。

统计数据包括：

- 命名空间中跨项目的存储使用情况。
- 超过 SaaS 存储限制或[私有化存储配额](../administration/settings/account_and_limit_settings.md#repository-size-limit)的存储使用情况。
- SaaS 上可用的购买的存储空间。

存储和网络使用量采用二进制测量系统（1024 单位倍数）进行计算。
存储使用情况以千字节 (KiB)、兆字节 (MiB) 或千兆字节 (GiB) 为单位显示。1 KiB 为 2^10 字节（1024 字节），1 MiB 为 2^20 字节（1024 千字节），1 GiB 为 2^30 字节（1024 兆字节）。

NOTE:
存储使用标签 `KB`、`MB` 和 `GB` 正在分别转换为 `KiB`、`MiB` 和 `GiB`。在此过渡期间，您可能会在 UI 和文档中看到 `KB`、`MB` 和 `GB`。

<a id="view-storage-usage"></a>

## 查看存储使用情况

先决条件：

- 要查看项目的存储使用情况，您必须至少具有项目的维护者角色或命名空间的所有者角色。
- 要查看群组命名空间的存储使用情况，您必须具有命名空间的所有者角色。

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目或群组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **存储** 选项卡以查看命名空间存储使用情况。
1. 要查看项目的存储使用情况，请从 **使用量配额** 页面的 **存储** 选项卡底部的表格中选择一个项目。

**使用量配额** 页面上的信息每 90 分钟更新一次。

如果您的命名空间显示 `'Not applicable.'`，请将提交推送到命名空间中的任意项目以重新计算存储情况。

### 查看项目派生存储使用情况 **(BASIC SAAS)**

消耗系数会被应用到项目派生所消耗的存储中，所以项目派生消耗的命名空间存储量会少于其实际大小。

要查看派生已使用的命名空间存储量：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目或群组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **存储** 选项卡。**总计** 列显示派生使用的命名空间存储量，其值将会是派生实际消耗的磁盘大小的一部分。

消耗系数应用于项目仓库、LFS 对象、作业产物、软件包、代码片段和 Wiki。

消耗系数不应用于基础版命名空间中的私有派生。

<a id="manage-storage-usage"></a>

## 管理存储使用情况

要管理您的存储使用情况，如果您是命名空间的所有者，您可以[为命名空间购买更多存储空间](../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)。

根据您的角色，您还可以使用以下方法来管理或减少存储空间：

- [减少软件包库存储空间](packages/package_registry/reduce_package_registry_storage.md)
- [减少依赖代理存储空间](packages/dependency_proxy/reduce_dependency_proxy_storage.md)
- [降低仓库大小](project/repository/reducing_the_repo_size_using_git.md)
- [减少容器镜像库存储空间](packages/container_registry/reduce_container_registry_storage.md)
- [降低 Wiki 仓库大小](../administration/wikis/index.md#reduce-wiki-repository-size)
- [管理产物到期期限](../ci/yaml/index.md#artifactsexpire_in)
- [减少构建产物存储空间](../ci/jobs/job_artifacts.md#delete-job-log-and-artifacts)

<!--To automate storage usage analysis and management, see the [storage management automation](storage_management_automation.md) documentation.-->

## 设置使用量配额 **(BASIC SELF)**

私有化部署实例没有存储和传输限制。管理员负责底层基础设施成本。管理员可以设置[仓库大小限制](../administration/settings/account_and_limit_settings.md#repository-size-limit)来管理仓库的大小。

## 存储限制 **(BASIC SAAS)**

<a id="project-storage-limit"></a>

### 项目存储限制

极狐GitLab SaaS 上的项目的 Git 仓库和 LFS 存储有 5 GiB 的存储限制。将来如果应用了极狐GitLab SaaS 命名空间存储限制，则会取消对项目存储的限制。

当项目的仓库和 LFS 存储达到配额时，该项目将被设置为只读状态。
您无法将更改推送到只读项目。要监控命名空间中每个仓库的大小，包括每个项目的细分情况，请[查看存储使用情况](#view-storage-usage)。要允许项目的仓库和 LFS 存储超出免费配额，您必须购买额外的存储空间。有关更多信息，请参阅[超额存储使用](#excess-storage-usage)。

<a id="excess-storage-usage"></a>

#### 超额存储使用

超额存储使用量是指项目仓库和 LFS 超出[项目存储限制](#project-storage-limit)的部分。如果没有可用的购买存储，则项目将被设置为只读状态。您无法将更改推送到只读项目。
要移除项目的只读状态，您必须为命名空间[购买更多存储空间](../subscriptions/gitlab_com/index.md#purchase-more-storage-and-transfer)。购买完成后，只读项目会自动恢复为标准状态。购买的可用存储量必须大于零。

**使用量配额** 页面的 **存储** 选项卡可能会警告您以下信息：

- 购买的可用存储空间不足。
- 如果购买的可用存储空间为 0，项目有变为只读状态的可能性。
- 由于购买的可用存储空间为 0，项目处于只读状态。对于只读项目，在其名称旁边显示 (**{information-o}**) 图标。

<a id="excess-storage-example"></a>

#### 超额存储示例

以下为命名空间的超额存储示例：

| 仓库     | 已用存储空间     | 超额存储空间    | 配额    | 状态            |
|--------|------------|-----------|-------|-------------------|
| Red    | 5 GiB      | 0 GiB     | 5 GiB | 只读 **{lock}** |
| Blue   | 3 GiB      | 0 GiB     | 5 GiB | 非只读        |
| Green  | 5 GiB      | 0 GiB     | 5 GiB | 只读 **{lock}** |
| Yellow | 2 GiB      | 0 GiB     | 5 GiB | 非只读        |
| **总计** | **15 GiB** | **0 GiB** | -     | -                 |

Red 和 Green 项目变为只读状态，因为它们的仓库和 LFS 使用量已达到配额。在此示例中，尚未购买额外的存储空间。

要移除 Red 和 Green 项目的只读状态，可以购买 20 GiB 的额外存储空间。

假设 Red 和 Green 项目的仓库和 LFS 所使用的存储空间分别超过 5 GiB 配额，则会扣减购买的存储空间。如下表所示，所有项目保持非只读状态，因为还有 10 GiB 购买的存储可用：20 GiB（购买的存储空间）- 10 GiB（使用的超额存储空间）。

| 仓库     | 已用存储空间     | 超额存储空间     | 配额    | 状态            |
|--------|------------|------------|-------|-------------------|
| Red    | 10 GiB     | 5 GiB      | 5 GiB | 非只读        |
| Blue   | 9 GiB      | 4 GiB      | 5 GiB | 非只读        |
| Green  | 6 GiB      | 1 GiB      | 5 GiB | 非只读        |
| Yellow | 2 GiB      | 0 GiB      | 5 GiB | 非只读        |
| **总计** | **27 GiB** | **10 GiB** | -     | -                 |

<a id="namespace-storage-limit"></a>

## 命名空间存储限制 **(BASIC SAAS)**

极狐GitLab 计划对 SaaS 上的命名空间实施存储限制。

极狐GitLab SaaS 上的命名空间具有 [5 GiB 的项目限制](#project-storage-limit)，并对命名空间存储应用软性限制。软性存储限制是极狐GitLab 尚未强制执行的限制，在应用命名空间存储限制后将成为强制限制。为了避免您的命名空间在应用命名空间存储限制后变为[只读](../user/read_only_namespaces.md)，请确保您的命名空间存储没有超过软性存储限制。

命名空间存储限制不适用于私有化部署的极狐GitLab，但管理员可以[管理仓库大小](../administration/settings/account_and_limit_settings.md#repository-size-limit)。

会被计入到总命名空间存储的存储类型包括：

- Git 仓库
- Git LFS
- 作业产物
- 容器镜像库
- 软件包库
- 依赖代理
- Wiki
- 代码片段

如果您的命名空间存储总量超过可用命名空间存储配额，则该命名空间下的所有项目都将变为只读。在移除只读状态之前，您都无法写入新数据。有关更多内容，请参见[受限操作](../user/read_only_namespaces.md#restricted-actions)。

在以下情况中，系统将通知您即将超出命名空间存储配额：

- 在命令行界面中，当您的命名空间已使用 95% 到 100% 的存储配额时，在每次 `git push` 操作后都会显示一条通知。
- 在 UI 中，当您的命名空间已使用 75% 到 100% 的存储配额时，会显示一条通知。
- 当命名空间存储使用率达到 70%、85%、95% 和 100% 时，系统会向具有所有者角色的成员发送电子邮件进行通知。

为了防止超出命名空间存储配额，您可以：

- [管理您的存储使用情况](#manage-storage-usage)。
- 如果您符合要求，您可以申请：
    - [极狐GitLab 教育公益计划](https://gitlab.cn/solutions/education/)
    - [极狐GitLab 开源项目公益支持计划](https://gitlab.cn/solutions/open-source/)
    - [极狐GitLab 初创企业未来独角兽计划](https://gitlab.cn/solutions/startups/)
- 考虑使用极狐GitLab 的[私有化部署实例](../subscriptions/self_management/index.md)，私有化部署的基础版本没有这些限制。
- 以每年 ￥388/10 GiB 的价格[购买额外存储空间](../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)。
- [申请免费试用](https://jihulab.com/-/trials/new)或[升级到专业版或旗舰版](https://gitlab.cn/pricing/)，其中包含更高的存储限额和更丰富的功能，可以让不断壮大的团队在不牺牲质量的情况下更快地交付。
<!--- [Talk to an expert](https://page.gitlab.com/usage_limits_help.html) for more information about your options.-->

## 相关主题

<!--  - [Automate storage management](storage_management_automation.md)-->
- [购买存储和传输空间](../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)
- [传输使用量](packages/container_registry/reduce_container_registry_data_transfer.md)
