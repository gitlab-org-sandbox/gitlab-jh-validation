---
stage: Growth
group: Acquisition
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 只读命名空间 **(BASIC SAAS)**

在极狐GitLab SaaS 中，当顶级命名空间出现以下情况时，它会处于只读状态：

- 命名空间可见性为私有并超出[免费用户限制](free_user_limit.md)。
- 超出[存储使用配额](usage_quotas.md)，无论命名空间可见性如何。

当命名空间处于只读状态时，页面顶部会出现一个横幅。

您向只读命名空间写入数据的能力会受到限制。有关详细信息，请参阅[受限操作](#restricted-actions)。

## 移除只读状态

要将命名空间还原到标准状态，您可以：

- 对于超出免费用户限制的命名空间：
    - 减少您命名空间中的[成员数量](free_user_limit.md#manage-members-in-your-namespace)。
    - [申请免费试用](https://jihulab.com/-/trials/new)，试用版本中的成员数量不受限制。
    - [购买付费版本](https://gitlab.cn/pricing/)。
- 对于超出存储限额的命名空间：
    - [为命名空间购买更多存储空间](../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)。
    - [管理存储使用情况](usage_quotas.md#manage-storage-usage)。

<a id="restricted-actions"></a>

## 受限操作

| 功能    | 受限操作                                                                                                                                                                                                             |
|-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 容器镜像库 | 创建、编辑和删除清理策略 <br> 将镜像推送到容器镜像库                                                                                                                                                                                    |
| 合并请求  | 创建和更新 MR                                                                                                                                                                                                         |
| 软件包库  | 发布软件包                                                                                                                                                                                                            |
| 仓库    | 添加标签 <br> 创建新分支 <br> 创建和更新提交状态 <br> 推送和强制推送到非受保护分支 <br> 推送和强制推送到受保护分支 <br> 上传文件 <br> 创建合并请求                                                                                                                      |
| CI/CD | 创建、编辑、管理和运行流水线 <br>  创建、编辑、管理和运行构建 <br>  创建和编辑管理环境 <br> 创建和编辑管理部署 <br>  创建和编辑管理集群 <br> 创建和编辑管理发布 |
| 命名空间  | **超出免费用户限制：** 邀请新用户                                                                                                                                                             |

当您在只读命名空间中执行受限操作时，可能会返回 `404` 错误信息。

## 相关主题

<!--- [常见问题 - 极狐GitLab SaaS 基础版本](https://about.gitlab.com/pricing/faq-efficient-free-tier/)-->
- [免费用户限制](free_user_limit.md)
- [存储使用配额](usage_quotas.md)
