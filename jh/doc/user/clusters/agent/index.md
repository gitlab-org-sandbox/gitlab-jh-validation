---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用极狐GitLab 连接 Kubernetes 集群

> - 引入于极狐GitLab 13.4。
> - 对 `grpcs` 的支持引入于极狐GitLab 13.6。
> - 通过 Early Adopter Program 在 `wss://kas.gitlab.com` 下，代理服务器在 JihuLab.com 上可用于极狐GitLab 13.10。
> - 引入于极狐GitLab 13.11，极狐GitLab 代理在 JihuLab.com 上可用。
> - 从专业版移动到基础版于极狐14.5。
> - 从"极狐GitLab Kubernetes 代理"重命名为 "Kubernetes 的极狐GitLab 代理"于极狐GitLab 14.6。
> - 将 Flux 推荐为 GitOps 解决方案于极狐GitLab 15.10。

您可以将 Kubernetes 集群与极狐GitLab 连接，部署、管理和监控您的云原生解决方案。

要将 Kubernetes 集群连接到极狐GitLab，您必须首先[在集群中安装代理](install/index.md)。

代理在集群中运行，您可以使用它来：

- 与位于防火墙或 NAT 后面的集群通信。
- 实时访问集群中的 API 端点。
- 推送有关集群中发生的事件的信息。
- 启用 Kubernetes 对象的缓存，这些对象以极低的延迟保持最新。

<!--
For more details about the agent's purpose and architecture, see the [architecture documentation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md).
-->

## 工作流

您可以从两个主要工作流中进行选择。推荐使用 GitOps 工作流。

### GitOps 工作流

在 [**GitOps** 工作流中](gitops.md)：

- 您将 Kubernetes manifests 保存在极狐GitLab 中。
- 在集群中安装极狐GitLab 代理。
- 每当您更新清单时，代理都会更新集群。
- 集群自动清理意外更改。它使用[服务器端应用](https://kubernetes.io/docs/reference/using-api/server-side-apply/)来修复第三方引入的任何不一致的配置。

这个工作流完全由 Git 驱动，被认为是**基于拉取的**流程，因为集群正在从极狐GitLab 仓库中提取更新。

### 极狐GitLab CI/CD 工作流

在 [**CI/CD** 工作流中](ci_cd_workflow.md)：

- 您将极狐GitLab CI/CD 配置为使用 Kubernetes API 来查询和更新集群。

此工作流被认为是**基于推送的**流程，因为极狐GitLab 正在将来自极狐GitLab CI/CD 的请求推送到您的集群。

在以下情况，使用此工作流：

- 当您有大量面向流水线的流程时。
- 当您需要迁移到代理但 GitOps 工作流无法支持您需要的用例时。

此工作流的安全模型较弱，不建议用于生产部署。

<a id="gitlab-agent-for-kubernetes-supported-cluster-versions"></a>

## 支持极狐GitLab 功能的 Kubernetes 版本

极狐GitLab 支持以下 Kubernetes 版本。如果您想在 Kubernetes 集群中运行极狐GitLab，您可能需要不同版本的 Kubernetes：

- 对于 [Helm Chart](https://docs.gitlab.cn/charts/installation/cloud/index.html)。
- 对于[极狐GitLab operator](https://docs.gitlab.cn/operator/installation.html#kubernetes)。

您随时可以将您的 Kubernetes 版本升级为受支持的版本：

- 1.27（支持将于 2024 年 7 月 22 日或 1.30 受支持时结束）
- 1.26（支持将于 2024 年 3 月 22 日或 1.29 受支持时结束）
- 1.25（支持将于 2023 年 10 月 22 日或 1.28 受支持时结束）

极狐GitLab 的目标是在首次发布三个月后支持新的 Kubernetes 小版本。极狐GitLab 在任何给定的时间至少支持三个可用于生产的 Kubernetes 小版本。

安装代理时，请使用与您的 Kubernetes 版本兼容的 Helm 版本。其他版本的 Helm 可能不可用。有关兼容版本的列表，请参阅 [Helm 版本支持政策](https://helm.sh/docs/topics/version_skew/)。

当我们放弃对仅支持已弃用 API 的 Kubernetes 版本的支持时，可以从极狐GitLab 代码库中移除对已弃用 API 的支持。

某些极狐GitLab 功能可能适用于此处未列出的版本。<!--[This epic](https://gitlab.com/groups/gitlab-org/-/epics/4827) tracks support for Kubernetes versions.-->

## 从历史的基于证书的集成迁移到代理

了解如何从基于证书的集成[迁移到 Kubernetes 代理](../../infrastruct/clusters/migrate_to_gitlab_agent.md)。

## 代理连接

代理打开与 KAS 的双向通道进行通信。
该通道用于代理和 KAS 之间的所有通信：

- 每个代理最多可以维护 500 个逻辑 gRPC 流，包括活跃的流和空闲的流。
- gRPC 流使用的 TCP 连接数量由 gRPC 本身决定。
- 每个连接的最长生命周期为两小时，并有一小时的宽限期。
    - KAS 前面的代理可能会影响连接的最长生命周期。在 JihuLab.com 上，是两个小时。宽限期为最长生命周期的 50%。

有关通道路由的详细信息，请参阅[在代理中路由 KAS 请求](https://jihulab.com/gitlab-cn/cluster-integration/gitlab-agent/-/blob/master/doc/kas_request_routing.md)。

## 相关主题

- [GitOps 工作流](gitops.md)
<!--[GitOps examples and learning materials](gitops.md#related-topics)-->
- [极狐GitLab CI/CD 工作流](ci_cd_workflow.md)
- [安装代理](install/index.md)
- [与代理一同使用](work_with_agent.md)
- [故障排查](troubleshooting.md)
<!-- [Guided explorations for a production ready GitOps setup](https://gitlab.com/groups/guided-explorations/gl-k8s-agent/gitops/-/wikis/home#gitlab-agent-for-kubernetes-gitops-working-examples)
- [CI/CD for Kubernetes examples and learning materials](ci_cd_workflow.md#related-topics)-->
- [向代理开发进行贡献](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc)
