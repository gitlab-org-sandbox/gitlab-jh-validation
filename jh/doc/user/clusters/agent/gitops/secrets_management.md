---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 在 GitOps 工作流程中管理 Kubernetes secret

您永远不应该将 Kubernetes secret 以未加密的形式存储在 `git` 存储库中。如果您使用 GitOps 工作流程，则可以按照以下步骤安全地管理您的 secret。

1. 设置 Sealed Secrets 控制器来管理 secrets。
1. 部署 Docker 凭证，以便集群可以从极狐GitLab Container Registry 中提取镜像。

## 先决条件

此设置需要：

- [为 GitOps 工作流配置的 Kubernetes 的极狐GitLab 代理](../gitops.md)。
- 访问集群以完成设置。

## 设置 Sealed Secrets 控制器以管理 secrets

您可以使用 [Sealed Secrets 控制器](https://github.com/bitnami-labs/sealed-secrets) 将加密的 secrets 安全地存储在 `git` 仓库中。控制器将 secret 解密为标准的 Kubernetes `Secret` 类资源。

1. 进入 [Sealed Secrets 发布页面](https://github.com/bitnami-labs/sealed-secrets/releases)，下载最新的 `controller.yaml` 文件。
1. 在极狐GitLab 中，转到包含 Kubernetes 清单的项目并上传 `controller.yaml` 文件。
1. 打开代理配置文件 (`config.yaml`)，如果需要，更新 `paths.glob` 模板以匹配 Sealed Secrets 清单。
1. 提交并推送更改到极狐GitLab。
1. 确认 Sealed Secrets 控制器安装成功：

   ```shell
   kubectl get pods -lname=sealed-secrets-controller -n kube-system
   ```

1. 按照 [Sealed Secrets 说明](https://github.com/bitnami-labs/sealed-secrets#homebrew)，安装 `kubeseal` 命令行实用程序。
1. 在不直接访问集群的情况下获取加密秘密所需的公钥：

   ```shell
   kubeseal --fetch-cert > public.pem
   ```

1. 将公钥提交到仓库。

有关 Sealed Secrets 控制器如何工作的更多详细信息，请查看[使用说明](https://github.com/bitnami-labs/sealed-secrets/blob/main/README.md#usage)。

## 部署 Docker 凭据

要从极狐GitLab 容器镜像库部署容器，您必须使用正确的 Docker 镜像库凭据配置集群。您可以通过部署 `docker-registry` 类型的 secrets 来实现此目的。

1. 生成至少具有 `read-registry` 权限的极狐GitLab 令牌。令牌可以是个人或项目访问令牌。
1. 创建 Kubernetes secret 清单 YAML 文件。根据需要更新值：

    ```shell
    kubectl create secret docker-registry gitlab-credentials --docker-server=registry.gitlab.example.com --docker-username=<gitlab-username> --docker-password=<gitlab-token> --docker-email=<gitlab-user-email> -n <namespace> --dry-run=client -o yaml > gitlab-credentials.yaml
    ```

1. 将 secret 加密到 `SealedSecret` 清单中：

   ```shell
   kubeseal --format=yaml --cert=public.pem < gitlab-credentials.yaml > gitlab-credentials.sealed.yaml
   ```
