---
stage: Secure
group: Composition analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 可操作容器扫描 **(ULTIMATE ALL)**

> - 引入于极狐GitLab 14.8。
> - starboard 指令废弃于极狐GitLab 15.4。starboard 指令计划移除于极狐GitLab 16.0。

## 启用可操作容器扫描

您可以使用可操作容器扫描来扫描集群中的容器镜像是否存在安全漏洞。您可以使扫描器按照 `agent config` 配置的方式运行，或者在包含代理的项目中设置 `scan execution policies`。

NOTE:
如果配置了 `agent config` 和 `scan execution policies`，优先使用 `scan execution policy` 的配置。

### 通过代理配置启用

要通过代理配置扫描 Kubernetes 集群中的所有镜像，使用包含 [CRON 表达式](https://en.wikipedia.org/wiki/Cron) 的 `cadence` 字段将 `container_scanning` 配置块添加到您的代理配置中，该表达式决定运行扫描的时间。

```yaml
container_scanning:
  cadence: '0 0 * * *' # Daily at 00:00 (Kubernetes cluster time)
```

`cadence` 字段是必需的。极狐GitLab 支持 cadence 字段的以下类型的 CRON 语法：

- 每日在指定时间每小时执行一次的 cadence，例如：`0 18 * * *`
- 每周在指定日期和指定时间执行一次的 cadence，例如：`0 13 * * 0`

NOTE:
如果受我们在实现中使用的 [cron](https://github.com/robfig/cron) 支持，[CRON 语法](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)的其他元素可能会在 cadence 字段中有效。然而，极狐GitLab 并不正式测试或支持。

NOTE:
使用 Kubernetes-agent pod 的系统时间在 [UTC](https://www.timeanddate.com/worldclock/timezone/utc) 中评估 CRON 表达式。

默认情况下，可操作容器扫描尝试扫描所有命名空间中的工作负载是否存在漏洞。您可以使用 `namespaces` 字段设置 `vulnerability_report` 块，该字段可用于限制扫描哪些命名空间。例如，如果您只想扫描 `default` 和 `kube-system` 命名空间，则可以使用以下配置：

```yaml
container_scanning:
  cadence: '0 0 * * *'
  vulnerability_report:
    namespaces:
      - default
      - kube-system
```

### 通过扫描执行策略启用

要通过扫描执行策略扫描 Kubernetes 集群中的所有镜像，我们可以使用[扫描支持策略编辑器](../../application_security/policies/scan-execution-policies.md#scan-execution-policy-editor)创建新的计划规则。

NOTE:
Kubernetes 代理必须在集群中运行才能扫描运行的容器镜像。

以下是在 Kubernetes 代理所附加的集群内启用可操作的容器扫描的策略示例：

```yaml
- name: Enforce Container Scanning in cluster connected through my-gitlab-agent for default and kube-system namespaces
  enabled: true
  rules:
  - type: schedule
    cadence: '0 10 * * *'
    agents:
      <agent-name>:
        namespaces:
        - 'default'
        - 'kube-system'
  actions:
  - scan: container_scanning
```

调度规则的关键是：

- `cadence`（必需）：运行扫描时的 [CRON 表达式](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)。
- `agents:<agent-name>`（必需）：用于扫描的代理名称。
- `agents:<agent-name>:namespaces`（可选）：要扫描的 Kubernetes 命名空间。如果省略，则扫描所有命名空间。

NOTE:
如果受我们在实现中使用的 [cron](https://github.com/robfig/cron) 支持，[CRON 语法](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)的其他元素可能会在 cadence 字段中有效。然而，极狐GitLab 并不正式测试或支持。

NOTE:
使用 Kubernetes-agent pod 的系统时间在 [UTC](https://www.timeanddate.com/worldclock/timezone/utc) 中计算 CRON 表达式。

您可以在[扫描执行策略文档](../../application_security/policies/scan-execution-policies.md#scan-execution-policies-schema)中查看完整架构。

## 配置扫描器资源要求

默认情况下，扫描器 pod 的默认资源要求为：

```yaml
requests:
  cpu: 100m
  memory: 100Mi
limits:
  cpu: 500m
  memory: 500Mi
```

您可以使用 `resource_requirements` 字段进行自定义。

```yaml
container_scanning:
  resource_requirements:
    requests:
      cpu: 200m
      memory: 200Mi
    limits:
      cpu: 700m
      memory: 700Mi
```

NOTE:
资源要求只能使用代理配置来设置。如果您通过 `scan execution policies` 启用了 `Operational Container Scanning`，则需要在代理配置文件中定义资源要求。

## 查看集群漏洞

在极狐GitLab中查看漏洞信息：

1. 在左侧边栏中，选择 **搜索或转到** 并找到包含代理配置文件的项目。
1. 选择 **操作 > Kubernetes 集群**。
1. 选择 **代理** 选项卡。
1. 选择一个代理查看集群漏洞。

![Cluster agent security tab UI](../img/cluster_agent_security_tab_v14_8.png)

您也可以在[可操作漏洞](../../../user/application_security/vulnerability_report/index.md#operational-vulnerabilities)中查看到该信息。

NOTE:
您必须至少具有开发者角色。

## 扫描私有镜像

> 引入于极狐 GitLab 16.4。

要扫描私有镜像，扫描器使用镜像拉取 secret（直接引用和来自服务账户）来拉取镜像。
