---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 授予用户 Kubernetes 访问权限 (Beta) **(BASIC ALL)**

> - 引入于极狐GitLab 16.1，[功能标志](../../../administration/feature_flags.md)为 `environment_settings_to_graphql`、`kas_user_access`、`kas_user_access_project` 和 `expose_authorized_cluster_agents`。此功能处于 [Beta](../../../policy/experiment-beta-support.md#beta) 阶段。
> - 功能标志 `environment_settings_to_graphql` 移除于极狐GitLab 16.2。
> - 功能标志 `kas_user_access`、`kas_user_access_project` 和 `expose_authorized_cluster_agents` 移除于极狐GitLab 16.2。

作为组织中 Kubernetes 集群的管理员，您可以向特定项目或群组的成员授予 Kubernetes 访问权限。

授予访问权限还会激活项目或群组的 Kubernetes Dashboard。

对于私有化部署的实例，请确保您：

- 将您的极狐GitLab 实例和 [KAS](../../../administration/clusters/kas.md) 托管在同一域上。
- 在极狐GitLab 的子域上托管 KAS。例如，极狐GitLab 在 `gitlab.com` 上，KAS 在 `kas.gitlab.com` 上。

## 配置 Kubernetes 访问权限

当您想要授予用户对 Kubernetes 集群的访问权限时，请配置访问权限。

先决条件：

- Kubernetes 的代理安装在 Kubernetes 集群中。
- 您必须具有开发人员或更高级别的角色。

配置访问权限：

- 在代理配置文件中，使用以下参数定义 `user_access` 关键字：

    - `projects`：成员应具有访问权限的项目列表。
    - `groups`：成员应具有访问权限的群组列表。
    - `access_as`：必需。对于普通访问，该值为 `{ agent: {...} }`。

配置访问权限后，请求将使用代理服务账户转发到 API 服务器。

例如：

```yaml
# .gitlab/agents/my-agent/config.yaml

user_access:
  access_as:
    agent: {}
  projects:
    - id: group-1/project-1
    - id: group-2/project-2
  groups:
    - id: group-2
    - id: group-3/subgroup
```

## 通过用户模拟配置访问 **(PREMIUM ALL)**

您可以授予对 Kubernetes 集群的访问权限，并将请求转换为经过身份验证的用户的模拟请求。

先决条件：

- Kubernetes 的代理安装在 Kubernetes 集群中。
- 您必须具有开发人员或更高级别的角色。

要使用用户模拟配置访问：

- 在代理配置文件中，使用以下参数定义 `user_access` 关键字：

    - `projects`：成员应具有访问权限的项目列表。
    - `groups`：成员应具有访问权限的群组列表。
    - `access_as`：必需。对于用户模拟，该值为 `{ user: {...} }`。

配置访问权限后，请求将转换为针对经过身份验证的用户的模拟请求。

### 用户模拟工作流

安装的 `agentk` 模拟给定的用户，如下所示：

- `UserName` 是 `gitlab:user:<username>`
- `Groups` 是：
    - `gitlab:user`：常见于极狐GitLab 用户的所有请求。
    - 每个授权项目中的每个角色的 `gitlab:project_role:<project_id>:<role>`。
    - 每个授权群组中的每个角色的 `gitlab:group_role:<group_id>:<role>`。
- `Extra` 携带有关请求的附加信息：
    - `agent.gitlab.com/id`：代理 ID。
    - `agent.gitlab.com/username`：极狐GitLab 用户的用户名。
    - `agent.gitlab.com/config_project_id`：代理配置项目 ID。
    - `agent.gitlab.com/access_type`：`personal_access_token` 或 `oidc_id_token` 或 `session_cookie`。

仅模拟配置文件中 `user_access` 下直接列出的项目和群组。例如：

```yaml
# .gitlab/agents/my-agent/config.yaml

user_access:
  access_as:
    user: {}
  projects:
    - id: group-1/project-1 # group_id=1, project_id=1
    - id: group-2/project-2 # group_id=2, project_id=2
  groups:
    - id: group-2 # group_id=2
    - id: group-3/subgroup # group_id=3, group_id=4
```

在此配置中：

- 如果用户仅是 `group-1` 的成员，他们仅会收到 Kubernetes RBAC 群组 `gitlab:project_role:1:<role>`。
- 如果用户是 `group-2` 的成员，他们会收到两个 Kubernetes RBAC 群组：
    - `gitlab:project_role:2:<role>`
    - `gitlab:group_role:2:<role>`

### RBAC 授权

模拟请求需要 `ClusterRoleBinding` 或 `RoleBinding` 来识别 Kubernetes 内部的资源权限。请参阅 [RBAC 授权](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) 了解合适的配置。

例如，如果您允许 `awesome-org/deployment` 项目（ID：123）中的维护者读取 Kubernetes 工作负载，则必须将 `ClusterRoleBinding` 资源添加到 Kubernetes 配置中：

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: my-cluster-role-binding
roleRef:
  name: view
  kind: ClusterRole
  apiGroup: rbac.authorization.k8s.io
subjects:
  - name: gitlab:project_role:123:maintainer
    kind: Group
```

## 使用 Kubernetes API 访问集群

> 引入于极狐GitLab 16.4。

您可以配置代理以允许极狐GitLab 用户使用 Kubernetes API 访问集群。

先决条件：

- 您有一个配置有 `user_access` 条目的代理。

授予 Kubernetes API 访问权限：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **操作 > Kubernetes 集群** 并检索要访问的代理的数字 ID。您需要 ID 来构造完整的 API 令牌。
1. 使用 `k8s_proxy` 范围创建一个[个人访问令牌](../../profile/personal_access_tokens.md)。您需要访问令牌来构建完整的 API 令牌。
1. 构建 `kube config` 条目来访问集群：
    1. 确保选择了合适的 `kube config`。
       例如，您可以设置 `KUBECONFIG` 环境变量。
    1. 将极狐GitLab KAS 代理集群添加到 `kube config` 中：

       ```shell
       kubectl config set-cluster <cluster_name> --server "https://kas.gitlab.com/k8s-proxy"
       ```

       `server` 参数指向您的极狐GitLab 实例的 KAS 地址。
       在 JihuLab.com 上，为 `https://kas.gitlab.com/k8s-proxy`。
       当您注册代理时，您可以获得您实例的 KAS 地址。

    1. 使用您的数字代理 ID 和个人访问令牌构建 API 令牌：

       ```shell
       kubectl config set-credentials <gitlab_user> --token "pat:<agent-id>:<token>"
       ```

    1. 添加上下文以合并集群和用户：

       ```shell
       kubectl config set-context <gitlab_agent> --cluster <cluster_name> --user <gitlab_user>
       ```

    1. 激活新的上下文：

       ```shell
       kubectl config use-context <gitlab_agent>
       ```

1. 检查配置是否成功：

    ```shell
    kubectl get nodes
    ```

配置的用户可以使用 Kubernetes API 访问您的集群。

## 相关主题

- [架构蓝图](https://jihulab.com/gitlab-cn/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_user_access.md)
<!--- [Dashboard for Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/2493)-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

