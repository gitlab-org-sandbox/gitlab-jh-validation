---
stage: none
group: Development
info: "See the Technical Writers assigned to Development Guidelines: https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-development-guidelines"
description: "Understand what 'GitLab features deployed behind flags' means."
layout: 'feature_flags'
---

# 极狐GitLab 功能可能受到功能标志的限制

<!--
> Feature flag documentation warnings were [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/227806) in GitLab 13.4.
-->

极狐GitLab 使用功能标志发布一些处于禁用状态的功能，允许它们由特定的用户组测试并战略性地推出，直到它们对所有人启用。

作为极狐GitLab 用户，这意味着极狐GitLab 版本中包含的某些功能可能对您不可用。

在这种情况下，您会在功能文档中看到类似这样的警告：

开发中的功能可能无法供您使用。[启用仍在开发中的功能时可能存在风险](../administration/feature_flags.md#risks-when-enabling-features-still-in-development)。

在版本历史记录中，您将找到有关功能标志状态的信息，包括私有化部署实例的功能是打开（“默认启用”）还是关闭（“默认禁用”）。

<!--
If you're a user of a GitLab self-managed instance and you want to try to use a
disabled feature, you can ask a [GitLab administrator to enable it](../administration/feature_flags.md),
although changing a feature's default state isn't recommended.

If you're a GitLab.com user and the feature is disabled, be aware that GitLab may
be working on the feature for potential release in the future.
-->