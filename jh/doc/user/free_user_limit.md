---
stage: Growth
group: Acquisition
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 免费用户限制 **(BASIC SAAS)**

五个用户限制适用于极狐GitLab SaaS 上具有私有可见性的顶级命名空间，此限制于 2024 年 1 月 2 日生效。

当应用五个用户限制时，超出用户限制的顶级私有命名空间将置于只读状态。这些命名空间无法向仓库、Git 大文件存储 (LFS)、软件包或镜像库写入新数据。有关受限操作的完整列表，请参阅[只读命名空间](read_only_namespaces.md)。

<a id="manage-members-in-your-namespace"></a>

## 管理命名空间中的成员

为了帮助用户管理免费用户限制，您可以查看并管理命名空间中所有项目和群组的成员总数。

先决条件：

- 您必须具有该群组的所有者角色。

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 要查看所有成员，请选择 **席位** 选项卡。
1. 要删除成员，请选择 **删除用户**。

如果您需要更多时间来管理您的成员，或者想要与超过 5 名成员的团队一起使用极狐GitLab 功能，您可以[申请免费试用](https://jihulab.com/-/trials/new)。试用期为 60 天，成员数量不受限制。

## 确定命名空间的用户数量

具有私有可见性级别的顶级命名空间中的每个用户都会计入五个用户的限制中，包括命名空间内群组、子群组和项目中的每个用户。

例如：

群组 `example-1` 拥有：

- 一名群组所有者 `A`。
- 一个名为 `subgroup-1` 的子群组，拥有一名成员 `B`。
    - `subgroup-1` 继承 `example-1` 的成员 `A`。
- `subgroup-1` 中名为 `project-1` 的项目，有两名成员 `C` 和 `D`。
    - `project-1` 继承 `subgroup-1` 的成员 `A` 和 `B`。

命名空间 `example-1` 拥有四个成员：`A`、`B`、`C` 和`D`。由于 `example-1` 只有四个成员，因此不受五个用户限制的影响。

群组 `example-2` 拥有：

- 一名群组所有者 `A`。
- 一个名为 `subgroup-2` 的子群组，拥有一名成员 `B`。
    - `subgroup-2` 继承 `example-2` 的成员 `A`。
- `subgroup-2` 中名为 `project-2a` 的项目，有两名成员 `C` 和 `D`。
    - `project-2a` 继承 `subgroup-2` 的成员 `A` 和 `B`。
- `subgroup-2` 中名为 `project-2b` 的项目，有两名成员 `E` 和 `F`。
    - `project-2b` 继承 `subgroup-2` 的成员 `A` 和 `B`。

命名空间 `example-2` 拥有六名成员：`A`、`B`、`C`、`D`、`E` 和 `F`。由于 `example-2` 拥有六名成员，因此受五个用户限制的影响。

