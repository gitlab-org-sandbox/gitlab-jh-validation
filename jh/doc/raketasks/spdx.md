---
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# SPDX 许可证列表导入 **(ULTIMATE SELF)**

极狐GitLab 提供了一个用于向极狐GitLab 实例上传 [SPDX 许可证列表](https://spdx.org/licenses/)的新副本的 Rake 任务。
需要此列表匹配许可证合规性策略<!--[许可证合规性策略](../user/compliance/license_compliance/index.md) -->的名称。

上传 PDX 证书列表的新副本，您需要运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:spdx:import

# source installations
bundle exec rake gitlab:spdx:import RAILS_ENV=production
```

要在离线环境<!--[离线环境](../user/application_security/offline_deployments/#defining-offline-environments)-->中执行此任务，
应该允许与 [`licenses.json`](https://spdx.org/licenses/licenses.json)建立出站连接。

