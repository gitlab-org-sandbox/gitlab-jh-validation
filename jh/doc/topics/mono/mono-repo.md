---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: howto, reference
---

# 使用 Mono 与极狐GitLab 协作 **(PREMIUM SELF)**

> - [引入](https://jihulab.com/gitlab-cn/gitlab/-/issues/2861)于极狐GitLab 16.4。[功能标志](../../user/feature_flags.md)为 `ff_monorepo`，默认禁用。
> - 多仓库 MR 只触发一条统一的主流水线[引入](https://jihulab.com/gitlab-cn/gitlab/-/issues/2861)于极狐GitLab 16.5。

在开发包含多个代码仓库的项目时，极狐GitLab 可以为开发人员提供终端工具 Mono 以集中化管理同一顶级群组下的多个仓库的代码提交，并进行统一的代码评审和合并等操作。
Mono 可以管理的多个代码仓库是指在同一个顶级群组下的仓库，包括父群组和子群组下的所有代码仓库。

## 使用 Mono 提交 MR

使用 Mono 提交多仓库 MR：

1. 参考[如何安装 Mono 与基本工作流程](getting-started.md)中的描述正确安装并初始化完成 Mono。
2. 打开终端并输入 `mono sync`，将 manifest 文件中配置的多个项目中的代码下载到本地。
3. 下载成功后，您可以使用 `mono info` 命令查看本地项目的信息。
4. 在仓库中创建分支。
   - 在本地的当前仓库中创建分支：`mono start FEATURE_BRANCH_NAME .`
   - 在指定的仓库中创建分支：`mono start FEATURE_BRANCH_NAME PROJECT_NAME`
   - 在本地的所有仓库中创建分支：`mono start FEATURE_BRANCH_NAME --all`
5. 在本地进行代码变更。
6. 输入 `mono sync` 检查是否存在代码冲突。更多有关命令行选项的内容，请参阅[命令行选项](command-line-options.md)。
7. 输入 `mono upload` 将本地代码变更推送到远端仓库。推送成功后，系统会自动在对应的仓库中创建 MR，并会在 MR 上统一添加标签 `monorepo`。

## 合并 MR

对于通过 Mono 创建的多仓库 MR，其合并方式与通过普通方式创建的 MR 不同。每个 MR 页面没有单独的 **合并** 按钮，而是在 MR 上方有一个统一的 **全部合并** 按钮。

必须满足以下条件，**全部合并** 按钮才会在页面上显示为可用。

- 您必须具有通过 Mono 管理的所有项目的合并权限。
- MR 都标有 `可以合并` 标签，`可以合并` 标签取决于流水线是否成功和 MR 是否符合用户设置的合并规则等条件。
- 当前没有其他 MR 列表正在合并。

NOTE:
如果在点击 **全部合并** 之后，某个 MR 因发生问题而中断，其他 MR 不会受其影响，可以正常合并。之后您需要手动修复那些合并失败的 MR，将其恢复到正常状态后，再次点击 **全部合并**。

## 多仓库 MR 只触发一条统一的主流水线

如果用户希望在提交多仓库 MR 时只触发一次流水线，以用来集中测试或实现其他用途，用户可以配置主流水线。

使用 Mono 提交多仓库 MR 并使用主流水线：

1. 参考[如何安装 Mono 与基本工作流程](getting-started.md)中的描述正确安装并初始化完成 Mono。
2. 在顶级群组下，新建一个专门用于运行主流水线的项目，项目名称必须为 `monorepo-ci-project` 并且该项目仓库中必须有 `main` 分支。
3. 在[安装准备](getting-started.md#installation-preparation)部分中包含的 manifest 文件中配置 `gitlab-url` 的地址，如 `gitlab-url=http://127.0.0.1:3000`，并添加 `enable-central-ci-pipeline="true"` 字段。示例如下：

   ```
   <?xml version="1.0" encoding="UTF-8"?>
   <manifest>
 
     <remote  name="origin"  fetch=".." />
     <default revision="main"
              remote="origin"
              sync-j="4"
              gitlab-url="http://127.0.0.1:3000"
              enable-central-ci-pipeline="true" />
     <project path="PROJECTA" name="root-group-for-testing-mono/PROJECTA" />
     <project path="sub-group/PROJECTB" name="root-group-for-testing-mono/sub-group/PROJECTB" />
     <project path="sub-group/PROJECTC" name="root-group-for-testing-mono/sub-group/PROJECTC" />
   
   </manifest>
   ```

4. 在终端中，输入 `mono init` 将配置文件下载到本地。
5. 输入 `mono sync` 同步 manifest 文件中配置的远端代码库。
6. 下载成功后，您可以使用 `mono info` 命令查看本地项目的信息。
7. 在仓库中创建分支。
   - 在本地的当前仓库中创建分支：`mono start FEATURE_BRANCH_NAME .`
   - 在指定的仓库中创建分支：`mono start FEATURE_BRANCH_NAME PROJECT_NAME`
   - 在本地的所有仓库中创建分支：`mono start FEATURE_BRANCH_NAME --all`
8. 在本地进行代码变更。
9. 输入 `mono sync` 检查是否存在代码冲突。更多有关命令行选项的内容，请参阅[命令行选项](command-line-options.md)。
10. 输入 `mono upload` 将本地代码变更推送到远端仓库。推送成功后，系统会自动在对应的仓库中创建 MR，并会在 MR 上统一添加标签 `monorepo`。此时终端会显示以下内容：

    ```
    Triggering central ci pipeline...
    To trigger the central ci pipeline, You need to fill in Personal Access Token.
    Related document: https://docs.gitlab.cn/jh/user/profile/personal_access_tokens.html
    Please enter your Token (Press Enter to skip):
    ```

11. Mono 需要调用极狐GitLab API 触发流水线，所以您需要输入您的极狐GitLab 的个人访问令牌。获取个人访问令牌：
    - 选择左上角的个人头像。
    - 选择 **偏好设置**。
    - 在左侧边栏中，选择 **访问令牌**。
    - 选择 **添加新令牌**。
    - 输入您的令牌名称并选择到期时间。在 **选择范围** 下，勾选 **api**。
    - 选择 **创建个人访问令牌**。
12. 复制您的个人访问令牌，并将其粘贴到终端的 `Please enter your Token (Press Enter to skip):` 后面。如果主流水线创建成功，终端会显示以下内容（假设 URL 地址为 `http://127.0.0.1:3000`，顶级群组的名称为 `top-level-group-name`，成功创建的主流水线的 ID 为 `678`）：

    ```
    Triggered pipeline: http://127.0.0.1:3000/top-level-group-name/monorepo-ci-project/-/pipelines/678
    ```

点击进入多仓库 MR 页面，您可以看到 MR 列表下成功触发了一条主流水线。
Mono 会在本地保存您输入的个人访问令牌。当您后续继续提交多仓库 MR 触发流水线时，您无需再次输入个人访问令牌。
