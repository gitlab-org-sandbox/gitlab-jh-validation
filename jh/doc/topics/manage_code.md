---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 管理您的代码 **(BASIC ALL)**

将源文件存储在仓库中，并使用合并请求对其进行更改。

- [仓库](../user/project/repository/index.md)
- [合并请求](../user/project/merge_requests/index.md)
