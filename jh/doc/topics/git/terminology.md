---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Git 术语

以下是常用的 Git 术语。

## 仓库

在极狐GitLab 中，文件存储在**仓库**中。仓库类似于您将文件存储在计算机上的文件夹或目录中的方式。

- **远端仓库**指的是极狐GitLab 中的文件。
- **本地副本**是指您计算机上的文件。

<!-- vale gitlab.Spelling = NO -->
<!-- vale gitlab.SubstitutionWarning = NO -->
通常，“repository”这个词被缩写为“repo”。
<!-- vale gitlab.Spelling = YES -->
<!-- vale gitlab.SubstitutionWarning = YES -->

在极狐GitLab 中，存储库包含在**项目**中。

## 派生

当您想为其他人的仓库做出贡献时，您可以复制它。
此副本称为[**派生**](../../user/project/repository/forking_workflow.md#creating-a-fork)。
该过程称为“创建派生”。

当您派生一个仓库时，您会在自己的命名空间中创建项目的副本。然后，您拥有修改项目文件和设置的写入权限。

例如，您可以将这个项目 <https://jihulab.com/gitlab-tests/sample-project/> 派生到您的命名空间中。
您现在拥有自己的仓库副本，您可以在 URL 中查看命名空间，例如 `https://jihulab.com/your-namespace/sample-project/`。
然后，您可以将仓库克隆到本地计算机，处理文件并将更改提交回原始仓库。

## 下载和克隆的区别

要在您的计算机上创建远端仓库文件的副本，您可以**下载**或**克隆**仓库。如果您下载它，则无法将仓库与极狐GitLab 上的远端仓库同步。

[克隆](../../gitlab-basics/start-using-git.md#clone-a-repository)仓库与下载相同，只是它保留了与远端仓库的 Git 连接。然后，您可以在本地修改文件并将更改上传到极狐GitLab 上的远端仓库。

<a id="pull-and-push"></a>

## 拉取和推送

保存仓库的本地副本并修改计算机上的文件后，您可以将更改上传到极狐GitLab。此操作称为**推送**到远端，您使用命令 [`git push`](../../gitlab-basics/start-using-git.md#send-changes-to-gitlabcom)。

当远端存储库更改时，您的本地副本落后，您可以使用远端仓库中的新更改，来更新本地副本。此操作称为从远端**拉取**，使用命令 [`git pull`](../../gitlab-basics/start-using-git.md#download-the-latest-changes-in-the-project)。
