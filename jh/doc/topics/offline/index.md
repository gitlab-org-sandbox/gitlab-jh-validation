---
stage: Systems
group: Distribution
description: Isolated installation.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 离线极狐GitLab **(FREE SELF)**

由于安全考虑，离线环境中的计算机与公共互联网隔离。本页面列出了在离线环境中运行极狐GitLab 的所有可用信息。

## 快速入门

如果您计划在物理隔离且离线网络上部署GitLab实例，请查看[快速入门指南](quick_start_guide.md)以获取配置步骤。

## 特性

遵循以下最佳实践，以在离线环境中使用极狐GitLab 功能：

- [在离线环境中操作极狐GitLab 安全扫描器](../../user/application_security/offline_deployments/index.md)。
