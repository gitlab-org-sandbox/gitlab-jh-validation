---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Cron

Cron 语法用于安排作业何时运行。

您可能需要使用 cron 语法字符串来创建[流水线计划](../../ci/pipelines/schedules.md)，或通过设置[部署冻结](../../user/project/releases/index.md#prevent-unintentional-releases-by-setting-a-deploy-freeze)，来防止意外发布。

## Cron 语法

Cron 计划使用一系列五个数字，以空格分隔：

```plaintext
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday)
# │ │ │ │ │
# │ │ │ │ │
# │ │ │ │ │
# * * * * * <command to execute>
```

在 cron 语法中，星号 (`*`) 表示“每个”，因此以下 cron 字符串有效：

- 每小时开始时运行一次：`0 * * * *`
- 每天午夜运行一次：`0 0 * * *`
- 每周在周日早上的午夜运行一次：`0 0 * * 0`
- 每月第一天午夜运行一次：`0 0 1 * *`
- 每月 22 日运行一次：`0 0 22 * *`
- 每个月的第二个星期一运行一次：`0 0 * * 1#2`
- 每年 1 月 1 日午夜运行一次：`0 0 1 1 *`
- 每隔一个星期日在 0900 时运行：`0 9 * * sun%2`

有关完整的 cron 文档，请参阅 [crontab(5) — Linux 手册页](https://man7.org/linux/man-pages/man5/crontab.5.html)。
通过在 Linux 或 MacOS 终端中输入 `man 5 crontab`，可以离线访问该文档。

## Cron 示例

```plaintext
# Run at 7:00pm every day:
0 19 * * *

# Run every minute on the 3rd of June:
* * 3 6 *

# Run at 06:30 every Friday:
30 6 * * 5
```

有关如何编写 cron 计划的更多示例，请参见 [crontab.guru](https://crontab.guru/examples.html)。

## 极狐GitLab 如何解析 cron 语法字符串

极狐GitLab 使用 [`fugit`](https://github.com/floraison/fugit) 解析服务器上的 cron 语法字符串，并使用 [cron-validator](https://github.com/TheCloudConnectors/cron-validator) 在浏览器中验证 cron 语法。极狐GitLab 使用 [`cRonstrue`](https://github.com/bradymholt/cRonstrue) 在浏览器中将 cron 转换为可读的字符串。
