---
stage: Manage
group: Foundations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 您的工作侧边栏

- 引入于 15.9 版本。

**您的工作**左侧边栏提供以下入口：

- [项目](../user/project/working_with_projects.md#view-projects)
- [群组](../user/group/index.md)
- [议题](../user/project/issues/index.md)
- [合并请求](../user/project/merge_requests/index.md)
- [待办事项](../user/todos.md)
- [里程碑](../user/project/milestones/index.md)
- [代码片段](../user/snippets.md#snippets)
- [活动](../user/profile/index.md#view-your-activity)
- [环境仪表盘](../ci/environments/environments_dashboard.md)
- [运维仪表盘](../user/operations_dashboard/index.md)
- [安全中心](../user/application_security/security_dashboard/index.md#security-center)
