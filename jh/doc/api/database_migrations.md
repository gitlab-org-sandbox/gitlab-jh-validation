---
stage: Data Stores
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 数据库迁移 API **(BASIC SELF)**

> 引入于极狐GitLab 16.2。

该 API 用于管理极狐GitLab 开发中的数据库迁移。

所有方法都需要管理员权限。

## 将迁移标记为成功

将挂起的迁移标记为已成功执行，以防止它们被 `db:migrate` 任务执行。在确定可以安全跳过失败的迁移后，使用此 API 来跳过失败的迁移。

```plaintext
POST /api/v4/admin/migrations/:version/mark
```

| 参数         | 类型      | 是否必需 | 描述                                                                       |
|------------|---------|------|--------------------------------------------------------------------------|
| `version`  | integer | yes  | 要跳过的迁移的版本时间戳                        |
| `database` | string  | no   | 跳过迁移的数据库名称。默认为 `main` |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
   --url "https://gitlab.example.com/api/v4/admin/migrations/:version/mark"
```
