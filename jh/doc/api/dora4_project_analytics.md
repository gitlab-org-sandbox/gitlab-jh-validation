---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, api
remove_date: '2022-05-18'
redirect_to: 'dora/metrics.md'
---

# DORA4 分析项目 API (已删除) **(ULTIMATE)**

WARNING:
此功能已在极狐Gitlab 13.11 中废弃，并在极狐GitLab 14.0 中删除。请改用 [DORA 指标 API](dora/metrics.md)。