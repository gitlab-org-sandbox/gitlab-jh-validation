---
stage: Govern
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 依赖项列表导出 API **(ULTIMATE ALL)**

对此端点的每次调用都需要身份验证。

## 创建流水线级别的依赖项列表导出 **(EXPERIMENT)**

> 引入于极狐GitLab 16.4，[功能标志](../administration/feature_flags.md)为 `merge_sbom_api`。默认启用。此功能处于[实验](../policy/experiment-beta-support.md#experiment)阶段。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。
要隐藏该功能，管理员可以禁用名为 `merge_sbom_api` 的[功能标志](../administration/feature_flags.md)。
在 JihuLab.com 上，此功能可用。

WARNING:
此功能是一个[实验性](../policy/experiment-beta-support.md#experiment)功能，如有更改，恕不另行通知。

为流水线中检测到的所有项目依赖项创建新的 CycloneDX JSON 导出。

如果经过身份验证的用户没有[读取依赖项](../user/permissions.md#custom-role-requirements)的权限，则此请求将返回 `403 Forbidden` 状态码。

SBOM 导出只能由导出作者访问。

```plaintext
POST /pipelines/:id/dependency_list_exports
```

| 参数            | 类型      | 是否必需 | 描述                                                                                |
|---------------|---------|------|-----------------------------------------------------------------------------------|
| `id`          | integer | yes  | 经过身份验证的用户可以访问的流水线的 ID  |
| `export_type` | string  | yes  | 必须设置为 `sbom`                                                                      |

```shell
curl --request POST --header "PRIVATE-TOKEN: <private_token>" "https://gitlab.example.com/api/v4/pipelines/1/dependency_list_exports" --data "export_type=sbom"
```

创建的依赖项列表导出 1 小时后自动删除。

响应示例：

```json
{
  "id": 2,
  "has_finished": false,
  "self": "http://gitlab.example.com/api/v4/dependency_list_exports/2",
  "download": "http://gitlab.example.com/api/v4/dependency_list_exports/2/download"
}
```

## 获取单个依赖项列表导出

获取单个依赖项列表导出。

```plaintext
GET /security/dependency_list_exports/:id
```

| 参数   | 类型      | 是否必需 | 描述                                        |
|------|---------|------|-------------------------------------------|
| `id` | integer | yes  | 依赖项列表导出 ID|

```shell
curl --header "PRIVATE-TOKEN: <private_token>" "https://gitlab.example.com/api/v4/security/dependency_list_exports/2"
```

正在生成依赖项列表导出时，状态码为 `202 Accepted`；准备就绪时，状态码为 `200 OK`。

响应示例：

```json
{
  "id": 4,
  "has_finished": true,
  "self": "http://gitlab.example.com/api/v4/dependency_list_exports/4",
  "download": "http://gitlab.example.com/api/v4/dependency_list_exports/4/download"
}
```

## 下载依赖项列表导出

下载单个依赖项列表导出。

```plaintext
GET /security/dependency_list_exports/:id/download
```

| 参数   | 类型      | 是否必需 | 描述                                    |
|------|---------|------|---------------------------------------|
| `id` | integer | yes  | 依赖项列表导出 ID|

```shell
curl --header "PRIVATE-TOKEN: <private_token>" "https://gitlab.example.com/api/v4/security/dependency_list_exports/2/download"
```

当依赖项列表导出未完成或未找到，响应为 `404 Not Found`。

响应示例：

```json
{
  "bomFormat": "CycloneDX",
  "specVersion": "1.4",
  "serialNumber": "urn:uuid:aec33827-20ae-40d0-ae83-18ee846364d2",
  "version": 1,
  "metadata": {
    "tools": [
      {
        "vendor": "Gitlab",
        "name": "Gemnasium",
        "version": "2.34.0"
      }
    ],
    "authors": [
      {
        "name": "Gitlab",
        "email": "support@gitlab.com"
      }
    ],
    "properties": [
      {
        "name": "gitlab:dependency_scanning:input_file",
        "value": "package-lock.json"
      }
    ]
  },
  "components": [
    {
      "name": "com.fasterxml.jackson.core/jackson-core",
      "purl": "pkg:maven/com.fasterxml.jackson.core/jackson-core@2.9.2",
      "version": "2.9.2",
      "type": "library",
      "licenses": [
        {
          "license": {
            "id": "MIT",
            "url": "https://spdx.org/licenses/MIT.html"
          }
        },
        {
          "license": {
            "id": "BSD-3-Clause",
            "url": "https://spdx.org/licenses/BSD-3-Clause.html"
          }
        }
      ]
    }
  ]
}

```
