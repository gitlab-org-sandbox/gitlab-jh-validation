---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 成员角色 API **(ULTIMATE ALL)**

> - 引入于极狐GitLab 15.4。[部署在 `customizable_roles` 功能标志后](../administration/feature_flags.md)，默认禁用。
> - 默认启用于极狐GitLab 15.9。
> - 读取漏洞添加于极狐GitLab 16.0。
> - 管理漏洞添加于极狐GitLab 16.1。
> - 读取依赖项添加于极狐GitLab 16.3。
> - 名称和描述字段添加于极狐GitLab 16.3。
> - 管理合并请求引入于极狐GitLab 16.4，[功能标志](../administration/feature_flags.md)为 `admin_merge_request`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。为了使其可用，管理员可以启用名为 `admin_merge_request` 的[功能标志](../administration/feature_flags.md)。
在 JihuLab.com 上，此功能不可用。

<a id="list-all-member-roles-of-a-group"></a>

## 列出群组的所有成员角色

获取经过身份验证的用户可查看的群组成员角色列表。

```plaintext
GET /groups/:id/member_roles
```

| 参数   | 类型             | 是否必需 | 描述                                                                                                       |
|------|----------------|------|----------------------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes) 及以下响应参数：

| 参数                       | 类型      | 描述                                                        |
|:-------------------------|:--------|:----------------------------------------------------------|
| `[].id`                  | integer | 成员角色 ID                                                   |
| `[].name`                | string  | 成员角色的名称                                                   |
| `[].description`         | string  | 成员角色的描述                                                   |
| `[].group_id`            | integer | 成员角色所属群组的 ID                                              |
| `[].base_access_level`   | integer | 成员角色的基本访问级别。有效值为 10（访客）、20（报告者）、30 （开发者）、40（维护者）或 50（所有者） |
| `[].admin_merge_request` | boolean | 管理项目合并请求的权限并启用 `download_code`                            |
| `[].admin_vulnerability` | boolean | 管理项目漏洞的权限                                                 |
| `[].read_code`           | boolean | 读取项目代码的权限                                                 |
| `[].read_dependency`     | boolean | 读取项目依赖项的权限                                                |
| `[].read_vulnerability`  | boolean | 读取项目漏洞的权限                                                 |

请求示例：

```shell
curl --header "Authorization: Bearer <your_access_token>" "https://gitlab.example.com/api/v4/groups/84/member_roles"
```

响应示例：

```json
[
  {
    "id": 2,
    "name": "Custom + code",
    "description: "Custom guest that can read code",
    "group_id": 84,
    "base_access_level": 10,
    "admin_merge_request": false,
    "admin_vulnerability": false,
    "read_code": true,
    "read_dependency": false,
    "read_vulnerability": false
  },
  {
    "id": 3,
    "name": "Guest + security",
    "description: "Custom guest that read and admin security entities",
    "group_id": 84,
    "base_access_level": 10,
    "admin_merge_request": false,
    "admin_vulnerability": true,
    "read_code": false,
    "read_dependency": true,
    "read_vulnerability": true
  }
]
```

<a id="add-a-member-role-to-a-group"></a>

## 向群组添加成员角色

> 创建自定义角色时添加名称和描述的功能引入于极狐GitLab 16.3。

向群组添加成员角色

```plaintext
POST /groups/:id/member_roles
```

要向群组添加成员角色，该群组必须处于 root 级别（没有父群组）。

| 参数                    | 类型             | 是否必需 | 描述                                                                       |
|-----------------------|----------------|------|--------------------------------------------------------------------------|
| `id`                  | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name`                | string         | yes  | 成员角色的名称                                                                  |
| `description`         | string         | no   | 成员角色的描述                                                                  |
| `base_access_level`   | integer        | yes  | 配置角色的基本访问级别。有效值为 10（访客）、20（报告者）、30 （开发者）、40（维护者）或 50（所有者）                |
| `admin_merge_request` | boolean        | no   | 管理项目合并请求的权限                                                              |
| `admin_vulnerability` | boolean        | no   | 管理项目漏洞的权限                                                                |
| `read_code`           | boolean        | no   | 读取项目代码的权限                                                                |
| `read_dependency`     | boolean        | no   | 读取项目依赖项的权限                                                               |
| `read_vulnerability`  | boolean        | no   | 读取项目漏洞的权限                                                                |

如果成功，则返回 [`201`](rest/index.md#status-codes) 及以下参数：

| 参数                    | 类型      | 描述            |
|:----------------------|:--------|:--------------|
| `id`                  | integer | 成员角色的 ID      |
| `name`                | string  | 成员角色的名称       |
| `description`         | string  | 成员角色的描述       |
| `group_id`            | integer | 成员角色所属的群组的 ID |
| `base_access_level`   | integer | 成员角色的基本访问级别   |
| `admin_merge_request` | boolean | 管理项目合并请求的权限   |
| `admin_vulnerability` | boolean | 管理项目漏洞的权限     |
| `read_code`           | boolean | 读取项目代码的权限     |
| `read_dependency`     | boolean | 读取项目依赖项的权限    |
| `read_vulnerability`  | boolean | 读取项目漏洞的权限     |

请求示例：

```shell
 curl --request POST --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" --data '{"name" : "Custom guest", "base_access_level" : 10, "read_code" : true}' "https://gitlab.example.com/api/v4/groups/84/member_roles"
```

响应示例：

```json
{
  "id": 3,
  "name": "Custom guest",
  "description": null,
  "group_id": 84,
  "base_access_level": 10,
  "admin_merge_requests": false,
  "admin_vulnerability": false,
  "read_code": true,
  "read_dependency": false,
  "read_vulnerability": false
}
```

<a id="remove-member-role-of-a-group"></a>

### 移除群组的成员角色

移除群组的成员角色。

```plaintext
DELETE /groups/:id/member_roles/:member_role_id
```

| 参数               | 类型             | 是否必需 | 描述                                                                       |
|------------------|----------------|------|--------------------------------------------------------------------------|
| `id`             | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `member_role_id` | integer        | yes  | 成员角色的 ID                                              |

如果成功，则返回 [`204`](rest/index.md#status-codes) 及空响应。

请求示例：

```shell
curl --request DELETE --header "Content-Type: application/json" --header "Authorization: Bearer <your_access_token>" "https://gitlab.example.com/api/v4/groups/84/member_roles/1"
```
