---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
remove_date: '2023-02-14'
redirect_to: 'feature_flags.md'
---

# 功能标志 Specs API （移除） **(PREMIUM)**

> 引入于极狐GitLab 12.5。

此 API 移除于 14.0。
现在使用[新 API](feature_flags.md)。 
