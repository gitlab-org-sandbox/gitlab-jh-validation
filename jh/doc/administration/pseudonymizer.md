---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Pseudonymizer（已删除） **(ULTIMATE)**

> 废弃于 14.7 版本并删除于 15.0 版本。

WARNING:
此功能废弃于 14.7 版本。
