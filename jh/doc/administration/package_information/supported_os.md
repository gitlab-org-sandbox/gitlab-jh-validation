---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---
# 支持的操作系统 **(BASIC SELF)**

极狐GitLab 正式支持 LTS 版本的操作系统。虽然像 Ubuntu 这样的操作系统在 LTS 和非 LTS 版本之间有明显的区别，但还有其他操作系统，例如 openSUSE，不遵循 LTS 概念。因此，为避免混淆，在任何时间点，极狐GitLab 支持的所有操作系统都列在[安装页面](https://gitlab.cn/install/)中。

下面列出了当前支持的操作系统及其可能的 EOL 日期。

NOTE:
`amd64` 和 `x86_64` 指的是相同的 64 位架构。`arm64` 和 `aarch64` 也可以互换，并且指的是相同的架构。

| OS 版本 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               | 首个支持的极狐GitLab 版本 | 架构            |                         安装文档                         | OS EOL  | 详情                                                      |
| ----------------------| ---------------------- | -------------- | :----------------------------------------------------------: |---------| ------------------------------------------------------------ |
| AlmaLinux 8           | 14.5.0   | x86_64、aarch64 | [安装文档](https://gitlab.cn/install/#almalinux-8) | 2029    | [AlmaLinux OS](https://almalinux.org/)|
| AlmaLinux 9           | 16.7.0   | x86_64、aarch64 | [安装文档](https://gitlab.cn/install/#almalinux-9) | 2032    | [AlmaLinux OS](https://almalinux.org/)|
| Debian 10             | 13.10.0   | amd64、arm64   | [安装文档](https://gitlab.cn/install/#debian) | 2024    | [LTS - Debian Wiki](https://wiki.debian.org/LTS)|
| Debian 11             | 14.6.0   | amd64、arm64    | [安装文档](https://gitlab.cn/install/#debian) | 2026    | [LTS - Debian Wiki](https://wiki.debian.org/LTS)|
| Debian 12             | 16.7.0   | amd64、arm64    | [安装文档](https://gitlab.cn/install/#debian) | 2028    | [LTS - Debian Wiki](https://wiki.debian.org/LTS)|
| RHEL 8                | 13.10.0   | x86_64、arm64   | [安装文档](https://gitlab.cn/install/#centos-7) | 2029.05 | [Red Hat Customer Portal](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates) |
| RHEL 9                | 16.7.0   | x86_64、arm64   | [安装文档](https://gitlab.cn/install/#centos-7) | 2032.05   | [Red Hat Customer Portal](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates) |
| Oracle Linux          | 13.10.0   | x86_64          | [安装文档](https://gitlab.cn/install/#centos-7) | 2024.07 | [Lifetime Support Policy](https://www.oracle.com/a/ocom/docs/elsp-lifetime-069338.pdf)                    |
| Scientific Linux      | 13.10.0   | x86_64          | [安装文档](https://gitlab.cn/install/#centos-7) | 2024.06 | [SL7 - Scientific Linux](https://scientificlinux.org/downloads/sl-versions/sl7/)|
| Ubuntu 20.04          | 13.10.0   | amd64、arm64    | [安装文档](https://gitlab.cn/install/#ubuntu) | 2025.04 | [Releases - Ubuntu Wiki](https://wiki.ubuntu.com/Releases)|
| Ubuntu 22.04          | 15.5.0   | amd64、arm64    | [安装文档](https://gitlab.cn/install/#ubuntu) | 2027.04 | [Releases - Ubuntu Wiki](https://wiki.ubuntu.com/Releases)|
| Ubuntu 24.04          | 17.1.0   | amd64、arm64    | [安装文档](https://gitlab.cn/install/#ubuntu) | 2029.04 | [Releases - Ubuntu Wiki](https://wiki.ubuntu.com/Releases)|
| 欧拉（openEuler）      | 15.5.4   | x86_64、aarch64  | [软件包](https://packages.gitlab.cn/#browse/search=keyword%3Daarch64%20AND%20format%3Dyum:c7545579a7a1539017d950a48b58bacb) | - | [openEuler 社区官网](https://www.openeuler.org)                           |
| 麒麟（KylinOS）        | 15.2.0   | arm64、x86_64    | [软件包](https://packages.gitlab.cn/#browse/search/apt=format%3Dapt%20AND%20group.raw%3Darm64:08909bf0c86cf6c9c697ec801486dcdb) | - | [麒麟软件官方网站](https://www.kylinos.cn/)|
| Tencent OpenCloudOS 8 | 16.11.0   | amd64    | [安装文档](https://gitlab.cn/install) | - | [OpenCloudOS 简介](https://www.tencentcloud.com/zh/document/product/213/46209)|
| Alibaba Cloud Linux3  | 16.11.0   | amd64    | [安装文档](https://gitlab.cn/install) | - | [Alibaba Cloud Linux](https://help.aliyun.com/zh/alinux/)|

NOTE:
CentOS 8 于 2021 年 12 月 31 日 EOL。<!--In GitLab 14.5 and later,
[CentOS builds work in AlmaLinux](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/954#note_730198505).
We will officially support all distributions that are binary compatible with Red Hat Enterprise Linux.
This gives users a path forward for their CentOS 8 builds at its end of life.-->

<!--
## Update GitLab package sources after upgrading the OS

After upgrading the Operating System (OS) as per its own documentation,
it may be necessary to also update the GitLab package source URL
in your package manager configuration.
If your package manager reports that no further updates are available,
although [new versions have been released](https://about.gitlab.com/releases/categories/releases/), repeat the
"Add the GitLab package repository" instructions
of the [Linux package install guide](https://about.gitlab.com/install/#content).
Future GitLab upgrades will now be fetched according to your upgraded OS.
-->

## 适用于 ARM64 的软件包

极狐GitLab 为某些支持的操作系统提供了 arm64/aarch64 软件包。
您可以在上表中查看您的操作系统架构是否受支持。

WARNING:
目前在 ARM 上运行极狐GitLab 仍然存在一些已知问题和限制<!--[已知问题和限制](https://gitlab.com/groups/gitlab-org/-/epics/4397)-->。

## 不再支持的操作系统

极狐GitLab 为操作系统提供 Linux 软件包，直到它们的 EOL（生命周期结束）为止。在操作系统 EOL 日期之后，极狐GitLab 将停止发布官方软件包。下面是不再支持的操作系统列表，以及对应的极狐GitLab 最后支持它们的版本：

| OS 版本      | OS EOL | 极狐GitLab 最后支持的版本 |
| ----------- | ------ | --- |
| Ubuntu 16.04 | [2021.04](https://ubuntu.com/info/release-end-of-life) | [13.12](https://packages.gitlab.cn/#browse/search=keyword%3Dgitlab-jh%20AND%20version%3D13.12*%20AND%20format%3Dapt%20AND%20repository_name%3Dubuntu-xenial) |
| Debian 9   | [2022.06](https://lists.debian.org/debian-lts-announce/2022/07/msg00002.html)        | [15.2](https://packages.gitlab.cn/#browse/search=keyword%3Dgitlab-jh%20AND%20version%3D15.2.*%20AND%20format%3Dapt%20AND%20repository_name%3Ddebian-stretch) |
| CentOS 7  | [2024.06](https://www.centos.org/centos-linux-eol/)        | [17.1](https://packages.gitlab.cn/#browse/search=keyword%3Dgitlab-jh%20AND%20version%3D17.1.*el8%20AND%20format%3Dyum%20AND%20repository_name%3Del) |
| CentOS 8  | [2021.12](https://www.centos.org/centos-linux-eol/)        | [14.6](https://packages.gitlab.cn/#browse/search=keyword%3Dgitlab-jh%20AND%20version%3D14.6.*el8%20AND%20format%3Dyum%20AND%20repository_name%3Del) |
| Ubuntu 18.04  | [2023.04](https://ubuntu.com/about/release-cycle)        | [16.11](https://packages.gitlab.cn/#browse/search=keyword%3Dgitlab-jh%20AND%20version%3D16.11.*%20AND%20format%3Dapt) |
