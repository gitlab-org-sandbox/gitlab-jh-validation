---
stage: Analytics
group: Analytics Instrumentation
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用统计 **(BASIC SELF)**

极狐(GitLab) 定期收集有关您实例的信息，以执行各种操作。

您可以[选择禁用](#enable-or-disable-usage-statistics)使用统计功能。

## Service Ping

Service Ping 是一个每周收集有效负载并将其发送到极狐(GitLab) 的流程。
<!--更多信息请参见 [Service Ping 指南](../../development/internal_analytics/service_ping/index.md)。-->启用 Service Ping 后，极狐GitLab 会从其他实例收集数据并启用依赖于 Service Ping 的某些[实例级分析功能](../../user/analytics/index.md)。

### 为什么要启用 Service Ping？

Service Ping 的主要目的是构建更好的极狐GitLab。我们收集有关如何使用极狐GitLab 的数据来了解功能或阶段的采用和使用情况。这些数据让我们深入了解极狐GitLab 如何增加价值，并帮助我们的团队了解人们使用极狐GitLab 的原因，以便做出更好的产品决策。

启用 Service Ping 还有其他几个好处：

- 分析极狐GitLab 安装期间用户的活动。
- 提供 [DevOps 分数](../analytics/dev_ops_reports.md)，让您了解整个实例从规划到监控的并发 DevOps 的使用情况概览。
- 提供更主动的支持（假设我们的客户成功经理和支持组织使用数据来提供更多价值）。
- 提供有关如何从极狐GitLab 投资中获得最大价值的见解和建议。
- 提供报告，显示与其他类似组织（匿名）的比较情况，并提供有关如何改进 DevOps 流程的具体意见和建议。
<!--- 参与我们的[注册功能计划](#registration-features-program)以获得免付费功能。

<a id="registration-features-program"></a>

## Registration Features Program

> Introduced in GitLab 14.1.

In GitLab versions 14.1 and later, GitLab Free customers with a self-managed instance running
GitLab Enterprise Edition can receive paid features by registering with GitLab and sending us
activity data through Service Ping. Features introduced here do not remove the feature from its paid
tier. Users can continue to access the features in a paid tier without sharing usage data.


### 14.1 及更高版本中的可用功能

- [从极狐GitLab 发送邮件](../email_from_gitlab.md)

### 14.4 及更高版本中的可用功能

- [仓库大小限制](../../administration/settings/account_and_limit_settings.md#repository-size-limit).
- [根据 IP 地址的群组访问限制](../../user/group/access_and_permissions.md#restrict-group-access-by-ip-address).

### 16.0 及更高版本中的可用功能

- [查看描述更改历史](../../user/discussions/index.md#view-description-change-history)
- [维护模式](../maintenance_mode/index.md)
- [可配置的议题看板](../../user/project/issue_board.md#configurable-issue-boards)
- [覆盖率引导的模糊测试](../../user/application_security/coverage_fuzzing/index.md)
- [密码复杂度要求](../../administration/settings/sign_up_restrictions.md#password-complexity-requirements)

NOTE:
参与尚不需要注册，但可能会在未来的里程碑中添加。

### 启用注册功能

1. 以具有管理员访问权限的用户身份登录。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 指标和分析**。
1. 展开 **使用情况统计** 部分。
1. 如果未启用，请选中 **启用服务 Ping** 复选框。
1. 选择 **启用注册功能** 复选框。
1. 选择 **保存更改**。

-->

## 版本检查

如果启用，版本检查会通过状态通知您新版本及其重要性。状态显示在所有经过身份验证的用户的帮助页面 (`/help`) 和管理中心页面上。状态为：

- 绿色：您正在运行最新版本的极狐GitLab。
- 橙色：您正在运行的不是最新版本的极狐GitLab，您可以更新到最新版本。
- 红色：您运行的极狐GitLab 版本容易受到攻击。您应该尽快安装带有安全修复程序的最新版本。

- ![Orange version check example](../settings/img/update-available.png)

极狐(GitLab) 收集您实例的版本和主机名（通过 HTTP Referer）作为版本检查的一部分。不收集其他信息。

此外，此信息还用于确定补丁必须向后移植到哪些版本，以确保活动的极狐GitLab 实例安全可靠。

如果您[禁用版本检查](#enable-or-disable-usage-statistics)，则不会收集此信息。

### 请求流程示例

以下示例显示了私有化部署的极狐GitLab 实例和极狐GitLab 版本应用程序之间的基本请求/响应流程：

```mermaid
sequenceDiagram
    participant GitLab instance
    participant Version Application
    GitLab instance->>Version Application: Is there a version update?
    loop Version Check
        Version Application->>Version Application: Record version info
    end
    Version Application->>GitLab instance: Response (PNG/SVG)
```

## 配置您的网络

要将使用情况统计信息发送到极狐(GitLab)，您必须允许网络流量从极狐GitLab 实例通往端口 `443` 上的主机 `version.gitlab.com`。

如果您的极狐GitLab 实例位于代理后面，请设置适当的[代理配置变量](https://docs.gitlab.cn/omnibus/settings/environment-variables.html)。

<a id="enable-or-disable-usage-statistics"></a>

## 启用或禁用使用情况统计

启用或禁用服务 Ping 和版本检查：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 指标和分析**。
1. 展开 **使用情况统计**。
1. 选中或清除 **启用版本检查** 和 **启用服务 Ping** 复选框。
1. 选择 **保存更改**。

NOTE:
服务 Ping 设置仅控制数据是与极狐GitLab 共享还是仅在内部使用。
即使您禁用 Service Ping，`gitlab_service_ping_worker` 后台作业仍会定期为您的实例生成 Service Ping 有效负载。
有效负载可在[服务使用数据](#manually-upload-service-ping-payload)管理部分中找到。

## 使用配置文件禁用使用统计

NOTE:
在极狐GitLab 配置文件中禁用 Service Ping 的方法在 13.10 到 13.12.3 版本中不起作用。
<!--For more information about how to disable it, see [troubleshooting](../../development/internal_analytics/service_ping/troubleshooting.md#cannot-disable-service-ping-with-the-configuration-file).-->

要禁用 Service Ping 并防止将来通过管理中心进行配置：

**对于使用 Linux 软件包进行安装：**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['usage_ping_enabled'] = false
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**对于源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     # ...
     gitlab:
       # ...
       usage_ping_enabled: false
   ```

1. 重启极狐GitLab：

   ```shell
   sudo service gitlab restart
   ```

## 查看 Service Ping 负载

您可以在管理中心中查看发送到极狐(GitLab) 的确切 JSON 负载。查看负载：

1. 以具有管理员访问权限的用户身份登录。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 指标和分析**。
1. 展开 **使用情况统计** 部分。
1. 选择 **预览负载**。

<!--For an example payload, see [Example Service Ping payload](../../development/internal_analytics/service_ping/index.md#example-service-ping-payload).-->

<a id="manually-upload-service-ping-payload"></a>

## 手动上传 Service Ping 负载

> - 引入于极狐GitLab 14.8，功能标志为 `admin_application_settings_service_usage_data_center`。默认禁用。
> - 功能标志移除于极狐GitLab 14.10。

即使您的实例没有互联网访问权限，或者 Service Ping cron 作业未启用，您也可以将 Service Ping 负载上传到极狐GitLab。

手动上传负载：

1. 以具有管理员访问权限的用户身份登录。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 服务** 使用数据。
1. 选择 **下载负载**。
1. 保存 JSON 文件。
1. 访问[服务使用数据中心](https://version.gitlab.cn/usage_data/new)。
1. 选择 **选择文件** 并从 p5 中选择文件。
1. 选择 **上传**。

上传的文件经过加密并使用安全的 HTTPS 协议发送。HTTPS 在 Web 浏览器和服务器之间创建安全通信通道，并保护传输的数据免受中间人攻击。

<!--If there are problems with the manual upload:

1. Open a confidential issue in the [security fork of version app project](https://gitlab.com/gitlab-org/security/version.gitlab.com).
1. Attach the JSON payload if possible.
1. Tag `@gitlab-org/analytics-section/analytics-instrumentation` who will triage the issue. -->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
