---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 服务条款和隐私政策 **(BASIC SELF)**

管理员可以强制接受服务条款和隐私政策。
启用此选项后，新用户和现有用户必须接受这些条款。

启用后，您可以在实例的 `-/users/terms` 页面查看服务条款，例如 `https://gitlab.example.com/-/users/terms`。

## 执行服务条款和隐私政策

为了强制接受服务条款和隐私政策：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **服务条款和隐私政策** 部分。
1. 选中 **所有用户必须接受服务条款和隐私政策才能访问极狐GitLab** 复选框。
1. 输入 **服务条款和隐私政策** 的文本。您可以在文本框中使用 [Markdown](../../user/markdown.md)。
1. 选择 **保存更改**。

对于条款的每次更新，都会存储一个新版本。当用户接受或拒绝条款时，极狐GitLab 会记录他们接受或拒绝的版本。

现有用户必须在其与极狐GitLab 下次交互时接受这些条款。
如果经过身份验证的用户拒绝这些条款，他们将被退出登录。

启用后，系统会向新用户的注册页面添加一个必选复选框：

![Sign up form](img/sign_up_terms.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
