---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: index
---

# 管理中心设置 **(BASIC SELF)**

作为极狐GitLab 私有化部署实例的管理员，您可以管理您部署的行为。

在 JihuLab.com 上无法访问 **管理中心**，并且只能由 JihuLab.com 管理员更改设置。
对于 JihuLab.com 实例的设置和限制，您可以阅读 [JihuLab.com 设置](../../user/jihulab_com/index.md)。

## 访问管理中心

访问 **管理中心**：

1. 以管理员的身份登录进入极狐GitLab 实例。
1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置**，然后选择要查看的设置群组：
    - [概览](#general)
    - [Geo](#geo)
    - [CI/CD](#cicd)
    - [集成](#integrations)
    - [指标与分析](#metrics-and-profiling)
    - [网络](#network)
    - [偏好设置](#preferences)
    - [报告](#reporting)
    - [仓库](#repository)
    - [模板](#templates)

<a id="general"></a>

### 概览

**概览** 设置包括：

- [可见性和访问控制](../settings/visibility_and_access_controls.md) - 设置默认值并限制可见性级别。配置导入源和 Git 访问协议。
- [账户和限制](../settings/account_and_limit_settings.md) - 设置项目和最大大小限制、会话持续时间、用户选项，并检查命名空间计划的功能可用性。
- [差异限制](../diff_limits.md) - 差异内容限制。
- [注册限制](../settings/sign_up_restrictions.md) - 配置用户创建新账户的方式。
- [登录限制](../settings/sign_in_restrictions.md) - 设置用户登录的要求。
  启用强制双重身份验证。
- [服务条款和隐私政策](../settings/terms.md) - 包括所有用户都必须接受的服务条款协议和隐私政策。
- [外部身份验证](../../administration/settings/external_authorization.md#configuration) - 外部分类政策授权。
- [网页终端](../integration/terminal.md#limiting-websocket-connection-time) -
  设置网页终端的最大会话时间。
- [FLoC](floc.md) - 启用或禁用群组联合学习 (FLoC) 跟踪。
- [GitLab for Slack 应用](../../user/admin_area/settings/slack_app.md) - 启用和配置 GitLab for Slack 应用。

<a id="cicd"></a>

### CI/CD

**CI/CD** 设置包括：

- [持续集成和部署](../../administration/settings/continuous_integration.md) -
  自动开发运维、Runner 和作业产物。
- [必要流水线配置](../../administration/settings/continuous_integration.md#required-pipeline-configuration) -
  设置实例范围内的自动包含的[流水线配置](../../ci/yaml/index.md)。
  该流水线配置是在项目自己的配置之后运行的。
- [软件包库](../../administration/settings/continuous_integration.md#package-registry-configuration) -
  与极狐GitLab 软件包库的使用和体验相关的设置。
  涉及启用这些设置的一些[风险](../../user/packages/container_registry/reduce_container_registry_storage.md#use-with-external-container-registries)。

## 安全和合规设置

- [许可证合规设置](security_and_compliance.md#choose-package-registry-metadata-to-sync)：通过镜像库类型启用或禁用软件包元数据同步。

<a id="geo"></a>

### Geo **(PREMIUM SELF)**

**Geo** 设置包括：

- [Geo](../geo/index.md) - 将您的极狐GitLab 实例复制到其他地理位置。
<!--Redirects to **Admin Area > Geo > Settings** are no
  longer available at **Admin Area > Settings > Geo** in [GitLab 13.0](https://gitlab.com/gitlab-org/gitlab/-/issues/36896).-->

<a id="integrations"></a>

### 集成

**集成** 设置包括：

- [Elasticsearch](../../integration/advanced_search/elasticsearch.md#enable-advanced-search) -
  Elasticsearch 集成。Elasticsearch AWS IAM。
- [Kroki](../integration/kroki.md#enable-kroki-in-gitlab) -
  允许使用 [kroki.io](https://kroki.io) 在 AsciiDoc 和 Markdown 文档中渲染图表。
- [Mailgun](../integration/mailgun.md) - 启用您的极狐GitLab 实例接收来自 Mailgun 的邀请电子邮件退回事件（如果它是您的电子邮件提供商）。
- [PlantUML](../integration/plantuml.md) - 允许在文档中渲染 PlantUML 图表。
- [客户体验改善和第三方优惠](../settings/third_party_offers.md) -
  控制客户体验改善内容和第三方优惠的显示。
- [Snowplow](../../development/internal_analytics/snowplow/index.md) - 配置 Snowplow 集成。
- [Google GKE](../../user/project/clusters/add_gke_clusters.md) - 您可以使用 Google GKE 集成从极狐GitLab 预配 GKE 集群。
- [Amazon EKS](../../user/project/clusters/add_eks_clusters.md) - 您可以使用 Amazon EKS 集成从极狐GitLab 预配 EKS 集群。

<a id="metrics-and-profiling"></a>

### 指标与分析

**指标与分析** 设置包括：

- [指标 - Prometheus](../monitoring/prometheus/gitlab_metrics.md) -
  启用并配置 Prometheus 指标。
- [指标 - Grafana](../monitoring/performance/grafana_configuration.md#integrate-with-gitlab-ui) -
  启用并配置 Grafana。
- [分析 - 性能栏](../monitoring/performance/performance_bar.md#enable-the-performance-bar-for-non-administrators) -
  允许特定群组中的非管理员用户访问性能栏。
- [使用情况统计](../settings/usage_statistics.md) - 启用或禁用版本检查和服务 Ping。

<a id="network"></a>

###  网络

**网络** 设置包括：

- 性能优化 - 影响极狐GitLab 性能的各种设置，包括：
    - [写入 `authorized_keys` 文件](../operations/fast_ssh_key_lookup.md#set-up-fast-lookup)。
    - [推送事件活动限制和批量推送事件](../settings/push_event_activities_limit.md)。
- [用户和 IP 速率限制](../settings/user_and_ip_rate_limits.md) - 配置 Web 和 API 请求的限制。
  这些速率限制可以被覆盖：
    - [软件包库速率限制](../settings/package_registry_rate_limits.md) - 为取代用户和 IP 速率限制的 Packages API 请求配置特定限制。
    - [Git LFS 速率限制](../settings/git_lfs_rate_limits.md) - 为取代用户和 IP 速率限制的 Git LFS 请求配置特定限制。
    - [文件 API 速率限制](../settings/files_api_rate_limits.md) - 为取代用户和 IP 速率限制的文件 API 请求配置特定限制。
    - [搜索速率限制](../instance_limits.md#search-rate-limit) - 为经过身份验证和未经身份验证的用户配置全局搜索请求速率限制。
    - [已废弃的 API 速率限制](../settings/deprecated_api_rate_limits.md) - 为取代用户和 IP 速率限制的废弃 API 请求配置特定限制。
- [出站请求](../../security/webhooks.md) - 允许从 webhook 和集成向本地网络发出请求，或拒绝所有出站请求。
- [受保护的路径](../settings/protected_pa​​ths.md) - 配置受 Rack Attack 保护的路径。
- [事件管理限制](../../operations/incident_management/index.md) - 限制可以发送到项目的入站告警数量。
- [备注创建限制](../settings/rate_limit_on_notes_creation.md) - 设置备注创建请求的速率限制。
- [获取单用户限制](../settings/rate_limit_on_users_api.md) - 在用户 API 端点上设置速率限制以按 ID 获取用户。
- [未经身份验证请求的项目 API 速率限制](../settings/rate_limit_on_projects_api.md) - 在项目列表 API 端点上设置未经身份验证请求的速率限制。

<a id="preferences"></a>

### 偏好设置

**偏好设置** 包括：

- [电子邮件](../settings/email.md) - 各种电子邮件设置。
- [新增功能](../whats-new.md) - 配置 **新增功能** 抽屉和内容。
- [帮助页面](help_page.md) - 帮助页面文本和支持页面 URL。
- [页面](../pages/index.md#custom-domain-verification) -
  静态网站的大小和域设置。
- [轮询间隔乘数](../polling.md) -
  配置极狐GitLab UI 轮询更新的频率。
- [Gitaly 超时](gitaly_timeouts.md) - 配置 Gitaly 超时。
- 本地化：
    - [默认一周的第一天](../../user/profile/preferences.md)
    - [时间跟踪](../../user/project/time_tracking.md#limit-displayed-units-to-hours)
- [Sidekiq 作业限制](../settings/sidekiq_job_limits.md) - 限制存储在 Redis 中的 Sidekiq 作业的大小。

<a id="reporting"></a>

### 报告

**报告** 设置包括：

- 垃圾邮件和反机器人保护：
    - 反垃圾邮件服务，例如 [reCAPTCHA](../../integration/recaptcha.md)、
      [Akismet](../../integration/akismet.md) 或 [Spamcheck](../reporting/spamcheck.md)。
    - [IP 地址限制](../reporting/ip_addr_restrictions.md)。
- [滥用报告](../review_abuse_reports.md) - 设置滥用报告的通知电子邮件。
- [Git 滥用率限制](../reporting/git_abuse_rate_limit.md) - 配置 Git 滥用率限制设置。 **(ULTIMATE SELF)**

<a id="repository"></a>

### 仓库

**仓库** 设置包括：

- [仓库的自定义初始分支名称](../../user/project/repository/branches/default.md#instance-level-custom-initial-branch-name) -
  为您的实例中创建的新仓库设置自定义分支名称。
- [仓库的初始默认分支保护](../../user/project/repository/branches/default.md#instance-level-default-branch-protection) -
  配置分支保护以应用于每个仓库的默认分支。
- [仓库镜像](visibility_and_access_controls.md#enable-project-mirroring) -
  配置仓库镜像。
- [仓库存储](../repository_storage_types.md) - 配置存储路径设置。
- 仓库维护：
    - [仓库检查](../repository_checks.md) - 配置仓库自动 Git 检查。
    - [Housekeeping](../housekeeping.md)。配置仓库自动 Git Housekeeping。
    - [删除非活动项目](../inactive_project_deletion.md)。配置非活动项目删除。
- [仓库静态对象](../static_objects_external_storage.md) -
  从外部存储（例如 CDN）提供仓库静态对象（例如 archives 和 blob）。 

<a id="templates"></a>

### 模板 **(PREMIUM SELF)**

**模板** 设置包括：

- [模板](instance_template_repository.md#configuration) - 设置实例范围的模板仓库。
- [自定义项目模板](../custom_project_templates.md) - 选择自定义项目模板源群组。

## 默认一周的第一天

您可以更改整个极狐GitLab 实例的[默认一周的第一天](../../user/profile/preferences.md)：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 滚动到 **本地化** 部分，然后选择您想要的一周的第一天。

## 默认语言

您可以更改整个极狐GitLab 实例的[默认语言](../../user/profile/preferences.md)：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 滚动到 **本地化** 部分，然后选择您所需的默认语言。
