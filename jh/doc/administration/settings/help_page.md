---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 自定义帮助和登录页面消息 **(BASIC SELF)**

在大型组织中，了解应该联系谁或去哪里寻求帮助非常有用。您可以在极狐GitLab `/help` 页面和极狐GitLab 登录页面上自定义并显示此类信息。

## 在帮助页面添加帮助消息

您可以添加帮助消息，该消息显示在极狐GitLab `/help` 页面的顶部（例如，
<https://gitlab.com/help>）：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **登录和帮助页面**。
1. 在 **在帮助页面上显示的附加文本** 中，输入您想要在 `/help` 上显示的信息。
1. 选择 **保存更改**。

您现在可以在 `/help` 上看到该消息。

NOTE:
默认情况下，未经身份验证的用户可以看到 `/help`。然而，如果[**公共**可见性级别](visibility_and_access_controls.md#restrict-visibility-levels)受到限制，`/help` 仅对经过身份验证的用户可见。

## 在登录页面添加帮助消息

您可以在极狐GitLab 登录页面添加帮助消息。该消息显示在登录页面上：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **登录和帮助页面**。
1. 在 **要在登录页面上显示的附加文本** 中，输入您想要显示在登录页面上的信息。
1. 选择 **保存更改**。

您现在可以在登录页面上看到该消息。

## 在帮助页面中隐藏与营销相关的条目

与极狐GitLab 营销相关的条目偶尔会显示在帮助页面上。要隐藏这些条目：

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **登录和帮助页面**。
1. 勾选 **在帮助页面隐藏与市场营销相关的条目** 复选框。
1. 选择 **保存更改**。

## 设置自定义支持页面 URL

您可以指定用户在以下情况下被跳转到的自定义 URL：

- 从帮助下拉列表中选择 **支持**。
- 在帮助页面上选择 **查看我们的网站以获取帮助**。

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **登录和帮助页面**。
1. 在 **支持页面 URL** 字段中，输入 URL。
1. 选择 **保存更改**。

## 重定向 `/help` 页面

> - 功能标志 `help_page_documentation_redirect` 移除于极狐GitLab 14.4。
> - 一般可用于极狐GitLab 14.4。

您可以将所有 `/help` 链接重定向到满足[必要要求](#destination-requirements)的目的地。

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **登录和帮助页面**。
1. 在 **文档页面 URL** 字段中，输入 URL。
1. 选择 **保存更改**。

如果“文档页面 URL”字段为空，极狐GitLab 实例将显示源自 [`doc` 目录](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/doc)的基本文档版本。

<a id="destination-requirements"></a>

### 目的地要求

当重定向 `/help` 时，极狐GitLab：

- 将请求重定向到指定的 URL。
- 将 `ee` 和文档路径（包括版本号）附加到 URL。
- 将 `.html` 附加到 URL，并在必要时删除 `.md`。

例如，如果 URL 设置为 `https://docs.gitlab.com`，则请求 `/help/administration/settings/help_page.md` 时将重定向到：
`https://docs.gitlab.com/${VERSION}/ee/administration/settings/help_page.html`。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
