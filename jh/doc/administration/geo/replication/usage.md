---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!-- Please update EE::GitLab::GeoGitAccess::GEO_SERVER_DOCS_URL if this file is moved) -->

# 使用 Geo 站点 **(PREMIUM SELF)**

设置[数据库复制并配置 Geo 节点](../index.md#setup-instructions)后，使用您最近的极狐GitLab 站点，就像使用主要站点一样。

您可以直接推送到**次要**站点（对于 HTTP、SSH，包括 Git LFS），请求被代理到主要站点。

推送到**次要**站点时看到的输出示例：

```shell
$ git push
remote:
remote: This request to a Geo secondary node will be forwarded to the
remote: Geo primary node:
remote:
remote:   ssh://git@primary.geo/user/repo.git
remote:
Everything up-to-date
```

NOTE:
如果您使用 HTTPS 而不是 [SSH](../../../user/ssh.md) 推送到辅助服务器，则不能在 URL 中存储凭据，例如 `user:password@URL`。相反，您可以将 [`.netrc` 文件](https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html) 用于类 Unix 操作系统或 `_netrc` 对于 Windows。在这种情况下，凭据存储为纯文本。如果您正在寻找一种更安全的方式来存储凭据，您可以使用 [Git 凭据存储](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)。

## 从 Geo 次要站点获取 Go 模块

Go 模块可以从次要站点中提取，但有许多限制：

- 需要 Git 配置（使用 `insteadOf`）从 Geo 次要站点获取数据。
- 对于私有项目，需要在 `~/.netrc` 中指定身份验证详细信息。

<!--
阅读[使用项目 `go get` 文档](../../../user/project/working_with_projects.md#fetch-go-modules-from-geo-secondary-sites)，了解更多信息。
-->
