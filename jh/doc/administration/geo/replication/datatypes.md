---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 支持的 Geo 数据类型 **(PREMIUM SELF)**

Geo 数据类型是一个或多个极狐GitLab 功能所需的特定数据类以存储相关信息。

为了使用 Geo 复制这些功能生成的数据，我们使用多种策略进行访问、传输和验证。

## 数据类型

我们区分以下三种数据类型：

- [Git 仓库](#git-repositories)
- [Blob](#blobs)
- [数据库](#database)

请参阅下表，了解我们复制的每个功能或组件、其相应的数据类型、复制和验证方法：

| 类型       | 功能/组件                              | 复制方法                                  | 验证方法             |
|:---------|:-----------------------------------|:--------------------------------------|:-----------------|
| 数据库 | PostgreSQL 中的应用数据                  | Native                                | Native               |
| 数据库 | Redis                              | 不适用 (*1*)                             | 不适用              |
| 数据库 | Elasticsearch                      |Native                                    | Native               |
| 数据库 | SSH 公钥                             | PostgreSQL 复制                         | PostgreSQL 复制    |
| Git      | 项目仓库                               | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Git      | 项目 Wiki 仓库                         | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Git      | 项目设计仓库                             | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Git      | 项目代码片段                             | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Git      | 私人代码片段                             | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Git      | 群组 Wiki 仓库                         | 具有 Gitaly 的 Geo                       | Gitaly 校验和       |
| Blob    | 用户上传 _（文件系统）_                      | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 用户上传 _（对象存储）_                      | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | LFS 对象 _（文件系统）_                    | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | LFS 对象 _（对象存储）_                    | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | CI 作业产物 _（文件系统）_                   | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | CI 作业产物 _（对象存储）_                   | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 归档 CI 构建跟踪 _（文件系统）_                | 具有 API 的 Geo                          | _未实施_            |
| Blob    | 归档 CI 构建跟踪 _（对象存储）_                | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 镜像库 _（文件系统）_                       | 具有 API 的 Geo/Docker API               | _SHA256 校验和_     |
| Blob    | 镜像库 _（对象存储）_                       | 具有 API/Managed 的 Geo/Docker API (*2*) | SHA256 校验和 (*3*) |
| Blob    | 软件包库 _（文件系统）_                      | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 软件包库 _（对象存储）_                      | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | Terraform 模块库 _（文件系统）_             | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | Terraform 模块库 _（对象存储）_             | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | Versioned Terraform State _（文件系统）_ | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | Versioned Terraform State _（对象存储）_ | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 外部合并请求差异 _（文件系统）_                  | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 外部合并请求差异 _（对象存储）_                  | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 流水线产物 _（文件系统）_                     | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 流水线产物 _（对象存储）_                     | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | Pages _（文件系统）_                     | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | Pages _（对象存储）_                     | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | CI 安全文件 _（文件系统）_                   | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | CI 安全文件 _（对象存储）_                   | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 事件指标镜像  _（文件系统）_                   | 具有 API/Managed 的 Geo                  | SHA256 校验和       |
| Blob    | 事件指标镜像  _（对象存储）_                   | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 告警指标镜像 _（文件系统）_                    | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 告警指标镜像 _（对象存储）_                    | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |
| Blob    | 依赖代理镜像 _（文件系统）_                    | 具有 API 的 Geo                          | SHA256 校验和       |
| Blob    | 依赖代理镜像 _（对象存储）_                    | 具有 API/Managed 的 Geo (*2*)            | SHA256 校验和 (*3*) |

- (*1*)：Redis 复制可用作具有 Redis sentinel 的 HA 的一部分。不在 Geo 站点之间使用。
- (*2*)：对象存储复制可以由 Geo 或您的对象存储提供商/设备本机复制功能执行。
- (*3*)：对象存储验证的[功能标志](../../feature_flags.md)为 `geo_object_storage_verification`，引入于 16.4 并默认启用。它使用文件大小的校验和来验证文件。

<a id="git-repositories"></a>

### Git 仓库

极狐GitLab 实例可以有一个或多个仓库分片。每个分片都有一个 Gitaly 实例，负责允许对本地存储的 Git 仓库进行访问和操作。它可以在机器上运行：

- 使用单个磁盘
- 将多个磁盘挂载为单个挂载点（如 RAID 阵列）
- 使用 LVM

极狐GitLab 不需要特殊的文件系统，并且可以与挂载的存储设备一起使用。但是，使用远程文件系统时可能会存在性能限制和一致性问题。

Geo 在 Gitaly 中触发垃圾收集，以对 Geo 次要站点上的派生仓库进行去重。

Gitaly gRPC API 通过三种可能的同步方式进行通信：

- 在两个 Geo 站点之间使用常规 Git 克隆/获取（使用特殊身份验证）
- 使用仓库快照（当第一种方法失败或仓库损坏时）
- 从管理中心手动触发（上述两者的组合）

每个项目最多可以有 3 个不同的仓库：

- 项目仓库：存储源代码
- Wiki 仓库：存储 Wiki 内容
- 设计仓库：索引设计产物（资产实际上位于 LFS 中）

它们都位于同一个分片中，并拥有相同的基本名称，为 Wiki 和设计仓库案例添加 `-wiki` 和 `-design` 后缀。

除此之外，还有代码片段仓库。它们可以连接到项目或某些特定用户。
两种类型都会同步到次要站点。

<a id="blobs"></a>

### Blob

极狐GitLab 将文件和 Blob（例如议题附件或 LFS 对象）存储到以下任一位置：

- 特定位置的文件系统
- [对象存储](../../object_storage.md)解决方案。对象存储解决方案可以是：
    - 基于云，例如 Amazon S3 和 Google Cloud Storage
    - 由您托管（如 MinIO）
    - 公开对象存储兼容 API 的存储设备

当使用文件系统存储而不是对象存储并且使用多个节点时，请使用网络挂载的文件系统来运行极狐GitLab。

关于复制和验证：

- 我们使用内部 API 请求传输文件和 Blob
- 使用对象存储，您可以：
    - 使用云提供商的复制功能
    - 让极狐GitLab 为您复制

<a id="database"></a>

### 数据库

极狐GitLab 依赖于存储在多个数据库中的数据来满足不同的用例。
PostgreSQL 是 Web 界面中用户生成内容的单一事实来源，例如议题内容、评论以及权限和凭据。

PostgreSQL 还可以保存某种级别的缓存数据，例如 HTML 渲染的 Markdown 和缓存的合并请求差异。
这也可以配置为卸载到对象存储。

我们使用 PostgreSQL 自己的复制功能将数据从**主要**站点复制到**次要**站点。

我们使用 Redis 作为缓存存储并为后台作业系统保存持久数据。由于这两个用例都具有同一 Geo 站点独有的数据，因此我们不会在站点之间进行复制。

Elasticsearch 是用于高级搜索的可选数据库。它可以改进源代码级别的搜索以及议题、合并请求和讨论中用户生成的内容。
Geo 中不支持 Elasticsearch。

## 复制/验证限制

下表列出了极狐GitLab 功能及其在**次要**站点上的复制和验证状态。

<!--
You can keep track of the progress to implement the missing items in
these epics/issues:

- [Geo: Improve the self-service Geo replication framework](https://gitlab.com/groups/gitlab-org/-/epics/3761)
- [Geo: Move existing blobs to framework](https://gitlab.com/groups/gitlab-org/-/epics/3588)
-->

### 功能标志后的复制数据

某些数据类型的复制需要启用相应的功能标志：

> - 其部署在功能标志后面，默认启用。
> - 其已在 JihuLab.com 上启用。
> - 无法针对每个项目进行启用或禁用。
> - 建议将其用于生产用途。
> - 对于私有化部署的极狐GitLab 实例，极狐GitLab 管理员可以选择[禁用](#enable-or-disable-replication-for-some-data-types)。 **(BASIC SELF)**

<a id="enable-or-disable-replication-for-some-data-types"></a>

#### 启用或禁用复制（对于某些数据类型）

某些数据类型的复制是在**默认启用**的功能标志后面发布的。[有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员](../../feature_flags.md)可以为您的实例禁用复制。您可以在下表的注释列中找到每种数据类型的功能标志名称。

要进行禁用，例如对于软件包文件复制：

```ruby
Feature.disable(:geo_package_file_replication)
```

要进行启用，例如对于软件包文件复制：

```ruby
Feature.enable(:geo_package_file_replication)
```

WARNING:
不在此列表中的功能，或者在**已复制**列中标为**否**的功能，不会复制到**次要**站点。如果不手动从这些功能复制数据而进行故障转移，则会导致数据**丢失**。
要在**次要**站点上使用这些功能，或者要成功执行故障转移，您必须使用其他方式复制其数据。


| 功能                                                                                                    | 复制（添加的极狐GitLab 版本）                                                  | 验证（添加的极狐GitLab 版本）                                                  | 极狐GitLab 管理的对象存储复制（添加的极狐GitLab 版本）                                | 极狐GitLab 管理的对象存储验证（添加的极狐GitLab 版本）  | 备注                                                                                                                                                                                |
|:------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------|:--------------------------------------------------------------------|:------------------------------------------------------------------|:------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [PostgreSQL 中的应用数据](../../postgresql/index.md)                                                        | **是** (13.10)                                                       | **是** (13.10)                                                        | 不适用                                                               | 不适用                                 |                                                                                                                                                                                   |
| [项目仓库](../../../user/project/repository/index.md)                                                     | **是** (13.10)                                                       | **是** (13.10)                                                        | 不适用                                                               | 不适用                                 | 在 16.2 版本中迁移到自服务框架。<br /><br />功能标志为 `geo_project_repository_replication`，默认启用于 16.3。<br /><br /> 所有项目，包括[归档项目](../../../user/project/settings/index.md#archive-a-project)，都会被复制。 |
| [项目 Wiki 仓库](../../../user/project/wiki/index.md)                                                     | **是** (13.10)<sup>2</sup>                                           | **是** (13.10)<sup>2</sup>                                            | 不适用                                                               | 不适用                                 | 在 15.11 中，迁移到自服务框架。<br /><br />功能标志为 `geo_project_wiki_repository_replication`，默认启用于 15.11。                                                                                       |
| [群组 Wiki 仓库](../../../user/project/wiki/group.md)                                                     | **是** (13.10)                                                       | **是** (16.3)                                                        | 不适用                                                               | 不适用                                 | 功能标志为 `geo_group_wiki_repository_replication`，默认启用。                                                                                                                               |
| [上传](../../uploads.md)                                                                                | **是** (13.10)                                                       | **是** (14.6)                                                        | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 复制位于功能标志 `geo_upload_replication` 后面，默认启用。验证位于功能标志 `geo_upload_verification` 后面，已在 14.8 中删除。                                                                                      |
| [LFS 对象](../../lfs/index.md)                                                                          | **是** (13.10)                                                       | **是** (14.6)                                                        | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 复制位于功能标志 `geo_lfs_object_replication` 后面，默认启用。验证位于功能标志 `geo_lfs_object_verification` 后面，已在 14.7 中删除。                                                                              |
| [个人代码片段](../../../user/snippets.md)                                                                   | **是** (13.10)                                                       | **是** (13.10)                                                        | 不适用                                                               | 不适用                                 |                                                                                                                                                                                   |
| [项目代码片段](../../../user/snippets.md)                                                                   | **是** (13.10)                                                       | **是** (13.10)                                                        | 不适用                                                               | 不适用                                 |                                                                                                                                                                                   |
| [CI 作业产物](../../../ci/jobs/job_artifacts.md)                                                          | **是** (13.10)                                                       | **是** (14.10)                                                       | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 验证位于功能标志 `geo_job_artifact_replication` 后面，在 14.10 中默认启用。                                                                                                                         |
| [CI 流水线产物](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/app/models/ci/pipeline_artifact.rb)     | **是** (13.11)                                                       | **是** (13.11)                                                       | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 流水线完成后保留其他产物。                                                                                                                                                                     |
| [CI 安全文件](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/app/models/ci/secure_file.rb)            | **是** (15.3)                                                        | **是** (15.3)                                                        | **是** (15.3)                                                      | [**是** (16.4)](object_storage.md)   | 验证位于功能标志 `geo_ci_secure_file_replication` 后面，在 15.3 中默认启用。                                                                                                                        |
| [容器镜像库](../../packages/container_registry.md)                                                         | **是** (13.10)<sup>1</sup>                                           | **是** (15.10)                                                       | **是** (13.10)<sup>1</sup>                                         | **是** (15.10)                       | 请参阅[说明](container_registry.md)以设置容器镜像库复制。                                                                                                                                         |
| [Terraform 模块库](../../../user/packages/terraform_module_registry/index.md)                            | **是** (14.0)                                                        | **是** (14.0)                                                        | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 在功能标志 `geo_package_file_replication` 后面，默认启用。                                                                                                                                     |
| [项目设计仓库](../../../user/project/issues/design_management.md)                                           | **是** (13.10)                                                       | **是** (16.1)                                                        | 不适用                                                               | 不适用                                 | 设计还需要复制 LFS 对象和上传。                                                                                                                                                                |
| [软件包库](../../../user/packages/package_registry/index.md)                                              | **是** (13.2)                                                        | **是** (13.10)                                                       | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 在功能标志 `geo_package_file_replication` 后面，默认启用。                                                                                                                                     |
| [版本 Terraform 状态](../../terraform_state.md)                                                           | **是** (13.5)                                                        | **是** (13.12)                                                       | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 复制位于功能标志 `geo_terraform_state_version_replication` 后面，默认启用。验证位于功能标志 `geo_terraform_state_version_verification` 后面，在 14.0 中删除。                                                     |
| [外部合并请求差异](../../merge_request_diffs.md)                                                              | **是** (13.5)                                                        | **是** (14.6)                                                        | **是** (15.1)                                                      | [**是** (16.4)](object_storage.md)   | 复制位于功能标志 `geo_merge_request_diff_replication` 后面，默认启用。验证位于功能标志 `geo_merge_request_diff_verification` 后面，在 14.7 中删除。                                                               |
| [版本代码片段](../../../user/snippets.md#versioned-snippets)                                                | **是** (13.7)                                                        | **是** (14.2)                                                        | 不适用                                                               | 不适用                                 | 在 13.11 中，验证由功能标志 `geo_snippet_repository_verification` 控制，已在 14.2 中删除。                                                                                                           |
| [极狐GitLab Pages](../../pages/index.md)                                                                | **是** (14.3)                                                        | **是** (14.6)                                                        | **是** (15.1)  | [**是** (16.4)](object_storage.md)   | 在功能标志 `geo_pages_deployment_replication` 后面，默认启用。验证位于功能标志 `geo_pages_deployment_verification` 后面，已在 14.7 中删除。                                                                     |
| [项目级安全文件](../../../ci/secure_files/index.md)                                                          | **是** (15.3)                                                        | **是** (15.3)                                                        | **是** (15.3)                                                      | [**是** (16.4)](object_storage.md)   |                                                                                                                                                                                   |
| [事件指标镜像](../../../operations/incident_management/incidents.md#metrics)                                | **是** (15.5)                                                        | **是**(15.5)                                                         | **是** (15.5)                                                      | [**是** (16.4)](object_storage.md)   | 复制/验证通过上传数据类型处理。                                                                                                                                                                  | |
| [告警指标镜像](../../../operations/incident_management/alerts.md#metrics-tab)                               | **是** (15.5)                                                        | **是** (15.5)                                                        | **是** (15.5)                                                      | [**是** (16.4)](object_storage.md)   | 复制/验证通过上传数据类型处理。                                                                                                                                                                  |
| [服务器端 Git Hook](../../server_hooks.md)                                                                | 未计划                                                                 | 否                                                                   | 不适用                                                               | 不适用                                 | 由于当前实施的复杂性、客户兴趣低以及 Hook 替代方案的可用性，因此未计划。                                                                                                                                           |
| [Elasticsearch 集成](../../../integration/advanced_search/elasticsearch.md)                             | 未计划                                                                 | 否                                                                   | 否                                                                 | 否                                   | 未计划，因为需要进一步的产品发现并且可以重建 Elasticsearch (ES) 集群。次要节点使用与主要节点相同的 ES 集群。                                                                                                                |
| [依赖代理镜像](../../../user/packages/dependency_proxy/index.md)                                            | **是** (15.7)   | **是** (15.7)   | **是** (15.7)                                                      | [**是** (16.4)](object_storage.md)   |                                                                                                                                                                                   |
| [漏洞导出](../../../user/application_security/vulnerability_report/index.md#export-vulnerability-details) | 未计划            | 否                                                                   | 否                                                                 | 否                                   | 未计划，因为这些信息短暂且敏感，可以根据需要再生。                                                                                                                                                         |

<sup>1</sup> 迁移到私有化部署架构于 15.5 版本。

<sup>2</sup> 迁移到自服务架构于 15.11 版本，功能标志为 `geo_project_wiki_repository_replication`，默认启用。
