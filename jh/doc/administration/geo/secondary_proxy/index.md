---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 次要站点的 Geo 代理 **(PREMIUM SELF)**

> - 引入于极狐GitLab 14.4，[功能标志](../../feature_flags.md)为 `geo_secondary_proxy`。默认禁用。
> - 为统一 URL 默认启用于极狐GitLab 14.6。
> - 为不同 URL 默认禁用于极狐GitLab 14.6，[功能标志](../../feature_flags.md)为 `geo_secondary_proxy_separate_urls`。
> - 为不同 URL 默认启用于极狐GitLab 15.1。

使用 Geo 代理可以：

- 让次要站点通过代理到主要站点来提供读写流量。
- 通过将只读操作定向到本地站点，有选择地加速复制数据类型。

启用后，次要站点的用户可以使用 Web UI，就像访问主要站点的 UI 一样。这显着改善了次要站点的整体用户体验。

通过次要代理，对次要 Geo 站点的 Web 请求将直接代理到主要 Geo 站点，并且用作读写站点。

代理由 `gitlab-workhorse` 组件完成。
通常发送到 Geo 次要站点上的 Rails 应用程序的流量会被代理到主要 Geo 站点的[内部 URL](../index.md#internal-url)。

使用次要代理的用例包括：

- 将所有 Geo 站点置于单个 URL 后面。
- 地理负载平衡流量，无需担心写入访问。

NOTE:
次要站点的 Geo 代理与 Git 推送操作的 Geo 代理/重定向是分开的。

## 为 Geo 站点设置统一的 URL

次要站点可以透明地提供读写流量。您可以使用单个外部 URL，以便请求可以到达主要 Geo 站点或任何使用 Geo 代理的次要 Geo 站点。

### 配置外部 URL 以将流量发送到两个站点

按照位置感知公共 URL 步骤创建所有 Geo 站点（包括主要站点）使用的单个 URL。

### 更新 Geo 站点以使用相同的外部 URL

1. 在您的 Geo 站点上，通过 SSH 连接到运行 Rails 的每个节点（Puma、Sidekiq、Log-Cursor）并将 `external_url` 更改为单个 URL 的值：
   
   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

1. 如果 URL 与已设置的 URL 不同，请重新配置更新的节点以使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

1. 为了匹配次要 Geo 站点上设置的新外部 URL，主要数据库需要反映此更改。

  在 **主要** 站点的 Geo 管理页面中，编辑使用次要代理的每个 Geo 次要站点，并将 `URL` 字段设置为单个 URL。
  确保主要站点也使用此 URL。

在 Kubernetes 中，主要站点可以使用[与 `global.hosts.domain` 下相同的域](https://docs.gitlab.cn/charts/advanced/geo/index.html)。

<a id="geo-proxying-with-separate-urls"></a>

## 具有单独 URL 的 Geo 代理

> 单独 URL 的 Geo 次要代理默认启用于极狐GitLab 15.1。

NOTE:
本节中描述的功能标志计划在未来版本中弃用并删除。

<!--
There are minor known issues linked in the
["Geo secondary proxying with separate URLs" epic](https://gitlab.com/groups/gitlab-org/-/epics/6865).
You can also add feedback in the epic about any use-cases that
are not possible anymore with proxying enabled.

If you run into issues, to disable this feature, disable the `geo_secondary_proxy_separate_urls` feature flag.
-->

1. 通过 SSH 连接到主要 Geo 站点上运行 Rails 的一个节点并运行：

   ```shell
   sudo gitlab-rails runner "Feature.disable(:geo_secondary_proxy_separate_urls)"
   ```

1. 在次要 Geo 站点上运行 Rails 的所有节点上重新启动 Puma：

   ```shell
   sudo gitlab-ctl restart puma
   ```

在 Kubernetes 中，您可以在工具箱 pod 中运行相同的命令。

## 限制

- 使用次要代理时，异步 Geo 复制可能会导致加速数据类型出现意外问题，这些数据类型可能会使复制到 Geo 次要服务器出现延迟。

<!--
  For example, we found a potential issue where
  [replication lag introduces read-after-write inconsistencies](https://gitlab.com/gitlab-org/gitlab/-/issues/345267).
  If the replication lag is high enough, this can result in Git reads receiving stale data when hitting a secondary.
-->

- 非 Rails 请求不会被代理，因此其他服务可能需要使用单独的、非统一的 URL 来确保请求始终发送到主要站点上。这些服务包括：

    - 极狐GitLab 容器镜像库 - 可以配置为使用单独的域。
    - 极狐GitLab Pages - 应始终使用单独的域，作为运行极狐GitLab Pages 的先决条件的一部分。

- 使用统一的 URL，Let's Encrypt 无法生成证书，除非它可以通过同一个域访问两个 IP。
  要将 TLS 证书与 Let's Encrypt 结合使用，您可以手动将域指向 Geo 站点之一，生成证书，然后将其复制到所有其他站点。

- 官方不支持使用 Geo 次要站点来加速 Runner。对此功能的支持已计划完成。如果主要站点和次要站点之间出现复制滞后，并且执行作业时次要站点上的流水线引用不可用，则作业将失败。

- 当次要代理与单独的 URL 一起使用时，仅当 SAML 身份提供商 (IdP) 允许应用程序使用多个回调 URL 进行配置时，才支持[使用 SAML 登录次要站点](../replication/single_sign_on.md#saml-with-separate-url-with-proxying-enabled)。

## 主要 Geo 站点关闭时次要站点的行为

考虑到 Web 流量被代理到主要站点，当主要站点无法访问时，次要站点的行为会有所不同：

- UI 和 API 流量返回与主要站点相同的错误（如果根本无法访问主要站点，则失败），因为它们是代理的。
- 对于正在访问的特定次要站点上已存在的仓库，Git 读取操作仍按预期工作，包括通过 HTTP(s) 或 SSH 进行身份验证。
- 未复制到次要站点的仓库的 Git 操作会返回与主要站点相同的错误，因为它们是代理的。
- 所有 Git 写入操作都会返回与主要站点相同的错误，因为它们是代理的。

## 由次要 Geo 站点加速的功能

发送到次要 Geo 站点的大多数 HTTP 流量都可以代理到主要 Geo 站点。有了这个架构，次要 Geo 站点能够支持写入请求。次要站点会本地处理某些**读取**请求以改善附近的时延和带宽。所有写入请求都被代理到主要站点。

下表详细介绍了当前通过 Geo 次要站点 Workhorse 代理测试的组件。
不涵盖所有的数据类型。

在此上下文中，加速读取是指从次要站点提供的读取请求，前提是次要站点上的组件数据是最新的。如果确定次要站点上的数据已过期，则将请求转发到主要站点上。对下表中未列出的组件的读取请求始终会自动转发到主要站点上。

| 功能/组件                   | 是否加速读取                             |
|:------------------------|:-----------------------------------|
| 项目、Wiki、设计仓库（使用 web UI） | **{dotted-circle}** 否              |
| 项目、Wiki 仓库（使用 Git）      | **{check-circle}** 是 <sup>1</sup>  |
| 项目、私有代码片段（使用 web UI）    | **{dotted-circle}** 否              |
| 项目、私有代码片段（使用 Git）       | **{check-circle}** 是 <sup>1</sup>  |
| 群组 Wiki 仓库（使用 web UI）   | **{dotted-circle}** 否              |
| 群组 Wiki 仓库（使用 Git）      | **{check-circle}** 是 <sup>1</sup>  |
| 用户上传                    | **{dotted-circle}** 否              |
| LFS 对象（使用 web UI）       | **{dotted-circle}** 否              |
| LFS 对象（使用 Git）          | **{check-circle}** 是               |
| Pages                   | **{dotted-circle}** 否 <sup>2</sup> |
| 高级搜索（使用 web UI）         | **{dotted-circle}** 否              |
| 容器镜像库                   | **{dotted-circle}** 否 <sup>3</sup> |

1. Git 读取由本地次要站点提供，而推送则代理到主要站点。
   选择性同步或仓库不本地存在于 Geo 次要站点上的情况会引发"未找到"错误。
1. Pages 可以使用相同的 URL（无访问控制），但必须进行单独配置并且不进行代理。
1. 容器镜像库仅推荐用于灾难恢复。如果次要站点的容器镜像库不是最新的，则读取请求将使用旧数据，因为该请求不会转发到主要站点上。

## 禁用 Geo 代理

当次要站点使用统一 URL（即与主要站点使用相同的 `external_url`）时，次要代理默认启用于次要站点。在这种情况下禁用代理往往没有帮助，因为根据路由情况，在同一 URL 上会提供完全不同的行为。

即使没有统一的 URL，15.1 中的次要站点也会默认启用次要代理。如果需要在次要站点上禁用代理，则禁用[具有单独 URL 的 Geo 代理](#geo-proxying-with-separate-urls) 中的功能标志要容易得多。但是，如果有多个次要站点，则可以使用本节中的说明来禁用每个站点的次要代理。

此外，`gitlab-workhorse` 服务每 10 秒轮询一次 `/api/v4/geo/proxy`。在 15.2 及更高版本中，如果未启用 Geo，则仅轮询一次。在 15.2 之前，您可以通过禁用次要代理来停止此轮询。

您可以在 Linux 软件包安装中按照以下步骤单独禁用每个 Geo 站点上的次要代理：

1. 通过 SSH 连接到次要 Geo 站点上的每个应用程序节点（直接提供用户流量）并添加以下环境变量：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   ```ruby
   gitlab_workhorse['env'] = {
     "GEO_SECONDARY_PROXY" => "0"
   }
   ```

1. 重新配置更新的节点以使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

在 Kubernetes 中，您可以使用 `--set gitlab.webservice.extraEnv.GEO_SECONDARY_PROXY="0"`，或在值文件中指定以下内容：

```yaml
gitlab:
  webservice:
    extraEnv:
      GEO_SECONDARY_PROXY: "0"
```
