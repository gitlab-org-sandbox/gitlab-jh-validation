---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Geo 与外部 PostgreSQL 实例 **(PREMIUM SELF)**

如果您使用*不是由 Omnibus 管理*的 PostgreSQL 实例，则适用本文档。
确保您使用 [Linux 软件包附带](../../package_information/postgresql_versions.md)的 PostgreSQL 版本之一，[避免版本不匹配](../index.md#requirements-for-running-geo)，防止必须重建 Geo 站点。

NOTE:
我们强烈建议运行 Linux 软件包托管的实例，因为它们正在积极开发和测试。我们的目标是与大多数外部（不由 Omnibus 管理）数据库兼容，但我们不保证兼容性。

## **主要**站点

1. 通过 SSH 连接到您的**主要站点上的 Rails 节点**，并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加：

   ```ruby
   ##
   ## Geo Primary role
   ## - configure dependent flags automatically to enable Geo
   ##
   roles ['geo_primary_role']

   ##
   ## The unique identifier for the Geo site. See
   ## https://docs.gitlab.com/ee/administration/geo_nodes.html#common-settings
   ##
   gitlab_rails['geo_node_name'] = '<site_name_here>'
   ```

1. 重新配置 **Rails 节点**，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

1. 在 **Rails 节点**上执行以下命令，将站点定义为**主要**站点：

   ```shell
   gitlab-ctl set-geo-primary-node
   ```

   此命令使用您在 `/etc/gitlab/gitlab.rb` 中定义的 `external_url`。

### 配置要复制的外部数据库

要设置外部数据库，您可以：

- 自行设置[流复制](https://www.postgresql.org/docs/12/warm-standby.html#STREAMING-REPLICATION-SLOTS)。
- 手动执行 Linux 软件包配置，如下所示。

#### 利用您的云提供商的工具来复制主数据库

假设您在 AWS EC2 上设置了一个使用 RDS 的主要站点。
您现在可以在不同区域创建只读副本，并且复制过程由 AWS 管理。确保您已根据需要设置网络 ACL（访问控制列表）、子网和安全组，以便次要 Rails 节点可以访问数据库。

<!--
The following instructions detail how to create a read-only replica for common
cloud providers:

- Amazon RDS - [Creating a Read Replica](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ReadRepl.html#USER_ReadRepl.Create)
- Azure Database for PostgreSQL - [Create and manage read replicas in Azure Database for PostgreSQL](https://docs.microsoft.com/en-us/azure/postgresql/single-server/how-to-read-replicas-portal)
- Google Cloud SQL - [Creating read replicas](https://cloud.google.com/sql/docs/postgres/replication/create-replica)
-->

设置只读副本后，您可以跳至[配置次要站点](#configure-secondary-site-to-use-the-external-read-replica)。

#### 手动配置主数据库以进行复制

[`geo_primary_role`](https://docs.gitlab.cn/omnibus/roles/#gitlab-geo-roles) 通过更改 `pg_hba.conf` 和 `postgresql.conf`，手动对外部数据库配置进行以下配置更改，并确保之后重新启动 PostgreSQL，使更改生效：

```plaintext
##
## Geo Primary Role
## - pg_hba.conf
##
host    all         all               <trusted primary IP>/32       md5
host    replication gitlab_replicator <trusted primary IP>/32       md5
host    all         all               <trusted secondary IP>/32     md5
host    replication gitlab_replicator <trusted secondary IP>/32     md5
```

```plaintext
##
## Geo Primary Role
## - postgresql.conf
##
wal_level = hot_standby
max_wal_senders = 10
wal_keep_segments = 50
max_replication_slots = 1 # number of secondary instances
hot_standby = on
```

## **次要**站点

### 手动配置副本数据库

手动对外部副本数据库的 `pg_hba.conf` 和 `postgresql.conf` 进行以下配置更改，并确保之后重新启动 PostgreSQL，使更改生效：

```plaintext
##
## Geo Secondary Role
## - pg_hba.conf
##
host    all         all               <trusted secondary IP>/32     md5
host    replication gitlab_replicator <trusted secondary IP>/32     md5
host    all         all               <trusted primary IP>/24       md5
```

```plaintext
##
## Geo Secondary Role
## - postgresql.conf
##
wal_level = hot_standby
max_wal_senders = 10
wal_keep_segments = 10
hot_standby = on
```

<a id="configure-secondary-site-to-use-the-external-read-replica"></a>

### 配置**次要**站点使用外部只读副本

使用 Linux 软件包时，[`geo_secondary_role`](https://docs.gitlab.cn/omnibus/roles/#gitlab-geo-roles) 具有三个主要功能：

1. 配置副本数据库。
1. 配置跟踪数据库。
1. 启用 [Geo Log Cursor](../index.md#geo-log-cursor)（本节未介绍）。

配置与外部只读副本数据库的连接并启用 Log Cursor：

1. SSH 到您的**次要**站点上的每个 **Rails、Sidekiq 和 Geo Log Cursor** 节点，并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb`，并添加以下内容：

   ```ruby
   ##
   ## Geo Secondary role
   ## - configure dependent flags automatically to enable Geo
   ##
   roles ['geo_secondary_role']

   # note this is shared between both databases,
   # make sure you define the same password in both
   gitlab_rails['db_password'] = '<your_password_here>'

   gitlab_rails['db_username'] = 'gitlab'
   gitlab_rails['db_host'] = '<database_read_replica_host>'

   # Disable the bundled Omnibus PostgreSQL, since we are
   # using an external PostgreSQL
   postgresql['enable'] = false
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#reconfigure-a-linux-package-installation)。

<a id="configure-the-tracking-database"></a>

### 配置跟踪数据库

**次要**站点使用单独的 PostgreSQL 安装作为跟踪数据库来跟踪复制状态，并自动从潜在的复制问题中恢复。当 `roles ['geo_secondary_role']` 设置时，Linux 软件包会自动配置跟踪数据库。
如果要在 Linux 软件包外部运行此数据库，请使用以下说明。

如果您为跟踪数据库使用云管理服务，您可能需要向跟踪数据库用户授予其他角色（默认情况为 `gitlab_geo`）。

这是为了在安装和升级期间安装扩展。作为替代方案，[确保手动安装扩展，并阅读未来极狐GitLab 升级期间可能出现的问题](../../../install/postgresql_extensions.md)。

要设置外部跟踪数据库，请按照以下说明操作：

<!--
NOTE:
If you want to use Amazon RDS as a tracking database, make sure it has access to
the secondary database. Unfortunately, just assigning the same security group is not enough as
outbound rules do not apply to RDS PostgreSQL databases. Therefore, you need to explicitly add an inbound
rule to the read-replica's security group allowing any TCP traffic from
the tracking database on port 5432.
-->

1. 根据[数据库需求文档](../../../install/requirements.md#database)设置 PostgreSQL。
1. 使用您选择的密码设置 `gitlab_geo` 用户，创建 `gitlabhq_geo_production` 数据库，并使该用户成为数据库的所有者。
1. 如果您**不**使用云管理的 PostgreSQL 数据库，请确保您的次要站点可以通过手动更改与您的跟踪数据库关联的 `pg_hba.conf` 与您的跟踪数据库通信。请记住之后重新启动 PostgreSQL，使更改生效：

    ```plaintext
    ##
    ## Geo Tracking Database Role
    ## - pg_hba.conf
    ##
    host    all         all               <trusted tracking IP>/32      md5
    host    all         all               <trusted secondary IP>/32     md5
    ```

1. SSH 进入极狐GitLab **次要**服务器并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 使用具有 PostgreSQL 实例的机器的连接参数和凭据编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   geo_secondary['db_username'] = 'gitlab_geo'
   geo_secondary['db_password'] = '<your_password_here>'

   geo_secondary['db_host'] = '<tracking_database_host>'
   geo_secondary['db_port'] = <tracking_database_port>      # change to the correct port
   geo_postgresql['enable'] = false     # don't use internal managed instance
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#reconfigure-a-linux-package-installation)。

1. 重新配置应该自动创建数据库。如果需要，您可以手动执行此任务。此任务（无论是自行运行还是在重新配置期间运行）要求数据库用户是超级用户。

   ```shell
   gitlab-rake db:create:geo
   ```

1. 重新配置应自动迁移数据库。如果需要，您可以手动迁移数据库，例如 `geo_secondary['auto_migrate'] = false`：

   ```shell
   gitlab-rake db:migrate:geo
   ```
