---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 在极狐GitLab 中进行维护 **(BASIC SELF)**

保持您的极狐GitLab 实例正常运行。

- [例行维护](../../administration/housekeeping.md)
- [使用许可证激活极狐GitLab](../../administration/license_file.md)
- [快速 SSH 密钥查询](../../administration/operations/fast_ssh_key_lookup.md)
- [文件系统性能基准](../../administration/operations/filesystem_benchmarking.md)
- [`gitlab-sshd`](../../administration/operations/gitlab_sshd.md)
- [Rails 控制台](../../administration/operations/rails_console.md)
- [使用 SSH 证书](../../administration/operations/ssh_certificates.md)
- [启用加密配置](../../administration/encrypted_configuration.md)
- [Rake 任务](../../raketasks/index.md)
- [备份和还原](../../administration/backup_restore/index.md)
- [非活跃项目删除](../../administration/inactive_project_deletion.md)
- [移动仓库](../../administration/operations/moving_repositories.md)
- [只读状态](../../administration/read_only_gitlab.md)
- [重启极狐GitLab](../../administration/restart_gitlab.md)
