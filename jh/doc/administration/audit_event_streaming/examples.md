---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 审计事件流示例

本文提供了审计事件流的示例。

<a id="audit-event-streaming-on-git-operations"></a>

## Git 操作的审计事件流

> - 引入于 14.9 版本，功能标志名为 `audit_event_streaming_git_operations`。默认禁用。
> - 在 SaaS 上启用于 15.0 版本。
> - 在私有化部署版上默认启用于 15.1 版本。
> - `details.author_class` 字段添加于 15.3 版本。
> - 一般可用于 15.6 版本。删除功能标志 `audit_event_streaming_git_operations`。

当登录用户推送、拉取或克隆项目的远程 Git 仓库时，可以发送审计事件流：

- [使用 SSH](../../user/ssh.md)。
- 使用 HTTP 或 HTTPS。
- 在 UI 中使用 **下载** 按钮 (**{download}**)。

对于未登录的用户，不会捕获审计事件。例如，在下载公开项目时。

要为 Git 操作配置审计事件流，请参阅[添加新的流目的地](index.md#add-a-new-streaming-destination)。

### Headers

> `X-Gitlab-Audit-Event-Type` 引入于 15.0 版本。

Headers 的格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
X-Gitlab-Audit-Event-Type: repository_git_operation
```

### SSH 事件的负载示例

抓取：

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "author_name": "Administrator",
    "author_class": "User",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "ssh",
      "action": "git-upload-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "Administrator",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-02-23T06:21:05.283Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

推送：

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "author_name": "Administrator",
    "author_class": "User",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "ssh",
      "action": "git-receive-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "Administrator",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-02-23T06:23:08.746Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

### 使用部署密钥的 SSH 事件的负载示例

> 引入于 15.3 版本。

抓取：

```json
{
  "id": 1,
  "author_id": -3,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "author_name": "deploy-key-name",
    "author_class": "DeployKey",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "ssh",
      "action": "git-upload-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "deploy-key-name",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-07-26T05:43:53.662Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

### HTTP 和 HTTPS 事件的负载示例

抓取：

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "author_name": "Administrator",
    "author_class": "User",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "http",
      "action": "git-upload-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "Administrator",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-02-23T06:25:43.938Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

推送：

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "author_name": "Administrator",
    "author_class": "User",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "http",
      "action": "git-receive-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "Administrator",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-02-23T06:26:29.294Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

### 带有部署令牌的 HTTP 和 HTTPS 事件的负载示例

抓取：

```json
{
  "id": 1,
  "author_id": -2,
  "entity_id": 22,
  "entity_type": "Project",
  "details": {
    "author_name": "deploy-token-name",
    "author_class": "DeployToken",
    "target_id": 22,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": {
      "protocol": "http",
      "action": "git-upload-pack"
    },
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "deploy-token-name",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-07-26T05:46:25.850Z",
  "target_type": "Project",
  "target_id": 22,
  "event_type": "repository_git_operation"
}
```

### 来自极狐GitLab UI 下载按钮的事件负载示例

抓取：

```json
{
  "id": 1,
  "author_id": 99,
  "entity_id": 29,
  "entity_type": "Project",
  "details": {
    "custom_message": "Repository Download Started",
    "author_name": "example_username",
    "author_class": "User",
    "target_id": 29,
    "target_type": "Project",
    "target_details": "example-group/example-project",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example_username",
  "entity_path": "example-group/example-project",
  "target_details": "example-group/example-project",
  "created_at": "2022-02-23T06:27:17.873Z",
  "target_type": "Project",
  "target_id": 29,
  "event_type": "repository_git_operation"
}
```

<a id="audit-event-streaming-on-merge-request-approval-actions"></a>

## 合并请求批准操作的审计事件流

> 引入于 14.9 版本。

与项目内执行的合并批准操作相关的审计事件流。

### Headers

Headers 的格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
X-Gitlab-Audit-Event-Type: audit_operation
```

### 负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 6,
  "entity_type": "Project",
  "details": {
    "author_name": "example_username",
    "target_id": 20,
    "target_type": "MergeRequest",
    "target_details": "merge request title",
    "custom_message": "Approved merge request",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example_username",
  "entity_path": "example-group/example-project",
  "target_details": "merge request title",
  "created_at": "2022-03-09T06:53:11.181Z",
  "target_type": "MergeRequest",
  "target_id": 20,
  "event_type": "audit_operation"
}
```

## 合并请求创建操作的审计事件流

> 引入于 15.2 版本。

使用 `/logs` 端点流式传输与合并请求创建操作相关的审计事件。

发送包含 `X-Gitlab-Audit-Event-Type` header 且值为 `merge_request_create` 的 API 请求。系统使用 JSON 有效负载进行响应，其中 `event_type` 字段设置为 `merge_request_create`。

### Headers

格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Audit-Event-Type: merge_request_create
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
```

### 负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 24,
  "entity_type": "Project",
  "details": {
    "author_name": "example_user",
    "target_id": 132,
    "target_type": "MergeRequest",
    "target_details": "Update test.md",
    "custom_message": "Added merge request",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "Administrator",
  "entity_path": "example-group/example-project",
  "target_details": "Update test.md",
  "created_at": "2022-07-04T00:19:22.675Z",
  "target_type": "MergeRequest",
  "target_id": 132,
  "event_type": "merge_request_create"
}
```

## 项目派生操作的审计事件流

> 引入于 15.2 版本。

使用 `/logs` 端点流式传输与项目派生创建操作相关的审计事件。

发送包含 `X-Gitlab-Audit-Event-Type` 标头和值为 `project_fork_operation` 的 API 请求。系统使用 JSON 有效负载进行响应，其中 `event_type` 字段设置为 `project_fork_operation`。

### Headers

格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Audit-Event-Type: project_fork_operation
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
```

### 负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 24,
  "entity_type": "Project",
  "details": {
    "author_name": "example_username",
    "target_id": 24,
    "target_type": "Project",
    "target_details": "example-project",
    "custom_message": "Forked project to another-group/example-project-forked",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example_username",
  "entity_path": "example-group/example-project",
  "target_details": "example-project",
  "created_at": "2022-06-30T03:43:35.384Z",
  "target_type": "Project",
  "target_id": 24,
  "event_type": "project_fork_operation"
}
```

<a id="audit-event-streaming-on-project-group-link-actions"></a>

## 项目群组关联操作的审计事件流

> 引入于 15.2 版本。

使用 `/logs` 端点流式传输与项目群组关联的创建、更新和删除相关的审计事件。

发送包含 `X-Gitlab-Audit-Event-Type` 标头的 API 请求，其值为：

- `project_group_link_create`
- `project_group_link_update`
- `project_group_link_destroy`

系统使用 JSON 有效负载响应，并将 `event_type` 字段设置为：

- `project_group_link_create`
- `project_group_link_update`
- `project_group_link_destroy`

### Headers 示例

格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Audit-Event-Type: project_group_link_create
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
```

### 创建项目群组关联的负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 24,
  "entity_type": "Project",
  "details": {
    "author_name": "example-user",
    "target_id": 31,
    "target_type": "Group",
    "target_details": "another-group",
    "custom_message": "Added project group link",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example-user",
  "entity_path": "example-group/example-project",
  "target_details": "another-group",
  "created_at": "2022-07-04T00:43:09.318Z",
  "target_type": "Group",
  "target_id": 31,
  "event_type": "project_group_link_create"
}
```

### 更新项目群组关联的负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 24,
  "entity_type": "Project",
  "details": {
    "author_name": "example-user",
    "target_id": 31,
    "target_type": "Group",
    "target_details": "another-group",
    "custom_message": "Changed project group link profile group_access from Developer to Guest",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example-user",
  "entity_path": "example-group/example-project",
  "target_details": "another-group",
  "created_at": "2022-07-04T00:43:28.328Z",
  "target_type": "Group",
  "target_id": 31,
  "event_type": "project_group_link_update"
}
```

### 删除项目群组关联的负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 24,
  "entity_type": "Project",
  "details": {
    "author_name": "example-user",
    "target_id": 31,
    "target_type": "Group",
    "target_details": "another-group",
    "custom_message": "Removed project group link",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example-user",
  "entity_path": "example-group/example-project",
  "target_details": "another-group",
  "created_at": "2022-07-04T00:42:56.279Z",
  "target_type": "Group",
  "target_id": 31,
  "event_type": "project_group_link_destroy"
}
```

<a id="audit-event-streaming-on-invalid-merge-request-approver-state"></a>

## 无效合并请求批准者状态的审计事件流

> 引入于 15.5 版本。

流传输与项目中无效的合并请求批准者状态相关的审计事件。

### Headers

Headers 的格式如下：

```plaintext
POST /logs HTTP/1.1
Host: <DESTINATION_HOST>
Content-Type: application/x-www-form-urlencoded
X-Gitlab-Event-Streaming-Token: <DESTINATION_TOKEN>
X-Gitlab-Audit-Event-Type: audit_operation
```

### 负载示例

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 6,
  "entity_type": "Project",
  "details": {
    "author_name": "example_username",
    "target_id": 20,
    "target_type": "MergeRequest",
    "target_details": { title: "Merge request title", iid: "Merge request iid", id: "Merge request id" },
    "custom_message": "Invalid merge request approver rules",
    "ip_address": "127.0.0.1",
    "entity_path": "example-group/example-project"
  },
  "ip_address": "127.0.0.1",
  "author_name": "example_username",
  "entity_path": "example-group/example-project",
  "target_details": "merge request title",
  "created_at": "2022-03-09T06:53:11.181Z",
  "target_type": "MergeRequest",
  "target_id": 20,
  "event_type": "audit_operation"
}
```
