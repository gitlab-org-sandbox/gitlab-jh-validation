---
stage: Enablement
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 Linux 软件包安装的独立 PostgreSQL **(BASIC SELF)**

如果您希望将数据库服务与极狐GitLab 应用程序服务器分开托管，您可以使用与 Linux 软件包安装打包在一起的 PostgreSQL 二进制文件来实现。<!--建议将此作为我们[最多 2,000 个用户的参考架构](../reference_architectures/2k_users.md)的一部分。-->

## 设置

1. SSH 连接到 PostgreSQL 服务器。
1. 使用来自下载页面的 *步骤 1 和 2* [下载并安装](https://about.gitlab.cn/install/)您想要的 Linux 软件包。
   - 不要完成下载页面上的任何其它步骤。
1. 为 PostgreSQL 生成密码哈希。假设您使用的是 `gitlab`（推荐）的默认用户名。该命令要求输入密码并进行确认。使用该命令在下一步中输出的值作为 `POSTGRESQL_PASSWORD_HASH` 的值。

   ```shell
   sudo gitlab-ctl pg-password-md5 gitlab
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下内容，适当更新占位符的值。

   - `POSTGRESQL_PASSWORD_HASH` - 上一步输出的值
   - `APPLICATION_SERVER_IP_BLOCKS` - 连接到数据库的 GitLab 应用程序服务器的 IP 子网或 IP 地址的列表，以空格分隔。示例：`%w(123.123.123.123/32 123.123.123.234/32)`

   ```ruby
   # Disable all components except PostgreSQL
   roles(['postgres_role'])
   prometheus['enable'] = false
   alertmanager['enable'] = false
   pgbouncer_exporter['enable'] = false
   redis_exporter['enable'] = false
   gitlab_exporter['enable'] = false

   postgresql['listen_address'] = '0.0.0.0'
   postgresql['port'] = 5432

   # Replace POSTGRESQL_PASSWORD_HASH with a generated md5 value
   postgresql['sql_user_password'] = 'POSTGRESQL_PASSWORD_HASH'

   # Replace XXX.XXX.XXX.XXX/YY with Network Address
   # ????
   postgresql['trust_auth_cidr_addresses'] = %w(APPLICATION_SERVER_IP_BLOCKS)

   # Disable automatic database migrations
   gitlab_rails['auto_migrate'] = false
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。
1. 记下 PostgreSQL 节点的 IP 地址或主机名、端口和纯文本密码。这些在稍后配置 GitLab 应用程序服务器时是必需的。
1. [启用监控](replication_and_failover.md#启用监控)

支持高级配置选项，可以根据需要添加。
