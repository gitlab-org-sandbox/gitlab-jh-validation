---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 配置您的极狐GitLab 安装 **(BASIC SELF)**

自定义并配置您的私有化部署极狐GitLab 安装。

- [身份认证](auth/index.md)
- [CI/CD](../administration/cicd.md)
- [配置](../administration/admin_area.md)
- [Consul](../administration/consul.md)
- [环境变量](../administration/environment_variables.md)
- [文件钩子](../administration/file_hooks.md)
- [Git 协议 v2](../administration/git_protocol.md)
- [接收电子邮件](../administration/incoming_email.md)
- [实例限制](../administration/instance_limits.md)<!-- [实例评审](../administration/instance_review.md)-->
- [PostgreSQL](../administration/postgresql/index.md)
- [负载均衡器](../administration/load_balancer.md)<!-- [NFS](../administration/nfs.md)-->
- [Postfix](../administration/reply_by_email_postfix_setup.md)<!-- [Redis](../administration/redis/index.md)-->
- [Sidekiq](../administration/sidekiq/index.md) <!--[S/MIME 签名](../administration/smime_signing_email.md)-->
- [仓库存储](../administration/repository_storage_paths.md)
- [对象存储](../administration/object_storage.md)
- [合并请求差异存储](../administration/merge_request_diffs.md)
- [静态对象外部存储](../administration/static_objects_external_storage.md)
- [Geo](../administration/geo/index.md)
- [灾难恢复 (Geo)](../administration/geo/disaster_recovery/index.md)
- [Kubernetes 代理服务器](../administration/clusters/kas.md)
- [服务器钩子](../administration/server_hooks.md)
- [Terraform 状态](../administration/terraform_state.md)
- [Terraform 限制](settings/terraform_limits.md)
- [软件包](../administration/packages/index.md)
- [网络终端](../administration/integration/terminal.md)
- [Wiki](../administration/wikis/index.md)
- [使 Markdown 缓存无效](../administration/invalidate_markdown_cache.md)
- [议题关闭样式](../administration/issue_closing_pattern.md)<!--[代码片段](../administration/snippets/index.md)--> <!--[Host the product documentation](../administration/docs_self_host.md)-->
