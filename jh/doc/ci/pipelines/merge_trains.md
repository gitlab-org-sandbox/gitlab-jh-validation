---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
last_update: 2019-07-03
---

# 合并队列 **(PREMIUM ALL)**

> 对[快进式合并](../../user/project/merge_requests/methods/index.md#fast-forward-merge)和[半线性](../../user/project/merge_requests/methods/index.md#merge-commit-with-semi-linear-history)合并方法的支持引入于极狐GitLab 16.5，[功能标志](../../administration/feature_flags.md)为 `fast_forward_merge_trains_support`。默认启用。

NOTE:
在 16.0 及更高版本中，**开启合并队列** 和 **流水线成功时开启合并队列** 按钮变更为 **设置为自动合并**。**从合并队列中删除** 变更为 **取消自动合并**。

使用合并队列对合并请求进行排队，并在将它们合并到目标分支之前验证它们的更改是否可以协同工作。

在频繁合并到默认分支的项目中，不同合并请求的更改可能会相互冲突。[合并结果流水线](merged_results_pipelines.md)确保更改适用于默认分支中的内容，但不适用于其他人同时合并的内容。

合并队列不适用于[半线性历史合并请求](../../user/project/merge_requests/methods/index.md#merge-commit-with-semi-linear-history)或[快进式合并请求](../../user/project/merge_requests/methods/index.md#fast-forward-merge)。

<!--
有关以下内容的更多信息：

- 合并队列如何工作，查看[合并队列工作流程](#merge-train-workflow)。
- Why you might want to use merge trains, read [How starting merge trains improve efficiency for DevOps](https://about.gitlab.com/blog/2020/01/30/all-aboard-merge-trains/).
-->

<a id="merge-train-workflow"></a>

## 合并队列工作流

当没有合并请求等待合并，并且您选择[**开始合并队列**](#start-a-merge-train)时，合并队列开始。极狐GitLab 启动合并队列流水线，验证更改是否可以合并到默认分支中。第一个流水线与[合并结果流水线](merged_results_pipelines.md)相同，在源分支和目标分支组合在一起的变化上运行。内部合并结果提交的作者是发起合并的用户。

要在第一个流水线完成后，立即合并第二个合并请求，请选择[**设置为自动合并**](#add-a-merge-request-to-a-merge-train)，并将其添加到队列。
第二个合并队列流水线运行在合并请求与目标分支相结合的变化上。类似地，如果您添加第三个合并请求，该流水线将在与目标分支合并的所有三个合并请求的更改上运行。流水线全部并行运行。

每个合并请求仅在以下情况下合并到目标分支：

- 合并请求的流水线成功完成。
- 在合并之前排队的所有其他合并请求。

如果合并队列流水线失败，则不会合并合并请求。极狐GitLab 从合并队列中删除该合并请求，并为在它之后排队的所有合并请求启动新的流水线。

例如：

三个合并请求（`A`、`B` 和 `C`）按顺序添加到合并队列中，将创建三个并行运行的合并结果流水线：

1. 第一个流水线运行在 `A` 与目标分支相结合的变化上。
1. 第二个流水线运行在来自 `A` 和 `B` 的变化以及目标分支上。
1. 第三个流水线运行在 `A`、`B` 和 `C` 与目标分支相结合的变化上。

如果 `B` 的流水线失败：

- 第一个流水线（`A`）继续运行。
- `B` 从队列中移除。
- `C` 的流水线[被取消](#automatic-pipeline-cancellation)，一个新的流水线开始，用于从 `A` 和 `C` 与目标分支相结合的变化（没有 `B` 变化）。

如果 `A` 成功完成，它会合并到目标分支，而 `C` 继续运行。添加到队列中的任何新合并请求包括目标分支中的 `A` 更改，以及合并队列中的 `C` 更改。

<a id="automatic-pipeline-cancellation"></a>

### 自动取消流水线

极狐GitLab CI/CD 检测冗余流水线，并取消它们以节省资源。

冗余合并队列流水线发生在：

- 流水线因合并队列中的合并请求之一而失败。
- 您[跳过合并队列并立即合并](#skip-the-merge-train-and-merge-immediately)。
- 您[从合并队列中删除合并请求](#remove-a-merge-request-from-a-merge-train)。

在这些情况下，极狐GitLab 必须为队列上的部分或全部合并请求创建新的合并队列流水线。旧流水线正在与合并队列中先前的组合更改进行比较，这些更改不再有效，因此这些旧流水线被取消。

<a id="enable-merge-trains"></a>

## 启用合并队列

先决条件：

- 您必须具有维护者角色。
- 您的代码库必须是极狐GitLab 代码库，而不是[外部代码库](../ci_cd_for_external_repos/index.md)。
- 您的流水线必须[配置为使用合并请求流水线](merge_request_pipelines.md#prerequisites)。否则，您的合并请求可能会陷入未解决状态，或者您的流水线可能会被丢弃。

要启用合并队列：

1. 在左侧边栏中，选择 **搜索或转到** 并找到您的项目。
1. 选择 **设置 > 合并请求**。
1. 在 **合并方法** 部分，验证是否选择了 **合并提交**。
1. 在 **合并选项** 部分：
   - 在 13.6 及更高版本，选择 **启用合并结果流水线** 和 **启用合并队列**。
   - 在 13.5 及更早版本，选择 **为合并结果启用合并队列和流水线**。<!--此外，必须正确设置[功能标志](#disable-merge-trains-in-gitlab-135-and-earlier)。-->
1. 选择 **保存更改**。

## 启动合并队列

先决条件：

- 您必须具有[权限](../../user/permissions.md)，才能合并或推送到目标分支。

要启动合并队列：

1. 访问合并请求。
1. 选择：
    - 当没有流水线运行时，**合并**。
    - 当有流水线运行时，**设置为自动合并**。

合并请求的合并队列状态显示在流水线部件下方，并显示类似于 `A new merge train has started and this merge request is the first of the queue.` 的消息。

现在可以将其他合并请求添加到队列中。

<a id="add-a-merge-request-to-a-merge-train"></a>

## 将合并请求添加到合并队列

先决条件：

- 您必须具有[权限](../../user/permissions.md)，才能合并或推送到目标分支。

将合并请求添加到合并队列：

1. 访问合并请求。
1. 选择：
    - 当没有流水线运行时，**合并**。
    - 当有流水线运行时，**设置为自动合并**。

合并请求的合并队列状态显示在流水线部件下方，并带有类似 `Added to the merge train. There are 2 merge requests waiting to be merged.` 的消息。

每个合并队列最多可以并行运行 20 个流水线。 如果您将超过 20 个合并请求添加到合并队列，额外的合并请求将排队等待流水线完成。等待加入合并队列的排队合并请求的数量没有限制。

## 从合并队列中删除合并请求

要从合并队列中删除合并请求，请选择 **取消自动合并**。
您可以稍后再次将合并请求添加到合并队列。

当您从合并队列中删除合并请求时：

- 合并请求的所有流水线在删除的合并请求重新启动后排队。
- 冗余流水线[被取消](#automatic-pipeline-cancellation)。

## 跳过合并队列并立即合并

如果您有高优先级合并请求，例如必须紧急合并的关键补丁，请选择 **立即合并**。

当您立即合并合并请求时：

- 重新创建当前合并队列。
- 所有流水线重新启动。
- 冗余流水线[被取消](#automatic-pipeline-cancellation)。

WARNING:
立即合并会使用大量的 CI/CD 资源。仅在紧急情况下使用此选项。

## 在 13.5 及更早版本中禁用合并队列 **(PREMIUM SELF)**

在 13.6 及更高版本中，您可以[在项目设置中启用或禁用合并队列](#enable-merge-trains)。

在 13.5 及更早版本中，合并队列会在启用[合并结果流水线](merged_results_pipelines.md)时自动启用。
要使用合并结果流水线但不合并队列，请启用 `disable_merge_trains` [功能标志](../../user/feature_flags.md)。

[有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员](../../administration/feature_flags.md)可以启用功能标志来禁用合并队列：

```ruby
Feature.enable(:disable_merge_trains)
```

启用此功能标志后，极狐GitLab 会取消现存的合并队列。

要禁用功能标志，即重新启用合并队列：

```ruby
Feature.disable(:disable_merge_trains)
```

## 故障排除

### 合并请求从合并队列中丢弃

如果合并请求在合并队列流水线运行时不可合并，合并队列会自动丢弃您的合并请求。例如，这可能是由以下原因引起的：

- 将合并请求更改为[草稿](../../user/project/merge_requests/drafts.md)。
- 合并冲突。
- 当启用"必须解决所有主题"时，有未解决的主题。

您可以从系统日志中找到合并请求从合并队列中丢弃的原因。
查看 **概览** 选项卡中的 **活动** 部分是否有类似以下内容的消息：
`User removed this merge request from the merge train because ...`

### 无法使用自动合并

启用合并队列时，无法使用[自动合并](../../user/project/merge_requests/merge_when_pipeline_succeeds.md)（之前名为 **流水线成功时合并**）以跳过合并队列。
<!--
See [issue 12267](https://gitlab.com/gitlab-org/gitlab/-/issues/12267) for more information.
-->

### 无法重试合并队列流水线

当合并队列流水线失败时，合并请求会从合并队列中丢弃且无法重试流水线。
合并队列流水线在合并请求中的更改和队列上已有的其他合并请求的更改的合并结果上运行。如果合并请求从队列中丢弃，则合并结果将过期，并且流水线无法重试。
您可以：

- 再次将合并请求添加到列车，触发新的流水线。
- 如果间歇性失败，请将 [`retry`](../yaml/index.md#retry) 关键字添加到作业中。
  如果重试后成功，则合并请求不会从合并队列中丢弃。

### 无法添加合并队列

当启用[**流水线必须成功**](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#require-a-successful-pipeline-for-merge)，但最新流水线失败时：

- **设置为自动合并** 或 **合并** 选项不可用。
- 合并请求显示 `The pipeline for this merge request failed. Please retry the job or push a new commit to fix the failure.`。

在将合并请求重新添加到合并队列之前，您可以尝试：

- 重试失败的作业。如果通过，并且没有其他作业失败，则流水线将标记为成功。
- 重新运行整个流水线。在 **流水线** 选项卡上，选择 **运行流水线**。
- 推送修复问题的新提交，也会触发新的流水线。

