---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 在极狐GitLab CI/CD 中使用 Azure Key Vault secret **(PREMIUM ALL)**

> 引入于极狐GitLab 和极狐GitLab Runner 16.3。

您可以在极狐GitLab CI/CD 流水线中使用存储在 [Azure Key Vault](https://azure.microsoft.com/en-us/products/key-vault/) 中的 secret。

先决条件：

- 在 Azure 上有一个 key vault。
- 拥有具有 key vault 权限的应用程序。
- 在 Azure 中配置 OpenID Connect 以检索临时凭据。<!--[Azure Key Vault](https://azure.microsoft.com/en-us/products/key-vault/)-->
- 将 [CI/CD 变量添加到您的项目](../variables/index.md#for-a-project)以提供有关 Vault 服务器的详细信息：
    - `AZURE_KEY_VAULT_SERVER_URL`：Azure Key Vault 服务器的 URL，例如 `https://vault.example.com`。
    - `AZURE_CLIENT_ID`：Azure 应用程序的客户端 ID。
    - `AZURE_TENANT_ID`：Azure 应用程序的租户 ID。

## 在 CI/CD 作业中使用 Azure Key Vault secret

您可以通过使用 [`azure_key_vault`](../yaml/index.md#secretsazure_key_vault) 关键字进行定义来使用存储在作业的 Azure Key Vault 中的 secret：

```yaml
job:
  id_tokens:
    AZURE_JWT:
      aud: 'azure'
  secrets:
    DATABASE_PASSWORD:
      token: AZURE_JWT
      azure_key_vault:
        name: 'test'
        version: 'test'
```

在此示例中：

- `name` 是 secret 的名称。
- `version` 是 secret 的版本。
- 极狐GitLab 从 Azure Key Vault 获取 secret 并将该值存储在临时文件中。
  该文件的路径存储在 `DATABASE_PASSWORD` CI/CD 变量中，类似于[文件类型 CI/CD 变量](../variables/index.md#use-file-type-cicd-variables)。
