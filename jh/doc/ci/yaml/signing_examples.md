---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用 Sigstore 进行无密钥签名和验证 **(BASIC SAAS)**

[Sigstore](https://www.sigstore.dev/) 项目提供了一个名为 [Cosign](https://docs.sigstore.dev/cosign/overview/) 的 CLI，可用于使用极狐GitLab CI/CD 构建的容器镜像的无密钥签名。无密钥签名有很多优点，包括无需管理、保护和轮换私钥。
Cosign 请求一个短期密钥对用于签名，将其记录在证书透明度日志中，然后将其丢弃。密钥是通过使用运行流水线的用户的 OIDC 身份从极狐GitLab 服务器获取的令牌生成的。该令牌包含证明该令牌是由 CI/CD 流水线生成的独特声明。要了解更多信息，请参阅有关无密钥签名的 Cosign [文档](https://docs.sigstore.dev/cosign/overview/#example-working-with-containers)。

有关极狐GitLab OIDC 声明和 Fulcio 证书扩展之间映射的详细信息，请参阅[将 OIDC 令牌声明映射到 Fulcio OID](https://github.com/sigstore/fulcio/blob/main/docs/oid-info.md#mapping-oidc-token-claims-to-fulcio-oids) 的极狐GitLab 专栏。

**要求**：

- 您必须使用 JihuLab.com。
- 您项目的 CI/CD 配置必须位于项目中。
- 您必须使用 `>= 2.0.1` 的 Cosign 版本。

## 容器镜像

### 对容器镜像进行签名

Cosign 可以使用极狐GitLab [ID 令牌](../secrets/id_token_authentication.md)进行[无密钥签名](https://docs.sigstore.dev/cosign/overview/#example-working-with-containers)。
令牌必须将 `sigstore` 设置为 [`aud`](../secrets/id_token_authentication.md#token-payload) 声明。当在 `SIGSTORE_ID_TOKEN` 环境变量中设置该令牌时，Cosign 可以自动使用该令牌。

**最佳实践**：

- 在同一个作业中构建并签署容器镜像，以防止镜像在签名之前被篡改。

请参阅 [Cosign 文档](https://docs.sigstore.dev/cosign/signing_with_containers/)了解有关签名的更多信息。

```yaml
build_and_sign:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  variables:
    COSIGN_YES: "true"
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  before_script:
    - apk add --update cosign
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - IMAGE_DIGEST=$(docker inspect --format='{{index .RepoDigests 0}}' $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA)
    - cosign sign $IMAGE_DIGEST
```

### 使用 Cosign 验证容器镜像

```yaml
verify:
  image: alpine:3.18
  stage: verify
  before_script:
    - apk add --update cosign docker
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - cosign verify "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" --certificate-identity "https://gitlab.com/my-group/my-project//path/to/.gitlab-ci.yml@refs/heads/main" --certificate-oidc-issuer "https://gitlab.com"
```

## 使用 Sigstore 和 npm 生成无密钥来源

您可以使用 Sigstore 和 npm 以及极狐GitLab CI/CD 对构建产物进行数字签名，而无需花费密钥管理的开销。

### 关于 npm 来源

[npm CLI](https://docs.npmjs.com/cli) 允许软件包维护者向用户提供来源证明。使用 npm CLI 来源生成允许用户信任并验证他们正在下载和使用的包是否来自您和构建它的构建系统。

有关如何发布 npm 包的更多信息，请参阅[极狐GitLab npm 软件包库](../../user/packages/npm_registry/index.md)。

### Sigstore

[Sigstore](https://www.sigstore.dev/) 是一组工具，软件包管理器和安全专家可以使用它们来保护其软件供应链免受攻击。它汇集了 Fulcio、Cosign 和 Rekor 等免费使用的开源技术，可以处理数字签名、验证和来源检查，从而使开源软件的分发和使用更加安全。

**相关主题**：

- [SLSA 来源定义](https://slsa.dev/provenance/v1)
- [npm 文档](https://docs.npmjs.com/generating-provenance-statements)
- [npm 来源 RFC](https://github.com/npm/rfcs/blob/main/accepted/0049-link-packages-to-source-and-build.md#detailed-steps-to-publish)

### 在极狐GitLab CI/CD 中生成来源

现在，Sigstore 支持如上所述的极狐GitLab OIDC，您可以将 npm 来源与极狐GitLab CI/CD 和 Sigstore 一起使用，为极狐GitLab CI/CD 流水线中的 npm 包生成并签署来源。

#### 先决条件

1. 将您的极狐GitLab [ID 令牌](../secrets/id_token_authentication.md) `aud` 设置为 `sigstore`。
1. 添加 `--provenance` 标志以让 npm 发布。

要添加到 `.gitlab-ci.yml` 文件的示例内容：

```yaml
image: node:latest

build:
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  script:
    - npm publish --provenance --access public
```

npm 极狐GitLab 模板也提供了此功能。<!--the example is in
the [templates documentation](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/npm.gitlab-ci.yml)-->

## 验证 npm 来源

npm CLI 还为最终用户提供验证包来源的功能。

```plaintext
npm audit signatures
audited 1 package in 0s
1 package has a verified registry signature
```

### 检查来源元数据

Rekor 透明度日志存储每个带有来源的发布的包的证书和证明。例如[以下示例的条目](https://search.sigstore.dev/?logIndex=21076013)。

由 npm 生成的来源文档示例：

```yaml
_type: https://in-toto.io/Statement/v0.1
subject:
  - name: pkg:npm/%40strongjz/strongcoin@0.0.13
    digest:
      sha512: >-
        924a134a0fd4fe6a7c87b4687bf0ac898b9153218ce9ad75798cc27ab2cddbeff77541f3847049bd5e3dfd74cea0a83754e7686852f34b185c3621d3932bc3c8
predicateType: https://slsa.dev/provenance/v0.2
predicate:
  buildType: https://github.com/npm/CLI/gitlab/v0alpha1
  builder:
    id: https://gitlab.com/strongjz/npm-provenance-example/-/runners/12270835
  invocation:
    configSource:
      uri: git+https://gitlab.com/strongjz/npm-provenance-example
      digest:
        sha1: 6e02e901e936bfac3d4691984dff8c505410cbc3
      entryPoint: deploy
    parameters:
      CI: 'true'
      CI_API_GRAPHQL_URL: https://gitlab.com/api/graphql
      CI_API_V4_URL: https://gitlab.com/api/v4
      CI_COMMIT_BEFORE_SHA: 7d3e913e5375f68700e0c34aa90b0be7843edf6c
      CI_COMMIT_BRANCH: main
      CI_COMMIT_REF_NAME: main
      CI_COMMIT_REF_PROTECTED: 'true'
      CI_COMMIT_REF_SLUG: main
      CI_COMMIT_SHA: 6e02e901e936bfac3d4691984dff8c505410cbc3
      CI_COMMIT_SHORT_SHA: 6e02e901
      CI_COMMIT_TIMESTAMP: '2023-05-19T10:17:12-04:00'
      CI_COMMIT_TITLE: trying to publish to gitlab reg
      CI_CONFIG_PATH: .gitlab-ci.yml
      CI_DEFAULT_BRANCH: main
      CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX: gitlab.com:443/strongjz/dependency_proxy/containers
      CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX: gitlab.com:443/strongjz/dependency_proxy/containers
      CI_DEPENDENCY_PROXY_SERVER: gitlab.com:443
      CI_DEPENDENCY_PROXY_USER: gitlab-ci-token
      CI_JOB_ID: '4316132595'
      CI_JOB_NAME: deploy
      CI_JOB_NAME_SLUG: deploy
      CI_JOB_STAGE: deploy
      CI_JOB_STARTED_AT: '2023-05-19T14:17:23Z'
      CI_JOB_URL: https://gitlab.com/strongjz/npm-provenance-example/-/jobs/4316132595
      CI_NODE_TOTAL: '1'
      CI_PAGES_DOMAIN: gitlab.io
      CI_PAGES_URL: https://strongjz.gitlab.io/npm-provenance-example
      CI_PIPELINE_CREATED_AT: '2023-05-19T14:17:21Z'
      CI_PIPELINE_ID: '872773336'
      CI_PIPELINE_IID: '40'
      CI_PIPELINE_SOURCE: push
      CI_PIPELINE_URL: https://gitlab.com/strongjz/npm-provenance-example/-/pipelines/872773336
      CI_PROJECT_CLASSIFICATION_LABEL: ''
      CI_PROJECT_DESCRIPTION: ''
      CI_PROJECT_ID: '45821955'
      CI_PROJECT_NAME: npm-provenance-example
      CI_PROJECT_NAMESPACE: strongjz
      CI_PROJECT_NAMESPACE_ID: '36018'
      CI_PROJECT_PATH: strongjz/npm-provenance-example
      CI_PROJECT_PATH_SLUG: strongjz-npm-provenance-example
      CI_PROJECT_REPOSITORY_LANGUAGES: javascript,dockerfile
      CI_PROJECT_ROOT_NAMESPACE: strongjz
      CI_PROJECT_TITLE: npm-provenance-example
      CI_PROJECT_URL: https://gitlab.com/strongjz/npm-provenance-example
      CI_PROJECT_VISIBILITY: public
      CI_REGISTRY: registry.gitlab.com
      CI_REGISTRY_IMAGE: registry.gitlab.com/strongjz/npm-provenance-example
      CI_REGISTRY_USER: gitlab-ci-token
      CI_RUNNER_DESCRIPTION: 3-blue.shared.runners-manager.gitlab.com/default
      CI_RUNNER_ID: '12270835'
      CI_RUNNER_TAGS: >-
        ["gce", "east-c", "linux", "ruby", "mysql", "postgres", "mongo",
        "git-annex", "shared", "docker", "saas-linux-small-amd64"]
      CI_SERVER_HOST: gitlab.com
      CI_SERVER_NAME: GitLab
      CI_SERVER_PORT: '443'
      CI_SERVER_PROTOCOL: https
      CI_SERVER_REVISION: 9d4873fd3c5
      CI_SERVER_SHELL_SSH_HOST: gitlab.com
      CI_SERVER_SHELL_SSH_PORT: '22'
      CI_SERVER_URL: https://gitlab.com
      CI_SERVER_VERSION: 16.1.0-pre
      CI_SERVER_VERSION_MAJOR: '16'
      CI_SERVER_VERSION_MINOR: '1'
      CI_SERVER_VERSION_PATCH: '0'
      CI_TEMPLATE_REGISTRY_HOST: registry.gitlab.com
      GITLAB_CI: 'true'
      GITLAB_FEATURES: >-
        elastic_search,ldap_group_sync,multiple_ldap_servers,seat_link,usage_quotas,zoekt_code_search,repository_size_limit,admin_audit_log,auditor_user,custom_file_templates,custom_project_templates,db_load_balancing,default_branch_protection_restriction_in_groups,extended_audit_events,external_authorization_service_api_management,geo,instance_level_scim,ldap_group_sync_filter,object_storage,pages_size_limit,project_aliases,password_complexity,enterprise_templates,git_abuse_rate_limit,required_ci_templates,runner_maintenance_note,runner_performance_insights,runner_upgrade_management,runner_jobs_statistics
      GITLAB_USER_ID: '31705'
      GITLAB_USER_LOGIN: strongjz
    environment:
      name: 3-blue.shared.runners-manager.gitlab.com/default
      architecture: linux/amd64
      server: https://gitlab.com
      project: strongjz/npm-provenance-example
      job:
        id: '4316132595'
      pipeline:
        id: '872773336'
        ref: .gitlab-ci.yml
  metadata:
    buildInvocationId: https://gitlab.com/strongjz/npm-provenance-example/-/jobs/4316132595
    completeness:
      parameters: true
      environment: true
      materials: false
    reproducible: false
  materials:
    - uri: git+https://gitlab.com/strongjz/npm-provenance-example
      digest:
        sha1: 6e02e901e936bfac3d4691984dff8c505410cbc3
```

## 构建产物

您可以使用 Cosign 对构建产物进行签名并验证使用 Cosign 签名的产物。

### 使用 Cosign 签名构建产物

Cosign 可以使用极狐GitLab [ID 令牌](../secrets/id_token_authentication.md)对构建产物进行无密钥签名。
令牌必须将 `sigstore` 设置为 [`aud`](../secrets/id_token_authentication.md#token-payload) 声明。当在 `SIGSTORE_ID_TOKEN` 环境变量中设置该令牌时，Cosign 可以自动使用该令牌。

**最佳实践**：

- 在同一作业中构建和签署产物，以防止产物在签名之前被篡改。

有关签名产物的更多信息，请参见 [Cosign 文档](https://docs.sigstore.dev/cosign/signing_with_blobs/#keyless-signing-of-blobs-and-files)。

```yaml
sign_artifact:
  stage: build
  image: alpine:latest
  variables:
    COSIGN_YES: "true"
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  before_script:
    - apk add --update cosign
  script:
    - echo "This is a build artifact" > artifact.txt
    - cosign sign-blob artifact.txt --bundle cosign.bundle
  artifacts:
    paths:
      - artifact.txt
      - cosign.bundle
```

### 使用 Cosign 验证构建产物

验证产物需要产物本身和 `cosign sign-blob` 生成的 `cosign.bundle` 文件。
`--certificate-identity` 选项应引用对产物进行签名的项目和分支。
`--certificate-oidc-issuer` 选项应引用对产物进行签名的极狐GitLab 实例。

有关验证签名产物的更多信息，请参见 [Cosign 文档](https://docs.sigstore.dev/cosign/verify/)。

```yaml
verify_artifact:
  stage: verify
  image: alpine:latest
  before_script:
    - apk add --update cosign
  script:
    - cosign verify-blob artifact.txt --bundle cosign.bundle --certificate-identity "https://gitlab.com/my-group/my-project//path/to/.gitlab-ci.yml@refs/heads/main" --certificate-oidc-issuer "https://gitlab.com"
```
