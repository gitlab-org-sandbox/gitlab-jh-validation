---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 在 Docker-in-Docker 中镜像库的身份验证

当您使用 Docker-in-Docker 时，[标准身份验证方法](using_docker_images.md#access-an-image-from-a-private-container-registry)无效，因为新的 Docker daemon 随服务一起启动。

## 选项 1：运行 `docker login`

在 [`before_script`](../yaml/index.md#before_script) 中，运行 `docker login`：

```yaml
image: docker:20.10.16

variables:
  DOCKER_TLS_CERTDIR: "/certs"

services:
  - docker:20.10.16-dind

build:
  stage: build
  before_script:
    - echo "$DOCKER_REGISTRY_PASS" | docker login $DOCKER_REGISTRY --username $DOCKER_REGISTRY_USER --password-stdin
  script:
    - docker build -t my-docker-image .
    - docker run my-docker-image /script/to/run/tests
```

要登录 Docker Hub，请将 `$DOCKER_REGISTRY` 留空或将其删除。

## 选项 2：在每个作业上挂载 `~/.docker/config.json`

如果您是极狐GitLab Runner 的管理员，则可以将具有身份验证配置的文件挂载到 `~/.docker/config.json`。
然后 runner 选择的每个作业都已经过认证。如果您使用的是官方 `docker:20.10.16` 镜像，则主目录位于 `/root` 下。

如果挂载配置文件，任何修改 `~/.docker/config.json` 的 docker 命令都会失败。例如，`docker login` 失败，因为该文件被挂载为只读。不要将其更改为只读，否则会导致问题。

以下是 `/opt/.docker/config.json` 的示例，遵循 [`DOCKER_AUTH_CONFIG`](using_docker_images.md#determine-your-docker_auth_config-data) 文档：

```json
{
    "auths": {
        "https://index.docker.io/v1/": {
            "auth": "bXlfdXNlcm5hbWU6bXlfcGFzc3dvcmQ="
        }
    }
}
```

### Docker

更新[卷挂载](https://docs.gitlab.cn/runner/configuration/advanced-configuration.html#volumes-in-the-runnersdocker-section)来包含该文件。

```toml
[[runners]]
  ...
  executor = "docker"
  [runners.docker]
    ...
    privileged = true
    volumes = ["/opt/.docker/config.json:/root/.docker/config.json:ro"]
```

### Kubernetes

使用此文件的内容创建一个 [ConfigMap](https://kubernetes.io/docs/concepts/configuration/configmap/)。您可以使用如下命令执行此操作：

```shell
kubectl create configmap docker-client-config --namespace gitlab-runner --from-file /opt/.docker/config.json
```

更新[卷挂载](https://docs.gitlab.cn/runner/executors/kubernetes.html#using-volumes)来包含该文件。

```toml
[[runners]]
  ...
  executor = "kubernetes"
  [runners.kubernetes]
    image = "alpine:3.12"
    privileged = true
    [[runners.kubernetes.volumes.config_map]]
      name = "docker-client-config"
      mount_path = "/root/.docker/config.json"
      # If you are running GitLab Runner 13.5
      # or lower you can remove this
      sub_path = "config.json"
```

## 选项 3：使用 `DOCKER_AUTH_CONFIG`

如果您已经定义了 [`DOCKER_AUTH_CONFIG`](using_docker_images.md#determine-your-docker_auth_config-data)，则可以使用该变量并将其保存在 `~/.docker/config.json` 中。

您可以通过多种方式定义此身份验证：

- 在 runner 配置文件中的 [`pre_build_script`](https://docs.gitlab.cn/runner/configuration/advanced-configuration.html#the-runners-section) 中。
- 在 [`before_script`](../yaml/index.md#before_script) 中。
- 在 [`script`](../yaml/index.md#script) 中。

以下示例显示 [`before_script`](../yaml/index.md#before_script)。
相同的命令适用于您实施的任何解决方案。

```yaml
image: docker:20.10.16

variables:
  DOCKER_TLS_CERTDIR: "/certs"

services:
  - docker:20.10.16-dind

build:
  stage: build
  before_script:
    - mkdir -p $HOME/.docker
    - echo $DOCKER_AUTH_CONFIG > $HOME/.docker/config.json
  script:
    - docker build -t my-docker-image .
    - docker run my-docker-image /script/to/run/tests
```
