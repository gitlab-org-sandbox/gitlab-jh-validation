---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 极狐GitLab 令牌概览 **(BASIC ALL)**

本文档列出了极狐GitLab 中使用的令牌、它们的用途以及安全指南（如适用）。

<a id="personal-access-tokens"></a>

## 个人访问令牌

您可以创建[个人访问令牌](../user/profile/personal_access_tokens.md)，进行以下身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制个人访问令牌的范围和到期日期。默认情况下，令牌从创建令牌的用户处继承权限。

您可以使用个人访问令牌 API 以编程方式操作，例如[轮换个人访问令牌](../api/personal_access_tokens.md#rotate-a-personal-access-token)。

当个人访问令牌距离到期日还有 7 天或更短时间时，您将收到一封电子邮件。

## OAuth2 令牌

极狐GitLab 可以充当 [OAuth2 provider](../api/oauth2.md)，允许其他服务代表用户访问极狐GitLab API。

您可以限制 OAuth2 令牌的范围和生命周期。

## 模拟令牌

[模拟令牌](../api/index.md#impersonation-tokens)是一种特殊类型的个人访问令牌。它只能由管理员为特定用户创建。模拟令牌可以帮助您构建应用程序或脚本，以特定用户身份使用极狐GitLab API、仓库和 registry 进行身份验证。

您可以限制模拟令牌的范围并设置到期日期。

## 项目访问令牌

[项目访问令牌](../user/project/settings/project_access_tokens.md#project-access-tokens)的范围为项目。与[个人访问令牌](#personal-access-tokens)一样，您可以使用它们进行以下身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制项目访问令牌的范围和到期日期。当您创建项目访问令牌时，极狐GitLab 会创建一个[用于项目的机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)。
项目的 Bot 用户是服务账户，不计入许可席位。

您可以使用[项目访问令牌 API](../api/project_access_tokens.md) 以编程方式操作，例如[轮换个人访问令牌](../api/personal_access_tokens.md#rotate-a-personal-access-token)。

当项目访问令牌距离到期日还有 7 天或更短时间时，所有项目维护者将会收到电子邮件通知。

## 群组访问令牌

[群组访问令牌](../user/group/settings/group_access_tokens.md#group-access-tokens)的范围为群组。与[个人访问令牌](#personal-access-tokens)一样，您可以使用它们进行身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制群组访问令牌的范围和到期日期。当您创建群组访问令牌时，极狐GitLab 会创建一个[用于群组的机器人用户](../user/group/settings/group_access_tokens.md#bot-users-for-groups)。
群组的 Bot 用户是服务账户，不计入许可席位。

您可以使用[群组访问令牌 API](../api/group_access_tokens.md) 以编程方式操作，例如[轮换个人访问令牌](../api/personal_access_tokens.md#rotate-a-personal-access-token)。

当群组访问令牌距离到期日还有 7 天或更短时间时，所有群组所有者将会收到电子邮件通知。

## 部署令牌

[部署令牌](../user/project/deploy_tokens/index.md)允许您在没有用户和密码的情况下，下载（`git clone`）或推送、拉取项目的软件包和容器镜像库镜像。部署令牌不能与极狐GitLab API 一起使用。部署令牌可以由项目维护者和所有者管理。

部署令牌可以由项目维护者或所有者管理。

## 部署密钥

[部署密钥](../user/project/deploy_keys/index.md)通过将 SSH 公钥导入极狐GitLab 实例，允许对仓库进行只读或读写访问。部署密钥不能与极狐GitLab API 或 registry 一起使用。

例如，这对于将仓库克隆到您的持续集成 (CI) 服务器很有用。通过使用部署密钥，您不必设置虚假用户账户。

项目维护者和所有者可以为项目仓库添加或启用部署密钥。

## Runner 身份验证令牌

在 16.0 及更高版本中，您可以使用 Runner 身份验证令牌来注册 Runner，而不是 Runner 注册令牌。Runner 注册令牌已弃用。

创建 Runner 及其配置后，您会收到用于注册 Runner 的 Runner 身份验证令牌。Runner 身份验证令牌本地存储在 [`config.toml`](https://docs.gitlab.cn/runner/configuration/advanced-configuration.html) 文件中，您可以使用该文件来配置 Runner。

当 Runner 从作业队列中获取作业时，它使用 Runner 身份验证令牌向极狐GitLab 进行身份验证。Runner 通过极狐GitLab 进行身份验证后，Runner 会收到一个[作业令牌](../ci/jobs/ci_job_token.md)，用于执行作业。

Runner 身份验证令牌保留在 Runner 机器上。以下执行器的执行环境只能访问作业令牌，而不能访问 Runner 身份验证令牌：

- Docker Machine
- Kubernetes
- VirtualBox
- Parallels
- SSH

对 Runner 文件系统的恶意访问可能会暴露 `config.toml` 文件和 Runner 身份验证令牌。攻击者可以使用 Runner 身份验证来克隆 Runner。

您可以使用 `runners` API 以编程方式[轮换或撤销 Runner 身份验证令牌](../api/runners.md#reset-runners-authentication-token-by-using-the-current-token)。

<a id="runner-registration-tokens"></a>

## Runner 注册令牌（已废弃）

WARNING:
传递 Runner 注册令牌的功能已被废弃，并计划在 18.0 中移除，同时支持某些配置参数。这是一个突破性的变化。极狐GitLab 实施了一项新的极狐GitLab Runner 令牌架构，引入了一种注册 Runner 的新方法，并消除了 Runner 注册令牌。

Runner 注册令牌用于向极狐GitLab 注册 Runner。群组或项目的所有者、实例管理员可以通过极狐GitLab 用户界面获取它们。注册令牌仅限于 Runner 注册，没有其它使用范围。

您可以使用 Runner 注册令牌添加在项目或群组中执行作业的 Runner。Runner 可以访问项目的代码，因此在分配项目和群组级别的权限时要小心。

## CI/CD 作业令牌

[CI/CD](../ci/jobs/ci_job_token.md) 作业令牌是一个短暂的令牌，仅在作业期间有效。它使 CI/CD 作业可以访问有限数量的 API 端点。
API 身份验证通过使用触发作业的用户的授权，使用作业令牌。

作业令牌因其生命周期短且范围有限而受到保护。如果多个作业在同一台机器上运行（例如使用 shell runner），则可能会泄漏。在 Docker 机器 runner 上，建议配置 `MaxBuilds=1`，确保 runner 机器只运行一个构建并在之后被销毁。这可能会影响性能，因为配置机器需要一些时间。

## 其他令牌

### Feed 令牌

每个用户都有一个不会过期的长期 Feed 令牌。该令牌允许：

- 对 RSS 阅读器进行身份验证以加载个性化 RSS Feed。
- 对日历应用程序进行身份验证以加载个性化日历。

您不能使用此令牌访问其他数据。

用户范围的 Feed 令牌可用于所有 Feed，但是 Feed 和日历 URL 使用仅对一种 Feed 有效的不同令牌而生成。

拥有您的令牌的任何人都可以像您一样读取活动并发布 RSS Feed 或您的日历 Feed，包括私密议题。如果发生这种情况，请[重置令牌](../user/profile/contributions_calendar.md#reset-the-user-activity-feed-token)。

### 传入电子邮件令牌

每个用户都有一个长期有效且不会过期的传入电子邮件令牌。此令牌允许用户[通过电子邮件创建新议题](../user/project/issues/create_issues.md#by-sending-an-email)，并包含在该用户的私人的项目特定电子邮件地址中。您不能使用此令牌访问其他数据。拥有您令牌的任何人都可以像您一样创建议题和合并请求。如果发生这种情况，请重置令牌。

## 可用范围

此表显示每个令牌的可用范围。可以在创建令牌时进一步限制范围。

|                             | API 访问 | Registry 访问 | 仓库访问 |
|-----------------------------|------------|-----------------|-------------------|
| 个人访问令牌       | ✅         | ✅              | ✅                |
| OAuth2 令牌                | ✅         | 🚫              | ✅                |
| 模拟令牌         | ✅         | ✅              | ✅                |
| 项目访问令牌        | ✅(1)      | ✅(1)           | ✅(1)             |
| 群组访问令牌          | ✅(2)      | ✅(2)           | ✅(2)             |
| 部署令牌                | 🚫         | ✅              | ✅                |
| 部署密钥                  | 🚫         | 🚫              | ✅                |
| Runner 注册令牌   | 🚫         | 🚫              | ✴️(3)              |
| Runner 验证令牌 | 🚫          | 🚫              | ✴️(3)              |
| 作业令牌                   | ✴️(4)       | 🚫              | ✅                |

1. 仅限于一个项目。
1. 仅限于一个群组。
1. Runner 注册和身份验证令牌不提供对仓库的直接访问，但可用于注册和验证一个新 runner，该 runner 可以执行可以访问仓库的作业。
1. 仅限于某些[端点](../ci/jobs/ci_job_token.md)。

## 安全注意事项

1. 像对待密码一样对待访问令牌并保证其安全。
1. 创建作用域令牌时，请考虑使用尽可能有限的作用域，以减少意外泄漏令牌的影响。
1. 创建令牌时，请考虑设置一个在任务完成后过期的令牌。例如，如果执行一次性导入，请将令牌设置为在几小时或一天后过期。这减少了令牌意外泄漏的影响，因为它在过期时毫无用处。
1. 如果您已经设置了演示环境来展示您一直在从事的项目，并且您正在录制视频或撰写描述该项目的博客文章，请确保您没有泄露敏感 secret（例如个人访问令牌（PAT）、Feed 令牌或触发令牌）。如果您已完成演示，则必须撤销在该演示期间创建的所有 secret。有关更多信息，请参阅[撤销 PAT](../user/profile/personal_access_tokens.md#revoke-a-personal-access-token)。
1. 将访问令牌添加到 URL 存在安全风险，尤其是在克隆或添加远程文件时，因为 Git 随后会将 URL 以纯文本形式写入其 `.git/config` 文件。 URL 通常也由代理和应用程序服务器记录，这使得系统管理员可以看到这些凭据。相反，使用类似 [`Private-Token` 标头](../api/rest/index.md#personalprojectgroup-access-tokens)的标头向 API 调用传递访问令牌。
1. 您还可以使用 [Git 凭证存储](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)来存储令牌。
1. 不要：
     - 在您的项目中以纯文本形式存储令牌。
     - 将代码、控制台命令或日志输出粘贴到议题、MR 描述或评论中时包含令牌。
       考虑一种方法，例如[在 CI 中使用外部 secret](../ci/secrets/index.md)。
1. 不要在控制台日志或产物中记录凭据。考虑[保护](../ci/variables/index.md#protect-a-cicd-variable)和[屏蔽](../ci/variables/index.md#mask-a-cicd-variable)您的凭据。
1. 定期检查所有类型的所有活动访问令牌并撤销不再需要的任何令牌。这包括：
     - 个人、项目和群组访问令牌。
     - Feed 令牌。
     - 触发令牌。
     - Runner 注册令牌。
     - 任何其他敏感 secret 等。
