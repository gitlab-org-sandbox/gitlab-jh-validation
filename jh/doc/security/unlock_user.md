---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 锁定用户 **(BASIC SELF)**

## 私有化部署用户

> 可配置的锁定用户策略引入于极狐GitLab 16.5。

默认情况下，用户登录尝试失败 10 次后将被锁定。这些用户保持锁定状态：

- 10 分钟，之后将自动解锁。
- 直到管理员在 10 分钟内从[管理中心](../administration/admin_area.md)或使用命令行解锁。

在 16.5 及更高版本中，管理员可以[使用 API](../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls) 进行配置：

- 锁定用户的失败登录尝试次数。
- 达到最大登录尝试失败次数后，锁定用户被锁定的时长（以分钟为单位）。

例如，管理员可以配置五次尝试登录失败后锁定用户，并且该用户将被锁定 60 分钟。

## JihuLab.com 用户

如果未启用 2FA，则用户在 24 小时内尝试登录 3 次失败后将被锁定。这些用户将保持锁定状态，直到：

- 他们下次成功登录，此时他们会收到一封包含六位解锁码的电子邮件，并重定向到验证页面，他们可以在其中输入解锁码来解锁账户。
- 验证账户所有权后，极狐GitLab 支持手动解锁账户。

如果启用 2FA，用户在尝试登录 3 次失败后将被锁定。30 分钟后账户将自动解锁。

## 从管理中心解锁用户

1. 在左侧边栏中，选择 **搜索或转到**。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **概览 > 用户**。
1. 使用搜索栏找到被锁定的用户。
1. 从 **用户管理** 下拉列表中选择 **解锁**。

## 使用命令行解锁用户

在十次失败的登录尝试后，用户进入锁定状态。

要解锁锁定的用户：

1. 通过 SSH 连接到您的极狐GitLab 服务器。
1. 启动 Ruby on Rails 控制台：

   ```shell
   ## For Omnibus GitLab
   sudo gitlab-rails console -e production

   ## For installations from source
   sudo -u git -H bundle exec rails console -e production
   ```

1. 找到要解锁的用户。您可以通过电子邮件进行搜索。

   ```ruby
   user = User.find_by(email: 'admin@local.host')
   ```

   您也可以通过 ID 进行搜索：

   ```ruby
   user = User.where(id: 1).first
   ```

1. 解锁用户：

   ```ruby
   user.unlock_access!
   ```

1. 使用 <kbd>Control</kbd>+<kbd>d</kbd> 退出控制台。

用户现在应该可以登录了。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
