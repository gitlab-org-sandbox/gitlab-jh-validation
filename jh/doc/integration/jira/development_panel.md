---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira 开发面板 **(BASIC ALL)**

您可以使用 Jira 开发面板直接在 Jira 中查看 Jira 议题的极狐GitLab 活动。
设置 Jira 开发面板：

- **对于 Jira Cloud**，请使用极狐GitLab 开发和维护的 [JiHu GitLab for Jira Cloud 应用程序](connect-app.md)。
- **对于 Jira Data Center 或 Jira Server**，使用由 Atlassian 开发和维护的 [Jira DVCS 连接器](dvcs/index.md)。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Jira development panel integration](https://www.youtube.com/watch?v=VjVTOmMl85M).
-->

## 功能可用性

下表显示了 Jira DVCS 连接器和 JiHu GitLab for Jira Cloud 应用程序的可用功能：

| 功能            | Jira DVCS 连接器         | JiHu GitLab for Jira Cloud 应用         |
|---------------|-----------------------|---------------------------------------|
| 智能提交          | **{check-circle}** 是  | **{check-circle}** 是                  |
| 同步合并请求        | **{check-circle}** 是  | **{check-circle}** 是                  |
| 同步分支          | **{check-circle}** 是  | **{check-circle}** 是                  |
| 同步提交          | **{check-circle}** 是  | **{check-circle}** 是                  |
| 同步现存数据        | **{check-circle}** 是  | **{check-circle}** 是（部分） <sup>1</sup> |
| 同步构建          | **{dotted-circle}** 否 | **{check-circle}** 是                  |
| 同步部署          | **{dotted-circle}** 否 | **{check-circle}** 是                  |
| 同步功能标志        | **{dotted-circle}** 否 | **{check-circle}** 是                  |
| 同步间隔          | 最多 60 分钟              | 实时                                    |
| 创建分支          | **{dotted-circle}** 否 | **{check-circle}** 是 （仅极狐GitLab SaaS） |
| 从分支创建合并请求     | **{check-circle}** 是  | **{check-circle}** 是                  |
| 从 Jira 议题创建分支 | **{dotted-circle}** 否 | **{check-circle}** 是（引入于 14.2）        |

请参见[极狐GitLab 数据同步到 Jira](connect-app.md#gitlab-data-synced-to-jira)。

## 极狐GitLab 中的关联项目

Jira 开发面板将 Jira 实例及其所有项目连接到以下内容：

- **对于 [JiHu GitLab for Jira Cloud 应用程序](connect-app.md)**，关联的极狐GitLab 群组或子组及其项目。
- **对于 [Jira DVCS 连接器](dvcs/index.md)**，关联的极狐GitLab 群组、子组或个人命名空间及其项目。

<a id="information-displayed-in-the-development-panel"></a>

## 开发面板显示的信息

您可以通过在极狐GitLab 中使用 ID 关联 Jira 议题来在 Jira 开发面板中[查看 Jira 议题的极狐GitLab 活动](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)。开发面板中显示的信息取决于您在极狐GitLab 中提到 Jira 议题 ID 的位置。

| 极狐GitLab：您提及 Jira 议题 ID 的位置              | Jira 开发面板：显示的信息                                                                                                                                                         |
|------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 合并请求标题或描述                                | 关联到合并请求<br>关联到部署<br>通过合并请求标题关联到流水线<br>通过合并请求描述关联到流水线<sup>1</sup><br>关联到分支<sup>2</sup><br>评审人信息和批准状态<sup>3</sup>                                                         |
| 分支名称                                     | 关联到分支<br>关联到开发                                                                                                                                                          |
| 提交消息                                     | 关联到提交<br>上次成功部署到环境后，关联到最多 5,000 次提交的部署 <sup>4</sup> <sup>5</sup> |
| [Jira Smart Commit](#jira-smart-commits) | 自定义评论、记录时间或工作流转换                                                                                                                                                        |

1. 引入于极狐GitLab 15.10。
1. 引入于极狐GitLab 15.11。
1. 引入于极狐GitLab 16.5。
1. 引入于极狐GitLab 16.2，[功能标志](../../administration/feature_flags.md)为 `jira_deployment_issue_keys`。默认启用。
1. 普遍可用于极狐GitLab 16.3。移除 `jira_deployment_issue_keys` 功能标志。

## Jira Smart Commit

先决条件：

- 您必须拥有具有相同电子邮件地址或用户名的极狐GitLab 和 Jira 用户账户。
- 命令必须位于提交消息的第一行。
- 提交消息不得超过一行。

Jira Smart Commit 是处理 Jira 议题的特殊命令。通过这些命令，您可以使用极狐GitLab：

- 向 Jira 议题添加自定义评论。
- 针对 Jira 议题记录时间。
- 将 Jira 议题转换为项目工作流中定义的任何状态。

Smart Commit 必须符合以下语法：

```plaintext
<ISSUE_KEY> <ignored text> #<command> <optional command parameters>
```

您可以在一个提交中执行一个或多个命令。

### Smart Commit 语法

| 命令               | 语法                                                           |
|------------------|--------------------------------------------------------------|
| 添加评论             | `KEY-123 #comment Bug is fixed`                              |
| 记录时间             | `KEY-123 #time 2w 4d 10h 52m Tracking work time`             |
| 关闭议题             | `KEY-123 #close Closing issue`                               |
| 记录时间和关闭议题        | `KEY-123 #time 2d 5h #close`                                 |
| 添加评论和转换为 **进行中** | `KEY-123 #comment Started working on the issue #in-progress` |

有关 Smart Commit 如何工作以及可以使用哪些命令的更多信息，请参阅

- [使用 Smart Commit 处理议题](https://support.atlassian.com/jira-software-cloud/docs/process-issues-with-smart-commits/)
- [使用 Smart Commit](https://confluence.atlassian.com/fisheye/using-smart-commits-960155400.html)

## 相关主题

- [对 Jira Server 中的开发面板进行故障排除](https://confluence.atlassian.com/jirakb/troubleshoot-the-development-panel-in-jira-server-574685212.html)
