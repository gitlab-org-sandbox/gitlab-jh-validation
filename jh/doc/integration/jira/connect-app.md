---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira Cloud 应用的极狐GitLab **(BASIC ALL)**

NOTE:
此页面包含有关在 JihuLab.com 上配置 JiHu GitLab for Jira Cloud 应用程序的信息。<!-- For administrator documentation, see [GitLab for Jira Cloud app administration](../../administration/settings/jira_cloud_app.md). -->

使用 [JiHu GitLab for Jira Cloud](https://marketplace.atlassian.com/apps/1221011/gitlab-com-for-jira-cloud?tab=overview&hosting=cloud) 应用程序，您可以连接极狐GitLab 和 Jira Cloud，实时同步开发信息。您可以在 [Jira 开发面板](development_panel.md)中查看此信息。

您可以使用 JiHu GitLab for Jira Cloud 关联顶级群组或子群组。无法直接关联项目或私有命名空间。

要在 JihuLab.com 上设置 JiHu GitLab for Jira Cloud 应用程序，请[安装 JiHu GitLab for Jira Cloud 应用程序](#install-the-gitlab-for-jira-cloud-app)。
对于 Jira Data Center 或 Jira Server，请使用 Atlassian 开发和维护的 [Jira DVCS 连接器](dvcs/index.md)。

<a id="gitlab-data-synced-to-jira"></a>

## 极狐GitLab 数据同步到 Jira

关联群组后，当您[提及 Jira 议题 ID](development_panel.md#information-displayed-in-the-development-panel) 时，该群组中所有项目的以下极狐GitLab 数据将同步到 Jira：

- 现有项目数据（关联群组之前）：
    - 最近 400 个合并请求
    - 最近 400 个分支以及对每个分支的最后一次提交（15.11 及更高版本）
- 新项目数据（关联群组后）：
    - 合并请求
    - 分支
    - 提交
    - 构建
    - 部署
    - 功能标志  

<a id="install-the-gitlab-for-jira-cloud-app"></a>

## 安装 JiHu GitLab for Jira Cloud 应用程序 **(BASIC SAAS)**

先决条件：

- 您必须拥有 Jira 实例的[站点管理员](https://support.atlassian.com/user-management/docs/give-users-admin-permissions/#Make-someone-a-site-admin)访问权限。
- 您的网络必须允许极狐GitLab 和 Jira 之间的入站和出站连接。

要安装 JiHu GitLab for Jira Cloud 应用程序：

1. 在 Jira 的顶部栏上，选择 **应用程序 > 探索更多应用程序**，然后搜索 `JiHu GitLab for Jira Cloud`。
1. 选择 **JiHu GitLab for Jira Cloud**，然后选择 **立即获取**。

或者，[直接从 Atlassian Marketplace 获取应用程序](https://marketplace.atlassian.com/apps/1231119/jihu-gitlab-for-jira-cloud?hosting=cloud&tab=overview)。

您现在可以[配置 JiHu GitLab for Jira Cloud](#configure-the-gitlab-for-jira-cloud-app)。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see
[Configure the GitLab for Jira Cloud app from the Atlassian Marketplace](https://youtu.be/SwR-g1s1zTo).
-->

<a id="configure-the-gitlab-for-jira-cloud-app"></a>

## 配置 JiHu GitLab for Jira Cloud 应用程序 **(FREE SAAS)**

> **添加命名空间** 重命名为 **关联群组** 于极狐GitLab 16.1。

先决条件：

- 您必须至少具有极狐GitLab 群组的维护者角色。
- 您必须拥有 Jira 实例的[站点管理员](https://support.atlassian.com/user-management/docs/give-users-admin-permissions/#Make-someone-a-site-admin)访问权限。

您可以通过将 JiHu GitLab for Jira Cloud 应用程序关联到一个或多个极狐GitLab 群组，将数据从极狐GitLab 同步到 Jira。

配置 JiHu GitLab for Jira Cloud 应用程序：

1. 在 Jira 的顶部栏上，选择 **应用程序 > 管理您的应用程序**。
1. 展开 **JiHu GitLab for Jira**。
1. 选择 **开始**。
1. 可选。选择 **更改极狐GitLab 版本** 将极狐GitLab 实例设置为与 Jira 一起使用。
1. 选择 **登录极狐GitLab**。
1. 对于您可以关联的群组，请选择 **关联群组**。
1. 要关联群组，请选择 **关联**。

关联到极狐GitLab 群组后，该群组中所有项目的数据都会同步到 Jira。
初始数据同步以每分钟 20 个项目的批次进行。
对于项目较多的群组，部分项目的数据同步会出现延迟。

## 更新 JiHu GitLab for Jira Cloud 应用程序

该应用程序的大多数更新都是全自动的。有关详细信息，请参阅 [Atlassian Marketplace 文档](https://developer.atlassian.com/platform/marketplace/upgrading-and-versioning-cloud-apps/)。

如果应用程序需要额外的权限，[必须首先在 Jira 中手动批准更新](https://developer.atlassian.com/platform/marketplace/upgrading-and-versioning-cloud-apps/#changes-that-require-manual-customer-approval)。

## 安全考虑

JiHu GitLab for Jira Cloud 应用程序连接极狐GitLab 和 Jira。数据必须在两个应用程序之间共享，并且必须授予双向的访问权限。

### 通过访问令牌访问 Jira

Jira 与极狐GitLab 共享访问令牌，以验证和授权向 Jira 推送数据。
作为应用程序安装过程的一部分，Jira 向极狐GitLab 发送包含访问令牌的握手请求。
握手使用[非对称 JWT](https://developer.atlassian.com/cloud/jira/platform/understanding-jwt-for-connect-apps/) 进行签名，并且访问令牌使用 `AES256-GCM` 加密存储在极狐GitLab 上。

## 故障排除

在 JihuLab.com 上配置 JiHu GitLab for Jira Cloud 应用程序时，您可能会遇到以下问题。

<!--
For self-managed GitLab, see [GitLab for Jira Cloud app administration](../../administration/settings/jira_cloud_app.md#troubleshooting).
-->

### `Failed to link group`

当您连接 JiHu GitLab for Jira Cloud 应用程序后，您可能会遇到以下错误：

```plaintext
Failed to link group. Please try again.
```

如果出现以下情况，则返回 `403` 状态码：

- 无法从 Jira 获取用户信息。
- 经过身份验证的 Jira 用户没有[站点管理员](https://support.atlassian.com/user-management/docs/give-users-admin-permissions/#Make-someone-a-site-admin)访问权限。

要解决此问题，请确保经过身份验证的用户是 Jira 站点管理员，然后重试。
