---
stage: none
group: unassigned
---

### CI Job: 监控 Upstream Feature Flag 的变动

极狐GitLab 是基于 Upstream GitLab 的扩展，在 Upstream GitLab 中有大量的 Feature Flag 来控制软件的行为。在某些时候，Upstream 会对 Feature Flag 进行修改，比如设置为默认打开，或者默认关闭，一定程度上就会改变极狐GitLab 的行为，所以极狐GitLab 需要监控 Upstream Feature Flag 的变化。

在极狐GitLab 的 Pipeline 中使用 `watch-upstream-feature-flags` job, 在每次 code sync 时，通过 `git diff` 判定 upstream 是否改动了 `config/feature_flags` 文件夹。

### Upstream Feature Flag 变动 Review

1. CI Job 发送 slack 消息通知 Dev/SRE 发现变更。
2. CI Job 会在极狐GitLab 项目中创建 Issue 记录变更，例如 [#1245](https://jihulab.com/gitlab-cn/gitlab/-/issues/1245) 。
3. 上游 Feature Flag 的变动很多，我们的目标是尽可能关注其中可能会对我们产生影响的几种变动，根据讨论当下我们需要重点关注以下情况：
    * 上游 Feature Flag 移除，我们 SaaS 对应的 Feature Flag 是打开的。
    * 上游 Feature Flag 变更默认值为 true。
    * 上游涉及 Feature Flag 变更的文件，在 /jh 目录下有过覆盖。
    * Feature Flag 变更涉及 SaaS 运营策略，如 `影响免费用户创建 projects 的数量`。
4. 当上游 Feature Flag 变更属于上面的几种情况，Dev 需要在 issue 里把变动的 Feature Flag 简单描述下内容，把相关 MR 或者 issue 列出来，可以参考这个 [issue](https://jihulab.com/gitlab-cn/gitlab/-/issues/1301)，并在 issue 中 cc @shreychen @lpeng1991 @daveliu @prajnamas，具体做法可以参考下面的 *Issue 处理流程*。
5. 如果 Dev 对变动的 Feature Flag 在 jihulab.com 的开关状态不明确，可以联系 SRE 同学查询。

### Issue 处理流程

- 找出 upstream sync commit 的 feature flag 对应的全部 yaml 文件
- 对每个 feature flag，找到它的 MR/Rollout URL
- 查看对应 MR diff，Review 每个改动文件，确认是否对 JH 有影响
- 记录结果。如判断会影响到 JH，需要 @ 相关人员（Leader/SRE）检查确认
- Comment 可参照如下写法：

  - 无影响

  ```markdown
  ## 新增功能 FF: `optimize_group_template_query`
  
  - introduced_by_url: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129399
  - rollout_issue_url: https://gitlab.com/gitlab-org/gitlab/-/issues/422390
  
  @daveliu 这是个性能优化，为了改善 https://gitlab.com/projects/new#create_from_template 页面 query 性能。后端文件改动都是新增代码，对 JH 无破坏性影响。
  ```

  - 有影响

  ```markdown
  ## 删除 FF：`npm_package_registry_fix_group_path_validation`

  - introduced_by_url: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127164
  - rollout_issue_url: https://gitlab.com/gitlab-org/gitlab/-/issues/420160

  @daveliu 这是个对 NPM package 命名约定的改动，原 MR 涉及文件没有 JH 覆盖处理。现在删除掉 FF，请 SRE @vincent_stchu 看下对 JH 有没有影响。
  ```



[代码如下](https://jihulab.com/gitlab-cn/gitlab/-/blob/main-jh/jh/.gitlab/ci/watch-upstream-feature-flags.gitlab-ci.yml):

```shell
watch-upstream-feature-flags:
  stage: test
  allow_failure: true
  extends:
    - .minimal-job
  rules:
    - if: $GITLAB_USER_NAME == "JH_SYNC_TOKEN" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - latest_commit=`git rev-parse --short HEAD`
    - echo "latest commit is $latest_commit"
    - latest_changes=`git diff $latest_commit^ $latest_commit --name-only`
    - echo "latest_changes is $latest_changes"
    - |-
        if [[ $latest_changes == *"config/feature_flags"* ]];
        then
            echo "Upstream feature flags changed! Change commit is $latest_commit"
            feature_flag_changes=$(git diff $latest_commit^ $latest_commit -- config/feature_flags)
            echo "feature_flag_changes is $feature_flag_changes"
            title="Feature flag changes detected"
            datetime=`TZ=UTC-8 date '+%F %T'`
            curl -X POST --header 'Content-Type:application/json' --header "PRIVATE-TOKEN:$JIHU_REPORTER_TOKEN" --data "{\"confidential\":\"true\",\"title\":\"$title $datetime\",\"description\":\"New Change in commit $latest_commit   \n see detail here, $CI_JOB_URL\n\",\"labels\":\"featureflag::changed\"}" https://jihulab.com/api/v4/projects/13953/issues
            curl -X POST \
              -H 'Content-type:application/json' \
              --data "{\"blocks\":[{\"type\":\"header\",\"text\":{\"type\":\"plain_text\",\"text\":\"$title\"}},{\"type\":\"section\",\"fields\":[{\"type\":\"mrkdwn\",\"text\":\"*Job:* <$CI_JOB_URL|#$CI_JOB_ID>\"},{\"type\":\"mrkdwn\",\"text\":\"*Issue:* <https://jihulab.com/gitlab-cn/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=featureflag%3A%3Achanged|link>\"}]}]}" \
              --url $FEATURE_FLAG_CHANNEL_URL
        else
            echo "Upstream feature flag not changed"
            curl -X POST -H "Content-Type:application/json" --data "{\"text\":\"Upstream feature flag not changed, $CI_JOB_URL\"}" \
              --url $FEATURE_FLAG_CHANNEL_URL
        fi
```
