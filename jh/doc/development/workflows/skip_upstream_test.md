---
stage: none
group: unassigned
---

### 处理 Upstream 失败的测试

极狐GitLab 修改了 Upstream GitLab 的某些逻辑，导致相关 Upstream 测试失败，经过与 Upstream 同事的 [讨论](https://gitlab.com/gitlab-jh/gitlab-jh-enablement/-/issues/166) ，我们确定了以下的修复流程:
1. 优先修复 Upstream 失败的测试，可以尽可能复用测试，增加极狐GitLab 的测试覆盖率。
2. 可以通过抽取 TestHelper + JH prepend_mod 的方式，保证 Upstream 测试正常运行。比如这个 [MR](https://jihulab.com/gitlab-cn/gitlab/-/merge_requests/532/diffs) 。
3. 如果修复测试成本过高，就可以考虑在极狐GitLab 项目中跳过运行该 Upstream 测试。在 `jh/spec/config/skip_specs.yml` 中添加失败的测试， 尽量不要跳过整个测试文件，可以通过添加 `description` 跳过某一个具体的测试。**同时，请在极狐GitLab 中添加新的测试覆盖被跳过的逻辑，并添加在 `jh_related_spec` 中，或者在 `skip_reason` 添加说明解释跳过该测试的原因以及后期修复该测试的计划。**

```yaml
- file_path: 'ee/spec/requests/trial_registrations_controller_spec.rb'
  description:
    - 'TrialRegistrationsController POST new when email_opted_in does not exist in params sets user email_opted_in to false'
    - 'TrialRegistrationsController POST new when email_opted_in is true in params sets user email_opted_in to true'
  jh_related_spec:
    - 'jh/spec/requests/jh/trial_registrations_controller_spec.rb #context email_opted_in'
  skip_reason: 'write down skip reason here'
```

### 临时跳过测试解决 main-jh pipeline 失败

如果 main-jh pipeline 因为 upstream 的改动失败，在无法联系 upstream 进行快速修复的情况下，为了不影响团队正常的开发流程，可以临时把失败的测试添加到 `skip_specs.yml` 中，保证 main-jh pipeline 运行正常。在修复完相关测试后，再从 `skip_specs.yml` 中移除刚才添加的测试。


### 后期计划
1. 在 CI Pipeline 中添加 Job，专门运行被跳过的 Upstream 测试，验证被跳过的 Upstream 测试在极狐GitLab 中确实失败。
