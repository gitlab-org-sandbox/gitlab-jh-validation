---
stage: none
group: unassigned
---

# 前端测试开发及风格指南

本文主要介绍极狐GitLab 前端测试相关约定及规范。更多未尽事项请参考 [GitLab 前端测试指南](https://docs.gitlab.com/ee/development/testing_guide/frontend_testing.html) 。

## 在极狐GitLab 下解决上游测试冲突

由于极狐GitLab 和上游功能存在差异，所以开发过程中需要覆盖部分上游测试用例。

### 通过 skip_list 跳过上游测试用例

我们采用 `Jest` 编写前端测试。如果在极狐GitLab 中覆盖了上游特性，导致了 `Jest` 测试用例失败，可以通过 `jh/spec/frontend/skip_list.js` 配置文件选择跳过失败的测试用例或者跳过整个 `spec` 文件。

以`spec/frontend/terms/components/app_spec.js`文件为例：

1. 如果需要跳过整个测试文件，则可以配置 `by_file` 如下：

```json
{
  "by_file": ["spec/frontend/terms/components/app_spec.js"],
  "by_case": {}
}
```
2. 如果需要跳过某个测试用例，需要在 `by_case` 指定用例对应的 `description full path` 。

**例如：**

<details><summary>点击展开</summary>
<p>

```js
describe('TermsApp', () => {

    it('renders terms of service as markdown', () => {

    });

    describe('accept button', () => {
        it('is disabled until user scrolls to the bottom of the terms', async () => {

        });

        describe('when user has permissions to accept', () => {
            it('renders form and button to accept terms', () => {

            });
        });

        describe('when user does not have permissions to accept', () => {
            it('renders continue button', () => {

            });
        });
    });

});
```

</p>
</details>

我们需要跳过 `is disabled until user scrolls to the bottom of the terms` 这个用例，则需要从最外层的 `TermsApp` 开始，逐层拼接 `description` 直到需要跳过的用例。

因此该用例的 `full path` 为 `TermsApp` + `accept button` + `is disabled until user scrolls to the bottom of the terms` 。

> **注意：**
> 
> 为了避免滥用跳过上游测试的情况，每次在 `skip_list.js` 中新增需要跳过的测试用例时，需在 `issue` 或者 `merge request` 中描述必须使用跳过上游测试的原因，并将链接添加在注释中。

配置 `skip_list.js` 如下：

```javascript
module.exports = {
  by_file: [],
  by_case: {
    "spec/frontend/terms/components/app_spec.js": [
      // https://gitlab.com/gitlab-jh/gitlab/-/merge_requests/123
      "TermsApp accept button is disabled until user scrolls to the bottom of the terms"
    ]
  }
}
```
