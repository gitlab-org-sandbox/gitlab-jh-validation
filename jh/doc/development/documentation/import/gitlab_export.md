### 导入 Gitlab 导出项目的代码流程介绍

Gitlab 导出的项目下载下来是一个 `#{日期}_#{命名空间名}_#{项目名}.tar.gz` 为名称的文件， 例如 `2022-02-08_07-02-583_zhzhang_test-export_export.tar.gz`, 解压后的目录是这样的

```shell
├── GITLAB_REVISION
├── GITLAB_VERSION
├── VERSION
├── lfs-objects.json
├── project.bundle
├── snippets
└── tree
    ├── project
    │   ├── auto_devops.ndjson
    │   ├── boards.ndjson
    │   ├── ci_cd_settings.ndjson
    │   ├── ci_pipelines.ndjson
    │   ├── container_expiration_policy.ndjson
    │   ├── custom_attributes.ndjson
    │   ├── error_tracking_setting.ndjson
    │   ├── external_pull_requests.ndjson
    │   ├── issues.ndjson
    │   ├── labels.ndjson
    │   ├── merge_requests.ndjson
    │   ├── metrics_setting.ndjson
    │   ├── milestones.ndjson
    │   ├── pipeline_schedules.ndjson
    │   ├── project_badges.ndjson
    │   ├── project_feature.ndjson
    │   ├── project_members.ndjson
    │   ├── prometheus_metrics.ndjson
    │   ├── protected_branches.ndjson
    │   ├── protected_environments.ndjson
    │   ├── protected_tags.ndjson
    │   ├── push_rule.ndjson
    │   ├── releases.ndjson
    │   ├── security_setting.ndjson
    │   ├── service_desk_setting.ndjson
    │   └── snippets.ndjson
    └── project.json
```
其中`tree`这个目录中存储了所有与该 `project` 相关联的所有的实体，包括 `issues` `merge_requests` 等等, `project.bundle` 这个文件是通过 `gitaly` 导出的该项目的 `仓库` 数据。

然后我们通过 点击 `新建项目/仓库` -> `导入项目` -> `Gitlab导出`， 然后填写相应的信息，在 `GitLab项目导出` 下选择 Gitlab 导出项目的压缩包，点击导入项目，新的项目就会被创建好，这个流程整体的代码运行过程是这样的。

项目运行首先会来到 `app/controllers/import/gitlab_projects_controller.rb` 中的 `create` 方法，`注意：我们在查看代码的时候，要先看一下ee目录和jh目录里有没有相应的覆写代码，因为系统会优先使用jh和ee当中的逻辑，优先级是jh > ee > ce`

```ruby
# app/controllers/import/gitlab_projects_controller.rb
...

def create
  unless file_is_valid?(project_params[:file])
    return redirect_back_or_default(options: { alert: _("You need to upload a GitLab project export archive (ending in .gz).") })
  end

  @project = ::Projects::GitlabProjectsImportService.new(current_user, project_params).execute

  if @project.saved?
    redirect_to(
      project_path(@project),
      notice: _("Project '%{project_name}' is being imported.") % { project_name: @project.name }
    )
  else
    redirect_back_or_default(options: { alert: "Project could not be imported: #{@project.errors.full_messages.join(', ')}" })
  end
end
  
  ...
```
我们看到 `create` 方法中会使用到 `::Projects::GitlabProjectsImportService` 这个 service 来创建 project，传递了 `current_user` 和 `project_params` 这两个参数进去，我们来看看 `project_params` 这个方法, 打印出来，是这种格式的，其中包含了项目名称 `name` 、项目路径 `path` 、上传的gitlab导出文件 `file` 的信息

```shell
#<ActionController::Parameters {"name"=>"test-import-project-001", "path"=>"test-import-project-001", "namespace_id"=>"126", "file"=>#<UploadedFile:0x000000016abbecc8 @tempfile=#<File:/Users/zhzhang/working/gitlab-development-kit/gitlab/public/uploads/tmp/uploads/2022-03-09_11-13-316_zhzhang_test_import_9_export.tar.gz3439983142>, @size=392164, @upload_duration=0.00347425, @content_type="application/octet-stream", @original_filename="2022-03-09_11-13-316_zhzhang_test_import_9_export.tar.gz", @sha256="fa32ace5eb08f3fd5ffcc1a6450043444f40b4f7803579f2964c4790a6e74fe9", @remote_id="">} permitted: true>
```

在 `::Projects::GitlabProjectsImportService` 中，生成了一系列参数，最终传入到 `::Projects::CreateService` 打印出来是这样的
```shell
#<ActionController::Parameters {"name"=>"test-import-project-001", "path"=>"test-import-project-001", "namespace_id"=>"126", "import_export_upload"=>#<ImportExportUpload id: nil, updated_at: nil, project_id: nil, import_file: nil, export_file: nil, group_id: nil, remote_import_url: nil>, "import_type"=>"gitlab_project"} permitted: true>
```

```ruby
# app/services/projects/gitlab_projects_import_service.rb

def execute
  prepare_template_environment(template_file)

  prepare_import_params

  ::Projects::CreateService.new(current_user, params).execute
end
```

接下来我们来到了 `::Projects::CreateService` ,  注意，此刻我们可以在项目中找到两个含有 projects/create_service.rb 的文件，一个是 `app/services/projects/create_service.rb` 一个是 `ee/app/services/ee/projects/create_service.rb` ,`app/services/projects/create_service.rb` 中， `CreateService` 是一个 `Class`， 而 `ee/app/services/ee/projects/create_service.rb` 中， `CreateService` 是一个 `Module`，且被 `CreateService` 这个 `Class` `prepend`了，所以在`CreateService` 在执行的时候，方法查找会优先去查找 `CreateService` 这个 `Module` 中的方法，从而实现了`ee` 对 `ce` 方法的覆盖

所以我们首先会来到 `ee/app/services/ee/projects/create_service.rb` 中的 `execute` 方法

```ruby
# ee/app/services/ee/projects/create_service.rb

def execute
  if create_from_template?
    return ::Projects::CreateFromTemplateService.new(current_user, params).execute
  end

  ...

  project = super do |project|
    # Repository size limit comes as MB from the view
    project.repository_size_limit = ::Gitlab::Utils.try_megabytes_to_bytes(limit) if limit

    ....
  end

  ...

  project
end
```


实际上我们这个导入过程，起作用的最终还是这个 `super` 方法回到了 `app/services/projects/create_service.rb` 中，其他在 `ee` 中执行的逻辑和我们这次导入关系不大

```ruby
# app/services/projects/create_service.rb

def execute
  if create_from_template?
    return ::Projects::CreateFromTemplateService.new(current_user, params).execute
  end

   project = Project.new(params)

  @project.visibility_level = @project.group.visibility_level unless @project.visibility_level_allowed_by_group?

  # If a project is newly created it should have shared runners settings
  # based on its group having it enabled. This is like the "default value"
  @project.shared_runners_enabled = false if !params.key?(:shared_runners_enabled) && @project.group && @project.group.shared_runners_setting != 'enabled'

  # Make sure that the user is allowed to use the specified visibility level
  if project_visibility.restricted?
    deny_visibility_level(@project, project_visibility.visibility_level)
    return @project
  end

  set_project_name_from_path

  # get namespace id
  namespace_id = params[:namespace_id]

  if namespace_id
    # Find matching namespace and check if it allowed
    # for current user if namespace_id passed.
    unless current_user.can?(:create_projects, parent_namespace)
      @project.namespace_id = nil
      deny_namespace
      return @project
    end
  else
    # Set current user namespace if namespace_id is nil
    @project.namespace_id = current_user.namespace_id
  end

  @relations_block&.call(@project)
  yield(@project) if block_given?

  validate_classification_label(@project, :external_authorization_classification_label)

  # If the block added errors, don't try to save the project
  return @project if @project.errors.any?

  @project.creator = current_user

  save_project_and_import_data

  Gitlab::ApplicationContext.with_context(project: @project) do
    after_create_actions if @project.persisted?

    import_schedule
  end

  @project
rescue ActiveRecord::RecordInvalid => e
  message = "Unable to save #{e.inspect}: #{e.record.errors.full_messages.join(", ")}"
  fail(error: message)
rescue StandardError => e
  @project.errors.add(:base, e.message) if @project
  fail(error: e.message)
end
```

经历了一番 project 的设置之后，我们来到了 `import_schedule` 方法

```ruby
# app/services/projects/create_service.rb

def import_schedule
  if @project.errors.empty?
    @project.import_state.schedule if @project.import? && !@project.bare_repository_import?
  else
    fail(error: @project.errors.full_messages.join(', '))
  end
end
```

在这个方法里，调用了project 的 import_state 的 schedule 方法, 在 `app/models/project_import_state.rb` 中，在这个model中使用了 `state_machine` 这个 gem 来管理不同的状态，他会定义一些状态转换，然后定义在实现这个转换之后，要执行什么方法，在我们这，他执行了project 的 `add_import_job` 方法

```ruby
# app/models/project_import_state.rb

after_transition [:none, :finished, :failed] => :scheduled do |state, _|
  state.run_after_commit do
    job_id = project.add_import_job

    if job_id
      correlation_id = Labkit::Correlation::CorrelationId.current_or_new_id
      update(jid: job_id, correlation_id_value: correlation_id)
    end
  end
end
```

在 `add_import_job` 中，最终来到了 `RepositoryImportWorker`, 虽然 ee 中对 `add_import_job` 进行了覆写，不过在我们这个上下文中不会有影响

```ruby
# app/models/project.rb

def add_import_job
  job_id =
    if forked?
      RepositoryForkWorker.perform_async(id)
    else
      RepositoryImportWorker.perform_async(self.id)
    end

  log_import_activity(job_id)

  job_id
end
```

然后我们来到了 `RepositoryImportWorker` ，这里传入了一个 project 的id

```ruby
# app/workers/repository_import_worker.rb

def perform(project_id)
  @project = Project.find(project_id)

  return unless start_import

  Gitlab::Metrics.add_event(:import_repository)

  service = Projects::ImportService.new(project, project.creator)
  result = service.execute

  # Some importers may perform their work asynchronously. In this case it's up
  # to those importers to mark the import process as complete.
  return if service.async?

  if result[:status] == :error
    fail_import(result[:message])

    raise result[:message]
  end

  project.after_import
end
```

在这个 worker 中，调用了 `Projects::ImportService` 这个service 来进行项目导入

```ruby
# app/services/projects/import_service.rb

def execute
  track_start_import

  add_repository_to_project

  download_lfs_objects

  import_data

  after_execute_hook

  success
 ...
end
```
在这里其实 `add_repository_to_project` 和 `download_lfs_objects` 都没有执行，因为导入gitlab的项目是由专门的importer来进行导入的，在这两个方法中，由于是导入gitlab导出的项目，执行到一半就return 掉了，没有继续执行，真正导入数据的是 `import_data` 这个方法

```ruby
# app/services/projects/import_service.rb

def import_data
  return unless has_importer?

  project.repository.expire_content_cache unless project.gitlab_project_import?

  unless importer.execute
    raise Error, s_('ImportProjects|The remote data could not be imported.')
  end
end
```

因为我们的 `import_type` 是 `gitlab_project`, 所以用到的importer 是 `Gitlab::ImportExport::Importer`

```ruby
# lib/gitlab/import_export/importer.rb

def execute
  if import_file && check_version! && restorers.all?(&:restore) && overwrite_project
    project
  else
    raise Projects::ImportService::Error, shared.errors.to_sentence
  end
rescue StandardError => e
  # If some exception was raised could mean that the SnippetsRepoRestorer
  # was not called. This would leave us with snippets without a repository.
  # This is a state we don't want them to be, so we better delete them.
  remove_non_migrated_snippets

  raise Projects::ImportService::Error, e.message
ensure
  remove_base_tmp_dir
  remove_import_file
end

...

def restorers
  [repo_restorer, wiki_restorer, project_tree, avatar_restorer, design_repo_restorer,
   uploads_restorer, lfs_restorer, statistics_restorer, snippets_repo_restorer]
end
```

在`Gitlab::ImportExport::Importer` 中，`restorers` 这个方法定义了所有需要从导出文件中导入到新的project的 `restorer`, 这些 `restorer` 依次执行来将数据转储，其中 `repo_restorer` 对应 `Gitlab::ImportExport::RepoRestorer`, 它将使用`bundle_file`来为新的project 创建repository。还有需要重点关注的就是 `project_tree`, 它在本次导入中对应 `Gitlab::ImportExport::Project::TreeRestorer`

```ruby
# lib/gitlab/import_export/project/tree_restorer.rb

def restore
  unless relation_reader
    raise Gitlab::ImportExport::Error, 'invalid import format'
  end

  @project_attributes = relation_reader.consume_attributes(importable_path)
  @project_members = relation_reader.consume_relation(importable_path, 'project_members')
    .map(&:first)

  # ensure users are mapped before tree restoration
  # so that even if there is no content to associate
  # users with, they are still added to the project
  members_mapper.map

  if relation_tree_restorer.restore
    import_failure_service.with_retry(action: 'set_latest_merge_request_diff_ids!') do
     @project.merge_requests.set_latest_merge_request_diff_ids!
    end

    true
  else
    false
  end
rescue StandardError => e
  @shared.error(e)
  false
end
```

在这个类中，使用了 `relation_reader` (也就是 `ImportExport::Json::NdjsonReader`) 来读取导出目录的各个文件，然后序列化好每个实体的attributes, 这里读取了 project 的 attributes 和 project_members 分别储存在 `@project_attributes` 和 `@project_members` 两个实例变量中，最终在 `relation_tree_restorer` 这个方法中，作为 `relation_tree_restorer_class`(也就是 `Gitlab::ImportExport::Project::RelationTreeRestorer`) 初始化的参数传入进去。如下 

```ruby
# lib/gitlab/import_export/project/tree_restorer.rb

def relation_tree_restorer
  @relation_tree_restorer ||= relation_tree_restorer_class.new(
    user: @user,
    shared: @shared,
    relation_reader: relation_reader,
    object_builder: object_builder,
    members_mapper: members_mapper,
    relation_factory: relation_factory,
    reader: reader,
    importable: @project,
    importable_attributes: @project_attributes,
    importable_path: importable_path
  )
end

def relation_tree_restorer_class
  RelationTreeRestorer
end
```

我们可以看到 `Gitlab::ImportExport::Project::RelationTreeRestorer` 是继承自 `Gitlab::ImportExport::Group::RelationTreeRestorer`, `Gitlab::ImportExport::Project::TreeRestorer` 中 `relation_tree_restorer` 调用的 `restore` 方法也是在`Gitlab::ImportExport::Group::RelationTreeRestorer`中才可以发现， 在`restore` 方法中，实现了与project 相关联的各个实体的创建, 在attributes的赋值中，也会过滤掉一些不需要的值，这个功能是通过`attributes_permitter` 来实现的，而允许导入哪些字段，实际上是在 `lib/gitlab/import_export/project/import_export.yml` 这个yml文件中定义的，不存在于这个yml文件中的字段，就会被过滤掉，从而不会实现赋值。

https://jihulab.com/gitlab-cn/gitlab/-/issues/656 这个issue就是由于没有在yml文件中给 events 添加project_id 导致在导入从gitlab导出的项目之后，有些event 的project_id 没有添加上，导致在个人动态页面显示时，无法通过路由找到该event 所属project 的链接，导致该页面无法正常使用。