## Repository

### GitLab Repository

关于 Repository，GitLab 的官方定义这么写道。

> A repository is where you store your code and make changes to it. Your changes are tracked with version control.   

一个仓库就是存储和改动我们的代码的地方，你的所有改动都会被版本控制所跟踪。

但是其实在 GitLab 这边，我们所说的 repository，比如调用 project 的 repository 方法的时候，这个 repository 其实只是一个类的实例，这个类就是 `Repository` 类。

`Repository` 是仓库的 model，位于 `app/models/repository.rb`， 它并没有继承自 ActiveRecord，只是一个类，所以它其实没有存储在数据库中，他的初始化函数接受5个参数。

```ruby
def initialize(full_path, container, shard:, disk_path: nil, repo_type: Gitlab::GlRepository::PROJECT)
  @full_path = full_path
  @shard = shard
  @disk_path = disk_path || full_path
  @container = container
  @commit_cache = {}
  @repo_type = repo_type
end
```

分别是 `full_path`、 `container`、 `shard`、 `disk_path`、 `repo_type`， 其中 `repo_type` 的默认值是 `Gitlab::GlRepository::PROJECT`，如果想要创建其他类型的repository，需要传入其他类型，具体的类型后边会讲到。

其中 `full_path` 是 `container` 的 `full_path`。

`container` 是仓库的所有者，如果这个仓库是一个 `project` 的仓库，那么 `container` 就是这个 `project`，如果是一个 `wiki` 的仓库，那么 `container` 就是 `wiki`，如果这个仓库是一个 `snippet` 的仓库，那么 `container` 就是这个 `snippet` ，如果是一个 `design` 的仓库，那么 `container` 就是这个 `design`。

`shard` 是 `Gitaly` 服务器的分片名称。

`disk_path` 是存储仓库的具体地址，每一个有仓库的 `container`，都会实现一个 `disk_path` 的方法， 这个方法是通过 `include HasRepository` 这个 module 引入的，要引入这个方法需要在 `include HasRepository` 之后实现一个 `storage` 方法，比如在 `app/models/snippet.rb` 中：

```ruby
class Snippet < ApplicationRecord
  ...
  include HasRepository

  ...

  def storage
    @storage ||= Storage::Hashed.new(self， prefix: Storage::Hashed::SNIPPET_REPOSITORY_PATH_PREFIX)
  end
```

在这个 `storage` 方法中返回一个实现了 `disk_path` 方法的实例， `HasRepository` 使用 `delegate` 来使引入 `HasRepository` 这个 module 的类来直接使用这个方法。

对应的 disk_path 格式为这样：

```ruby
"#{@prefix}/#{disk_hash[0..1]}/#{disk_hash[2..3]}/#{disk_hash}"
```

开头是一个 prefix 然后后边是 disk_hash 的前四位，然后是 disk_hash 本身，例如`@hashed/5a/39/5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd`，其中 disk_hash 是用 project 的 `id` 进行 `Digest::SHA2.hexdigest` 哈希得到的。这个 prefix 也是可以自己定义的，默认的就是 `@hashed`, 比如 snippet 的仓库 prefix 就是 `@snippets`。

`repo_type` 是 repository 的类别，类别目前有四种，分别为：

- Gitlab::GlRepository::PROJECT

- Gitlab::GlRepository::WIKI

- Gitlab::GlRepository::SNIPPET

- Gitlab::GlRepository::DESIGN

在 GitLab 中，不光 project 会拥有一个 repository， wiki、代码片、design 都会拥有一个 repository，用来存储数据。

我们来看一下 `repo_type`, `repo_type` 是定义在 `lib/gitlab/gl_repository/repo_type.rb` 中：

```ruby
module Gitlab
  class GlRepository
    class RepoType
      attr_reader :name,
                  :access_checker_class,
                  :repository_resolver,
                  :container_class,
                  :project_resolver,
                  :guest_read_ability,
                  :suffix

      def initialize(
        name:,
        access_checker_class:,
        repository_resolver:,
        container_class: default_container_class,
        project_resolver: nil,
        guest_read_ability: :download_code,
        suffix: nil)
        @name = name
        @access_checker_class = access_checker_class
        @repository_resolver = repository_resolver
        @container_class = container_class
        @project_resolver = project_resolver
        @guest_read_ability = guest_read_ability
        @suffix = suffix
      end
...
```

它的初始化函数传入了七个参数，这个 `RepoType` 类主要是在 `lib/gitlab/gl_repository.rb` 当中使用，这边定义了我们上边所介绍的四个 `repo_type`：

```ruby
...
PROJECT = RepoType.new(
  name: :project,
  access_checker_class: Gitlab::GitAccessProject,
  repository_resolver: -> (project) { ::Repository.new(project.full_path, project, shard: project.repository_storage, disk_path: project.disk_path) }
).freeze
WIKI = RepoType.new(
  name: :wiki,
  access_checker_class: Gitlab::GitAccessWiki,
  repository_resolver: -> (container) do
    wiki = container.is_a?(Wiki) ? container : container.wiki # Also allow passing a Project, Group, or Geo::DeletedProject
    ::Repository.new(wiki.full_path, wiki, shard: wiki.repository_storage, disk_path: wiki.disk_path, repo_type: WIKI)
  end,
  container_class: ProjectWiki,
  project_resolver: -> (wiki) { wiki.try(:project) },
  guest_read_ability: :download_wiki_code,
  suffix: :wiki
).freeze
SNIPPET = RepoType.new(
  name: :snippet,
  access_checker_class: Gitlab::GitAccessSnippet,
  repository_resolver: -> (snippet) { ::Repository.new(snippet.full_path, snippet, shard: snippet.repository_storage, disk_path: snippet.disk_path, repo_type: SNIPPET) },
  container_class: Snippet,
  project_resolver: -> (snippet) { snippet&.project },
  guest_read_ability: :read_snippet
).freeze
DESIGN = ::Gitlab::GlRepository::RepoType.new(
  name: :design,
  access_checker_class: ::Gitlab::GitAccessDesign,
  repository_resolver: -> (project) { ::DesignManagement::Repository.new(project) },
  suffix: :design
).freeze
...
```

其中比较值得注意的是每个 repo_type 的 `repository_resolver` 参数，这个参数是一个 Proc 函数，是通过 `Repository` 类的初始化方法创建一个新的实例。在这些 repo_type 中，PROJECT、WIKI、SNIPPET 都是使用 `Repository` 实例化的，而 DESIGN 不同，它是通过一个继承自 `Repository` 的类，`DesignManagement::Repository` 这个类来实例化的，这个类中覆盖了 `Repository` 的初始化方法，在 DESIGN 所属的 project 的基础上，在 full_path 和 disk_path 参数中添加了 DESIGN 的 suffix，也就是 `.design`，然后完成了仓库的初始化。

在 Gitaly 中，DESIGN 的仓库其实是和它所属的 project 存储在同一个平级路径下的, 值得一提的是，WIKI 也是和它所属的 project 存储在同一个平级路径下的，比如一个 project 的 disk_path 是 `@hashed/5a/39/5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd`，那么它的 wiki 的 disk_path 就是 `@hashed/5a/39/5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.wiki`，它的 design 的 disk_path 就是 `@hashed/5a/39/5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.design`。这个时候 Gitaly 这边存储仓库的目录 `@hashed/5a/39/` 下的内容是：

```shell
$ ll
total 0
drwx------  6 zhzhang  staff   192B  6  9 12:51 5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.design.git
drwx------  8 zhzhang  staff   256B  6  9 12:51 5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.git
drwx------  6 zhzhang  staff   192B  6  9 12:51 5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.wiki.git
$ cd 5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.git
$ ls
HEAD                 config               info                 language-stats.cache objects              refs
```

这边需要注意的点是 `5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.design.git`、`5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.git`、`5a39cadd1b007093db50744797c7a04a34f73b35ed444704206705b02597d6fd.wiki.git` 都是目录，这三个目录分别就是 project 的 design 仓库目录、project 仓库目录和 project 的 wiki 仓库目录，每个目录中存储的是仓库的具体信息。

这边可能会产生一个疑问，就是 design 为什么不可以像 wiki 一样，直接使用 Repository 的 new 方法， 传入自己的 full_path 等这些参数来实现呢，这个我觉得在是因为在 design 仓库中需要实现其他的一些方法，所以没有沿用 `include HasRepository` 这种方式，而是采用了继承 `Repository` 类的这种形式。

如果我们调用一个 project 的 repository 方法，实际上是调用了 `app/models/project.rb` 中的 `repository` 方法：

```ruby
def repository
  @repository ||= Gitlab::GlRepository::PROJECT.repository_for(self)
end
```

这个方法把 project 本身当成参数传入进去获得的一个 Repository 类的实例，这个方法调用了 `Gitlab::GlRepository::PROJECT` 的 `repository_for` 方法，而 `repository_for` 其实就是 call 的 PROJECT 这个 `repo_type` 的 `repository_resolver` 这个 Proc 函数:

```ruby
::Repository.new(project.full_path, project, shard: project.repository_storage, disk_path: project.disk_path)
```

这个 Proc 函数调用了 `app/models/repository.rb` 中 Repository 这个 class 的 new 方法，为这个 project 生成一个 repository 的实例。


### RawRepository

在 GitLab 中，每个 repository 都对应一个 raw_repository，对应的代码位于 `lib/gitlab/git/repository.rb`， 在这个文件中，包含了与 Gitaly RPC交互的逻辑。

GitLab 与 Gitaly 仓库交互的整个流程就是  `project  -> repository -> raw_repository -> gitaly -> raw_repository -> repository`。

比如我们想获取一个 repository 的一些 commits， 我们调用 `project.repository.commits` 方法， 首先这个方法会来到 `app/models/repository.rb` 的commits 方法中：

```ruby
def commits(ref = nil, opts = {})
  options = {
    repo: raw_repository,
    ref: ref,
    path: opts[:path],
    author: opts[:author],
    follow: Array(opts[:path]).length == 1,
    limit: opts[:limit],
    offset: opts[:offset],
    skip_merges: !!opts[:skip_merges],
    after: opts[:after],
    before: opts[:before],
    all: !!opts[:all],
    first_parent: !!opts[:first_parent],
    order: opts[:order],
    literal_pathspec: opts.fetch(:literal_pathspec, true),
    trailers: opts[:trailers]
  }

  commits = Gitlab::Git::Commit.where(options)
  commits = Commit.decorate(commits, container) if commits.present?

  CommitCollection.new(container, commits, ref)
end
```

这个方法接收两个参数，第一个是 `ref` 参数，也就是分支参数， 第二个是 `opts`，是一个 hash 参数，在 options 中，传入了 `raw_repository`, 这个 `raw_repository` 是通过 `initialize_raw_repository` 这个方法生成的：

```ruby
def initialize_raw_repository
  Gitlab::Git::Repository.new(shard,
                              disk_path + '.git',
                              repo_type.identifier_for_container(container),
                              container.full_path)
end
```

这个方法返回了一个 `Gitlab::Git::Repositroy` 实例， 传入了 `shard`， `disk_path`， `repo_type` 的 `container identifier` 以及 `container` 的 `full_path`。

然后我们继续看 `app/models/repository.rb` 中的 commits 方法，这个方法最终调用了 `Gitlab::Git::Commit` 的 where 方法，把整理好的 options 传了进去，走到了 `lib/git/commit.rb` 中的 where 方法，这是一个类方法：

```ruby
class << self
  def where(options)
    repo = options.delete(:repo)
    raise 'Gitlab::Git::Repository is required' unless repo.respond_to?(:log)

    repo.log(options)
  end
end
```

这个方法最终还是调用了 raw_repository 的 log 方法，传入了 options, 走到了 `lib/git/repository.rb`：

```ruby
def log(options)
  default_options = {
    limit: 10,
    offset: 0,
    path: nil,
    author: nil,
    follow: false,
    skip_merges: false,
    after: nil,
    before: nil,
    all: false
  }

  options = default_options.merge(options)
  options[:offset] ||= 0

  limit = options[:limit]
  if limit == 0 || !limit.is_a?(Integer)
    raise ArgumentError, "invalid Repository#log limit: #{limit.inspect}"
  end

  wrapped_gitaly_errors do
    gitaly_commit_client.find_commits(options)
  end
end
```

在 raw_repository 这边，初始化好 options 参数，然后使用这个参数，调用了 `gitaly_commit_client` 的 `find_commits` 方法：

```ruby
def gitaly_commit_client
  @gitaly_commit_client ||= Gitlab::GitalyClient::CommitService.new(self)
end
```

`Gitlab::GitalyClient::CommitService` 定义在 `lib/gitlab/gitaly_client/commit_service.rb`：

```ruby
 def find_commits(options)
  request = Gitaly::FindCommitsRequest.new(
    repository:   @gitaly_repo,
    limit:        options[:limit],
    offset:       options[:offset],
    follow:       options[:follow],
    skip_merges:  options[:skip_merges],
    all:          !!options[:all],
    first_parent: !!options[:first_parent],
    global_options: parse_global_options!(options),
    disable_walk: true, # This option is deprecated. The 'walk' implementation is being removed.
    trailers: options[:trailers]
  )
  request.after    = GitalyClient.timestamp(options[:after]) if options[:after]
  request.before   = GitalyClient.timestamp(options[:before]) if options[:before]
  request.revision = encode_binary(options[:ref]) if options[:ref]
  request.author   = encode_binary(options[:author]) if options[:author]
  request.order    = options[:order].upcase.sub('DEFAULT', 'NONE') if options[:order].present?

  request.paths = encode_repeated(Array(options[:path])) if options[:path].present?

  response = GitalyClient.call(@repository.storage, :commit_service, :find_commits, request, timeout: GitalyClient.medium_timeout)
  consume_commits_response(response)
end
```

最终使用 Gitaly 这个 gem 当中的 `Gitaly::FindCommitsRequest` 类实例化一个 requset，里边包含我们发起 grpc 请求的参数，包括 `@gitaly_repo`、`limit`、`offset` 等等，其中这个 `@gitaly_repo` 是通过调用 `raw_repository` 的 `gitaly_repository` 方法得到的：

```ruby
# lib/gitlab/git/repository.rb
def gitaly_repository
  Gitlab::GitalyClient::Util.repository(@storage, @relative_path, @gl_repository, @gl_project_path)
end
```

这个方法最终调用了 `Gitlab::GitalyClient::Util` 的 `repository` 方法：

```ruby
# lib/gitlab/gitaly_client/util.rb
def repository(repository_storage, relative_path, gl_repository, gl_project_path)
  git_env = Gitlab::Git::HookEnv.all(gl_repository)
  git_object_directory = git_env['GIT_OBJECT_DIRECTORY_RELATIVE'].presence
  git_alternate_object_directories = Array.wrap(git_env['GIT_ALTERNATE_OBJECT_DIRECTORIES_RELATIVE'])

  Gitaly::Repository.new(
    storage_name: repository_storage,
    relative_path: relative_path,
    gl_repository: gl_repository.to_s,
    git_object_directory: git_object_directory.to_s,
    git_alternate_object_directories: git_alternate_object_directories,
    gl_project_path: gl_project_path
  )
end
```

最终返回的是一个 `Gitaly::Reposiotry` 类的实例，其中的信息包含了 repository 所存储在的 storage、relative_path、repository container 的 identifier 和 project 的 full_path。

然后通过 `GitalyClient` 发起了 Gitaly 请求。

我们来看看 Gitaly 这边的代码。

```go
// internal/gitaly/service/commit/find_commits.go
func (s *server) findCommits(ctx context.Context, req *gitalypb.FindCommitsRequest, stream gitalypb.CommitService_FindCommitsServer) error {
  opts := git.ConvertGlobalOptions(req.GetGlobalOptions())
  repo := s.localrepo(req.GetRepository())

  logCmd, err := repo.Exec(ctx, getLogCommandSubCmd(req), opts...)
  if err != nil {
    return fmt.Errorf("error when creating git log command: %v", err)
  }

  objectReader, cancel, err := s.catfileCache.ObjectReader(ctx, repo)
  if err != nil {
    return fmt.Errorf("creating catfile: %v", err)
  }
  defer cancel()

  getCommits := NewGetCommits(logCmd, objectReader)

  if calculateOffsetManually(req) {
    if err := getCommits.Offset(int(req.GetOffset())); err != nil {
      // If we're at EOF, then it means that the offset has been greater than the
      // number of available commits. We do not treat this as an error, but
      // instead just return EOF ourselves.
      if errors.Is(err, io.EOF) {
        return nil
      }

      return fmt.Errorf("skipping to offset %d: %w", req.GetOffset(), err)
    }
  }

  if err := streamCommits(getCommits, stream, req.GetTrailers()); err != nil {
    return fmt.Errorf("error streaming commits: %v", err)
  }
  return nil
}
```

重点在于 `logCmd, err := repo.Exec(ctx, getLogCommandSubCmd(req), opts...)` 这一句，实际上 Gitaly 是在我们从 GitLab 上传给它的那个 git 仓库的地址，在这个 git 仓库中执行了 `git log` 命令, 在这个命令中根据我们传的 options 参数加入不同的 flag 来执行，最后获取执行后的结果，通过 GRPC response 的形式返回给 GitLab 这边。

```go
// internal/gitaly/service/commit/find_commits.go
func getLogCommandSubCmd(req *gitalypb.FindCommitsRequest) git.SubCmd {
  logFormatOption := "--format=%H"
  if req.GetTrailers() {
    logFormatOption += "%x00%(trailers:unfold,separator=%x00)"
  }
  subCmd := git.SubCmd{Name: "log", Flags: []git.Option{git.Flag{Name: logFormatOption}}}

  //  We will perform the offset in Go because --follow doesn't play well with --skip.
  //  See: https://gitlab.com/gitlab-org/gitlab-ce/issues/3574#note_3040520
  if req.GetOffset() > 0 && !calculateOffsetManually(req) {
    subCmd.Flags = append(subCmd.Flags, git.Flag{Name: fmt.Sprintf("--skip=%d", req.GetOffset())})
  }
  limit := req.GetLimit()
  if calculateOffsetManually(req) {
    limit += req.GetOffset()
  }
  subCmd.Flags = append(subCmd.Flags, git.Flag{Name: fmt.Sprintf("--max-count=%d", limit)})
  ...

  return subCmd
}
```

GitLab 这边接收到请求，最后通过 `consume_commits_response` 方法解析请求的返回值。

```ruby
# lib/gitlab/gitaly_client/commit_service.rb
def consume_commits_response(response)
  response.flat_map do |message|
    message.commits.map do |gitaly_commit|
      Gitlab::Git::Commit.new(@repository, gitaly_commit)
    end
  end
end
```

这个方法通过 `Gitlab::Git::Commit` 类来实例化一个 commit，我们称之为 raw_commit，其中 gitaly_commit 中包含了这个commit的信息。

最终在 `app/models/repository.rb` 的 commits 方法中，会对这些 raw_commit 进行组织，生成一批 `app/models/commit.rb` 这个类的对象，并把它们组合实例化成一个 `CommitCollection` 对象，这个类在 `app/models/commit_collection.rb` 中，定义了一些对一批 commits 调用的方法，例如 `committers` 方法，就是返回这一批 commits 中所有的 commiter。

```ruby
# app/models/commit_collection.rb
def committers
  emails = without_merge_commits.map(&:committer_email).uniq

  User.by_any_email(emails)
end
```

到这里，就是 GitLab repository 从 Gitaly 中获取数据的全部的流程。

### Others

#### Cache

由于 repository 需要请求 Gitaly ，从 Gitaly 那边拿数据，所以 cache 是一定要做的，repositroy 使用 `Gitlab::RepositoryCacheAdapter` 来实现方法的 cache。

```ruby
class_methods do
  # Caches and strongly memoizes the method.
  #
  # This only works for methods that do not take any arguments.
  #
  # name     - The name of the method to be cached.
  # fallback - A value to fall back to if the repository does not exist, or
  #            in case of a Git error. Defaults to nil.
  def cache_method(name, fallback: nil)
    uncached_name = alias_uncached_method(name)

    define_method(name) do
      cache_method_output(name, fallback: fallback) do
        __send__(uncached_name) # rubocop:disable GitlabSecurity/PublicSend
      end
    end
  end
...
```

通过给 `Repository` 类注入一个 类方法 `cache_method` 来实现方法的 cache，在注释中也有写到，这个 cache 方法只适用于不接受任何参数的方法。 `fallback` 参数是当仓库不存在的时候，直接返回的一个值。这个 `cache_method` 方法，首先使用 alias_method 定了一个加了 `_uncached_` 前缀的方法，并关联这两个方法，然后重新定义原方法名，使用 `strong_memoize` 模块用一个实例变量存储这个值，并调用原方法名将这个方法的返回值存储到 Redis 中。

另外还有 `cache_method_as_redis_set` `cache_method_asymmetrically` 这些缓存方法，`cache_method_as_redis_set` 把方法的返回值缓存在 Redis 的 set 中，这个适合缓存数组的返回值，例如 `branch_names` `tag_names` 这些方法。 `cache_method_asymmetrically` 缓存一些 `truthy value`，比如 `exists?`、`has_visible_content?`、`root_ref`。

比如 repository 使用 `cache_method_as_redis_set` 来缓存 `branch_names` 这个方法, 使用 `cache_method_asymmetrically` 来缓存 `root_ref`。

```ruby
cache_method_as_redis_set :branch_names, fallback: []
cache_method_asymmetrically :root_ref
```

#### GitalyClient

```ruby
def gitaly_ref_client
  @gitaly_ref_client ||= Gitlab::GitalyClient::RefService.new(self)
end

def gitaly_commit_client
  @gitaly_commit_client ||= Gitlab::GitalyClient::CommitService.new(self)
end

def gitaly_repository_client
  @gitaly_repository_client ||= Gitlab::GitalyClient::RepositoryService.new(self)
end

def gitaly_operation_client
  @gitaly_operation_client ||= Gitlab::GitalyClient::OperationService.new(self)
end

def gitaly_remote_client
  @gitaly_remote_client ||= Gitlab::GitalyClient::RemoteService.new(self)
end

def gitaly_blob_client
  @gitaly_blob_client ||= Gitlab::GitalyClient::BlobService.new(self)
end

def gitaly_conflicts_client(our_commit_oid, their_commit_oid)
  Gitlab::GitalyClient::ConflictsService.new(self, our_commit_oid, their_commit_oid)
end

def praefect_info_client
  @praefect_info_client ||= Gitlab::GitalyClient::PraefectInfoService.new(self)
end
```

在 `lib/gitlab/git/repository.rb` 中，有很多的 GitalyClient，它们都是用来和 Gitaly 交互用的：

- `gitaly_ref_client` 进行和分支相关的操作，比如获取 branches，获取 tags，合并 branches。
- `gitaly_commit_client` 进行和 commit 相关的操作，比如获取 commits，列出所有文件，进行 diff 操作，语言比例计算，commit 数据统计等等。
- `gitaly_repository_client` 运行和 repository 相关的操作，比如创建 repository 获取 repository 大小等等。
- `gitaly_operation_client` 运行一些对项目仓库的操作，比如添加 tag， 移除 tag，添加分支等。
- `gitaly_remote_client` 运行获取远端信息的操作，比如如果我们的项目是 mirror 自其他地址或从其他地址导入，我们可以通过这个 service 来获取远端的 ref。
- `gitaly_blob_client` 运行的是 blob 相关的操作，例如列出所有 blob，获取一个 blob 等。
- `gitaly_conflicts_client` 是处理冲突的一个 service， 它可以进行列出所有冲突，解决冲突的操作。
- `praefect_info_client` 是用来查询 praefect 信息的 service。 
