---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 更新日志条目

本文指导用户何时及如何生成更新日志条目文件，以及更新日志流程的信息和历史等。

## 概览

[`/jh/CHANGELOG.md`](https://jihulab.com/gitlab-cn/gitlab/-/blob/main-jh/jh/CHANGELOG.md) 是用来展示 `/jh/` 目录下的改动历史，文件中的每个要点或**条目**都是从 Git 提交的主题行生成的。当提交包含 `Changelog` [Git Trailer](https://git-scm.com/docs/git-interpret-trailers)，并且在更新日志中包括 `JH: true` 的时候，提交将包含在 `/jh/CHANGELOG.md` 文件中。生成更新日志时，系统会自动添加作者和合并请求的详细信息。

`Changelog` Trailer 支持以下值：

- `added`：新功能
- `fixed`：错误修复
- `changed`：功能变更
- `deprecated`：废弃
- `removed`：功能移除
- `security`：安全修复
- `performance`：性能提升
- `other`：其他

以下是包含更新日志的 Git 提交示例：

```plaintext
Update git vendor to gitlab

Now that we are using gitaly to compile git, the git version isn't known
from the manifest, instead we are getting the gitaly version. Update our
vendor field to be `gitlab` to avoid cve matching old versions.

Changelog: changed
```

### JH 版本更新

对于在 `/jh/` 目录下的改动，**您必须添加** `JH: true`。

```plaintext
Remove project peformance analytics feature flag

Changelog: changed
JH: true
```

### 覆盖关联的合并请求

极狐GitLab 在生成更新日志时，会自动将合并请求和提交进行关联。
如果想覆盖关联的合并请求，您可以使用 `MR` Trailer 指定一个替代的合并请求。

```plaintext
Remove project peformance analytics feature flag

Changelog: changed
MR: https://jihulab.com/gitlab-cn/gitlab/-/merge_requests/563
```

您必须填写完整的合并请求 URL。


<!--
**Do not** add the trailer for changes that apply to both EE and CE.-->

<a id="what-warrants-a-changelog-entry"></a>

## 更新日志条目要求

- 任何引入数据库迁移（无论是否是规律迁移）或数据迁移的变更都**必须**添加对应的更新日志条目，即使它的功能标志已被禁用。
- 安全修复<!--[安全修复](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md)-->**必须**添加更新日志条目，并且 `Changelog` Trailer 需要设置为 `security`。
- 任何面向用户的变更都**必须**添加更新日志条目。例如："极狐GitLab 现在为所有文本使用系统字体"。
- 任何对 REST 和 GraphQL API 作出的面向终端的变更都**必须**添加更新日志条目。
  <!--详情请参见 [GraphQL 重大变更组成部分的完整列表](api_graphql_styleguide.md#breaking-changes)。-->
- 任何引入高级搜索迁移<!--[高级搜索迁移](elasticsearch.md#creating-a-new-advanced-search-migration)-->的变更都**必须**添加更新日志条目。
- 如果在一个发布中引入了回退问题，同时在这个发布中又进行了修复（例如，修复了月度发布候选版本中引入的错误），这种情况**不应该**添加更新日志条目。
- 任何面向开发者（例如：重构、解决技术债或改动测试套件）的变更都**不应该**添加更新日志条目。例如 "减少了周期分析模型规格中创建的数据库记录"。
- *任何*来自社区成员的贡献，无论多微小，如果贡献者希望添加更新日志条目，**或许**可以忽略这些规定，为他们添加更新日志条目。 
- 任何实验性<!--[实验性](experiment_guide/index.md)-->的变更都**不应该**添加更新日志条目。
- 仅进行了文档改动的 MR **不应该**添加更新日志条目。

<!--详情请参见[如何使用功能标志处理更新日志条目](feature_flags/index.md#changelog)。-->

## 好的更新日志条目

好的更新日志条目的内容应该丰富且简洁，能够让读者在*丝毫不了解上下文*的基础上知道进行了哪些变更。
如果您无法将更新日志写得既丰富又简洁，可以适当偏重于简洁性。

- **需要改进的更新日志：** 访问项目的顺序。
- **好的更新日志：** 在 "访问项目" 下拉列表的顶部展示用户的星标项目。

第一个例子没能让用户了解关于变更位置、原因及用户收益的任何信息。

- **需要改进的更新日志：** 将（文本）复制到剪贴板。
- **好的更新日志：** 更新 "复制到剪贴板" 工具提示信息，提示用户粘贴内容。

第一个例子描述得太模糊，没有提供任何上下文信息。

- **需要改进的更新日志：** 在微型流水线图表和构建下拉列表中修复并改进 CSS 和 HTML 问题。
- **好的更新日志：** 在微型流水线图表和构建下拉列表中修复工具提示信息及悬停状态。

第一个例子太着重于实施细节。用户根本不关心我们是否改动了 CSS 和 HTML，他们只关心改动的*最终结果*。

- **需要改进的更新日志：** 剥离 `find_commits_by_message_with_elastic` 返回的推送阵列对象中的 `nil`。
- **好的更新日志：** 修复了 500 个由垃圾收集推送引用的 Elasticsearch 结果而引发的错误。

第一个例子重点在于我们*如何*修复了错误，而不是修复了*什么*。
第二个例子清晰地描述了用户的*最终收益*（减少了 500 个错误），以及*时间*（使用 Elasticsearch 搜索提交的时候）。

您需要思路清晰，能够以更新日志读者的角度考虑问题。
这条更新日志是否有价值？是否为日志读者提供了变更的*位置*和*原因*信息？

## 如何生成更新日志条目

当您进行提交时，系统会在提交信息中添加 Git Trailer。您可以使用文档编辑器实现。
如果您想在现有的提交上添加 Trailer，您需要改动提交（如果是最新的提交），或者使用 `git rebase -i` 进行交互式变基。

如果想更新最后一次提交，请运行以下命令：

```shell
git commit --amend
```

您可以向提交信息中添加 `Changelog` Trailer。如果您已经向远端分支推送了之前的提交，您需要强制推送新提交。

```shell
git push -f origin your-branch-name
```

如果想编辑旧的或多个提交，您可以使用 `git rebase -i HEAD~N`。其中 `N` 是要变基的提交的后 N 个数字。
假设在您的分支上有 3 个提交：A、B 和 C。
如果想更新 B 提交，您需要运行：

```shell
git rebase -i HEAD~2
```

这会开启最后两个提交的交互式变基会话。开启之后，您会看到文本编辑器及以下内容：

```plaintext
pick B Subject of commit B
pick C Subject of commit C
```

如果想更新 B 提交，您需要将 `pick` 改为 `reword`，然后保存并退出编辑器。
关闭之后，您会看到新的编辑器实例，让您编辑 B 提交的提交信息。 
您需要添加 Trailer，然后保存并退出编辑器。
如果一切顺利，B 提交已经更新了。

有关交互式编辑的更多信息，请参见 [Git 文档](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)。

---

[返回开发文档](index.md)


