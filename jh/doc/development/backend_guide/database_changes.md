# JiHu 数据库迁移指南

如果 JiHu 的贡献包含对 PostgreSQL 的数据库迁移，将会有这两种情况:

1. 对上游的贡献既包含数据库迁移，也包含代码变更; 
1. 对上游的贡献只包含数据库迁移，没有代码变更。

以下的指南都是针对第二种情况。 对于第一种情况，我们遵从上游仓库的常规贡献流程。

## 数据库结构变更

对于数据库结构的变更，我们会把所有的数据库迁移都提交到上游仓库，再通过仓库镜像机制更新到 JiHu 的仓库中。
这样做保证了 CE/EE/JH 中的数据库结构的统一，方便用户在他们之间进行版本迁移。

这样的做法是对 CE/EE 的扩展，CE 和 EE 也是共享一套完全相同的数据库结构。

对于 JiHu 专有的数据库对象，比如表/字段/索引，应该对它们添加明确的 PostgreSQL 注释，这样能向上游仓库解释: 为什么只创建数据库对象却没有代码应用它，也能帮助上游忽略相关的表/字段/索引，从而提高数据库性能。

详情参考: [gitlab-org/database-team/team-tasks#192](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/192) 。

### 添加字段

JiHu 的贡献可能会向一个已经存在的表中添加字段，但是上游的代码并不会使用这些字段。 为了清楚起见，需要采取一下措施:

1. 对该字段添加 PostgreSQL 注释 `JiHu-specific`，来说明这是 JiHu 特有的字段;
1. 基于 PostgreSQL 注释，上游不会把 JiHu 特有的字段暴露给 ActiveRecord ，JiHu 中不会有这样的限制; (上游尚未实现)
1. 添加字段的属性必须是 nullable 的，除非指定了默认值。

### 添加表/视图

当 JiHu 的贡献包含创建一张新表时，我们需要对它添加 PostgreSQL 注释 `JiHu-specific` 。 上游会阻止这些表映射到 ActiveRecord 模型 (上游尚未实现)。

是允许 JiHu 对既有的表添加外键的，但是务必使用 `ON DELETE` 来保证级联删除，这对于在 CE/EE 和 JiHu 之间的迁移至关重要。

### 添加索引

在 JiHu 专有的表中添加索引无需额外的顾虑。 如果对既有表中的 JiHu 专有的字段添加索引，则索引必须是体积更小的部分索引，比如:

```sql
CREATE INDEX user_details_phone ON user_details (phone_number) WHERE phone_number IS NOT NULL
```

如果对既有表中的既有字段添加索引，则需要 [上游数据库团队](https://about.gitlab.com/handbook/engineering/development/enablement/database) 的评审。

以上所有的情况，都需要在索引的注释中标记 `JiHu-specific` 。

[可选] 为了降低 GitLab.com 的开销，上游可能会忽略创建 JiHu 专有的索引。

### 其他数据库对象

添加其他其他数据库对象 (比如，触发器/方法/扩展) 需要 [上游数据库团队](https://about.gitlab.com/handbook/engineering/development/enablement/database) 的评审。

## 数据迁移

JiHu 对既有数据的迁移也会合并到上游仓库中。 上游不需要执行这些迁移，特别是 GitLab.com 。

上游正计划实现一种机制，能让特定的环境中运行特定的数据库迁移，具体参见 [Epic &6705](https://gitlab.com/groups/gitlab-org/-/epics/6705) 。

在实现这种机制之前，JiHu 专有的数据迁移都需要 [上游数据库团队](https://about.gitlab.com/handbook/engineering/development/enablement/database) 的评审。

## 在 CE/EE 和 JiHu 之间迁移

CE/EE 和 JiHu 使用完全相同的数据库结构，所以它们之间可以无缝切换。

## 取舍与讨论

我们认识到选定的模式具有以下缺点:

| 缺点 | 措施 |
|---|---|
| 添加了上游无用的数据库对象 | 通过 `JiHu-specific` 注释来减少误解 |
| 对于创建和维护 JiHu 专有的数据库对象，上游的安装实例 (包含 GitLab.com) 需要承担额外的开销  | 对于 GitLab.com，可以选择忽略 JiHu 专有的索引，因为它不需要升级到 JiHu |
| JiHu 对数据库的修改都需要提交到上游仓库，限制了灵活性 | 这加深了数据库设计方面的交流 |
| 代码评审的开销: 对于脱离了场景和代码的数据库设计，很难提供有意义的反馈 | 提供相关代码和充分的背景知识 |

### 落选方案: JiHu 维护专有的迁移

我们还设计了一种方案，[让 JiHu 单独维护专有的数据库迁移](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90336)。

JiHu 希望在 `jh` 目录下独立维护 JiHu 专有的数据库迁移，这样就不需要把所有的数据库修改都提交到上游仓库。

这大大提高了 JiHu 在数据库方面的灵活性，但是代价是很难支持 CE/EE 和 JiHu 之间的无缝切换。
因为这会造成 CE/EE 和 JiHu 之间存在差异(类似 Git 分支分叉)，这里有一个具体的示例来解释 [issue #161](https://gitlab.com/gitlab-jh/gitlab/-/issues/161) 。

我们此时没有选择这个方案，是因为我们愿意以迭代效率为代价换取 CE/EE 和 JiHu 之间的无缝切换的能力。

## 数据库评审的注意事项

事实证明，对于没有相关代码上下文的数据库迁移，评审者和维护者很难提供有效的建议。
为了帮助评审者更好理解上下文，我们需要在请求数据库评审之前，先将对应的代码准备好并对其进行评审。
还需要在 MR 中链接相关的代码，并提供有用的背景信息。

其他情况，我们参考 [如何准备数据库评审的 MR](database_review.md#how-to-prepare-the-merge-request-for-a-database-review)。

## 从 fork 的仓库中提交数据库迁移的 MR

为了提高评审的效率，建议在 fork 的项目中配置 [`DANGER_GITLAB_API_TOKEN`](https://docs.gitlab.com/ee/development/dangerbot.html#limitations) 。

配置之后会允许运行 Danger，它会根据 MR 内容推荐合适的评审者。
