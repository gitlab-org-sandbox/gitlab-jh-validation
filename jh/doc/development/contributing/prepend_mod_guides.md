---
stage: none
group: unassigned
---

# prepend_mod 指南

## 在 EE 的后端代码中扩展 CE 的功能

如果某个功能是基于现有的 CE 功能构建的，那么需要在 EE 命名空间中编写一个模块，并将其注入到 CE 中。

具体的做法是在 CE 对应文件的末尾添加 `prepend_mod`，例如：

```ruby
class User < ActiveRecord::Base
  # ... lots of code here ...
end

User.prepend_mod

```

更多细节请参考上游文档 [Extend CE features with EE backend code](https://docs.gitlab.com/ee/development/ee_features.html#extend-ce-features-with-ee-backend-code)。

## 在 JH 的后端代码中扩展 CE 和 EE 的功能

跟 EE 扩展 CE 类似，JH 的功能也可以基于 CE 或 EE 的功能进行扩展。

如果 JH 的功能同时基于 CE 和 EE，那么 CE 中一定已经存在了 `prepend_mod`，
这种情况下只需要编写新的 JH 模块，EE 和 JH 模块会依次被注入到 CE 中。

如果 JH 的功能只基于 CE，那么就需要向 CE 对应的文件中添加 `prepend_mod`，再编写 JH 的模块。JH 的模块可以单独注入到 CE 中。

## 向上游贡献 prepend_mod 的注意事项

1. 向贡献的 `prepend_mod` 添加注释

```ruby
# Added for JiHu
Llm::MergeRequests::SummarizeDiffService.prepend_mod
```

由于相当一部分上游的贡献者不了解 JH 的存在，他们在考虑删减 `prepend_mod` 时，只考虑 EE 是否还需要依赖它。

假设此时 JH 和 EE 同时依赖某个 CE 模块，当上游贡献者对 EE 的功能进行重构，移除了 EE 的代码，那么该贡献者会很自然地删除 CE 中的 `prepend_mod`。
那么 JH 的功能就会因为缺失 `prepend_mod` 而遭到破坏。

添加注释能提醒上游贡献者 JH 也依赖此处的 CE 模块，有效减少误删除的可能。

2. 并行审阅

只添加 `prepend_mod` 的 MR 是非常容易进行审阅的，虽然它依然需要经历完整的审阅流程，然而我们可以通过并行邀请审阅来加速这一流程。

对于后端审阅者，由于 `gitlab-org/maintainers/rails-backend` 组里有太多的贡献者，邀请这个大组是不合适的。
推荐的做法是从 `Reviewer roulette` 中挑选合适的审阅者，或者通过 Git blame 查询对应模块的贡献者。

对于安全审查，可以尽早邀请 `gitlab-com/gl-security/appsec` 进行，跟后端审阅并行进行能节省很多等待的时间。

更多技术讨论请参考[该议题](https://gitlab.com/gitlab-org/gitlab/-/issues/426083) 。
