# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CloudConnector::StatusChecks::StatusService, feature_category: :cloud_connector do
  it 'has the expected default probes' do
    expect(described_class::DEFAULT_PROBES).to match_array([
      an_instance_of(CloudConnector::StatusChecks::Probes::LicenseProbe),
      an_instance_of(CloudConnector::StatusChecks::Probes::EndToEndProbe),
      an_instance_of(CloudConnector::StatusChecks::Probes::HostProbe).and(
        have_attributes(host: 'cloud.gitlab.com', port: 443)
      ),
      an_instance_of(CloudConnector::StatusChecks::Probes::HostProbe).and(
        have_attributes(host: 'customers.stg.jihulab.com', port: 443)
      ),
      an_instance_of(CloudConnector::StatusChecks::Probes::AccessProbe)
    ])
  end
end
