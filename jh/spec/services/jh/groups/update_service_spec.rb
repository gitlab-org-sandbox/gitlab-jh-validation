# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Groups::UpdateService, feature_category: :groups_and_projects do
  let!(:user) { create(:user) }
  let!(:group) { create(:group) }

  describe "#execute" do
    context 'when updating mr custom page' do
      let(:service) do
        described_class.new(group, user, {
          mr_custom_page_title: 'Custom title',
          mr_custom_page_url: 'http://example.com'
        })
      end

      before do
        group.add_owner(user)
      end

      context 'when group has no jh_namespace_setting' do
        it 'creates jh_namespace_setting' do
          expect { service.execute }.to change { group.jh_namespace_setting }.from(nil).to(
            an_instance_of(Extend::NamespaceSetting))
          expect(group.jh_namespace_setting.mr_custom_page_title).to eq('Custom title')
          expect(group.jh_namespace_setting.mr_custom_page_url).to eq('http://example.com')
        end
      end

      context 'when group has jh_namespace_setting' do
        before do
          group.build_jh_namespace_setting(mr_custom_page_title: 'Old title', mr_custom_page_url: 'http://old.example.com')
          group.jh_namespace_setting.save!
        end

        it 'updates jh_namespace_setting' do
          service.execute

          expect(group.jh_namespace_setting.mr_custom_page_title).to eq('Custom title')
          expect(group.jh_namespace_setting.mr_custom_page_url).to eq('http://example.com')
        end
      end

      context 'when FF is disabled' do
        before do
          stub_feature_flags(ff_mr_custom_page_tab: false)
        end

        it 'does not update jh_namespace_setting' do
          expect { service.execute }.not_to change { group.jh_namespace_setting }
        end
      end

      context 'when jh database is not set' do
        before do
          allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
        end

        it 'does not update jh_namespace_setting' do
          expect { service.execute }.not_to change { group.jh_namespace_setting }
        end
      end
    end
  end
end
