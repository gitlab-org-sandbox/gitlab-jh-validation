# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Commits::FetchEffectiveLinesCountService do
  let_it_be(:project) { create(:project, :repository) }
  let_it_be(:commit) { project.commit(project.default_branch) }

  let(:subject) { described_class.new(project, commit).execute }

  describe '#execute' do
    it 'returns the commit information' do
      expect(subject).to eq({
        id: commit.id,
        author_name: commit.author_name,
        author_email: commit.author_email,
        date: commit.committed_date.strftime("%Y-%m-%d"),
        additions: commit.stats.additions,
        deletions: commit.stats.deletions,
        effective_additions: 1,
        effective_deletions: 0
      })
    end

    it 'caches the result', :use_clean_rails_redis_caching do
      expect { subject }.to change { Rails.cache.fetch("commit_effective_lines-#{project.full_path}-#{commit.id}") }
    end
  end
end
