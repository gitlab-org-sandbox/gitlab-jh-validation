# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabSubscriptions::UploadLicenseService, feature_category: :plan_provisioning do
  subject(:execute_service) { described_class.new(params, path_to_subscription_page).execute }

  let(:path_to_subscription_page) { '/admin/subscription' }
  let(:params) { { data: build(:license, data: gitlab_license.export).data } }

  shared_examples 'license creation result' do |status:, persisted:|
    it "returns #{status == :success ? 'a success' : 'an error'}" do
      result = execute_service
      license = result.payload[:license]

      expect(result.status).to eq(status)
      expect(license).to be_an_instance_of(License)
      expect(license.persisted?).to eq(persisted)
      expect(result.message).to eq(message)
    end
  end

  shared_examples 'unsuccessful license upload scenarios' do
    context 'with an invalid license key' do
      let(:params) { { data: 'invalid_license_key' } }
      let(:message) { 'The license key is invalid. Make sure it is exactly as you received it from JiHu(GitLab)' }

      include_examples 'license creation result', status: :error, persisted: false
    end

    context 'with an expired license key' do
      let(:license_traits) { super() << :expired }
      let(:message) { 'This license has already expired.' }

      include_examples 'license creation result', status: :error, persisted: false
    end
  end

  context 'when license key belongs to an offline cloud license', :with_license do
    let(:license_traits) { [:offline] }
    let(:attributes) { {} }
    let(:gitlab_license) { build(:gitlab_license, *license_traits, attributes) }

    include_examples 'unsuccessful license upload scenarios'
  end

  context 'when license key belongs to a legacy license', :with_license do
    let(:license_traits) { [:legacy] }
    let(:attributes) { {} }
    let(:gitlab_license) { build(:gitlab_license, *license_traits, attributes) }

    context 'and for a trial' do
      let(:license_traits) { super() << :trial }

      include_examples 'unsuccessful license upload scenarios'
    end

    include_examples 'unsuccessful license upload scenarios'
  end
end
