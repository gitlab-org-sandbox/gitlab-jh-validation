# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ServicePing::SubmitService do
  it 'returns correct host in JH' do
    expect(described_class.new.send(:base_url)).to eq ENV['VERSION_DOT_HOST'] || described_class::JH_BASE_URL
  end
end
