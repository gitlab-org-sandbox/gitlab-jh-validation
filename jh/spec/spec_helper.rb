# frozen_string_literal: true

require Rails.root.join("spec/support/helpers/stub_requests.rb")

# JH support helpers has been loaded, see: jh/config/initializers/require_jh_spec_support_helpers.rb
Dir[Rails.root.join("jh/spec/support/helpers/**/*.rb")].each { |f| require f }

Dir[Rails.root.join("jh/spec/support/shared_contexts/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("jh/spec/support/shared_examples/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("jh/spec/support/**/*.rb")].each { |f| require f }

require_relative '../lib/jh/skip_specs'

config_path = File.expand_path("config/skip_specs.yml", __dir__)
skip_specs = JH::SkipSpecs.new(config_path)

RSpec.configure do |config|
  include JHDnsHelpers

  config.before do
    permit_upstream!
    # not sure why upstream permit_redis! failed, will ask about upstream
    permit_jh_redis!
  end

  config.before(:all) do
    stub_feature_flags(jh_only_block_seat_for_team_plan: false)
  end

  config.before do |example|
    # We use the tag `phone_verification_code_enabled` to enable phone verification code in tests.
    # Add it to your test if you want to enable the phone verification code feature.
    # For example: describe '...', :phone_verification_code_enabled do
    # But we do not mock application_settings on test that use tag `do_not_mock_admin_mode_setting`
    if example.metadata[:phone_verification_code_enabled]
      stub_application_setting(phone_verification_code_enabled: true)
    end

    if example.metadata[:hk_saas]
      allow(Gitlab).to receive(:com?).and_return(true)
      stub_env('SAAS_REGION', 'HK')
    end

    # Set default value of `JH_AI_PROVIDER`
    stub_env('JH_AI_PROVIDER', example.metadata[:ai_provider].presence || :open_ai)

    # Set to false to avoid affecting Upstream's tests
    stub_feature_flags(ff_invitation_email_rate_limit: false)
    stub_feature_flags(jh_only_block_seat_for_team_plan: false)

    # Default enable ones_issues_integration to avoid upstream integration test failed
    stub_licensed_features(ones_issues_integration: true)
  end

  if skip_specs.skipped_list.any?
    config.around do |example|
      Timeout.timeout(300) { example.run } unless skip_specs.skipped?(example)
    end
  end
end
