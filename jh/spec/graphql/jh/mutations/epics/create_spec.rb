# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mutations::Epics::Create, feature_category: :groups_and_projects do
  let_it_be(:group) { create(:group) }
  let_it_be(:user) { create(:user) }
  let_it_be(:milestone) { create(:milestone, group: group) }
  let_it_be(:iteration) { create(:iteration, group: group) }
  let(:title) { 'JH epic title' }
  let(:default_args) { { group_path: group.full_path, title: title } }
  let(:args) { {} }
  let(:epic) { nil }

  subject(:mutation) { described_class.new(object: group, context: { current_user: user }, field: nil) }

  before_all do
    group.add_developer(user)
  end

  describe '#resolve' do
    before do
      stub_licensed_features(epics: true, milestone_and_iteration_in_epic: true)
    end

    subject(:result) { mutation.resolve(args) }

    it_behaves_like 'unavailable process the epic JH special associations in the invalid cases'

    context 'with both milestone and iteration ids exist' do
      let(:args) { default_args.merge(milestone_id: milestone.id, sprint_id: iteration.id) }

      it 'creates the epic associations of milestone and iteration' do
        epic = result[:epic]
        expect(epic[:title]).to eq(title)
        expect(epic.jh_epic).to be_present
        expect(epic.jh_epic.jh_milestone).to be_present
        expect(epic.jh_epic.jh_iteration).to be_present
      end
    end

    context 'with only milestone or iteration id exists' do
      context 'when milestone id exists' do
        let(:args) { default_args.merge(milestone_id: milestone.id) }

        it 'creates the epic associations of milestone' do
          epic = result[:epic]
          expect(epic[:title]).to eq(title)
          expect(epic.jh_epic).to be_present
          expect(epic.jh_epic.jh_milestone).to be_present
          expect(epic.jh_epic.jh_iteration).not_to be_present
        end
      end

      context 'when iteration id exists' do
        let(:args) { default_args.merge(sprint_id: iteration.id) }

        it 'creates the epic associations of iteration' do
          epic = result[:epic]
          expect(epic[:title]).to eq(title)
          expect(epic.jh_epic).to be_present
          expect(epic.jh_epic.jh_milestone).not_to be_present
          expect(epic.jh_epic.jh_iteration).to be_present
        end
      end
    end
  end
end
