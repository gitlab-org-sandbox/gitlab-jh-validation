# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Mutations::Epics::Update, feature_category: :groups_and_projects do
  let_it_be(:group) { create(:group) }
  let_it_be(:user) { create(:user) }
  let_it_be(:epic) { create(:epic, group: group) }
  let_it_be(:milestone) { create(:milestone, group: group) }
  let_it_be(:iteration) { create(:iteration, group: group) }
  let(:title) { 'JH epic title' }
  let(:default_args) { { group_path: group.full_path, iid: epic.iid, title: title } }
  let(:args) { {} }

  subject(:mutation) { described_class.new(object: group, context: { current_user: user }, field: nil) }

  before_all do
    group.add_developer(user)
  end

  describe '#resolve' do
    before do
      stub_licensed_features(epics: true, milestone_and_iteration_in_epic: true)
    end

    subject(:result) { mutation.resolve(args) }

    it_behaves_like 'unavailable process the epic JH special associations in the invalid cases'

    context 'when epic does not has milestone and iteration' do
      context 'with mutation on both milestone and iteration' do
        let(:args) { default_args.merge(milestone_id: milestone.id, sprint_id: iteration.id) }

        it 'updates the epic associations of milestone and iteration' do
          epic = result[:epic]
          expect(epic[:title]).to eq(title)
          expect(epic.jh_epic).to be_present
          expect(epic.jh_epic.jh_milestone).to be_present
          expect(epic.jh_epic.jh_iteration).to be_present
        end
      end

      context 'with the milestone or iteration mutation' do
        context 'when request the milestone' do
          let(:args) { default_args.merge(milestone_id: milestone.id) }

          it 'updates the epic associations of milestone' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_milestone).to be_present
            expect(epic.jh_epic.jh_iteration).not_to be_present
          end
        end

        context 'when request the iteration' do
          let(:args) { default_args.merge(sprint_id: iteration.id) }

          it 'updates the epic associations of iteration' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_milestone).not_to be_present
            expect(epic.jh_epic.jh_iteration).to be_present
          end
        end
      end
    end

    context 'when epic has milestone and iteration already' do
      let_it_be(:old_milestone) { create(:milestone, group: group) }
      let_it_be(:old_iteration) { create(:iteration, group: group) }

      before do
        old_milestone.create_jh_milestone!
        old_iteration.create_jh_iteration!
        epic.create_jh_epic!(milestone_id: old_milestone.id, sprint_id: old_iteration.id)
      end

      context 'with mutation on both milestone and iteration' do
        let(:args) { default_args.merge(milestone_id: milestone.id, sprint_id: iteration.id) }

        it 'updates the epic associations of milestone and iteration' do
          epic = result[:epic]
          expect(epic[:title]).to eq(title)
          expect(epic.jh_epic).to be_present
          expect(epic.jh_epic.jh_milestone).to be_present
          expect(epic.jh_epic.jh_iteration).to be_present
          expect(epic.jh_epic.jh_milestone.milestone.id).to eq(milestone.id)
          expect(epic.jh_epic.jh_iteration.iteration.id).to eq(iteration.id)
        end
      end

      context 'with mutation on only milestone or iteration' do
        context 'when request the milestone' do
          let(:args) { default_args.merge(milestone_id: milestone.id) }

          it 'updates the epic associations of milestone' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_milestone).to be_present
            expect(epic.jh_epic.jh_iteration.iteration.id).to eq(old_iteration.id)
            expect(epic.jh_epic.jh_milestone.milestone.id).to eq(milestone.id)
          end
        end

        context 'when request the iteration' do
          let(:args) { default_args.merge(sprint_id: iteration.id) }

          it 'updates the epic associations of iteration' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_iteration).to be_present
            expect(epic.jh_epic.jh_milestone.milestone.id).to eq(old_milestone.id)
            expect(epic.jh_epic.jh_iteration.iteration.id).to eq(iteration.id)
          end
        end
      end

      context 'with mutation to clear milestone or iteration' do
        context 'when request the milestone as nil' do
          let(:args) { default_args.merge(milestone_id: nil) }

          it 'clear the epic associations of milestone' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_milestone).not_to be_present
            expect(epic.jh_epic.jh_iteration.iteration.id).to eq(old_iteration.id)
          end
        end

        context 'when request the iteration as nil' do
          let(:args) { default_args.merge(sprint_id: nil) }

          it 'clear the epic associations of iteration' do
            epic = result[:epic]
            expect(epic[:title]).to eq(title)
            expect(epic.jh_epic).to be_present
            expect(epic.jh_epic.jh_iteration).not_to be_present
            expect(epic.jh_epic.jh_milestone.milestone.id).to eq(old_milestone.id)
          end
        end
      end
    end
  end
end
