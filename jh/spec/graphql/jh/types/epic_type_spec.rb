# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabSchema.types['Epic'], feature_category: :portfolio_management do
  include GraphqlHelpers
  include_context 'includes EpicAggregate constants'

  let(:fields) do
    %i[
      id iid title titleHtml description descriptionHtml confidential state group
      parent author labels start_date start_date_is_fixed start_date_fixed
      start_date_from_milestones start_date_from_inherited_source due_date
      due_date_is_fixed due_date_fixed due_date_from_milestones due_date_from_inherited_source
      closed_at created_at updated_at children has_children has_issues
      has_parent web_path web_url relation_path reference issues user_permissions
      notes discussions relative_position subscribed participants
      descendant_counts descendant_weight_sum upvotes downvotes
      user_notes_count user_discussions_count health_status current_user_todos
      award_emoji events ancestors color text_color blocked blocking_count
      blocked_by_count blocked_by_epics default_project_for_issue_creation
      has_children_within_timeframe commenters milestone iteration name
    ]
  end

  it { expect(described_class).to have_graphql_fields(fields) }
end
