# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Resolvers::EpicsResolver, feature_category: :portfolio_management do
  include GraphqlHelpers

  let_it_be(:current_user) { create(:user) }
  let_it_be(:group) { create(:group) }
  let_it_be(:epic1)   do
    create(:epic, group: group, title: 'first created', created_at: 3.days.ago, updated_at: 2.days.ago,
      start_date: 10.days.ago, end_date: 10.days.from_now)
  end

  before_all do
    group.add_developer(current_user)
  end

  before do
    stub_licensed_features(epics: true, milestone_and_iteration_in_epic: true)
  end

  shared_examples 'unavailable filter by JH group milestone' do
    it 'returns empty result' do
      epics = resolve_epics(group_milestone_title: milestone.title)

      expect(epics).to be_empty
    end
  end

  describe '#resolve' do
    context 'when search by group self milestone' do
      let_it_be(:milestone) { create(:milestone, group: group) }
      let_it_be(:epic2) do
        create(:epic, group: group, title: 'second created', created_at: 3.days.ago, updated_at: 2.days.ago,
          start_date: 10.days.ago, end_date: 10.days.from_now)
      end

      before do
        milestone.create_jh_milestone!
        epic2.create_jh_epic!(milestone_id: milestone.id)
      end

      context 'with the invalid cases' do
        context 'with SAAS env', :saas do
          it_behaves_like 'unavailable filter by JH group milestone'
        end

        context 'with feature flag disabled' do
          before do
            stub_feature_flags(epic_support_milestone_and_iteration: false)
          end

          it_behaves_like 'unavailable filter by JH group milestone'
        end

        context 'with license feature disabled' do
          before do
            stub_licensed_features(epics: true, milestone_and_iteration_in_epic: false)
          end

          it_behaves_like 'unavailable filter by JH group milestone'
        end

        context 'with jh database not configured' do
          before do
            allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
          end

          it_behaves_like 'unavailable filter by JH group milestone'
        end
      end

      context 'with milestone' do
        it 'filters epics by self milestone' do
          epics = resolve_epics(group_milestone_title: milestone.title)

          expect(epics).to match_array([epic2])
        end

        it 'returns empty result if milestone is not assigned to any epic' do
          milestone2 = create(:milestone, group: group)
          epics = resolve_epics(group_milestone_title: milestone2.title)

          expect(epics).to be_empty
        end
      end

      context 'with subgroups' do
        let(:sub_group) { create(:group, parent: group) }
        let(:milestone) { create(:milestone, group: sub_group) }
        let!(:epic3)    { create(:epic, group: sub_group, iid: epic2.iid) }

        before do
          sub_group.add_developer(current_user)
          epic3.create_jh_epic!(milestone_id: milestone.id)
        end

        it 'filters by milestones in subgroups' do
          expect(resolve_epics(group_milestone_title: milestone.title)).to contain_exactly(epic2, epic3)
        end
      end
    end
  end

  def resolve_epics(args = {}, obj = group, context = { current_user: current_user })
    resolve(described_class, obj: obj, args: args, ctx: context, arg_style: :internal)
  end
end
