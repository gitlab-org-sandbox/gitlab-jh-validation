# frozen_string_literal: true

FactoryBot.modify do
  factory :user do
    preferred_language { 'en' }
  end
end
