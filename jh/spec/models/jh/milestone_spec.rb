# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Milestone, feature_category: :database do
  describe 'associations' do
    it { is_expected.to have_one(:jh_milestone).class_name('Extend::Milestone') }
    it { is_expected.to have_many(:jh_epics).class_name('Extend::Epic').through(:jh_milestone) }
  end
end
