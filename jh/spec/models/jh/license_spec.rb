# frozen_string_literal: true

require "spec_helper"

RSpec.describe License, feature_category: :sm_provisioning do
  describe '#paid?', :with_license do
    let(:license) { build(:license, plan: License::TEAM_PLAN) }

    it 'returns true for team license' do
      expect(license.paid?).to be_truthy
    end
  end
end
