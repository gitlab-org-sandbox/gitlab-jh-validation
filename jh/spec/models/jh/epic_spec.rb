# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Epic do
  describe 'validations' do
    it_behaves_like "content validation", :epic, :title
    it_behaves_like "content validation", :epic, :description
  end

  describe 'associations' do
    it { is_expected.to have_one(:jh_epic).class_name('Extend::Epic') }
  end
end
