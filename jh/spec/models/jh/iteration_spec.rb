# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Iteration, feature_category: :database do
  describe 'associations' do
    it { is_expected.to have_one(:jh_iteration).class_name('Extend::Iteration') }
    it { is_expected.to have_many(:jh_epics).class_name('Extend::Epic').through(:jh_iteration) }
  end
end
