# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Extend::Iteration, feature_category: :database do
  describe 'model' do
    it { is_expected.to have_db_index(:sprint_id) }
  end

  describe 'associations' do
    it do
      is_expected.to belong_to(:iteration).class_name('::Iteration')
        .inverse_of(:jh_iteration).with_foreign_key('sprint_id')
    end

    it do
      is_expected.to have_many(:jh_epics).class_name('Extend::Epic')
        .inverse_of(:jh_iteration).with_foreign_key('sprint_id').with_primary_key('sprint_id')
    end
  end
end
