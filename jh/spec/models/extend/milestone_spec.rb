# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Extend::Milestone, feature_category: :database do
  describe 'associations' do
    describe 'model' do
      it { is_expected.to have_db_index(:milestone_id) }
    end

    it { is_expected.to belong_to(:milestone).class_name('::Milestone').inverse_of(:jh_milestone) }

    it do
      is_expected.to have_many(:jh_epics).class_name('Extend::Epic')
        .inverse_of(:jh_milestone).with_primary_key('milestone_id')
    end
  end
end
