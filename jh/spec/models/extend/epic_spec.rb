# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Extend::Epic, feature_category: :database do
  describe 'model' do
    it { is_expected.to have_db_index(:epic_id) }
    it { is_expected.to have_db_index(:milestone_id) }
    it { is_expected.to have_db_index(:sprint_id) }
    it { is_expected.to have_db_index([:epic_id, :milestone_id]) }
    it { is_expected.to have_db_index([:epic_id, :sprint_id]) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:epic).class_name('::Epic').inverse_of(:jh_epic) }

    it do
      is_expected.to belong_to(:jh_milestone).class_name('Extend::Milestone')
        .inverse_of(:jh_epics).with_foreign_key('milestone_id').with_primary_key('milestone_id')
    end

    it do
      is_expected.to belong_to(:jh_iteration).class_name('Extend::Iteration')
        .inverse_of(:jh_epics).with_foreign_key('sprint_id').with_primary_key('sprint_id')
    end
  end
end
