# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Extend::NamespaceSetting, feature_category: :database do
  describe 'associations' do
    describe 'model' do
      it { is_expected.to have_db_index(:namespace_id) }
    end

    it { is_expected.to belong_to(:namespace).class_name('::Namespace').inverse_of(:jh_namespace_setting) }
  end

  describe 'default values' do
    subject(:namespace_setting) { described_class.new }

    it { expect(namespace_setting.mr_custom_page_title).to eq('') }
    it { expect(namespace_setting.mr_custom_page_url).to eq('') }
  end

  describe 'mr_custom_page_url validation' do
    it 'disallow invalid urls' do
      non_ascii_url = 'http://gitlab.com/user/project1/wiki/something€'
      excessively_long_url = "https://gitla#{'b' * 2048}.com"

      is_expected.not_to allow_values(
        non_ascii_url,
        excessively_long_url
      ).for(:mr_custom_page_url)
    end

    it 'allow valid urls' do
      expected_url = 'https://example.com?mr=$MERGE_REQUEST_IID&project=$PROJECT_ID&user_id=$USER_ID&username=$USERNAME'
      external_url = 'http://runbook.gitlab.com/'
      internal_url = 'http://192.168.1.1'
      blank_url = ''
      nil_url = nil

      is_expected.to allow_value(
        expected_url,
        external_url,
        internal_url,
        blank_url,
        nil_url
      ).for(:mr_custom_page_url)
    end
  end
end
