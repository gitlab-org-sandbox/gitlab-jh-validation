# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ApplicationHelper do
  describe '#promo_host' do
    it 'returns the default promo host' do
      expect(helper.promo_host).to eq('about.gitlab.cn')
    end
  end

  describe '#page_class' do
    let(:current_user) { nil }

    before do
      allow(helper).to receive(:current_user) { current_user }
    end

    it 'in jihu environment' do
      expect(helper.page_class).to include('jh-page-wrapper')
    end
  end
end
