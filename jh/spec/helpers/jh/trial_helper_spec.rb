# frozen_string_literal: true

require 'spec_helper'

RSpec.describe JH::TrialHelper, feature_category: :purchase do
  let(:phone_without_area_code) { '15612341234' }
  let(:phone) { "+86#{phone_without_area_code}" }
  let(:encrypted_phone) { ::Gitlab::CryptoHelper.aes256_gcm_encrypt(phone) }
  let!(:user) { create(:user, phone: encrypted_phone) }

  before do
    allow(::Gitlab).to receive(:jh?).and_return(true)
    allow(::Gitlab).to receive(:com?).and_return(true)
  end

  describe '#create_lead_form_data' do
    before do
      allow(helper).to receive(:current_user).and_return(user)
    end

    it 'includes user phone' do
      expect(helper.create_lead_form_data).to include(phone_number: phone)
    end

    context 'when request contains a utm params' do
      let(:utm_params) do
        {
          utm_source: 'baidu',
          utm_medium: 'pinpaici-PC',
          utm_keyword: 'jihukaiyuan',
          step: 'lead'
        }
      end

      let(:extra_params) do
        {
          first_name: '_params_first_name_',
          last_name: '_params_last_name_',
          company_name: '_params_company_name_',
          company_size: '_company_size_',
          phone_number: '15612341234',
          country: '_country_',
          state: '_state_'
        }
      end

      let(:params) do
        ActionController::Parameters.new(extra_params.merge(utm_params))
      end

      before do
        allow(helper).to receive(:params).and_return(params)
      end

      it 'submit path includes utm params' do
        expect_path = trials_path(utm_params)
        expect(helper.create_lead_form_data[:submit_path]).to eq(expect_path)
      end
    end
  end
end
