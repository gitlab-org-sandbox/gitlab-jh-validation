# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequestsHelper, feature_category: :code_review_workflow do
  include Users::CalloutsHelper
  include ApplicationHelper
  include PageLayoutHelper
  include ProjectsHelper
  include ProjectForksHelper
  include IconsHelper
  include IssuablesHelper
  include MarkupHelper

  describe '#tab_link_for' do
    let(:merge_request) { create(:merge_request, :simple) }
    let(:options) { {} }

    subject { tab_link_for(merge_request, :custom_page, options) { 'Discussion' } }

    describe '"custom_page" works as expected' do
      let(:options) { { force_link: true } }

      it 'removes the data-toggle attributes' do
        is_expected.not_to match(/data-toggle="tabvue"/)
      end
    end

    describe '#sticky_header_data' do
      let(:group) { create(:group) }
      let(:project) { create(:project, group: group) }
      let(:imported_from) { :none }
      let(:user) { create(:user) }
      let(:merge_request) do
        create(:merge_request, source_project: project, target_project: project, imported_from: imported_from)
      end

      before do
        allow(helper).to receive(:current_user).and_return(user)
        merge_request.project.group.build_jh_namespace_setting(
          mr_custom_page_title: 'Custom title',
          mr_custom_page_url: 'https://example.com?mr_iid=$MERGE_REQUEST_IID&project_id=$PROJECT_ID&user_id=$USER_ID&username=$USERNAME'
        )
      end

      subject(:data) { helper.sticky_header_data(project, merge_request) }

      context 'when ff ff_mr_custom_page_tab' do
        before do
          allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(true)
        end

        it 'has sticky data for custom_page' do
          expect(data[:tabs].last).to include('custom_page')
        end
      end

      context 'when ff custom_mr_page disabled' do
        before do
          stub_feature_flags(ff_mr_custom_page_tab: false)
        end

        it 'does not have sticky data for custom_page' do
          expect(data[:tabs].last).not_to include('custom_page')
        end
      end
    end

    describe '#custom_page_configured?' do
      let(:project) { create(:project) }

      subject(:configured) { helper.custom_page_configured?(project) }

      context 'when ff enabled' do
        context 'when database is not configured' do
          before do
            allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
          end

          it 'returns false' do
            expect(configured).to be_falsey
          end
        end

        context 'when database is configured' do
          before do
            allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(true)
          end

          it 'returns true' do
            expect(configured).to be_truthy
          end
        end
      end

      context 'when ff disabled' do
        before do
          stub_feature_flags(ff_mr_custom_page_tab: false)
        end

        context 'when database is configured' do
          before do
            allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(true)
          end

          it 'returns false' do
            expect(configured).to be_falsey
          end
        end

        context 'when database is not configured' do
          before do
            allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
          end

          it 'returns false' do
            expect(configured).to be_falsey
          end
        end
      end
    end

    describe '#mr_custom_page_url' do
      let(:group) { create(:group) }
      let(:project) { create(:project, group: group) }
      let(:imported_from) { :none }
      let(:user) { create(:user) }
      let(:merge_request) do
        create(:merge_request, source_project: project, target_project: project, imported_from: imported_from)
      end

      subject(:url) { helper.mr_custom_page_url(merge_request) }

      context 'when url is configured' do
        before do
          allow(helper).to receive(:current_user).and_return(user)
          merge_request.project.group.build_jh_namespace_setting(
            mr_custom_page_title: 'Custom title',
            mr_custom_page_url: 'https://example.com?mr_iid=$MERGE_REQUEST_IID&project_id=$PROJECT_ID&user_id=$USER_ID&username=$USERNAME'
          )
        end

        it 'returns formatted url' do
          expect(url).to eq("https://example.com?mr_iid=#{merge_request.iid}&project_id=#{project.id}&user_id=#{user.id}&username=#{user.username}")
        end
      end

      context 'when url is not configured' do
        it 'returns nil' do
          expect(url).to be_nil
        end
      end
    end
  end
end
