# frozen_string_literal: true

require "spec_helper"

RSpec.describe Billing::PlanComponent, :aggregate_failures, type: :component do
  include SubscriptionPortalHelpers

  let(:namespace) { build(:group) }
  let(:plans_data) { billing_plans_data.map { |plan| Hashie::Mash.new(plan) } }
  let(:plan) { plans_data.detect { |x| x.code == plan_name } }
  let(:current_plan_code) { nil }
  let(:current_plan) { Hashie::Mash.new({ code: current_plan_code }) }

  subject(:component) { described_class.new(plan: plan, namespace: namespace, current_plan: current_plan) }

  before do
    allow(component).to receive(:plan_purchase_url).and_return('_purchase_url_')

    render_inline(component)
  end

  shared_examples 'plan tracking' do
    it 'has expected tracking attributes' do
      css = "[data-track-action='click_button'][data-track-label='plan_cta'][data-track-property='#{plan_name}']" \
        "[data-track-experiment='promote_premium_billing_page']"

      expect(page).to have_css(css)
    end
  end

  context 'with free plan' do
    let(:plan_name) { 'free' }

    it 'has pricing info' do
      expect(page).to have_content('¥ 0')
      expect(page).not_to have_content('Billed annually')
    end

    it 'has expected feature titles' do
      expected_titles = ["Bring your own JiHu GitLab CI runners", "5 users per top-level private group",
        "400 CI/CD minutes per month", "5GB storage per project"]
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end

    it 'has expected annual_price_text' do
      expected_annual_price_text = "Billed annually at #{component.send(:price_per_year)}"
      expect(component.send(:annual_price_text)).to eq(expected_annual_price_text)
    end

    context 'with trial as current plan' do
      let(:current_plan_code) { ::Plan::PREMIUM_TRIAL }

      it 'does not have header for the current plan' do
        expect(page).not_to have_content('Your current plan')
        expect(page).not_to have_selector('.gl-bg-gray-100')
      end
    end
  end

  context 'with premium plan' do
    let(:plan_name) { 'premium' }

    it 'has expected feature titles' do
      expected_titles = ["Everything from Basic", "Efficient code review", "Advanced CI/CD",
        "Enterprise agile management", "Enterprise User and Incident Management",
        "10,000 CI/CD minutes per month",
        "Professional technical support", "5GB storage per project"]
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end

    context 'with trial as current plan' do
      let(:current_plan_code) { ::Plan::PREMIUM_TRIAL }

      it 'does not have header for the current plan' do
        expect(page).not_to have_content('Recommended')
        expect(page).not_to have_selector('.header-recommended')
      end

      it 'has outline secondary button as cta' do
        expect(page).to have_selector('.btn-confirm-secondary')
      end
    end
  end

  context 'with ultimate plan' do
    let(:plan_name) { 'ultimate' }

    it 'has expected feature titles' do
      expected_titles = ["Everything from Premium", "Advanced security testing",
        "Dynamic Application Security Testing", "Security Dashboards",
        "Portfolio Management", "Value stream analysis", "Free guest users",
        "50,000 CI/CD minutes per month", "Professional technical support",
        "5GB storage per project"]
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end

    context 'with trial as current plan' do
      let(:current_plan_code) { ::Plan::PREMIUM_TRIAL }

      it 'has primary button as cta' do
        expect(page).to have_selector('.btn-confirm')
        expect(page).not_to have_selector('.btn-confirm-secondary')
      end
    end
  end

  def expect_to_plan_feature_titles(plan, expected_titles)
    titles = plan['features'].map { |feature| feature.fetch('title', '') }
    expect(titles).to eq(expected_titles)
  end
end
