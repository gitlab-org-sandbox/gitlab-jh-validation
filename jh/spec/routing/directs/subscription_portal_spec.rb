# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Custom URLs', 'Subscription Portal', feature_category: :subscription_management do
  include SubscriptionPortalHelper

  let(:env_value) { nil }

  describe 'subscriptions_comparison_url' do
    subject { subscriptions_comparison_url }

    it { is_expected.to eq("https://gitlab.cn/pricing/?type=saas#features-comparison") }
  end
end
