# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CountriesController do
  describe 'GET #index' do
    it 'does include JH Market and other countries but no denied countries' do
      get :index

      resultant_countries = json_response.map { |row| row[1] }

      expect(resultant_countries).not_to include(*World::COUNTRY_DENYLIST)
      expect(resultant_countries).to include(*World::JH_MARKET)
    end
  end
end
