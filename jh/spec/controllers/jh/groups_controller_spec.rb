# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GroupsController do
  let(:user) { create(:user) }
  let(:group) { create_default(:group) }

  describe 'PUT update' do
    before do
      group.add_owner(user)
      sign_in(user)
    end

    it 'filters out invalid params' do
      expect(::Groups::UpdateService).to receive(:new).with(
        group, user, ActionController::Parameters.new(
          mr_custom_page_title: 'test-title',
          mr_custom_page_url: 'https://example.com'
        ).permit!
      ).and_call_original

      post :update, params: { id: group.to_param, group: {
        mr_custom_page_title: 'test-title',
        mr_custom_page_url: 'https://example.com',
        invalid_param: 'invalid'
      } }
    end
  end
end
