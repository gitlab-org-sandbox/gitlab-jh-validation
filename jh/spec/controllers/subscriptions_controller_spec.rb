# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SubscriptionsController do
  let_it_be(:user) { create(:user) }

  describe 'GET #new' do
    context 'when the user is authenticated' do
      before do
        sign_in(user)
      end

      context 'when the user already has a customers dot account' do
        before do
          allow(Gitlab::SubscriptionPortal::Client)
            .to receive(:get_billing_account_details)
            .with(user)
            .and_return({
              success: true,
              billing_account_details: { "billingAccount" => { "zuoraAccountName" => "sample-account" } }
            })
        end

        context 'when URL has no plan_id param' do
          before do
            get :new
          end

          it { is_expected.to redirect_to "https://about.gitlab.cn/pricing" }
        end
      end
    end
  end

  describe 'POST #create', :snowplow do
    subject(:create_request) do
      post :create,
        params: params,
        as: :json
    end

    let(:params) do
      {
        setup_for_company: setup_for_company,
        customer: { company: 'My company', country: 'NL' },
        subscription: { plan_id: 'x', quantity: 2, source: 'some_source' },
        idempotency_key: idempotency_key
      }
    end

    let(:idempotency_key) { 'idempotency-key' }

    let(:setup_for_company) { true }

    context 'with authorized user' do
      let_it_be(:pay_url) { "http://gdk.test:5000/invoices/uuid/payments/new" }
      let_it_be(:service_response) do
        {
          success: true,
          data: {
            success: true,
            invoice_data: {
              invoice: {
                pay_url: pay_url
              }
            }
          }
        }
      end

      let_it_be(:group) { create(:group) }

      before do
        sign_in(user)
        allow_any_instance_of(GitlabSubscriptions::CreateService).to receive(:execute).and_return(service_response)
        allow_any_instance_of(EE::Groups::CreateService).to receive(:execute).and_return(
          ServiceResponse.success(payload: { group: group }))
      end

      context 'on successful creation of a subscription' do
        it 'returns the group edit location in JSON format' do
          allow(Gitlab.config.gitlab).to receive(:url).and_return(::Gitlab::Saas.staging_com_url)
          create_request

          callback_url = ::Gitlab::Utils.append_path(::Gitlab::Saas.staging_com_url,
            "/-/subscriptions/groups/#{group.path}/edit")
          callback_url = "#{callback_url}?plan_id=x&quantity=2"
          location = "#{pay_url}?redirect_after_success=#{CGI.escape(callback_url)}"
          expect(response.parsed_body).to eq({ "location" => location })
        end
      end
    end
  end
end
