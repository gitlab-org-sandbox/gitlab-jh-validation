# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabSubscriptions::Trials::StatusWidgetPresenter, :saas, feature_category: :acquisition do
  using RSpec::Parameterized::TableSyntax

  describe '#attributes' do
    context 'when has different plan' do
      let(:group) do
        build(:group) do |g|
          build(:gitlab_subscription, :active_trial, :ultimate_trial, namespace: g, trial_starts_on: Time.current,
            trial_ends_on: 30.days.from_now)
        end
      end

      where(:plan_name, :expected_plan_title) do
        :free | s_('JH|License|Free')
        :premium | s_('JH|License|Premium')
        :ultimate | s_('JH|License|Ultimate')
        :starter | s_('JH|License|Starter')
        :premium_trial | s_('JH|License|Premium Trial')
        :ultimate_trial | s_('JH|License|Ultimate Trial')
      end

      with_them do
        before do
          allow_any_instance_of(GitlabSubscription).to receive(:plan_name).and_return(plan_name)
        end

        it 'returns the translated plan name' do
          result = described_class.new(group).attributes[:trial_status_widget_data_attrs][:plan_name]
          expect(result).to eq(expected_plan_title)
        end
      end
    end
  end
end
