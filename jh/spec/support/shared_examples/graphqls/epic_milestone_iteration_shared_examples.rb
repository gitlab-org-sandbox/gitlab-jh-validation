# frozen_string_literal: true

RSpec.shared_examples 'unavailable process the epic JH special associations in the invalid cases' do
  shared_examples 'epic JH extend associations are not persisted' do
    it 'does not has the associations of milestone and iteration' do
      epic = result[:epic]
      expect(epic[:title]).to eq(title)
      expect(epic.jh_epic).not_to be_present
      expect(epic.jh_epic&.jh_milestone).not_to be_present
      expect(epic.jh_epic&.jh_iteration).not_to be_present
    end
  end

  describe 'with epic' do
    context 'when milestone or iteration ids is invalid' do
      let(:args) { default_args.merge(milestone_id: non_existing_record_id, sprint_id: non_existing_record_id) }

      it 'raises a not accessible error' do
        expect { result }.to raise_error(Gitlab::Graphql::Errors::ResourceNotAvailable)
      end
    end

    context 'when milestone is not a group milestone' do
      let(:tmp_milestone) { create(:milestone, group: nil) }
      let(:args) { default_args.merge(milestone_id: tmp_milestone.id) }

      it 'raises a not accessible error' do
        expect { result }.to raise_error(Gitlab::Graphql::Errors::ResourceNotAvailable)
      end
    end

    context 'when milestone or iteration not in params' do
      let(:args) { default_args }

      it_behaves_like 'epic JH extend associations are not persisted'
    end

    context 'when milestone or iteration ids is valid' do
      let(:args) { default_args.merge(milestone_id: milestone.id, sprint_id: iteration.id) }

      context 'with SAAS env', :saas do
        it_behaves_like 'epic JH extend associations are not persisted'
      end

      context 'with feature flag disabled' do
        before do
          stub_feature_flags(epic_support_milestone_and_iteration: false)
        end

        it_behaves_like 'epic JH extend associations are not persisted'
      end

      context 'with license feature disabled' do
        before do
          stub_licensed_features(epics: true, milestone_and_iteration_in_epic: false)
        end

        it_behaves_like 'epic JH extend associations are not persisted'
      end

      context 'with jh database not configured' do
        before do
          allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
        end

        it_behaves_like 'epic JH extend associations are not persisted'
      end
    end
  end
end
