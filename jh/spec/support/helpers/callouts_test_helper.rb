# frozen_string_literal: true

module JH
  module CalloutsTestHelper
    def callouts_trials_link_path
      '/-/trial_registrations/new?glm_content=gold-callout&glm_source=jihulab.com'
    end
  end
end
