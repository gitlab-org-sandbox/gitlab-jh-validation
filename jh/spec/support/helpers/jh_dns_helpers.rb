# frozen_string_literal: true

require Rails.root.join("spec/support/helpers/dns_helpers")

module JHDnsHelpers
  # not sure why upstream permit_redis! failed, will ask about upstream
  def permit_jh_redis!
    allow(Addrinfo).to receive(:getaddrinfo).with(anything, anything, nil, :STREAM, anything, anything, any_args)
                                            .and_call_original
  end

  def permit_upstream!
    block_dns!
  end
end
