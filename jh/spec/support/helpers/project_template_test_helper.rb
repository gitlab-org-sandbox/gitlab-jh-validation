# frozen_string_literal: true

module JH
  module ProjectTemplateTestHelper
    def all_templates
      super + ['dongtai_iast']
    end
  end
end
