# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Registration group and project creation flow', :js, :saas do
  include SaasRegistrationHelpers

  let_it_be(:user) { create(:user, onboarding_in_progress: true) }

  before do
    stub_application_setting(import_sources: %w[gitlab_project])
    # https://gitlab.com/gitlab-org/gitlab/-/issues/340302
    allow(Gitlab::QueryLimiting::Transaction).to receive(:threshold).and_return(158)

    stub_saas_features(onboarding: true)
    sign_in(user)
    visit users_sign_up_welcome_path

    expect(page).to have_content('Welcome to GitLab') # rubocop:disable RSpec/ExpectInHook -- need verify content

    choose 'Just me'
    choose 'Create a new project'
    click_on 'Continue'
  end

  it 'A user can create a group and project' do
    page.within '[data-testid="url-group-path"]' do
      expect(page).to have_content('{group}')
    end

    page.within '[data-testid="url-project-path"]' do
      expect(page).to have_content('{project}')
    end

    fill_in 'group_name', with: 'test group'

    fill_in 'blank_project_name', with: 'test project'

    page.within '[data-testid="url-group-path"]' do
      expect(page).to have_content('test-group')
    end

    page.within '[data-testid="url-project-path"]' do
      expect(page).to have_content('test-project')
    end

    click_on 'Create project'

    expect_to_be_in_learn_gitlab
  end
end
