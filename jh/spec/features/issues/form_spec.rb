# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'New/edit issue', :js, feature_category: :team_planning do
  include ActionView::Helpers::JavaScriptHelper
  include ListboxHelpers

  let_it_be(:project)   { create(:project, :repository) }
  let_it_be(:user)      { create(:user) }
  let_it_be(:user2)     { create(:user) }
  let_it_be(:guest)     { create(:user) }
  let_it_be(:milestone) { create(:milestone, project: project) }
  let_it_be(:label)     { create(:label, project: project) }
  let_it_be(:label2)    { create(:label, project: project) }
  let_it_be(:scoped_label) { create(:label, name: 'priority::value', project: project) }
  let_it_be(:scoped_label2) { create(:label, name: 'priority::value2', project: project) }
  let_it_be(:issue)     { create(:issue, project: project, assignees: [user], milestone: milestone) }
  let_it_be(:issue2)    { create(:issue, project: project, assignees: [user], milestone: milestone) }
  let_it_be(:confidential_issue) do
    create(:issue, project: project, assignees: [user], milestone: milestone, confidential: true)
  end

  let_it_be(:options) do
    {
      "issue" => [{
        "scope" => "priority",
        "required" => true
      }],
      "epic" => [{
        "scope" => "sub_type",
        "required" => true
      }]
    }
  end

  let(:current_user) { user }

  before_all do
    project.add_maintainer(user)
    project.add_maintainer(user2)
    project.add_guest(guest)
  end

  before do
    stub_feature_flags(visible_label_selection_on_metadata: false)
    stub_licensed_features(multiple_issue_assignees: false, issue_weights: false)

    sign_in(current_user)
  end

  describe 'new issue' do
    before do
      stub_feature_flags(jh_custom_labels: true)
      stub_jh_application_setting(jh_custom_labels: options)
      visit new_project_issue_path(project)
    end

    it 'allows user to create new issue' do
      fill_in 'Title (required)', with: 'title'
      fill_in 'Description', with: 'title'

      expect(find('a', text: 'Assign to me')).to be_visible
      click_button 'Unassigned'

      wait_for_requests

      page.within '.dropdown-menu-user' do
        click_link user2.name
      end
      expect(find('input[name="issue[assignee_ids][]"]', visible: false).value).to match(user2.id.to_s)
      page.within '.js-assignee-search' do
        expect(page).to have_content user2.name
      end
      expect(find('a', text: 'Assign to me')).to be_visible

      click_link 'Assign to me'
      assignee_ids = page.all('input[name="issue[assignee_ids][]"]', visible: false)

      expect(assignee_ids[0].value).to match(user.id.to_s)

      page.within '.js-assignee-search' do
        expect(page).to have_content user.name
      end
      expect(find('a', text: 'Assign to me', visible: false)).not_to be_visible

      click_button 'Select milestone'
      click_button milestone.title
      expect(find('input[name="issue[milestone_id]"]', visible: false).value).to match(milestone.id.to_s)
      expect(page).to have_button milestone.title

      click_button _('Select label')
      wait_for_all_requests
      within_testid('sidebar-labels') do
        click_button label.title
        click_button label2.title
        click_button _('Close')
        wait_for_requests
      end

      click_button _('Select priority')

      within_testid('new-fields-col') do
        find_all('.gl-new-dropdown-item').first.click
        wait_for_all_requests
      end

      click_button 'Create issue'

      page.within '.issuable-sidebar' do
        page.within '.assignee' do
          expect(page).to have_content "Assignee"
        end

        page.within '.milestone' do
          expect(page).to have_text milestone.title
        end

        page.within '.labels' do
          expect(page).to have_content label.title
          expect(page).to have_content label2.title
        end
      end

      within_testid 'breadcrumb-links' do
        issue = Issue.find_by(title: 'title')

        expect(page).to have_text("Issues #{issue.to_reference}")
      end
    end

    it 'displays an error message when submitting an invalid form' do
      click_button 'Create issue'

      page.within('[data-testid="new-fields-col"]') do
        expect(page).to have_text(_('This field is required'))
      end
    end
  end
end
