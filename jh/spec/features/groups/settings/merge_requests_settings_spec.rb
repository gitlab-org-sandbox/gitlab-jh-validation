# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Groups > Settings > General > Merge requests', feature_category: :groups_and_projects do
  let_it_be(:user) { create(:user) }
  let_it_be(:group) { create(:group) }

  before_all do
    group.add_owner(user)
  end

  before do
    stub_licensed_features(group_level_merge_checks_setting: true)
    sign_in(user)
    visit(edit_group_path(group, anchor: 'js-merge-requests-settings'))
  end

  context 'with custom page settings', :js do
    it 'shows custom page settings' do
      page.within '.mr-custom-page-settings' do
        expect(page).to have_content 'Custom page title'
        expect(page).to have_content 'Custom page URL'
      end
    end

    it 'sets custom page settings' do
      page.within '.mr-custom-page-settings' do
        fill_in 'group[mr_custom_page_title]', with: 'Custom page title'
        fill_in 'group[mr_custom_page_url]', with: 'https://example.com'
        click_on('Save changes')
      end

      wait_for_requests

      page.within '.mr-custom-page-settings' do
        expect(page).to have_field('group[mr_custom_page_title]', with: 'Custom page title')
        expect(page).to have_field('group[mr_custom_page_url]', with: 'https://example.com')
      end
    end

    context 'when FF is disabled' do
      before do
        stub_feature_flags(ff_mr_custom_page_tab: false)
        page.refresh
      end

      it 'does not show custom page settings' do
        expect(page).not_to have_content 'Custom page title'
        expect(page).not_to have_content 'Custom page URL'
      end
    end

    context 'when jh database is not set' do
      before do
        allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
        page.refresh
      end

      it 'does not show custom page settings' do
        expect(page).not_to have_content 'Custom page title'
        expect(page).not_to have_content 'Custom page URL'
      end
    end
  end

  context 'with MR checks settings', :js do
    it 'sets MR checks settings' do
      page.within '.merge-request-settings-form' do
        check 'Pipelines must succeed'
        click_on('Save changes')
      end

      wait_for_requests

      page.within '.merge-request-settings-form' do
        expect(page).to have_checked_field('Pipelines must succeed')
      end
    end
  end
end
