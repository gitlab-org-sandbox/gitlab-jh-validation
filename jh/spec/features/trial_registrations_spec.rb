# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Trial registrations on EE', :js do
  let(:new_user) { build_stubbed(:user) }

  before do
    allow(Gitlab).to receive(:com?).and_return(true)
    stub_feature_flags(arkose_labs_signup_challenge: false)
  end

  def fill_in_trial_registration_form
    fill_in 'new_user_username', with: new_user.username
    fill_in 'new_user_email', with: new_user.email
    fill_in 'new_user_first_name', with: new_user.first_name
    fill_in 'new_user_last_name', with: new_user.last_name
    fill_in 'new_user_password', with: new_user.password
  end

  describe 'password complexity', :js do
    let(:path_to_visit) { new_trial_registration_path }
    let(:password_input_selector) { :new_user_password }
    let(:submit_button_selector) { _('Continue') }

    it_behaves_like 'password complexity validations'

    context 'when all password complexity rules are enabled' do
      include_context 'with all password complexity rules enabled'

      context 'when all rules are matched' do
        let(:password) { '12345aA.' }

        it 'creates the user' do
          visit path_to_visit

          fill_in_trial_registration_form
          fill_in password_input_selector, with: password

          expect { click_button submit_button_selector }.to change { User.count }.by(1)
          expect(page).to have_current_path new_user_session_path, ignore_query: true
        end
      end
    end
  end
end
