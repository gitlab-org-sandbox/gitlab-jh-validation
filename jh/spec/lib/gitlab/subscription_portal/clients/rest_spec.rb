# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::SubscriptionPortal::Clients::Rest, feature_category: :subscription_management do
  let(:client) { Gitlab::SubscriptionPortal::Client }
  let(:message) { nil }
  let(:http_method) { :post }
  let(:response) { nil }
  let(:parsed_response) { nil }
  let(:gitlab_http_response) do
    instance_double(
      HTTParty::Response,
      code: response.code,
      response: response,
      body: {},
      parsed_response: parsed_response
    )
  end

  let(:headers) do
    {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'X-Admin-Email' => 'gl_com_api@gitlab.com',
      'X-Admin-Token' => 'customer_admin_token'
    }
  end

  shared_examples 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header' do
    let(:response) { Net::HTTPSuccess.new(1.0, '201', 'OK') }

    it 'sends the default User-Agent' do
      headers['User-Agent'] = "GitLab/#{Gitlab::VERSION}"

      expect(Gitlab::HTTP).to receive(http_method).with(anything,
        hash_including(headers: headers)).and_return(gitlab_http_response)

      subject
    end
  end

  describe '#generate_trial' do
    subject do
      client.generate_trial({})
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'

    it "nests in the trial_user param if needed" do
      expect(client).to receive(:http_post).with('trials', anything, { trial_user: { foo: 'bar' } })

      client.generate_trial(foo: 'bar')
    end
  end

  describe '#generate_addon_trial' do
    subject do
      client.generate_addon_trial({})
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'

    it "nests in the trial_user param if needed" do
      expect(client).to receive(:http_post).with('trials/create_addon', anything, { trial_user: { foo: 'bar' } })

      client.generate_addon_trial(foo: 'bar')
    end
  end

  describe '#generate_lead' do
    subject do
      client.generate_lead({})
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#generate_iterable' do
    subject do
      client.generate_iterable({})
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#create_subscription' do
    let(:headers) do
      {
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'X-Customer-Email' => 'customer@example.com',
        'X-Customer-Token' => 'token'
      }
    end

    subject do
      client.create_subscription({}, 'customer@example.com', 'token')
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#create_customer' do
    subject do
      client.create_customer({})
    end

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#payment_form_params' do
    subject do
      client.payment_form_params('cc', 123)
    end

    let(:http_method) { :get }

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#payment_method' do
    subject do
      client.payment_method('1')
    end

    let(:http_method) { :get }

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#validate_payment_method' do
    subject do
      client.validate_payment_method('test_payment_method_id', {})
    end

    let(:http_method) { :post }

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end

  describe '#customers_oauth_app_uid' do
    subject do
      client.customers_oauth_app_uid
    end

    let(:http_method) { :get }

    it_behaves_like 'a request that sends the GITLAB_QA_USER_AGENT value in the "User-Agent" header'
  end
end
