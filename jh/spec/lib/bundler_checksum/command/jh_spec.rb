# frozen_string_literal: true

require 'spec_helper'
require Rails.root.join('jh/vendor/gems/bundler-checksum/lib/bundler_checksum/command/jh.rb')

RSpec.describe BundlerChecksum::Command::Jh, :silence_stdout do
  let_it_be(:upstream_lockfile) do
    <<~FILE
      GEM
        remote: https://rubygems.org/
        specs:
          connection_pool (2.4.0)
          redis (5.0.6)
            redis-client (>= 0.9.0)
          redis-client (0.14.1)
            connection_pool

      PLATFORMS
        ruby

      DEPENDENCIES
        redis

      BUNDLED WITH
         2.4.12

    FILE
  end

  let_it_be(:jh_lockfile) do
    <<~FILE
      GIT
        remote: https://github.com/TencentCloud/tencentcloud-sdk-ruby.git
        revision: a02d46963a57b0d48ef112e3fba208e911de9be9
        tag: 1.0.200
        glob: tencentcloud-sdk-common/tencentcloud-sdk-common.gemspec
        specs:
          tencentcloud-sdk-common (1.0.200)

      GEM
        remote: https://rubygems.org/
        specs:
          chinese_pinyin (1.1.0)
          connection_pool (2.4.0)
          redis (5.0.6)
            redis-client (>= 0.9.0)
          redis-client (0.14.1)
            connection_pool

      PLATFORMS
        ruby

      DEPENDENCIES
        chinese_pinyin
        redis
        tencentcloud-sdk-common!

      BUNDLED WITH
         2.4.12

    FILE
  end

  let_it_be(:gem_response) do
    <<~FILE
      [
          {
              "authors": "Richard Huang, Hong, Liang",
              "built_at": "2021-04-18T00:00:00.000Z",
              "created_at": "2021-04-18T23:31:01.708Z",
              "description": "translate chinese hanzi to pinyin.",
              "downloads_count": 527363,
              "metadata": {},
              "number": "1.1.0",
              "summary": "translate chinese hanzi to pinyin.",
              "platform": "ruby",
              "rubygems_version": ">= 1.3.6",
              "ruby_version": ">= 0",
              "prerelease": false,
              "licenses": [
                  "MIT"
              ],
              "requirements": [],
              "sha": "129d5673891c5e6f1647fe6367eb1a9e7f11969d5eab3584615514e23b286437"
          }
      ]
    FILE
  end

  let_it_be(:jh_checksum) do
    <<~FILE
      [
      {"name":"chinese_pinyin","version":"1.1.0","platform":"ruby","checksum":"129d5673891c5e6f1647fe6367eb1a9e7f11969d5eab3584615514e23b286437"}
      ]
    FILE
  end

  before do
    allow(described_class).to receive(:jh_lockfile).and_return(Bundler::LockfileParser.new(jh_lockfile))
    allow(described_class).to receive(:upstream_lockfile).and_return(Bundler::LockfileParser.new(upstream_lockfile))

    stub_request(:get, "https://rubygems.org/api/v1/versions/chinese_pinyin.json")
      .to_return(status: 200, body: gem_response)
  end

  describe '.checksums' do
    subject { described_class.checksums }

    it 'generates jh checksums' do
      expect(subject).to match Gitlab::Json.parse(jh_checksum, symbolize_names: true)
    end

    context 'with invalid Gem' do
      let(:gem_response) { 'Not found this RubyGem' }

      it 'raises error when not found on Rubygems' do
        expect { subject }.to raise_error 'chinese_pinyin 1.1.0 not found on Rubygems!'
      end
    end
  end

  describe '.execute' do
    it 'writes json to jh/Gemfile.checksum.jh' do
      expect(File).to receive(:write).with(Rails.root.join('jh/Gemfile.checksum.jh'), jh_checksum)
      described_class.execute
    end
  end
end
