# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Gitlab::GonHelper', type: :controller do
  controller(ActionController::Base) do
    include Gitlab::GonHelper
    before_action :add_gon_variables

    def gon_all_variables
      render json: Gon.all_variables
    end
  end

  let(:user) { create(:user) }

  before do
    routes.draw { get "all_variables" => "anonymous#gon_all_variables" }
    Gon.clear
    sign_in user

    get :gon_all_variables
  end

  describe '#add_gon_variables', :saas do
    it 'contains JH expected keys' do
      expect(json_response).to have_key 'posthog_api_key'
      expect(json_response).to have_key 'jh_custom_labels_enabled'
      expect(json_response).to have_key 'jh_custom_labels'
    end
  end
end
