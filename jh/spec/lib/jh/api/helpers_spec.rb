# frozen_string_literal: true

require 'spec_helper'

RSpec.describe JH::API::Helpers, feature_category: :api do
  include Rack::Test::Methods

  let_it_be(:user) { build(:user) }
  let_it_be(:project) { create(:project) }

  let(:helper) do
    Class.new do
      include API::Helpers
      include API::APIGuard::HelperMethods
      include JH::API::Helpers
    end
  end

  subject { helper.new }

  describe '#require_pages_enabled!' do
    before do
      subject.instance_variable_set(:@initial_current_user, user)

      allow(subject).to receive(:user_project).and_return(project)
      allow(project).to receive(:pages_available?).and_return(pages_enabled)
    end

    context 'when pages are enabled' do
      let(:pages_enabled) { true }

      it 'does not return not found' do
        expect(subject).not_to receive(:not_found!)

        subject.require_pages_enabled!
      end
    end

    context 'when pages are not enabled' do
      let(:pages_enabled) { false }

      it 'returns not found' do
        expect(subject).to receive(:not_found!)

        subject.require_pages_enabled!
      end
    end
  end
end
