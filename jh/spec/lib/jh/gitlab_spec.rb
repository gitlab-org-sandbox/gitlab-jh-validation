# frozen_string_literal: true

require 'spec_helper'

RSpec.describe JH::Gitlab, feature_category: :environment_management do
  describe '.hk?' do
    shared_examples 'is Hong Kong environment' do
      it 'hk? return true' do
        expect(described_class.hk?).to eq true
      end
    end

    shared_examples 'is not Hong Kong environment' do
      it 'hk? return false' do
        expect(described_class.hk?).to eq false
      end
    end

    context 'when SaaS region is HK' do
      before do
        stub_env('SAAS_REGION', 'HK')
      end

      it_behaves_like 'is Hong Kong environment'
    end

    context 'when SaaS region is not HK' do
      before do
        stub_env('SAAS_REGION', 'SOME_OTHER_REGION')
      end

      it_behaves_like 'is not Hong Kong environment'
    end

    context 'when has not envronment variable SAAS_REGION' do
      it_behaves_like 'is not Hong Kong environment'
    end
  end
end
