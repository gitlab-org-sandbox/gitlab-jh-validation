# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequestSerializer do
  let_it_be(:group) { create(:group) }
  let_it_be(:user) { create(:user) }
  let_it_be(:project) { create(:project, :public, group: group) }
  let_it_be(:resource) { create :merge_request, source_project: project, author: user }

  let(:json_entity) do
    described_class.new(current_user: user)
      .represent(resource, serializer: serializer)
      .with_indifferent_access
  end

  context 'when serializer is "monorepo"' do
    let(:serializer) { 'monorepo' }

    before do
      allow(::MergeRequests::MonorepoService).to receive(:monorepo_enabled?).and_return(true)
    end

    it 'matches json schema' do
      expect(json_entity.to_json).to match_schema(
        Rails.root.join('jh/spec/fixtures/api/schemas/entities/merge_request_monorepo.json')
      )
    end
  end

  context 'when serializer is "default"' do
    let(:serializer) { 'default' }

    context 'when MR has set custom page' do
      before do
        resource.project.group.build_jh_namespace_setting(
          mr_custom_page_title: 'Custom title',
          mr_custom_page_url: 'https://example.com?mr_iid=$MERGE_REQUEST_IID&project_id=$PROJECT_ID&user_id=$USER_ID&username=$USERNAME'
        )
      end

      it 'exposes custom page attributes' do
        expect(json_entity[:mr_custom_page_title]).to eq('Custom title')
        expect(json_entity[:mr_custom_page_url]).to eq("https://example.com?mr_iid=#{resource.iid}&project_id=#{project.id}&user_id=#{user.id}&username=#{user.username}")
      end
    end

    context 'when FF is disabled' do
      before do
        stub_feature_flags(ff_mr_custom_page_tab: false)
      end

      it 'does not expose custom page attributes' do
        expect(json_entity).not_to include(:mr_custom_page_title, :mr_custom_page_url)
      end
    end

    context 'when jh database is not set' do
      before do
        allow(Gitlab::Database).to receive(:jh_database_configured?).and_return(false)
      end

      it 'does not expose custom page attributes' do
        expect(json_entity).not_to include(:mr_custom_page_title, :mr_custom_page_url)
      end
    end
  end
end
