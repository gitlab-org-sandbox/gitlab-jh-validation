# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ApplicationController, type: :request do
  shared_examples 'redirections' do |path_symbol|
    let(:path) { send(path_symbol) }

    subject(:sign_in) do
      post user_session_path(user: { login: user.username, password: user.password })
    end

    context 'when SaaS', :saas, :aggregate_failures do
      it 'redirects to 401 and redirects back after login', :saas do
        get path
        expect(response).to have_gitlab_http_status(:unauthorized)
        expect(request.session[:user_return_to]).to eq path

        sign_in
        expect(response).to redirect_to path
      end
    end

    context 'when Self-managed' do
      it 'redirects to the sign-in page' do
        get path

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  context 'with redirection about 401 unauthorized' do
    let_it_be(:user) { create(:user) }
    let(:private_project) { create(:project, :private) }
    let(:private_project_path) { project_path(private_project) }
    let(:non_existing_path) { '/non-existing-path' }

    it_behaves_like 'redirections', :private_project_path
    it_behaves_like 'redirections', :non_existing_path
  end
end
