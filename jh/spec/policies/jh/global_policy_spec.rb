# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GlobalPolicy do
  let(:current_user) { create(:user) }
  let(:user) { create(:user) }

  subject { described_class.new(current_user, [user]) }

  describe 'phone verification' do
    shared_examples 'checking access for phone verification' do |ability|
      it { is_expected.to be_allowed(ability) }

      context 'when it is JH SaaS', :saas, :phone_verification_code_enabled do
        it { is_expected.not_to be_allowed(ability) }

        context 'when phone verified' do
          before do
            current_user.update!(phone: 'phone')
          end

          it { is_expected.to be_allowed(ability) }
        end

        context 'when phone is skip_real_name_verification' do
          before do
            allow(current_user).to receive(:skip_real_name_verification?).and_return(true)
          end

          it { is_expected.to be_allowed(ability) }
        end
      end
    end

    shared_examples 'checking access with different roles and abilities for phone verification' do |ability|
      context 'with regular user' do
        it_behaves_like 'checking access for phone verification', ability
      end

      context 'with admin' do
        let(:current_user) { create(:admin) }

        it_behaves_like 'checking access for phone verification', ability
      end

      context 'with anonymous' do
        let(:current_user) { nil }

        it { is_expected.to be_allowed(ability) }

        context 'when it is JH SaaS', :saas, :phone_verification_code_enabled do
          it { is_expected.to be_allowed(ability) }
        end
      end
    end

    describe 'api access' do
      it_behaves_like 'checking access with different roles and abilities for phone verification', :access_api
    end

    describe 'git access' do
      it_behaves_like 'checking access with different roles and abilities for phone verification', :access_git
    end
  end
end
