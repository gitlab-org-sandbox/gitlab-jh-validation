export const mockEndpoint = 'contributors';
export const mockBranch = 'main';
export const mockChartData = [
  {
    author_name: 'John',
    author_email: 'jawnnypoo@gmail.com',
    date: '2019-05-05',
    additions: 100,
    deletions: 300,
    effective_additions: 100,
    effective_deletions: 200,
  },
  {
    author_name: 'John',
    author_email: 'jawnnypoo@gmail.com',
    date: '2019-03-03',
    additions: 2000,
    deletions: 500,
    effective_additions: 200,
    effective_deletions: 100,
  },
];
