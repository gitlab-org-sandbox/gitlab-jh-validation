const API_HOST = 'https://posthog1.gitlab.cn';

export const posthogMockData = Object.freeze({
  posthog_api_key: 'test-key',
  posthog_api_host: API_HOST,
});
