import Vue from 'vue';
import VueApollo from 'vue-apollo';
import waitForPromises from 'helpers/wait_for_promises';
import createMockApollo from 'helpers/mock_apollo_helper';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import { createAlert } from '~/alert';
import { stubComponent } from 'helpers/stub_component';
import SidebarEditableItem from '~/sidebar/components/sidebar_editable_item.vue';
import SidebarMilestoneDropdown from 'jh/epic/components/sidebar_milestone_dropdown.vue';
import getEpicMilestoneQuery from 'jh/epic/queries/epic_milestone.query.graphql';
import setEpicMilestoneMutation from 'jh/epic/queries/group_epic_milestone.mutation.graphql';
import { sidebarDropdownI18n } from 'jh/epic/constants';
import MilestoneSelect from 'jh/epic/components/milestone_select.vue';
import {
  mockDropdownInjection,
  mockActiveMilestoneData,
  mockExpiredMilestoneData,
  composeMilestoneMutationResponse,
  mockActiveMilestoneQueryResponse,
  mockExpiredMilestoneQueryResponse,
  mockEmptyMilestoneQueryResponse,
} from './mock_data';

jest.mock('~/alert', () => {
  return {
    createAlert: jest.fn(),
  };
});

describe('SidebarMilestoneDropdown', () => {
  let wrapper;

  const createComponent = ({
    mutationResolver = jest.fn(),
    queryResolver = jest.fn().mockResolvedValue(mockActiveMilestoneQueryResponse),
  } = {}) => {
    Vue.use(VueApollo);
    wrapper = shallowMountExtended(SidebarMilestoneDropdown, {
      provide: mockDropdownInjection,
      propsData: {
        issuableAttribute: 'Milestone',
      },
      apolloProvider: createMockApollo([
        [getEpicMilestoneQuery, queryResolver],
        [setEpicMilestoneMutation, mutationResolver],
      ]),
      stubs: {
        SidebarEditableItem: stubComponent(SidebarEditableItem, {
          template: `
          <div>
            <slot name='collapsed' />
            <slot name='collapsed-right' />
            <slot />
          </div>
          `,
          methods: {
            collapse: jest.fn(),
          },
        }),
      },
    });
  };
  const updateMilestone = (milestone) => {
    const milestoneSelect = wrapper.findComponent(MilestoneSelect);
    milestoneSelect.vm.$emit('set-milestone', milestone);
  };

  describe('rendering', () => {
    it('should render active milestone', async () => {
      createComponent();
      await waitForPromises();

      expect(wrapper.text()).not.toContain(sidebarDropdownI18n.none);
      expect(wrapper.text()).not.toContain(sidebarDropdownI18n.expired);
    });

    it('should render expired milestone', async () => {
      createComponent({
        queryResolver: jest.fn().mockReturnValue(mockExpiredMilestoneQueryResponse),
      });
      await waitForPromises();

      expect(wrapper.text()).toContain(sidebarDropdownI18n.expired);
    });

    it('renders no milestone selection', async () => {
      createComponent({
        queryResolver: jest.fn().mockResolvedValue(mockEmptyMilestoneQueryResponse),
      });
      await waitForPromises();

      expect(wrapper.text()).toContain(sidebarDropdownI18n.none);
    });
  });

  describe('updating', () => {
    it('updates current milestone with a new one', async () => {
      createComponent({
        mutationResolver: jest.fn().mockResolvedValue(mockExpiredMilestoneData),
      });
      await waitForPromises();
      expect(wrapper.text()).toContain(mockActiveMilestoneData.title);

      updateMilestone(mockExpiredMilestoneData);
      await waitForPromises();
      expect(wrapper.text()).toContain(mockExpiredMilestoneData.title);
    });

    it('unset milestone when new milestone is null', async () => {
      createComponent({
        mutationResolver: jest.fn().mockResolvedValue(composeMilestoneMutationResponse()),
      });
      await waitForPromises();

      updateMilestone({
        id: null,
      });
      await waitForPromises();

      expect(wrapper.text()).toContain(sidebarDropdownI18n.none);
    });

    it('should fail with an error message', async () => {
      const errorMessage = 'request with error';

      createComponent({
        mutationResolver: jest
          .fn()
          .mockResolvedValue(composeMilestoneMutationResponse(null, [errorMessage])),
      });

      updateMilestone({
        id: null,
      });
      await waitForPromises();

      expect(createAlert).toHaveBeenCalledWith(
        expect.objectContaining({
          message: errorMessage,
        }),
      );
    });
  });
});
