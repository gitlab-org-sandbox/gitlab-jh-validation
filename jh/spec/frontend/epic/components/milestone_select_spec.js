import VueApollo from 'vue-apollo';
import Vue from 'vue';
import { shallowMount } from '@vue/test-utils';
import createMockApollo from 'helpers/mock_apollo_helper';
import waitForPromises from 'helpers/wait_for_promises';
import groupMilestoneQuery from '~/sidebar/queries/group_milestones.query.graphql';
import DropdownWidget from '~/vue_shared/components/dropdown/dropdown_widget/dropdown_widget.vue';
import EpicMilestoneSelect from 'jh/epic/components/milestone_select.vue';
import { mockMilestonesData } from './mock_data';

Vue.use(VueApollo);

describe('EpicMilestoneSelect', () => {
  let wrapper;
  let mockApollo;
  const fullPath = '/jihulab-org';
  const milestones = mockMilestonesData.data.workspace.attributes.nodes;

  const createComponent = () => {
    mockApollo = createMockApollo([
      [groupMilestoneQuery, jest.fn().mockResolvedValue(mockMilestonesData)],
    ]);

    wrapper = shallowMount(EpicMilestoneSelect, {
      provide: {
        fullPath,
      },
      apolloProvider: mockApollo,
    });
  };

  const findDropdownWidget = () => wrapper.findComponent(DropdownWidget);

  beforeEach(async () => {
    createComponent();
    findDropdownWidget().vm.$emit('shown');
    await waitForPromises();
  });

  it('renders the component correctly', () => {
    expect(findDropdownWidget().exists()).toBe(true);
  });

  it('gets correct milestone data', () => {
    const dropdownWidget = findDropdownWidget();
    expect(dropdownWidget.props('options')).toEqual(milestones);
  });

  describe('emits events correctly', () => {
    it('changes search query', () => {
      const dropdownWidget = findDropdownWidget();

      const updatedSearch = 'test';
      dropdownWidget.vm.$emit('set-search', updatedSearch);

      expect(wrapper.vm.search).toBe(updatedSearch);
    });

    it('sets milestone correctly', () => {
      const dropdownWidget = findDropdownWidget();

      const milestone = milestones[0];
      dropdownWidget.vm.$emit('set-option', milestone);

      expect(wrapper.vm.localSelected).toBe(milestone);
      expect(wrapper.emitted('set-milestone')).toEqual([[milestone]]);
    });
  });
});
