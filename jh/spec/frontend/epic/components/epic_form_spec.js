import { GlForm, GlFormInput } from '@gitlab/ui';
import { shallowMount } from '@vue/test-utils';
import { TEST_HOST } from 'helpers/test_constants';
import MarkdownEditor from '~/vue_shared/components/markdown/markdown_editor.vue';
import { getIdFromGraphQLId } from '~/graphql_shared/utils';
import EpicForm from 'jh/epic/components/epic_form.vue';
import EpicEnhancementForm from 'jh/epic/components/epic_enhancement.vue';

jest.mock('~/lib/utils/url_utility', () => ({
  visitUrl: jest.fn(),
}));

jest.mock('~/autosave');

const TEST_GROUP_PATH = 'gitlab-org';
const TEST_NEW_EPIC = { data: { createEpic: { epic: { webUrl: TEST_HOST } } } };
const originalGon = window.gon;

describe('ee/epic/components/epic_form.vue', () => {
  let wrapper;

  const createWrapper = ({ mutationResult = TEST_NEW_EPIC } = {}) => {
    wrapper = shallowMount(EpicForm, {
      provide: {
        iid: '1',
        groupPath: TEST_GROUP_PATH,
        groupEpicsPath: TEST_HOST,
        labelsManagePath: TEST_HOST,
        markdownPreviewPath: TEST_HOST,
        markdownDocsPath: TEST_HOST,
      },
      mocks: {
        $apollo: {
          mutate: jest.fn().mockResolvedValue(mutationResult),
        },
      },
    });
  };

  const findForm = () => wrapper.findComponent(GlForm);
  const findTitle = () => wrapper.findComponent(GlFormInput);
  const findDescription = () => wrapper.findComponent(MarkdownEditor);
  const findEpicEnhancementForm = () => wrapper.findComponent(EpicEnhancementForm);

  describe('when mounted', () => {
    beforeEach(() => {
      createWrapper();
    });

    it('should render the form', () => {
      expect(findForm().exists()).toBe(true);
    });

    describe('epic enhancement', () => {
      const setFeatureFlag = (state = true) => {
        window.gon = { ...originalGon, epic_support_milestone_and_iteration: state };
      };

      afterAll(() => {
        window.gon = originalGon;
      });

      it('should not render epic enhancement form', () => {
        createWrapper();

        expect(findEpicEnhancementForm().exists()).toBe(false);
      });

      it('should render epic enhancement form', () => {
        setFeatureFlag();
        createWrapper();

        expect(findEpicEnhancementForm().exists()).toBe(true);
      });

      describe('form submission', () => {
        const title = 'Status page MVP';
        const description = '### Goal\n\n- [ ] Item';
        const sprintId = 1;
        const milestone = { id: 'gid://gitlab/milestone/2', title: 'Milestone 1' };

        beforeEach(() => {
          setFeatureFlag();
          createWrapper();
        });

        it('sets the milestone data', () => {
          const epicEnhancementForm = findEpicEnhancementForm();

          findTitle().vm.$emit('input', title);
          findDescription().vm.$emit('input', description);
          epicEnhancementForm.vm.$emit('set-milestone', milestone);
          findForm().vm.$emit('submit', { preventDefault: () => {} });
          expect(wrapper.vm.$apollo.mutate).toHaveBeenCalledWith(
            expect.objectContaining({
              variables: expect.objectContaining({
                input: expect.objectContaining({
                  milestoneId: getIdFromGraphQLId(milestone.id),
                }),
              }),
            }),
          );
        });

        it('sets the iteration data', () => {
          const epicEnhancementForm = findEpicEnhancementForm();

          findTitle().vm.$emit('input', title);
          findDescription().vm.$emit('input', description);
          epicEnhancementForm.vm.$emit('set-iteration', sprintId);
          findForm().vm.$emit('submit', { preventDefault: () => {} });
          expect(wrapper.vm.$apollo.mutate).toHaveBeenCalledWith(
            expect.objectContaining({
              variables: expect.objectContaining({
                input: expect.objectContaining({
                  sprintId,
                }),
              }),
            }),
          );
        });
      });
    });
  });
});
