import { shallowMount } from '@vue/test-utils';

import IterationDropdown from 'jh/epic/components/iteration_dropdown.vue';
import EpicIterationSelect from 'jh/epic/components/iteration_select.vue';

describe('EpicIterationSelect', () => {
  let wrapper;

  const createComponent = () => {
    wrapper = shallowMount(EpicIterationSelect);
  };

  const findIterationDropdown = () => wrapper.findComponent(IterationDropdown);

  beforeEach(() => {
    createComponent();
  });

  it('renders component correctly', () => {
    expect(findIterationDropdown().exists()).toBe(true);
  });

  describe('emits iteration data', () => {
    it('with proper data', () => {
      const id = 1;
      const iteration = { id: `gid://gitlab/Group/${id}`, title: 'Iteration 1' };
      findIterationDropdown().vm.$emit('select', iteration);
      expect(wrapper.emitted('set-iteration')).toEqual([[id]]);
    });

    it('with a wrong id', () => {
      const iteration = { id: 'gid://gitlab/1', title: 'Iteration 1' };
      findIterationDropdown().vm.$emit('select', iteration);
      expect(wrapper.emitted('set-iteration')).toEqual([[null]]);
    });

    it('without id', () => {
      const iteration = { id: null, title: 'Iteration 1' };
      findIterationDropdown().vm.$emit('select', iteration);
      expect(wrapper.emitted('set-iteration')).toEqual([['0']]);
    });

    it('without iteration', () => {
      findIterationDropdown().vm.$emit('select', null);
      expect(wrapper.emitted('set-iteration')).toEqual([['']]);
    });
  });
});
