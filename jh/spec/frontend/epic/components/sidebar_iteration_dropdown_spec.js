import Vue from 'vue';
import VueApollo from 'vue-apollo';
import waitForPromises from 'helpers/wait_for_promises';
import createMockApollo from 'helpers/mock_apollo_helper';
import { stubComponent } from 'helpers/stub_component';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import { createAlert } from '~/alert';
import SidebarEditableItem from '~/sidebar/components/sidebar_editable_item.vue';
import SidebarIterationDropdown from 'jh/epic/components/sidebar_iteration_dropdown.vue';
import IterationSelect from 'jh/epic/components/iteration_select.vue';
import getEpicIterationQuery from 'jh/epic/queries/epic_iteration.query.graphql';
import setEpicIterationMutation from 'jh/epic/queries/group_epic_iteration.mutation.graphql';
import { sidebarDropdownI18n } from 'jh/epic/constants';
import {
  mockDropdownInjection,
  mockIterationCadence,
  mockIterationCadence2,
  mockIteration2,
  mockIterationQueryResponse,
  mockEmptyIterationQueryResponse,
  composeIterationMutationResponse,
} from './mock_data';

jest.mock('~/alert', () => {
  return {
    createAlert: jest.fn(),
  };
});

describe('SidebarIterationDropdown', () => {
  let wrapper;

  const createComponent = ({
    mutationResolver = jest.fn(),
    queryResolver = jest.fn().mockResolvedValue(mockIterationQueryResponse),
  } = {}) => {
    Vue.use(VueApollo);
    wrapper = shallowMountExtended(SidebarIterationDropdown, {
      provide: mockDropdownInjection,
      propsData: {
        issuableAttribute: 'Iteration',
      },
      apolloProvider: createMockApollo([
        [getEpicIterationQuery, queryResolver],
        [setEpicIterationMutation, mutationResolver],
      ]),
      stubs: {
        SidebarEditableItem: stubComponent(SidebarEditableItem, {
          template: `
          <div>
            <slot name='collapsed' />
            <slot name='collapsed-right' />
            <slot />
          </div>
          `,
          methods: {
            collapse: jest.fn(),
          },
        }),
      },
    });
  };

  const updateIteration = (iteration) => {
    const iterationSelect = wrapper.findComponent(IterationSelect);
    iterationSelect.vm.$emit('select', iteration);
  };

  describe('rendering', () => {
    it('should render a selected iteration', async () => {
      createComponent();
      await waitForPromises();

      expect(wrapper.text()).toContain(mockIterationCadence.title);
    });

    it('renders no milestone selection', async () => {
      createComponent({
        queryResolver: jest.fn().mockResolvedValue(mockEmptyIterationQueryResponse),
      });
      await waitForPromises();

      expect(wrapper.text()).toContain(sidebarDropdownI18n.none);
    });
  });

  describe('updating', () => {
    it('updates current milestone with a new one', async () => {
      createComponent({
        mutationResolver: jest
          .fn()
          .mockResolvedValue(composeIterationMutationResponse(mockIteration2)),
      });
      await waitForPromises();
      expect(wrapper.text()).toContain(mockIterationCadence.title);

      updateIteration(mockIteration2);

      await waitForPromises();
      expect(wrapper.text()).toContain(mockIterationCadence2.title);
    });

    it('unset milestone when new milestone is null', async () => {
      createComponent({
        mutationResolver: jest.fn().mockResolvedValue(composeIterationMutationResponse()),
      });
      await waitForPromises();
      expect(wrapper.text()).toContain(mockIterationCadence.title);

      updateIteration({
        id: null,
      });
      await waitForPromises();

      expect(wrapper.text()).toContain(sidebarDropdownI18n.none);
    });

    it('should fail with an error message', async () => {
      const errorMessage = 'request with error';

      createComponent({
        mutationResolver: jest
          .fn()
          .mockResolvedValue(composeIterationMutationResponse(null, [errorMessage])),
      });

      updateIteration({
        id: null,
      });
      await waitForPromises();

      expect(createAlert).toHaveBeenCalledWith(
        expect.objectContaining({
          message: errorMessage,
        }),
      );
    });
  });
});
