export const mockMilestonesData = {
  data: {
    workspace: {
      id: 'gid://gitlab/Project/7',
      attributes: {
        nodes: [
          {
            id: 'gid://gitlab/Milestone/59',
            title: 'test milestone',
            webUrl: '/groups/flightjs/-/milestones/1',
            dueDate: '2023-12-19',
            expired: false,
            __typename: 'Milestone',
            state: 'active',
          },
          {
            id: 'gid://gitlab/Milestone/35',
            title: 'v4.0',
            webUrl: '/flightjs/Flight/-/milestones/5',
            dueDate: null,
            expired: false,
            __typename: 'Milestone',
            state: 'active',
          },
          {
            id: 'gid://gitlab/Milestone/34',
            title: 'v3.0',
            webUrl: '/flightjs/Flight/-/milestones/4',
            dueDate: null,
            expired: false,
            __typename: 'Milestone',
            state: 'active',
          },
          {
            id: 'gid://gitlab/Milestone/32',
            title: 'v1.0',
            webUrl: '/flightjs/Flight/-/milestones/2',
            dueDate: null,
            expired: false,
            __typename: 'Milestone',
            state: 'active',
          },
          {
            id: 'gid://gitlab/Milestone/31',
            title: 'v0.0',
            webUrl: '/flightjs/Flight/-/milestones/1',
            dueDate: null,
            expired: false,
            __typename: 'Milestone',
            state: 'active',
          },
          {
            id: 'gid://gitlab/Milestone/46',
            title: 'Sprint - Voluptas est quasi omnis libero eius ut eos.',
            webUrl: '/flightjs/Flight/-/milestones/6',
            dueDate: '2023-05-14',
            expired: true,
            __typename: 'Milestone',
            state: 'active',
          },
        ],
        __typename: 'MilestoneConnection',
      },
      __typename: 'Project',
    },
  },
};

export const mockDropdownInjection = {
  fullPath: 'test/path',
  iid: '1',
};

export const mockActiveMilestoneData = mockMilestonesData.data.workspace.attributes.nodes[0];
export const mockExpiredMilestoneData = {
  ...mockActiveMilestoneData,
  expired: true,
};

const generateQueryResponse = (milestoneData = null) => ({
  data: {
    workspace: {
      id: 'gid://gitlab/Group/7',
      issuable: {
        id: 'gid://gitlab/Epic/1',
        attribute: milestoneData,
        __typename: 'Epic',
      },
      __typename: 'Group',
    },
  },
});

export const mockActiveMilestoneQueryResponse = generateQueryResponse(mockActiveMilestoneData);
export const mockExpiredMilestoneQueryResponse = generateQueryResponse(mockExpiredMilestoneData);
export const mockEmptyMilestoneQueryResponse = generateQueryResponse();

export const composeMilestoneMutationResponse = (milestoneData = null, errors = []) => ({
  data: {
    updateIssuableMilestone: {
      errors,
      issuable: {
        id: 'gid://gitlab/Epic/75',
        attribute: milestoneData,
        __typename: 'Epic',
      },
      __typename: 'UpdateEpicPayload',
    },
  },
});

export const composeIterationMutationResponse = (iterationData = null, errors = []) => ({
  data: {
    updateIssuableIteration: {
      errors,
      issuable: {
        id: 'gid://gitlab/Epic/75',
        attribute: iterationData,
        __typename: 'Epic',
      },
      __typename: 'UpdateEpicPayload',
    },
  },
});

export const mockIterationCadence = {
  id: 'gid://gitlab/Iterations::Cadence/1',
  title: 'GitLab.org Iterations',
  durationInWeeks: 1,
  __typename: 'IterationCadence',
};

export const mockIterationCadence2 = {
  ...mockIterationCadence,
  title: 'JihuLab.org Iterations',
};

export const mockIteration = {
  id: 'gid://gitlab/Iteration/1',
  title: 'iteration 1',
  iterationCadence: mockIterationCadence,
  webUrl: 'http://gdk.test/groups/test/-/iterations/41109',
  startDate: '2021-10-05',
  dueDate: '2021-10-10',
  __typename: 'Iteration',
};

export const mockIteration2 = {
  ...mockIteration,
  id: 'gid://gitlab/Iteration/2',
  title: 'iteration 2',
  iterationCadence: mockIterationCadence2,
};

export const mockIterationQueryResponse = generateQueryResponse(mockIteration);
export const mockIterationQueryResponse2 = generateQueryResponse(mockIteration2);
export const mockEmptyIterationQueryResponse = generateQueryResponse();
