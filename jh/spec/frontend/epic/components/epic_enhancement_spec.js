import { shallowMount } from '@vue/test-utils';

import EpicEnhancementForm from 'jh/epic/components/epic_enhancement.vue';
import EpicMilestoneSelect from 'jh/epic/components/milestone_select.vue';
import EpicIterationSelect from 'jh/epic/components/iteration_select.vue';

describe('EpicEnhancementForm', () => {
  let wrapper;

  const createComponent = () => {
    wrapper = shallowMount(EpicEnhancementForm);
  };

  const findMilestoneSelect = () => wrapper.findComponent(EpicMilestoneSelect);
  const findIterationSelect = () => wrapper.findComponent(EpicIterationSelect);

  beforeEach(() => {
    createComponent();
  });

  it('should render components correctly', () => {
    expect(findMilestoneSelect().exists()).toBe(true);
    expect(findIterationSelect().exists()).toBe(true);
  });

  describe('emit data to the parent form', () => {
    it('should emit milestone change', () => {
      const milestone = { id: 1, title: 'Milestone 1' };
      findMilestoneSelect().vm.$emit('set-milestone', milestone);
      expect(wrapper.emitted('set-milestone')).toEqual([[milestone]]);
    });

    it('should emit iteration change', () => {
      const iteration = { id: 1, title: 'Iteration 1' };
      findIterationSelect().vm.$emit('set-iteration', iteration);
      expect(wrapper.emitted('set-iteration')).toEqual([[iteration]]);
    });
  });
});
