import { GlIcon } from '@gitlab/ui';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import JobPill from 'jh/ci/pipeline_editor/components/graph/job_pill.vue';

const jobName = 'test job';
const pipelineId = '123';

describe('<JobPill />', () => {
  let wrapper;
  const createComponent = () => {
    wrapper = shallowMountExtended(JobPill, {
      propsData: {
        jobName,
        pipelineId,
      },
    });
  };

  it('deletes job when clicked', () => {
    createComponent();

    wrapper.findComponent(GlIcon).vm.$emit('click');

    expect(wrapper.emitted('delete-job')).toHaveLength(1);
  });
});
