import MockAdapter from 'axios-mock-adapter';
import $ from 'jquery';
import axios from '~/lib/utils/axios_utils';
import initMrPage from 'helpers/init_vue_mr_page_helper';
import { stubPerformanceWebAPI } from 'helpers/performance';
import '~/lib/utils/common_utils';
import { ExtendedMergeRequestTabs as MergeRequestTabs } from 'jh/merge_request_tabs';

describe('JHMergeRequestTabs', () => {
  const testContext = {};
  const stubLocation = {};
  let locationMock;
  const setLocation = (stubs) => {
    const defaults = {
      pathname: '',
      search: '',
      hash: '',
    };
    $.extend(stubLocation, defaults, stubs || {});
  };

  beforeEach(() => {
    stubPerformanceWebAPI();
    initMrPage();
    locationMock = jest.spyOn(window, 'location', 'get').mockImplementation(() => stubLocation);
    testContext.class = new MergeRequestTabs();
    setLocation();

    testContext.spies = {
      history: jest.spyOn(window.history, 'pushState').mockImplementation(() => {}),
    };

    gl.mrWidget = {};
  });

  afterEach(() => {
    locationMock.mockRestore();
    document.body.innerHTML = '';
  });

  describe('setCurrentAction', () => {
    let mock;

    beforeEach(() => {
      mock = new MockAdapter(axios);
      mock.onAny().reply({ data: {} });
      testContext.subject = testContext.class.setCurrentAction;
    });

    afterEach(() => {
      mock.restore();
      window.history.replaceState({}, '', '/');
    });

    it('changes from custom_page', () => {
      setLocation({
        pathname: '/foo/bar/-/merge_requests/1/custom_page',
      });

      expect(testContext.subject('diffs')).toBe('/foo/bar/-/merge_requests/1/diffs');
      expect(testContext.subject('commits')).toBe('/foo/bar/-/merge_requests/1/commits');
    });
  });
});
