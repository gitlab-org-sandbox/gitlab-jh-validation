# frozen_string_literal: true

# Prepended onto ::QA::Runtime::User
module QA
  module JH
    module Runtime
      module User
        def phone
          ::QA::Runtime::Env.user_phone
        end

        def github_user?
          ::QA::Runtime::Env.qa_oauth_github_username.present? && ::QA::Runtime::Env.qa_oauth_github_password.present?
        end
      end
    end
  end
end
