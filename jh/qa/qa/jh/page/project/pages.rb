# frozen_string_literal: true

module QA
  module JH
    module Page
      module Project
        module Pages
          def accept_authorization
            find_element('button[testid="authorization-button"]').click
          end
        end
      end
    end
  end
end
