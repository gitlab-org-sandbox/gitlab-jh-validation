# frozen_string_literal: true

module QA
  module JH
    module Page
      module Project
        module SubMenus
          module Issues
            def go_to_ones_issues
              hover_issues do
                click_element(:sidebar_menu_item_link, menu_item: 'ONES')
              end
            end

            private

            def hover_issues
              scroll_to_element(:sidebar_menu_link, menu_item: 'Issues')
              find_element(:sidebar_menu_link, menu_item: 'Issues').hover

              yield
            end
          end
        end
      end
    end
  end
end
