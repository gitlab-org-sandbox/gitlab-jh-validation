# frozen_string_literal: true

module QA
  module JH
    module Page
      module Project
        module Menu
          def go_to_performance_analytics
            open_analyze_submenu('Performance Analytics')
          end
        end
      end
    end
  end
end
