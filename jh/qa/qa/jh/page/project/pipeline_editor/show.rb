# frozen_string_literal: true

module QA
  module JH
    module Page
      module Project
        module PipelineEditor
          module Show
            def auto_clear_and_write_to_editor(text)
              if page.driver.browser.capabilities.platform.include? 'mac'
                find_element(:source_editor_container).fill_in(with: text,
                  fill_options: { clear: [[:command, 'a'], :delete] })
              else
                find_element(:source_editor_container).fill_in(with: text,
                  fill_options: { clear: [[:control, 'a'], :delete] })
              end
            end
          end
        end
      end
    end
  end
end
