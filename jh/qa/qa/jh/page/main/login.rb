# frozen_string_literal: true

module QA
  module JH
    module Page
      module Main
        module Login
          # @override :fill_in_credential
          def fill_in_credential(user)
            phone_num = user.respond_to?(:phone) ? user.phone : user.username
            fill_element 'username-field', phone_num || user.username
            fill_element 'password-field', user.password
          end
        end
      end
    end
  end
end
