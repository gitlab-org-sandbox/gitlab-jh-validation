# frozen_string_literal: true

module QA
  module JH
    module Page
      module Registration
        module Welcome
          def self.prepended(base)
            base.view 'ee/app/views/registrations/welcome/_jobs_to_be_done.html.haml' do
              element :registration_reason_dropdown
            end
          end

          def jh_choose_setup_for_company_if_available
            choose_element(:setup_for_company_radio, true)
          end

          def choose_what_do_you_want_to_do_if_available
            has_element?(:setup_for_company_radio, wait: 1)
            click_element(:joining_project_false_radio) if has_element?(:joining_project_false_radio, wait: 1)
          end

          def select_registration_reason(reason)
            select_element(:registration_reason_dropdown, reason)
          end
        end
      end
    end
  end
end
