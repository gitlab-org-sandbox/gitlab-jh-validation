# frozen_string_literal: true

module QA
  module JH
    module Support
      module Data
        module Image
          def ci_test_image
            'dwdraju/alpine-curl-jq:latest'
          end
        end
      end
    end
  end
end
