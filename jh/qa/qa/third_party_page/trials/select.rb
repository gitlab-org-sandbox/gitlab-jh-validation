# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Trials
      class Select < ::QA::Page::Base
        def select_group(group)
          find('svg[data-testid=chevron-down-icon]').click
          find('li', text: group.to_s).click
          QA::Runtime::Logger.info("Choose #{group}")
        end

        def choose_company
          click_element(:"trial-company-radio")
        end

        def choose_personal
          click_element(:"trial-individual-radio")
        end

        def start_your_free_trial
          click_element(:"start-your-free-trial-button")
        end
      end
    end
  end
end
