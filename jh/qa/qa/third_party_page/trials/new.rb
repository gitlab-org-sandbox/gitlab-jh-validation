# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Trials
      class New < ::QA::Page::Base
        def fill_first_name(first_name)
          fill_element(:"first-name", first_name)
        end

        def fill_last_name(last_name)
          fill_element(:"last-name", last_name)
        end

        def fill_company_name(company_name)
          fill_element(:"company-name-field", company_name)
        end

        def select_quantity_of_employees(quantity)
          select_element(:"company-size-dropdown", quantity)
        end

        def select_country(country)
          select_element(:country, country)
        end

        def select_province(province)
          select_element(:state, province)
        end

        def fill_telephone(telephone)
          fill_element(:"phone-number-field", telephone)
        end

        def continue_to_trial
          click_element("continue-button")
        end
      end
    end
  end
end
