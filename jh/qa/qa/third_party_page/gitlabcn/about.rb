# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Gitlabcn
      class About < ::QA::Page::Base
        def go_to_trial
          page.find('a.submit-btn', text: /申请免费试用/).click
        end

        def switch_to_saas
          page.find('#saas').click
        end
      end
    end
  end
end
