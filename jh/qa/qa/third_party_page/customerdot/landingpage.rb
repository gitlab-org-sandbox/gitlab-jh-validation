# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Landingpage < QA::Page::Base
        def go_to_receipts
          has_text?('View receipts') ? click_link('View receipts') : click_link('发票管理')
          QA::Runtime::Logger.info("Click view receipts")
        end

        def go_to_profile
          find('li[data-testid=profile]').click
          QA::Runtime::Logger.info("Click my profile")
          retry_until(reload: true, sleep_interval: 2, max_attempts: 3, message: 'Find profile settings') do
            has_text?('Profile settings')
          end
          click_link('Profile settings')
          QA::Runtime::Logger.info("Go to  account details")
        end

        def sign_out
          find('li[data-testid=profile]').click
          QA::Runtime::Logger.info("Click my profile")
          has_text?('Sign out') ? click_link('Sign out') : click_link('退出')
          QA::Runtime::Logger.info("Sign out from customerdot")
        end
      end
    end
  end
end
