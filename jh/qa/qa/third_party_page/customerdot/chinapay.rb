# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Chinapay < ::QA::Page::Base
        def choose_card
          QA::Runtime::Logger.info("visit #{page.current_url}")
          find('.mbc').click
          QA::Runtime::Logger.info("Choose card")
        end

        def go_to_payment
          find('.bank_pay_btn').click
          QA::Runtime::Logger.info("Go to Chinapay")
        end

        def payment_success
          find('a', text: /支付成功/).click
          QA::Runtime::Logger.info("Payment success!")
        end

        def back_to_customerdot
          find('a', text: /立即跳转/).click
          QA::Runtime::Logger.info("Back to Customerdot")
        end
      end
    end
  end
end
