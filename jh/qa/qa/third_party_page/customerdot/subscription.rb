# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Subscription < ::QA::Page::Base
        def enter_quantity(quantity)
          find('input[data-testid=number-of-users]').fill_in(with: quantity)
          QA::Runtime::Logger.info("Filling #{quantity} in number of users")
        end

        def continue_to_billing
          find('span.gl-button-text', text: /Continue to billing/).click
          QA::Runtime::Logger.info("Continue to billing")
        end

        def choose_the_province(province = '北京市')
          find('select[data-testid=state]').select(province)
          QA::Runtime::Logger.info("Fill :state with #{province}")
        end

        def fill_the_street_address(street_address = 'jh-qa')
          find('input[data-testid=street_address_1]').fill_in(with: street_address)
          QA::Runtime::Logger.info("Fill :street_address with #{street_address}")
        end

        def continue_to_payment
          find('span.gl-button-text', text: /Continue to payment/).click
          QA::Runtime::Logger.info("Continue to payment")
        end

        def confirm_privacy_and_terms
          return unless has_element?('privacy-and-terms-confirm')

          click_element('privacy-and-terms-confirm')
          QA::Runtime::Logger.info("Confirm privacy and terms")
        end

        def confirm_purchase
          wait_until(max_duration: 10, sleep_interval: 3) do
            find('span.gl-button-text', text: /Confirm purchase/).click
          end

          QA::Runtime::Logger.info("Continue to payment")
        end

        def fill_addon_quantity(quantity)
          find('input[data-testid=quantity]').fill_in(with: quantity)
          QA::Runtime::Logger.info("Filling #{quantity} in addon page")
        end
      end
    end
  end
end
