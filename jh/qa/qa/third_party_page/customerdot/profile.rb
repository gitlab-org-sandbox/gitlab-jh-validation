# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Profile < QA::Page::Base
        def get_user_first_name
          find('#first_name').value
        end

        def get_user_last_name
          find('#last_name').value
        end

        def get_user_email
          find('#register_email').value
        end

        def has_user_information_area?
          has_css?('#personal-details')
        end
      end
    end
  end
end
