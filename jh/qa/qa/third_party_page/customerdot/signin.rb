# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Signin < ::QA::Page::Base
        def login(user: nil)
          find('button[type=submit]').click
          QA::Runtime::Logger.info("Click login button")

          wait_if_retry_later

          fill_element 'username-field', user&.username || Runtime::User.username
          fill_element 'password-field', user&.password || Runtime::User.password

          click_element 'sign-in-button'

          Support::WaitForRequests.wait_for_requests

          wait_for_gitlab_to_respond
        end
      end
    end
  end
end
