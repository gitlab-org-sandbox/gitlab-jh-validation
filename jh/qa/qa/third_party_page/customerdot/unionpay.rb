# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Unionpay < ::QA::Page::Base
        def fill_bank_account
          Support::Waiter.wait_until(retry_on_exception: true, reload_page: page, sleep_interval: 2) do
            find('#cardNumber').fill_in(with: ::QA::Runtime::Env.bank_account)
          end
          QA::Runtime::Logger.info("Card number is entered")
        end

        def go_to_phone_verification
          Support::Waiter.wait_until do
            find('#btnNext').click
          end
          QA::Runtime::Logger.info("Click next to go to phone verification")
        end

        def sent_phone_sms_code
          Support::Waiter.wait_until do
            find('#btnGetCode').click
          end
          QA::Runtime::Logger.info("Click to get phone sms code")
        end

        def fill_phone_sms_code
          Support::Waiter.wait_until(retry_on_exception: true, reload_page: page, sleep_interval: 2) do
            find('#smsCode').fill_in(with: ::QA::Runtime::Env.phone_sms_code)
          end
          QA::Runtime::Logger.info("Phone code is entered")
        end

        def confirm_payment
          Support::Waiter.wait_until do
            find('#btnCardPay').click
          end
          QA::Runtime::Logger.info("Click to confirm payment")
        end

        def has_payment_successful?
          success = has_text?('Payment Succeeded') || has_text?('您已成功支付')
          QA::Runtime::Logger.info("Payment has #{success ? 'been successful' : 'not been successful'}")
          success
        end

        def back_to_customer_dot
          Support::Waiter.wait_until do
            find('#btnBack').click
          end
          QA::Runtime::Logger.info("Click return to merchant")
        end

        def complete_phone_verification
          Support::Waiter.wait_until(reload_page: page, sleep_interval: 5) do
            sent_phone_sms_code
            fill_phone_sms_code
            confirm_payment
            Support::WaitForRequests.wait_for_requests
            has_payment_successful?
          end
        end
      end
    end
  end
end
