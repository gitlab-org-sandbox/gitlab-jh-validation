# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Customers < ::QA::Page::Base
        def has_form?
          has_css?('form[method=post]')
        end

        def fill_the_customer_form
          fill_company_name
          select_company_size
          select_province
          click_register_button
        end

        private

        def fill_company_name
          find("#company").fill_in(with: 'jihu-qa')
        end

        def select_company_size
          find("#company_size").select("100-499")
        end

        def select_province
          find("#state").select("河北省")
        end

        def click_register_button
          find('button[data-qa-value=register]').click
        end
      end
    end
  end
end
