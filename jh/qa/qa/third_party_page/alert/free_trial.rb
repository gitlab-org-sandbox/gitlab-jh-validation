# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Alert
      class FreeTrial < ::QA::Page::Base
        def trial_actived_message?
          has_text?('Congratulations, your free trial is activated.')
        end
      end
    end
  end
end
