# frozen_string_literal: true

module QA
  RSpec.describe 'JH Plan', :smoke, only: { subdomain: :release } do
    describe 'Group Epic' do
      include Support::Dates

      let(:epic_title) { 'This is a group epic' }
      let(:milestone_title) { 'Group milestone for e2e' }
      let(:iteration_title) { 'Group iteration for e2e' }
      let(:description) { 'Milestone or Iteration for test' }
      let(:start_date) { current_date_yyyy_mm_dd }
      let(:due_date) { next_month_yyyy_mm_dd }
      let(:group_iteration_name) { "#{group.path} Iterations" }

      let(:group) do
        Resource::Group.fabricate_via_api! do |group|
          group.path = "group-to-test-epic-#{SecureRandom.hex(8)}"
        end
      end

      let!(:milestone) do
        Resource::GroupMilestone.fabricate_via_api! do |milestone|
          milestone.group = group
          milestone.title = milestone_title
          milestone.description = description
          milestone.start_date = start_date
          milestone.due_date = due_date
        end
      end

      let!(:iteration) do
        EE::Resource::GroupIteration.fabricate_via_api! do |iteration|
          iteration.group = group
          iteration.title = iteration_title
          iteration.description = description
          iteration.start_date = start_date
          iteration.due_date = due_date
        end
      end

      before do
        Flow::Login.sign_in
        group.visit!
      end

      after do
        group.remove_via_api!
      end

      it 'creates an epic with associated milestone', \
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/24' do
        QA::Page::Group::Menu.perform(&:go_to_epics)
        QA::EE::Page::Group::Epic::Index.perform(&:click_new_epic)
        QA::EE::Page::Group::Epic::New.perform do |new_epic_page|
          new_epic_page.set_title(epic_title)
        end

        Page::Group::Epic::Show.perform do |new_epic_page|
          new_epic_page.select_milestone(milestone_title)
        end

        QA::EE::Page::Group::Epic::New.perform(&:create_new_epic)

        expect(page).to have_content(epic_title)
      end

      it 'creates an epic with associated iteration', \
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/25' do
        QA::Page::Group::Menu.perform(&:go_to_epics)
        QA::EE::Page::Group::Epic::Index.perform(&:click_new_epic)
        QA::EE::Page::Group::Epic::New.perform do |new_epic_page|
          new_epic_page.set_title(epic_title)
        end

        Page::Group::Epic::Show.perform do |new_epic_page|
          new_epic_page.select_iteration(iteration_title)
        end

        QA::EE::Page::Group::Epic::New.perform(&:create_new_epic)

        expect(page).to have_content(epic_title)
      end

      it 'edits epic to set associated milestone/iteration', \
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/26' do
        QA::Page::Group::Menu.perform(&:go_to_epics)
        QA::EE::Page::Group::Epic::Index.perform(&:click_new_epic)
        QA::EE::Page::Group::Epic::New.perform do |new_epic_page|
          new_epic_page.set_title(epic_title)
        end

        QA::EE::Page::Group::Epic::New.perform(&:create_new_epic)
        expect(page).to have_content(epic_title)

        Page::Group::Epic::Show.perform do |new_epic_page|
          new_epic_page.edit_milestone(milestone_title)
          expect(new_epic_page).to have_milestone_set_successfully(milestone_title)
        end

        Page::Group::Epic::Show.perform do |new_epic_page|
          new_epic_page.edit_iteration(iteration_title)
          expect(new_epic_page).to have_iteration_set_successfully(group_iteration_name)
        end
      end
    end
  end
end
