# frozen_string_literal: true

module QA
  RSpec.describe 'JH Create', :smoke, only: { subdomain: :release } do
    describe 'Custom Page' do
      let(:root_group) { create(:sandbox, path: "group-to-test-custom-page-#{SecureRandom.hex(4)}") }
      let(:project) { create(:project, name: "project-to-test-custom-page-#{SecureRandom.hex(4)}", group: root_group) }

      let(:merge_request) do
        create(:merge_request, title: 'Merge request with custom page', project: project)
      end

      let(:custom_page_title) { 'Custom Page For e2e' }
      let(:custom_page_url) { 'https://example.com?mr=$MERGE_REQUEST_IID&project=$PROJECT_ID&user_id=$USER_ID&username=$USERNAME' }

      before do
        Flow::Login.sign_in
        root_group.visit!
      end

      after do
        root_group.remove_via_api!
      end

      it 'configures a custom page for merge request view', \
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/27' do
        Page::Group::Menu.perform(&:go_to_general_settings)
        Page::Group::Settings::MergeRequest.perform do |setting|
          setting.configure_custom_page(custom_page_title, custom_page_url)
        end

        merge_request.visit!

        Page::Group::Settings::MergeRequest.perform do |custom_page|
          expect(custom_page).to have_custom_page(custom_page_title)
        end
      end
    end
  end
end
