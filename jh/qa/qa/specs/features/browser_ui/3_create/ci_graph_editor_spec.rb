# frozen_string_literal: true

module QA
  RSpec.describe 'JH Create', :smoke, :require_admin, except: { subdomain: :production } do
    describe 'CI Graph Editor' do
      let(:group) { create(:group, name: "group-to-test-ci-graph-editor-#{SecureRandom.hex(4)}") }

      let(:project) do
        create(:project, :with_readme, name: "project-to-ci-graph-editor-#{SecureRandom.hex(4)}", group: group)
      end

      let(:current_stage) { 'deploy' }
      let(:new_stage) { 'notice' }
      let(:new_job) { 'notice_job' }
      let(:job_script) { 'echo "The pipeline has been completed"' }

      before do
        Runtime::Feature.enable(:jh_ci_graph_editor)
        Flow::Login.sign_in
        group.visit!
        project.visit!
      end

      after do
        group.remove_via_api!
      end

      it 'edits pipeline file with GUI editor',
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/28' do
        Page::Project::Menu.perform(&:go_to_pipeline_editor)
        Page::Project::PipelineEditor::CiGraphEditor.perform(&:edit_pipeline_with_graph_editor)
        Page::Project::PipelineEditor::CiGraphEditor.perform do |ci|
          ci.add_stage_after(current_stage, new_stage)
          ci.add_job_for_stage(new_stage, new_job, job_script)
        end

        Page::Project::PipelineEditor::CiGraphEditor.perform(&:switch_to_edit_tab)
        Page::Project::PipelineEditor::Show.perform(&:submit_changes)
        Page::Project::PipelineEditor::Show.perform(&:dismiss_file_tree_popover)

        Page::Project::PipelineEditor::CiGraphEditor.perform do |commit|
          expect(commit).to have_pipeline_commit_message
        end
      end
    end
  end
end
