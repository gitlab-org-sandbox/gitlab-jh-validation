# frozen_string_literal: true

module QA
  RSpec.describe 'JH Fulfillment', :smoke, :require_admin, only: { subdomain: :staging } do
    describe 'Purchase' do
      let(:api_client) { Runtime::API::Client.as_admin }

      let(:user) do
        Resource::User.fabricate_via_api! do |user|
          user.email = "jihu-qa+#{SecureRandom.hex(4)}@gitlab.cn"
          user.api_client = api_client
          user.hard_delete_on_api_removal = true
        end
      end

      let(:group_for_trial) do
        Resource::Sandbox.fabricate! do |sandbox|
          sandbox.path = "test-group-fulfillment-#{SecureRandom.hex(4)}"
          sandbox.api_client = api_client
        end
      end

      before do
        Flow::Login.sign_in(as: user)
      end

      describe 'Free Trial Process' do
        context 'when on billing page with multiple eligible namespaces' do
          let!(:group) do
            Resource::Sandbox.fabricate! do |sandbox|
              sandbox.path = "test-group-fulfillment-#{SecureRandom.hex(4)}"
              sandbox.api_client = api_client
            end
          end

          before do
            group_for_trial.visit!
            group.visit!
            Page::Group::Menu.perform(&:go_to_billing)
          end

          after do
            user.remove_via_api!
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/3' do
            Page::Group::Settings::Billing.perform(&:go_to_free_trial)

            ::QA::Flow::JhTrial.start_trial(group_for_trial.path)
            ::QA::ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Premium Trial Plan"))
                .to be(true)
            end
          end
        end

        context 'when on billing page with only one eligible namespace' do
          before do
            group_for_trial.visit!
            Page::Group::Menu.perform(&:go_to_billing)
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/16' do
            Page::Group::Settings::Billing.perform(&:go_to_free_trial)
            ::QA::Flow::JhTrial.start_trial(group_for_trial.path, skip_select: true)

            ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)

            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Premium Trial Plan"))
                .to be(true)
            end
          end
        end

        context 'with a newly registered user' do
          before do
            visit_free_trial_from_learning_page(group_for_trial.id)
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/20' do
            ::QA::Flow::JhTrial.start_trial(skip_select: true, country: 'Hong Kong')

            ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?).to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("is currently using the Premium Trial Plan")).to be(true)
            end
          end
        end

        private

        # rubocop:disable Layout/LineLength -- the url is longer than the line limit
        def visit_free_trial_from_learning_page(namespace_id)
          visit "#{Runtime::Env.gitlab_url}/-/trials/new?glm_content=onboarding-start-trial&glm_source=gitlab.com&namespace_id=#{namespace_id}"
        end
        # rubocop:enable Layout/LineLength
      end
    end
  end
end
