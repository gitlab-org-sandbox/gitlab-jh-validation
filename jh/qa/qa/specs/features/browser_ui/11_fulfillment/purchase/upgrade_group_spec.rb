# frozen_string_literal: true

module QA
  RSpec.describe 'JH Fulfillment', :smoke, :requires_admin, only: { subdomain: :staging, tld: '.com' } do
    describe 'Purchase' do
      describe 'group plan' do
        let(:admin_api_client) { Runtime::API::Client.as_admin }
        let(:ci_minutes) { 1000 }
        let(:premium_ci_minutes) { 10000 }
        let(:team_ci_minutes) { 2000 }
        let(:max_seats_in_team_plan) { 10 }
        let(:user) do
          Resource::User.fabricate_via_api! do |user|
            user.email = "jihu-qa+#{SecureRandom.hex(4)}@gitlab.cn"
            user.api_client = admin_api_client
            user.hard_delete_on_api_removal = true
          end
        end

        let(:group) do
          Resource::Sandbox.fabricate! do |sandbox|
            sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
          end
        end

        before do
          Flow::Login.sign_in(as: user)
          group.visit!
        end

        after do
          user.remove_via_api!
        end

        it 'upgrades from free to ultimate',
          testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/52' do
          Flow::JhPurchase.upgrade_subscription('ultimate')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Ultimate Plan")).to be(true)
          end
        end

        it 'upgrades from free to premium',
          testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/53' do
          Flow::JhPurchase.upgrade_subscription('premium')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Premium Plan")).to be(true)
          end
        end

        it 'upgrades from free to team',
          testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/54' do
          Flow::JhPurchase.upgrade_subscription('team')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Team Plan")).to be(true)
          end
        end

        it 'upgrades from trial to ultimate plan',
          testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/51' do
          ::QA::Page::Group::Menu.perform(&:go_to_billing)
          Page::Group::Settings::Billing.perform(&:go_to_free_trial)
          ::QA::Flow::JhTrial.start_trial(group.path, skip_select: true)
          ::QA::ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
            expect(free_trial_alert.trial_actived_message?)
              .to eq(true)
          end
          group.visit!

          Flow::JhPurchase.upgrade_subscription('ultimate')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Ultimate Plan")).to be(true)
          end
        end

        it 'upgrades from trial to team plan',
          testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/55' do
          ::QA::Page::Group::Menu.perform(&:go_to_billing)
          Page::Group::Settings::Billing.perform(&:go_to_free_trial)
          ::QA::Flow::JhTrial.start_trial(group.path, skip_select: true)
          ::QA::ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
            expect(free_trial_alert.trial_actived_message?)
              .to eq(true)
          end
          group.visit!

          Flow::JhPurchase.upgrade_subscription('team')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Team Plan")).to be(true)
          end
        end

        context 'when with existing CI minutes pack' do
          let(:ci_minutes_quantity) { 5 }

          before do
            Resource::Project.fabricate_via_api! do |project|
              project.name = 'ci-minutes'
              project.group = group
              project.initialize_with_readme = true
              project.api_client = Runtime::API::Client.as_admin
            end

            Flow::JhPurchase.purchase_ci_minutes(ci_minutes_quantity)
            group.visit!
          end

          it 'upgrades from free to premium with correct CI minutes',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/57' do
            Flow::JhPurchase.upgrade_subscription('premium')

            expected_minutes = ci_minutes * ci_minutes_quantity
            plan_limits = premium_ci_minutes
            group.visit!
            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_content?("#{group.path} is currently using the Premium Plan")).to be(true)
            end

            Page::Group::Menu.perform(&:go_to_usage_quotas)
            Page::Group::Settings::UsageQuotas.perform do |usage_quotas|
              usage_quotas.switch_to_ci_minutes
              expect(usage_quotas.plan_ci_minutes).to eq(plan_limits.to_i)
              expect(usage_quotas.purchased_ci_minutes).to eq(expected_minutes.to_i)
            end
          end

          it 'upgrades from free to team with correct CI minutes',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/58' do
            Flow::JhPurchase.upgrade_subscription('team')

            expected_minutes = ci_minutes * ci_minutes_quantity
            plan_limits = team_ci_minutes
            group.visit!
            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_content?("#{group.path} is currently using the Team Plan")).to be(true)
            end

            Page::Group::Menu.perform(&:go_to_usage_quotas)
            Page::Group::Settings::UsageQuotas.perform do |usage_quotas|
              usage_quotas.switch_to_ci_minutes
              expect(usage_quotas.plan_ci_minutes).to eq(plan_limits.to_i)
              expect(usage_quotas.purchased_ci_minutes).to eq(expected_minutes.to_i)
            end
          end
        end

        context 'when purchase the team plan' do
          it 'exceeds the seats limit',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/56' do
            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform(&:go_to_upgrade_team)
            Flow::JhPurchase.fill_subscription_quantity(max_seats_in_team_plan + 1)

            ::QA::ThirdPartyPage::Customerdot::Subscription.perform do |subscription|
              expect(subscription.has_content?('Subscription exceeds the team plan seats limit')).to be(true)
            end
          end
        end
      end
    end
  end
end
