# frozen_string_literal: true

module QA
  module Flow
    module JhTrial
      extend self

      def start_trial(group_path = nil, skip_select: false, country: 'China', province: '北京市')
        ::QA::ThirdPartyPage::Trials::New.perform do |new|
          new.fill_company_name('Jihu QA')
          new.select_quantity_of_employees('500 - 1,999')
          select_location(country: country, province: province)
          new.fill_telephone('13800000000')
          new.continue_to_trial
        end

        return if skip_select || group_path.nil?

        ::QA::ThirdPartyPage::Trials::Select.perform do |select|
          select.select_group(group_path)
          select.choose_company
          select.start_your_free_trial
        end
      end

      def select_location(country: nil, province: nil)
        ::QA::ThirdPartyPage::Trials::New.perform do |new|
          if country
            new.select_country(country)
            # Skip province selection if the country is not China
            next if country != 'China'
          end

          new.select_province(province)
        end
      end
    end
  end
end
