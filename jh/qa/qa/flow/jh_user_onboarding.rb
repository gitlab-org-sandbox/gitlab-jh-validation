# frozen_string_literal: true

module QA
  module Flow
    module JhUserOnboarding
      extend self

      def jh_onboard_user
        Page::Registration::Welcome.perform do |welcome_page|
          if welcome_page.has_get_started_button?
            welcome_page.select_role('Other')
            welcome_page.select_registration_reason('我想学习Git基础知识')
            welcome_page.jh_choose_setup_for_company_if_available
            welcome_page.choose_what_do_you_want_to_do_if_available
            welcome_page.click_get_started_button
          end
        end
      end
    end
  end
end
