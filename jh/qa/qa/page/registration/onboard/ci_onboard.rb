# frozen_string_literal: true

module QA
  module Page
    module Registration
      module Onboard
        class CiOnboard < Page::Base
          view 'ee/app/assets/javascripts/pages/projects/learn_gitlab/components/learn_gitlab_section_link.vue' do
            element 'uncompleted-learn-gitlab-link'
          end
          view 'app/assets/javascripts/sidebar/components/assignees/assignees.vue' do
            element 'assign-yourself'
          end

          view 'app/assets/javascripts/sidebar/components/assignees/assignee_title.vue' do
            element 'edit-link'
          end

          view 'app/assets/javascripts/sidebar/components/assignees/uncollapsed_assignee_list.vue' do
            element 'username'
          end

          view 'app/assets/javascripts/invite_members/components/invite_modal_base.vue' do
            element 'invite-modal-cancel'
          end

          def go_to_on_boarding_ci_page
            click_on '设置您的第一个项目的 CI/CD'
          end

          def assign_assignee(user)
            click_element('assign-yourself')

            wait_until(reload: false) do
              has_element?('username', text: user, wait: 0)
            end

            refresh
          end

          def close_invite_modal
            click_element('invite-modal-cancel') if has_element?('invite-modal-cancel', wait: 3)
          end

          def start_jh_premium_free_trial
            find('a', text: 'Start a free trial of JiHu GitLab Premium').click
          end
        end
      end
    end
  end
end
