# frozen_string_literal: true

module QA
  module Page
    module Registration
      module GroupsProjects
        class InviteMembers < Page::Base
          view 'app/assets/javascripts/invite_members/components/members_token_select.vue' do
            element 'members-token-select-input'
          end

          view 'app/assets/javascripts/invite_members/components/invite_modal_base.vue' do
            element 'invite-modal'
            element 'access-level-dropdown'
          end

          def add_member(username, access_level = 'Developer')
            within_element('invite-modal-initial-content') do
              fill_element('members-token-select-input', username)
              Support::WaitForRequests.wait_for_requests
              click_button(username, match: :prefer_exact)
              set_access_level(access_level)
            end

            send_invite
          end

          def set_access_level(access_level)
            # Guest option is selected by default, skipping these steps if desired option is 'Guest'
            return if access_level == 'Guest'

            select_element('access-level-dropdown', access_level)
          end

          def send_invite
            click_element 'invite-modal'
            Support::WaitForRequests.wait_for_requests
            page.refresh
          end
        end
      end
    end
  end
end
