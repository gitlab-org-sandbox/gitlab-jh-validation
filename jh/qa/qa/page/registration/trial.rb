# frozen_string_literal: true

module QA
  module Page
    module Registration
      class Trial < Page::Base
        view 'ee/app/assets/javascripts/registrations/components/company_form.vue' do
          element 'company_name'
          element 'company_size'
          element 'phone_number'
          element 'website_url'
        end

        def has_company_name?
          has_element?('company_name', wait: 1)
        end

        def submit_company_trial_info
          fill_element('company_name', "QA company name #{Time.new.to_i}") if has_company_name?
          select_element('company_size', "1 - 99")
          fill_element('phone_number', "130#{Time.new.to_i}")
          fill_element('website_url', "wwww.#{Time.new.to_i}.com")
        end
      end
    end
  end
end
