# frozen_string_literal: true

module QA
  module Page
    module Project
      module Settings
        module Services
          class Ones < QA::Page::Base
            view 'app/assets/javascripts/integrations/edit/components/dynamic_field.vue' do
              element 'service-url-field', ':data-testid="`${fieldId}-field`"'
              element 'service-api_url-field', ':data-testid="`${fieldId}-field`"'
              element 'service-namespace-field', ':data-testid="`${fieldId}-field`"'
              element 'service-project_key-field', ':data-testid="`${fieldId}-field`"'
              element 'service-user_key-field', ':data-testid="`${fieldId}-field`"'
              element 'service-api_token-field', ':data-testid="`${fieldId}-field`"'
            end

            view 'app/assets/javascripts/integrations/edit/components/integration_form_actions.vue' do
              element 'save-changes-button'
            end

            view 'app/assets/javascripts/vue_shared/issuable/list/components/issuable_tabs.vue' do
              element 'opened-issuables-tab', ':data-testid="`${tab.name}-issuables-tab`"'
            end

            view 'app/assets/javascripts/integrations/index/components/integrations_table.vue' do
              element 'ones-link', ':data-testid="`${item.name}-link`"'
            end

            def click_ones_link
              click_element :ones_link
            end

            def has_open_button_element?(text)
              has_element?('opened-issuables-tab', text: text)
            end

            def fill_in_field
              add_service_url(Runtime::Env.ones_service_url)
              add_namespace(Runtime::Env.ones_service_namespace)
              add_project_key(Runtime::Env.ones_service_project_key)
              add_user_key(Runtime::Env.ones_service_user_key)
              add_api_token(Runtime::Env.ones_service_api_token)
              click_save_changes_button
              wait_until(reload: false) do
                has_element?('save-changes-button', wait: 1) ? !find_element('save-changes-button').disabled? : true
              end
            end

            private

            def add_service_url(url)
              fill_element('service-url-field', url)
            end

            def add_namespace(namespace)
              fill_element('service-namespace-field', namespace)
            end

            def add_project_key(project_key)
              fill_element('service-project_key-field', project_key)
            end

            def add_user_key(user_key)
              fill_element('service-user_key-field', user_key)
            end

            def add_api_token(api_token)
              fill_element('service-api_token-field', api_token)
            end

            def click_save_changes_button
              click_element('save-changes-button')
            end
          end
        end
      end
    end
  end
end
