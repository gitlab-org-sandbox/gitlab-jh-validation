# frozen_string_literal: true

module QA
  module Page
    module Project
      module PipelineEditor
        class CiGraphEditor < QA::Page::Base
          view 'jh/app/assets/javascripts/ci/pipeline_editor/components/ui/pipeline_editor_empty_state.vue' do
            element 'create-new-ci-with-graph-editor'
          end

          view 'app/assets/javascripts/ci/pipeline_editor/components/popovers/walkthrough_popover.vue' do
            element 'ctaBtn'
          end

          view 'jh/app/assets/javascripts/ci/pipeline_editor/components/graph/pipeline_graph.vue' do
            element 'add-job-button'
            element 'stage-action-insert-after'
          end

          view 'app/assets/javascripts/ci/pipeline_editor/components/job_assistant_drawer/accordion_items/job_setup_item.vue' do # rubocop:disable QA/ElementWithPattern
            element 'job-name-input'
            element 'job-script-input'
          end

          view 'jh/app/assets/javascripts/ci/pipeline_editor/components/drawer/stage_creation_modal.vue' do
            element 'stage-creation-modal-content'
            element 'stage-creation-name-input'
            element 'stage-creation-modal-confirmation'
          end

          def create_new_ci_with_graph_editor
            click_element 'create-new-ci-with-graph-editor'
          end

          def switch_to_graph_editor_tab
            find('span', text: 'GUI Editor').click
          end

          def switch_to_edit_tab
            within_element('file-editor-container') do
              find('a[role="tab"] span', text: 'Edit', exact_text: true).click
            end
          end

          def dismiss_walkthrough_popover
            click_element 'ctaBtn'
          end

          def add_stage_after(current_stage, new_stage)
            within_element("stage-#{current_stage}") do
              click_element('base-dropdown-toggle')
              click_element('stage-action-insert-after')
            end

            within_element('stage-creation-modal-content') do
              find_element('stage-creation-name-input').fill_in(with: new_stage)
              click_element('stage-creation-modal-confirmation')
            end
          end

          def add_job_for_stage(stage, job_name, job_script)
            within_element("stage-#{stage}") do
              click_element('add-job-button')
            end

            find_element('job-name-input').fill_in(with: job_name)
            find_element('job-script-input').fill_in(with: job_script)
            click_element('confirm-button')
          end

          def edit_pipeline_with_graph_editor
            create_new_ci_with_graph_editor
            dismiss_walkthrough_popover
            switch_to_graph_editor_tab
          end

          def has_pipeline_commit_message?
            within_element('pipeline-commit') do
              has_text?('Update .gitlab-ci.yml file')
            end
          end
        end
      end
    end
  end
end
