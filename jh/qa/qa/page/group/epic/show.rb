# frozen_string_literal: true

module QA
  module Page
    module Group
      module Epic
        class Show < QA::Page::Base
          include QA::Page::Component::Dropdown

          view 'app/assets/javascripts/vue_shared/components/dropdown/dropdown_widget/dropdown_widget.vue' do
            element 'labels-dropdown-content'
          end

          view 'jh/app/assets/javascripts/epic/components/milestone_select.vue' do
            element 'dropdown-milestone'
          end

          view 'jh/app/assets/javascripts/epic/components/iteration_select.vue' do
            element 'dropdown-iteration'
          end

          view 'jh/app/assets/javascripts/epic/components/sidebar_milestone_dropdown.vue' do
            element 'milestone-select'
          end

          view 'jh/app/assets/javascripts/epic/components/sidebar_iteration_dropdown.vue' do
            element 'iteration-select'
          end

          view 'app/assets/javascripts/boards/components/sidebar/board_editable_item.vue' do
            element 'edit-button'
          end

          def select_milestone(milestone)
            click_element('dropdown-milestone')
            within_element 'labels-dropdown-content' do
              click_button(text: milestone)
            end
          end

          def select_iteration(iteration)
            click_element('dropdown-iteration')
            select_item(iteration, css: 'li.gl-new-dropdown-item')
          end

          def edit_milestone(milestone)
            within_element('milestone-select') do
              click_element('edit-button')
            end

            within_element 'labels-dropdown-content' do
              click_button(text: milestone)
            end
          end

          def edit_iteration(iteration)
            within_element('iteration-select') do
              click_element('edit-button')
            end

            select_item(iteration, css: 'li.gl-new-dropdown-item')
          end

          def has_milestone_set_successfully?(milestone)
            has_element?('milestone-select', text: milestone)
          end

          def has_iteration_set_successfully?(iteration)
            has_element?('iteration-select', text: iteration)
          end
        end
      end
    end
  end
end
