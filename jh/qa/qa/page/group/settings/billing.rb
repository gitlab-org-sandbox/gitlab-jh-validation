# frozen_string_literal: true

require 'date'

module QA
  module Page
    module Group
      module Settings
        class Billing < ::QA::Page::Base
          view 'ee/app/helpers/billing_plans_helper.rb' do
            element :"start-your-free-trial-button"
          end

          view 'ee/app/components/billing/plan_component.rb' do
            element "upgrade-to-premium"
            element "upgrade-to-ultimate"
          end

          view 'jh/app/components/new_billing/plan_component.rb' do
            element "upgrade-to-team"
          end

          def go_to_free_trial
            click_element(:"start-your-free-trial-button")
          end

          def go_to_upgrade_premium
            click_element("upgrade-to-premium")
          end

          def go_to_upgrade_ultimate
            click_element("upgrade-to-ultimate")
          end

          def go_to_upgrade_team
            click_element("upgrade-to-team")
          end

          def has_free_trial_widget?
            within("#trial-status-sidebar-widget") do
              has_text?("Premium Trial")
            end
          end

          private

          def calculate_end_date(duration = 60)
            (Date.today + duration).strftime("%Y-%m-%d")
          end
        end
      end
    end
  end
end
