# frozen_string_literal: true

module QA
  module Page
    module Group
      module Settings
        class MergeRequest < QA::Page::Base
          view 'jh/app/views/groups/settings/merge_requests/_merge_requests.html.haml' do
            element 'custom-page-title-input'
            element 'custom-page-url-input'
            element 'save-custom-page-changes-button'
          end

          view 'jh/app/views/projects/merge_requests/_page.html.haml' do
            element 'custom-tab'
          end

          def configure_custom_page(custom_page_title, custom_group_url)
            expand_merge_request_settings_content
            scroll_to_custom_page_settings
            fill_element('custom-page-title-input', custom_page_title)
            fill_element('custom-page-url-input', custom_group_url)
            click_element('save-custom-page-changes-button')
          end

          def has_custom_page?(custom_page_title)
            has_element?('custom-tab', text: custom_page_title)
          end

          private

          def scroll_to_custom_page_settings
            scroll_to_element 'custom-page-title-input'
          end

          def expand_merge_request_settings_content
            within '#js-merge-requests-settings' do
              find('button[type=button]', text: /Expand/).click if has_button?('Expand')
            end
          end
        end
      end
    end
  end
end
