# frozen_string_literal: true

module QA
  module Page
    module Group
      module Settings
        class UsageQuotas < ::QA::Page::Base
          view 'ee/app/assets/javascripts/usage_quotas/storage/components/storage_usage_statistics.vue' do
            element 'purchase-more-storage'
          end

          view 'ee/app/assets/javascripts/usage_quotas/pipelines/components/app.vue' do
            element 'additional-compute-minutes'
            element 'buy-compute-minutes'
            element 'plan-compute-minutes'
          end

          view 'ee/app/assets/javascripts/usage_quotas/pipelines/tab_metadata.js' do
            element 'pipelines-tab'
          end

          view 'app/assets/javascripts/usage_quotas/storage/tab_metadata.js' do
            element 'storage-tab'
          end

          def purchased_ci_minutes
            find_element('additional-compute-minutes').text.split('/').last.match(/\d+/)[0].to_f
          end

          def plan_ci_minutes
            find_element('plan-compute-minutes').text.split('/').last.match(/\d+/)[0].to_f
          end

          def switch_to_ci_minutes
            click_element('pipelines-tab')
          end

          def buy_ci_minutes
            click_element('buy-compute-minutes')
          end

          def switch_to_storage
            click_element('storage-tab')
          end

          def buy_storage
            click_element('purchase-more-storage')
          end

          def has_purchase_more_button?
            has_css?('span.gl-button-text', text: /Purchase more storage/)
          end

          def buy_more_storage
            find('span.gl-button-text', text: /Purchase more storage/).click
          end

          def purchased_storage
            if has_css?('span[data-testid=denominator-total]')
              find('span[data-testid=denominator-total]').text.split('/').last.match(/\d+\.\d+/)[0].to_f
            else
              find_element("storage-purchased").text.split('/').last.match(/\d+\.\d+/)[0].to_f
            end
          end

          def switch_window
            browser = page.driver.browser
            browser.switch_to.window(browser.window_handles.last) if browser.window_handles.length == 2
          end

          def close_window
            browser = page.driver.browser
            return unless browser.window_handles.length == 2

            browser.close
            browser.switch_to.window(browser.window_handles.first)
            refresh
          end
        end
      end
    end
  end
end
