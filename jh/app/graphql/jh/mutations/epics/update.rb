# frozen_string_literal: true

module JH
  module Mutations
    module Epics
      module Update
        extend ActiveSupport::Concern
        extend ::Gitlab::Utils::Override
        include ::Gitlab::Graphql::Authorize::AuthorizeResource

        prepended do
          prepend ::Mutations::Epics::ScopedEpicArguments
        end

        override :resolve
        def resolve(args)
          validate_milestone(args[:milestone_id]) if args.key?(:milestone_id)
          validate_iteration(args[:sprint_id]) if args.key?(:sprint_id)

          super
        end
      end
    end
  end
end
