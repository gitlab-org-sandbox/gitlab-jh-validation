# frozen_string_literal: true

module JH
  module Resolvers
    module EpicsResolver
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      prepended do
        argument :group_milestone_title, GraphQL::Types::String,
          required: false,
          description: "Filter epics by group milestone title, not computed from epic's issues."
      end
    end
  end
end
