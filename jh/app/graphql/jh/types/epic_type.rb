# frozen_string_literal: true

module JH
  module Types
    module EpicType
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      prepended do
        field :milestone, ::Types::MilestoneType,
          null: true, description: 'Milestone of the epic.'
        field :iteration, ::Types::IterationType,
          null: true, description: 'Iteration of the epic.'

        def milestone
          object&.jh_epic&.jh_milestone&.milestone
        end

        def iteration
          object&.jh_epic&.jh_iteration&.iteration
        end
      end
    end
  end
end
