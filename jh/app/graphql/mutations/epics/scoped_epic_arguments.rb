# frozen_string_literal: true

module Mutations
  module Epics
    module ScopedEpicArguments
      extend ActiveSupport::Concern

      prepended do
        argument :milestone_id,
          GraphQL::Types::Int,
          required: false,
          description: 'Milestone ID to be assigned to the epic.'

        argument :sprint_id,
          GraphQL::Types::Int,
          required: false,
          description: 'Iteration ID to be assigned to the epic.'
      end

      private

      # rubocop: disable CodeReuse/ActiveRecord -- need to check if exists in db
      def validate_milestone(milestone_id)
        return if milestone_id.nil?

        milestone = ::Milestone.find_by_id(milestone_id)
        raise_resource_not_available_error! 'Milestone is invalid' unless milestone && milestone.group_milestone?
      end

      def validate_iteration(sprint_id)
        return if sprint_id.nil?

        raise_resource_not_available_error! 'Iteration is invalid' unless ::Iteration.exists?(sprint_id)
      end
      # rubocop: enable CodeReuse/ActiveRecord
    end
  end
end
