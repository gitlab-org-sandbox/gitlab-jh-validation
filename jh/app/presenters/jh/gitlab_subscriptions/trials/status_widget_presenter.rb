# frozen_string_literal: true

module JH
  module GitlabSubscriptions
    module Trials
      module StatusWidgetPresenter
        include BillingPlansHelper
        extend ::Gitlab::Utils::Override

        private

        override :widget_data_attributes
        def widget_data_attributes
          attrs = super
          attrs[:plan_name] = plan_namespaces_map[namespace.gitlab_subscription.plan_name] || attrs[:plan_name]
          attrs
        end
      end
    end
  end
end
