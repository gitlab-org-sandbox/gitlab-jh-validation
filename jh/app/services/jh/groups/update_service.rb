# frozen_string_literal: true

module JH
  module Groups
    module UpdateService
      extend ::Gitlab::Utils::Override
      JH_SETTINGS_PARAMS = [:mr_custom_page_title, :mr_custom_page_url].freeze

      override :execute
      def execute
        unless ::Feature.enabled?(:ff_mr_custom_page_tab, group) && ::Gitlab::Database.jh_database_configured?
          remove_jh_params
          return super
        end

        return super unless jh_namespace_settings_params.present?

        if group.jh_namespace_setting.nil?
          group.build_jh_namespace_setting(jh_namespace_settings_params)
        else
          group.jh_namespace_setting.assign_attributes(jh_namespace_settings_params)
        end

        begin
          group.jh_namespace_setting.save!
        rescue ActiveRecord::RecordInvalid => e
          group.errors.add(:base, e.message)
        end

        remove_jh_params

        super
      end

      private

      def jh_namespace_settings_params
        params.slice(*JH_SETTINGS_PARAMS)
      end

      def remove_jh_params
        JH_SETTINGS_PARAMS.each do |nsp|
          params.delete(nsp)
        end
      end
    end
  end
end
