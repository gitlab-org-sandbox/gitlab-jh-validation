# frozen_string_literal: true

# rubocop:disable GitlabSecurity/PublicSend -- we need send here
#
module JH
  module IssuableBaseService
    extend ::Gitlab::Utils::Override

    attr_accessor :milestone_id, :sprint_id, :upstream_milestone, :upstream_iteration

    JH_EXTEND_MODEL_ZERO_ID = 0
    JH_EPIC_ASSOCIATION_ACTIONS = %i[create update].freeze
    JH_EPIC_ASSOCIATION_IDS = %i[milestone_id sprint_id].freeze

    def initialize(container:, current_user: nil, params: {})
      # it need to support updating milestone and iteration as nil, so set default value to 0
      @milestone_id = JH_EXTEND_MODEL_ZERO_ID
      @sprint_id = JH_EXTEND_MODEL_ZERO_ID
      super
    end

    private

    JH_EPIC_ASSOCIATION_ACTIONS.each do |action|
      override :"prepare_#{action}_params"
      define_method(:"prepare_#{action}_params") do |issuable|
        store_milestone_and_iteration_ids if jh_epic_issuable_type?(issuable)
      end

      JH_EPIC_ASSOCIATION_IDS.each do |association_id|
        define_method(:"able_to_#{action}_jh_epic_with_#{association_id}?") do
          return send(:"#{association_id}_set_value?") if action == :update

          send(association_id) && send(:"#{association_id}_set_value?") if action == :create
        end
      end
    end

    override :create
    def create(issuable, skip_system_notes: false)
      return super unless jh_epic_issuable_type?(issuable)

      process_jh_epic_mileston_iteration(issuable, __method__)
    end

    override :update
    def update(issuable)
      return super unless jh_epic_issuable_type?(issuable)

      process_jh_epic_mileston_iteration(issuable, __method__)
    end

    def process_jh_epic_mileston_iteration(issuable, action)
      # call `super` on each action to get the epic
      epic = method(action).super_method.call(issuable)
      handle_epic_jh_associations!(epic, action)
      epic
    end

    def handle_epic_jh_associations!(upstream_epic, action)
      return unless associate_milestone_and_iteration_to_epic_allowed?(upstream_epic)

      ::Gitlab::Database::QueryAnalyzers::PreventCrossDatabaseModification.temporary_ignore_tables_in_transaction(
        ::Gitlab::Database::JH_IGNORE_TABLES_IN_TRANSACTION, url: "https://jihulab.com/gitlab-cn/gitlab/-/issues/3836"
      ) do
        find_or_init_milestone_iteration_and_jh_extends

        if send(:"able_to_#{action}_jh_epic_with_milestone_id?")
          Extend::Epic.upsert({ epic_id: upstream_epic.id, milestone_id: milestone_id },
            unique_by: %i[epic_id])
        end

        if send(:"able_to_#{action}_jh_epic_with_sprint_id?")
          Extend::Epic.upsert({ epic_id: upstream_epic.id, sprint_id: sprint_id },
            unique_by: %i[epic_id])
        end
      end
    end

    def store_milestone_and_iteration_ids
      self.milestone_id = params.delete(:milestone_id) if params.key?(:milestone_id)
      self.sprint_id = params.delete(:sprint_id) if params.key?(:sprint_id)
    end

    def associate_milestone_and_iteration_to_epic_allowed?(issuable)
      JH::Gitlab.epic_support_milestone_and_iteration? &&
        issuable.group.licensed_feature_available?(:milestone_and_iteration_in_epic)
    end

    def find_or_init_milestone_iteration_and_jh_extends
      if milestone_id && milestone_id_set_value?
        self.upstream_milestone = ::Milestone.find_by_id(milestone_id)
        upstream_milestone&.create_jh_milestone! unless upstream_milestone&.jh_milestone
      end

      return unless sprint_id && sprint_id_set_value?

      self.upstream_iteration = ::Iteration.find_by_id(sprint_id)
      upstream_iteration&.create_jh_iteration! unless upstream_iteration&.jh_iteration
    end

    def milestone_id_set_value?
      milestone_id != JH_EXTEND_MODEL_ZERO_ID
    end

    def sprint_id_set_value?
      sprint_id != JH_EXTEND_MODEL_ZERO_ID
    end

    def jh_epic_issuable_type?(issuable)
      issuable.is_a?(::Epic)
    end
  end
end
# rubocop:enable GitlabSecurity/PublicSend
