# frozen_string_literal: true

module JH
  module PreferredLanguageSwitcher
    include ActionController::Cookies
    extend ActiveSupport::Concern

    prepended do
      private

      def preferred_language
        default_preferred_language =
          ::Feature.enabled?(:qa_enforce_locale_to_en) ? 'en' : ::Gitlab::CurrentSettings.default_preferred_language

        cookies[:preferred_language].presence_in(::Gitlab::I18n.available_locales) ||
          selectable_language(marketing_site_language) ||
          selectable_language(browser_languages) ||
          default_preferred_language
      end
    end
  end
end
