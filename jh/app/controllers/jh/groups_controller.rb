# frozen_string_literal: true

module JH
  module GroupsController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    protected

    override :group_params_attributes
    def group_params_attributes
      super + Groups::UpdateService::JH_SETTINGS_PARAMS
    end
  end
end
