# frozen_string_literal: true

module JH
  module HelpController
    extend ::Gitlab::Utils::Override

    ORIGINAL_EN_DOC_BASE_URL_REGEXP = %r{https?://docs.gitlab.com}

    override :documentation_file_path
    def documentation_file_path
      return super if ORIGINAL_EN_DOC_BASE_URL_REGEXP.match? documentation_base_url

      @documentation_file_path ||= [version_segment, 'jh', "#{@path}.html"].compact.join('/')
    end

    override :path_to_doc
    def path_to_doc(file_name)
      if current_user && current_user.preferred_language != 'zh_CN'
        jh_en_doc_path = Rails.root.join('jh/doc-en', file_name)

        return File.exist?(jh_en_doc_path) ? jh_en_doc_path : super
      end

      Rails.root.join('jh/doc', file_name)
    end
  end
end
