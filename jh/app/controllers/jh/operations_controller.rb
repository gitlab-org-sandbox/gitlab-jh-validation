# frozen_string_literal: true

module JH
  module OperationsController
    include RedirectHomePageWhenNonlogin
  end
end
