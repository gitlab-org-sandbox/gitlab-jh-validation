import Vue from 'vue';
import {
  PRIVATE_VISIBILITY_LEVEL,
  INTERNAL_VISIBILITY_LEVEL,
  PUBLIC_VISIBILITY_LEVEL,
} from './constants';
import NewProjectTerms from './components/new_project_terms.vue';

export function initNewProjectTerms(containerSelector = '') {
  const container = containerSelector ? document.querySelector(containerSelector) : document;
  const el = container.querySelector('.js-new-project-terms');
  if (!el) {
    return;
  }

  const { visibilityLevel } = el.dataset;

  // eslint-disable-next-line no-new
  new Vue({
    el,
    data() {
      return {
        visibilityLevel: parseInt(visibilityLevel, 10),
      };
    },
    mounted() {
      container.addEventListener('click', (e) => {
        const targetElementId = e.target.id;
        const isVisibilityLevelInput = [
          PRIVATE_VISIBILITY_LEVEL,
          INTERNAL_VISIBILITY_LEVEL,
          PUBLIC_VISIBILITY_LEVEL,
        ].some((level) => targetElementId === `project_visibility_level_${level}`);
        if (isVisibilityLevelInput) {
          this.visibilityLevel = parseInt(e.target.value, 10);
        }
      });
    },
    render(h) {
      return h(NewProjectTerms, {
        props: {
          visibilityLevel: this.visibilityLevel,
        },
      });
    },
  });
}
