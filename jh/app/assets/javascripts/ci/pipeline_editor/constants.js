import { TABS_INDEX as CE_TABS_INDEX } from '~/ci/pipeline_editor/constants';

export const GUI_EDITOR_TAB = 'GUI_EDITOR_TAB';

export const TABS_INDEX = {
  ...CE_TABS_INDEX,
  [GUI_EDITOR_TAB]: '4',
};
