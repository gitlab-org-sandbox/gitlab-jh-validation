import 'ee/pages/projects/new/index';
import { initNewProjectTerms } from 'jh/projects/new';

if (window.gon.dot_com) {
  initNewProjectTerms('#create-from-template-pane');
  initNewProjectTerms('#blank-project-pane');
  initNewProjectTerms('#import-project-pane');
  initNewProjectTerms('#ci-cd-project-pane');
}
