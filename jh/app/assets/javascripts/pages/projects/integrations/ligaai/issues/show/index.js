import initLigaaiIssueShow from 'jh/integrations/ligaai/issues_show/ligaai_issues_show_bundle';

initLigaaiIssueShow({ mountPointSelector: '.js-ligaai-issues-show-app' });
