import initLigaaiIssuesList from 'jh/integrations/ligaai/issues_list/ligaai_issues_list_bundle';

initLigaaiIssuesList({ mountPointSelector: '.js-ligaai-issues-list' });
