import { s__ } from '~/locale';

export const LICENSE_MAP = {
  starter: s__('JH|License|Starter'),
  premium: s__('JH|License|Premium'),
  ultimate: s__('JH|License|Ultimate'),
};
