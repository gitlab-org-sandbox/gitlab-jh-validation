import { __ } from '~/locale';
import { LocalizedIssuableAttributeType as EELocalizedIssuableAttributeType } from 'ee_else_ce/sidebar/constants';

export const milestoneDropdownI18n = {
  label: __('Milestone'),
  selectMilestone: __('Select milestone'),
  searchMilestone: __('Search milestones'),
};

export const iterationDropdownI18n = {
  label: __('Iteration'),
};

export const DEFAULT_SELECTED_MILESTONE = {
  id: null,
  title: __('Milestone'),
};

const NO_ITEM_ID = 'NO_ITEM_ID';

export const noMilestone = {
  id: NO_ITEM_ID,
  title: __('No milestone'),
};

export const noIteration = {
  id: NO_ITEM_ID,
  title: __('No iteration'),
};

export const LocalizedIssuableAttributeType = {
  ...EELocalizedIssuableAttributeType,
  Iteration: __('Iteration'),
};

export const sidebarDropdownI18n = {
  expired: __('(expired)'),
  none: __('None'),
  epic: __('Epic'),
};
