export const mockBranchesData = {
  code: 200,
  data: [
    {
      name: 'fix/gb/fix-variables-collection-item-file-stable-ee',
    },
    {
      name: '11-0-stable-ee',
    },
    {
      name: '15-1-stable-jh',
    },
    {
      name: '11-8-stable-ee',
    },
    {
      name: '14-0-stable-jh',
    },
    {
      name: '8-2-stable-ee',
    },
    {
      name: '16-3-stable-ee',
    },
    {
      name: '14-9-stable-ee',
    },
    {
      name: 'hp-test-14-4-stable-ee',
    },
    {
      name: 'update-canonical-15-11-stable-ee',
    },
    {
      name: 'rp/14-8-stable-ee',
    },
    {
      name: '9-1-stable-ee',
    },
    {
      name: '16-3-stable-jh',
    },
    {
      name: '10-8-stable-ee',
    },
    {
      name: '11-4-stable-ee',
    },
    {
      name: '11-10-stable-ee',
    },
    {
      name: '12-0-stable-ee',
    },
    {
      name: '12-9-stable-ee',
    },
    {
      name: '8-0-stable-ee',
    },
    {
      name: '11-5-stable-ee',
    },
    {
      name: '12-1-stable-ee',
    },
    {
      name: 'pre-main-jh',
    },
    {
      name: '11-11-stable-ee',
    },
    {
      name: '10-4-stable-ee',
    },
    {
      name: '15-8-stable-jh',
    },
    {
      name: '15-5-stable-jh',
    },
    {
      name: '14-10-stable-jh',
    },
    {
      name: '8-4-stable-ee',
    },
    {
      name: '16-2-stable-ee',
    },
    {
      name: '8-11-stable-ee',
    },
    {
      name: '8-16-stable-ee',
    },
    {
      name: '14-9-stable-jh',
    },
    {
      name: '14-5-stable-ee',
    },
    {
      name: '12-2-stable-ee',
    },
    {
      name: '14-2-stable-ee',
    },
    {
      name: '9-5-stable-ee',
    },
    {
      name: '15-0-stable-jh',
    },
    {
      name: '12-10-stable-ee',
    },
    {
      name: '14-4-stable-ee',
    },
    {
      name: 'jts/fix-13-12-stable',
    },
    {
      name: '15-6-stable-ee',
    },
    {
      name: '99-4-stable-ee',
    },
    {
      name: 'v1.4.1',
    },
    {
      name: '15-10-stable-ee',
    },
    {
      name: '8-17-stable-ee',
    },
    {
      name: '13-0-stable-ee',
    },
    {
      name: '15-0-stable-ee',
    },
    {
      name: '10-5-stable-ee',
    },
    {
      name: '15-6-stable-jh',
    },
    {
      name: '11-6-stable-ee',
    },
    {
      name: 'fix-gitaly-ci-10-8-stable-ee',
    },
    {
      name: '8-3-stable-ee',
    },
    {
      name: '15-2-stable-ee',
    },
    {
      name: '15-8-stable-ee',
    },
    {
      name: '16-0-stable-jh',
    },
    {
      name: '14-8-stable-ee',
    },
    {
      name: '14-4-9999-stable',
    },
    {
      name: '14-10-stable-ee',
    },
    {
      name: '10-2-stable-ee',
    },
    {
      name: 'sync-sec-15-11-stable-ee',
    },
    {
      name: '8-7-stable-ee',
    },
    {
      name: '8-14-stable-ee',
    },
    {
      name: '2-3-stable',
    },
    {
      name: '16-6-stable-ee',
    },
    {
      name: 'main',
    },
    {
      name: '14-1-stable-ee',
    },
    {
      name: '16-1-stable-jh',
    },
    {
      name: '16-0-stable-ee',
    },
    {
      name: '10-3-stable-ee',
    },
    {
      name: '15-3-stable-jh',
    },
    {
      name: '13-7-stable-ee',
    },
    {
      name: '13-5-stable-ee',
    },
    {
      name: '14-4-stable-jh',
    },
    {
      name: '13-10-stable-jh',
    },
    {
      name: '14-2-stable-jh',
    },
    {
      name: '15-2-stable-jh',
    },
    {
      name: '13-6-stable-ee',
    },
    {
      name: '7-13-stable-ee',
    },
    {
      name: '14-6-stable-ee',
    },
    {
      name: '14-1-stable-jh',
    },
    {
      name: '7-12-stable-ee',
    },
    {
      name: 'main-jh',
    },
    {
      name: '15-7-stable-ee',
    },
    {
      name: '13-2-stable-ee',
    },
    {
      name: '9-3-stable-ee',
    },
    {
      name: 'ce-8-5-stable-to-ee-8-5-stable-ee',
    },
    {
      name: '9-4-stable-ee',
    },
    {
      name: '16-6-stable-jh',
    },
    {
      name: '15-9-stable-jh',
    },
    {
      name: 'merge-15-11-stable-ee',
    },
    {
      name: '8-5-stable-ee',
    },
    {
      name: '14-3-stable-ee',
    },
    {
      name: '11-2-stable-ee',
    },
    {
      name: 'if-205273-project_auth_periodic_recompute-13-1-stable',
    },
    {
      name: '2-2-stable',
    },
    {
      name: '16-4-stable-jh',
    },
    {
      name: '15-11-stable-jh',
    },
    {
      name: '14-5-stable-jh',
    },
    {
      name: '15-4-stable-jh',
    },
    {
      name: '15-4-stable-ee',
    },
    {
      name: '14-7-stable-jh',
    },
    {
      name: '16-4-stable-ee',
    },
    {
      name: '8-6-stable-ee',
    },
    {
      name: '10-1-stable-ee',
    },
    {
      name: '13-10-stable-ee',
    },
    {
      name: '13-4-stable-ee',
    },
    {
      name: '8-9-stable-ee',
    },
    {
      name: 'project-management',
    },
    {
      name: '7-11-stable-ee',
    },
    {
      name: '13-12-stable-ee',
    },
    {
      name: '13-11-stable-ee',
    },
    {
      name: '14-8-stable-jh',
    },
    {
      name: '13-11-stable-jh',
    },
    {
      name: '15-5-stable-ee',
    },
    {
      name: '13-1-stable-ee',
    },
    {
      name: '9-3-actually-stable-ee',
    },
    {
      name: '2-0-stable',
    },
    {
      name: '1-7-stable',
    },
    {
      name: '13-3-stable-ee',
    },
    {
      name: 'fix-karma-test-utils-master-overflow-11-9-stable-ee',
    },
    {
      name: '15-3-stable-ee',
    },
    {
      name: '16-2-stable-jh',
    },
    {
      name: '15-11-stable-ee',
    },
    {
      name: 'rs-pick-bvl-security-stable',
    },
    {
      name: '11-7-stable-ee',
    },
    {
      name: 'revert-feature-from-12-7-stable-ee',
    },
    {
      name: '14-0-stable-ee',
    },
    {
      name: '13-9-stable-ee',
    },
    {
      name: '7-14-stable-ee',
    },
    {
      name: '15-7-stable-jh',
    },
    {
      name: '12-5-stable-ee',
    },
    {
      name: '8-10-stable-ee',
    },
    {
      name: '14-6-stable-jh',
    },
    {
      name: '15-1-stable-ee',
    },
    {
      name: '16-5-stable-jh',
    },
    {
      name: '12-7-stable-ee',
    },
    {
      name: '10-7-stable-ee',
    },
    {
      name: 'state-counter-cache-stable',
    },
    {
      name: '12-8-stable-ee',
    },
    {
      name: 'fix/gb/jobs-queueing-revert-e9e800f5-stable-ee',
    },
    {
      name: '8-1-stable-ee',
    },
    {
      name: 'master',
    },
    {
      name: '12-4-stable-ee',
    },
    {
      name: 'update-gitaly-version-on-12-7-stable-ee',
    },
    {
      name: '8-8-stable-ee',
    },
    {
      name: '8-15-stable-ee',
    },
    {
      name: '8-13-stable-ee',
    },
    {
      name: '13-8-stable-ee',
    },
    {
      name: '13-12-stable-jh',
    },
    {
      name: '14-4-999-stable',
    },
    {
      name: '14-3-stable-jh',
    },
    {
      name: '11-1-stable-ee',
    },
    {
      name: '15-9-stable-ee',
    },
    {
      name: '12-3-stable-ee',
    },
    {
      name: '11-9-stable-ee',
    },
    {
      name: '11-3-stable-ee',
    },
    {
      name: '14-7-stable-ee',
    },
    {
      name: '9-2-stable-ee',
    },
    {
      name: 'hp-test-14-4-3-stable-ee',
    },
    {
      name: 'sh-test-8-9-stable-ee',
    },
    {
      name: '10-6-stable-ee',
    },
    {
      name: '9-0-stable-ee',
    },
    {
      name: '16-1-stable-ee',
    },
    {
      name: '8-12-stable-ee',
    },
    {
      name: '16-5-stable-ee',
    },
    {
      name: '15-10-stable-jh',
    },
    {
      name: '10-0-stable-ee',
    },
    {
      name: '12-6-stable-ee',
    },
  ],
  meta: {},
};

export const mockUsersData = {
  code: 200,
  data: [],
  // data: [
  //   {
  //     id: 232,
  //     name: 'Xuan Xing',
  //     username: 'Xuanxing',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 302,
  //     name: '王凯旋',
  //     username: 'wangkaixuan',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/2757/avatar.png',
  //   },
  //   {
  //     id: 8850,
  //     name: '张鋆',
  //     username: 'junzhang1',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 294,
  //     name: '张健-sales',
  //     username: 'zhangjian0127',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/15209/avatar.png',
  //   },
  //   {
  //     id: 293,
  //     name: '🤖 JiHu Bot 🤖',
  //     username: 'jihulab-bot',
  //     email: null,
  //     avatar_url:
  //       'https://jihulab.com/uploads/-/system/user/avatar/4239/avatar-bot-jihulab-bot.png',
  //   },
  //   {
  //     id: 14,
  //     name: '刘剑桥',
  //     username: 'liujianqiao',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/729/avatar.png',
  //   },
  //   {
  //     id: 339,
  //     name: '张鋆',
  //     username: 'blank581040',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 298,
  //     name: '巧慧 郑',
  //     username: 'Freja',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 24,
  //     name: '胡鹏',
  //     username: 'phu',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/21/avatar.png',
  //   },
  //   {
  //     id: 13,
  //     name: 'huanxin',
  //     username: 'huanxin',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/29008/avatar.png',
  //   },
  //   {
  //     id: 231,
  //     name: 'jing jin',
  //     username: 'jinjing',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/608/avatar.png',
  //   },
  //   {
  //     id: 272,
  //     name: '胡睿智',
  //     username: 'rzhu',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/16/avatar.png',
  //   },
  //   {
  //     id: 254,
  //     name: '李 凯',
  //     username: 'Franky-Li',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/509/avatar.png',
  //   },
  //   {
  //     id: 336,
  //     name: '蔡若南',
  //     username: 'rncai',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 246,
  //     name: 'Shaun McCann',
  //     username: 'shaun',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 4,
  //     name: 'Jing Liu (Grace)',
  //     username: 'higrace',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/5232/avatar.png',
  //   },
  //   {
  //     id: 291,
  //     name: 'ZZJ',
  //     username: 'zzj2',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/11086/avatar.png',
  //   },
  //   {
  //     id: 252,
  //     name: '毛超',
  //     username: 'chaomao',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/1233/avatar.png',
  //   },
  //   {
  //     id: 334,
  //     name: 'rui peng',
  //     username: 'pengrui1',
  //     email: null,
  //     avatar_url: null,
  //   },
  //   {
  //     id: 213,
  //     name: 'Xuefeng Yin',
  //     username: 'xuefeng',
  //     email: null,
  //     avatar_url: 'https://jihulab.com/uploads/-/system/user/avatar/173/avatar.png',
  //   },
  // ],
  meta: {
    current_page: 1,
    next_page: 2,
    prev_page: 0,
    total_page: 8,
    total_count: 150,
    per: 20,
  },
};

export const mockPeopleSummary = {
  code: 200,
  data: {
    active_points: 0,
    active_points_per_capita: 0,
    create_issues: 0,
    create_issues_per_capita: 0,
    issue_comments: 0,
    issue_comments_per_capita: 0,
    dev_duration: 0,
    dev_duration_per_capita: 0,
    merge_request_comments: 0,
    merge_request_comments_per_capita: 0,
    code_lines: 100,
    code_lines_per_capita: 0,
    valid_code_lines: 20,
    valid_code_lines_per_capita: 20,
    blank_code_lines: 0,
    comment_code_lines: 0,
    comment_code_lines_per_capita: 0,
    code_addition_lines: 0,
    code_deletion_lines: 0,
    commits: 0,
    commits_per_capita: 0,
    create_merge_requests: 0,
    create_merge_requests_per_capita: 0,
    review_merge_requests: 0,
    review_merge_requests_per_capita: 0,
  },
  meta: {},
};

export const mockChartData = {
  code: 200,
  data: {
    date_list: [
      '2024-07-06',
      '2024-07-07',
      '2024-07-08',
      '2024-07-09',
      '2024-07-10',
      '2024-07-11',
      '2024-07-12',
      '2024-07-13',
      '2024-07-14',
      '2024-07-15',
      '2024-07-16',
      '2024-07-17',
      '2024-07-18',
      '2024-07-19',
      '2024-07-20',
      '2024-07-21',
      '2024-07-22',
      '2024-07-23',
      '2024-07-24',
      '2024-07-25',
      '2024-07-26',
      '2024-07-27',
      '2024-07-28',
      '2024-07-29',
      '2024-07-30',
      '2024-07-31',
      '2024-08-01',
      '2024-08-02',
      '2024-08-03',
      '2024-08-04',
      '2024-08-05',
    ],
    active_points: [
      10, 20, 30, 35, 22, 20, 20, 0, 0, 0, 10, 0, 0, 40, 0, 45, 23, 0, 44, 0, 0, 29, 0, 39, 0, 50,
      0, 51, 0, 28, 0,
    ],
  },
  meta: {},
};
export const mockTableData = {
  code: 200,
  data: [
    // {
    //   gitlab_user: {
    //     id: 215,
    //     name: 'gy tao',
    //     username: 'zookeeper',
    //   },
    //   active_points: 0,
    //   last_active_date: null,
    //   created_issues_count: 0,
    //   closed_issues_count: 0,
    //   issue_comments_count: 0,
    //   created_merge_requests_count: 0,
    //   merged_merge_requests_count: 0,
    //   merge_request_comments_count: 0,
    //   code_lines_count: 0,
    //   valid_code_lines_count: 0,
    //   comment_code_lines_count: 0,
    //   blank_code_lines_count: 0,
    //   code_addition_lines_count: 0,
    //   code_deletion_lines_count: 0,
    //   commits_count: 0,
    //   successful_pipelines_count: 0,
    //   failed_pipelines_count: 0,
    //   successful_pipelines_rate: 0.0,
    //   dev_duration: 0,
    //   file_change_count: 0,
    //   approved_reviews_count: 0,
    //   assigned_reviews_count: 0,
    // },
  ],
  // {
  //   gitlab_user: {
  //     id: 27313,
  //     name: '赵 辉',
  //     username: 'FerryZhao',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 29441,
  //     name: 'jack tom',
  //     username: 'tandanpgis',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 1,
  //     name: '毛超',
  //     username: 'chaomao',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 213,
  //     name: 'Xuefeng Yin',
  //     username: 'xuefeng',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 4,
  //     name: 'Jing Liu (Grace)',
  //     username: 'higrace',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 214,
  //     name: '彭亮',
  //     username: 'lpeng1991',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 6,
  //     name: 'Gavin Wang',
  //     username: 'gavin',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 16,
  //     name: 'Shiyuan Chen',
  //     username: 'shreychen',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // {
  //   gitlab_user: {
  //     id: 5,
  //     name: 'ren zhitong',
  //     username: 'orozot',
  //   },
  //   active_points: 0,
  //   last_active_date: null,
  //   created_issues_count: 0,
  //   closed_issues_count: 0,
  //   issue_comments_count: 0,
  //   created_merge_requests_count: 0,
  //   merged_merge_requests_count: 0,
  //   merge_request_comments_count: 0,
  //   code_lines_count: 0,
  //   valid_code_lines_count: 0,
  //   comment_code_lines_count: 0,
  //   blank_code_lines_count: 0,
  //   code_addition_lines_count: 0,
  //   code_deletion_lines_count: 0,
  //   commits_count: 0,
  //   successful_pipelines_count: 0,
  //   failed_pipelines_count: 0,
  //   successful_pipelines_rate: 0.0,
  //   dev_duration: 0,
  //   file_change_count: 0,
  //   approved_reviews_count: 0,
  //   assigned_reviews_count: 0,
  // },
  // ],
  meta: {
    current_page: 1,
    next_page: 2,
    prev_page: null,
    total_page: 20,
    total_count: 199,
    per: 10,
  },
};
