import $ from 'jquery';
import MergeRequestTabs from '~/merge_request_tabs';
import { createAlert } from '~/alert';
import { s__ } from '~/locale';
import { checkPageAndAction } from '~/lib/utils/common_utils';
import { isInVueNoteablePage } from '~/lib/utils/dom_utils';
import { renderGFM } from '~/behaviors/markdown/render_gfm';

const shouldLoadPageBundle = () =>
  isInVueNoteablePage() || checkPageAndAction('merge_requests', 'custom_page');

function destroyPipelines(app) {
  if (app && app.$destroy) {
    app.$destroy();

    document.querySelector('#commit-pipeline-table-view').innerHTML = '';
  }

  return null;
}

function toggleLoader(state) {
  $('.mr-loading-status .loading').toggleClass('hide', !state);
}

const pageBundles = {
  show: () => import(/* webpackPrefetch: true */ '~/mr_notes/mount_app'),
  diffs: () => import(/* webpackPrefetch: true */ '~/diffs'),
};

export class ExtendedMergeRequestTabs extends MergeRequestTabs {
  setCurrentAction(action) {
    this.currentAction = action;

    // Remove a trailing '/commits' '/diffs' '/pipelines'
    const { location } = window;
    let newState = location.pathname.replace(
      /\/(commits|diffs|pipelines|custom_page)(\.html)?\/?$/,
      '',
    );

    // Append the new action if we're on a tab other than 'notes'
    if (this.currentAction !== 'show' && this.currentAction !== 'new') {
      newState += `/${this.currentAction}`;
    }

    // Ensure parameters and hash come along for the ride
    newState += location.search + location.hash;

    if (window.location.pathname !== newState) {
      window.history.pushState(
        {
          url: newState,
          action: this.currentAction,
        },
        document.title,
        newState,
      );
    } else {
      window.history.replaceState(
        {
          url: window.location.href,
          action,
        },
        document.title,
        window.location.href,
      );
    }

    return newState;
  }

  tabShown(action, href, shouldScroll = true) {
    toggleLoader(false);

    if (action !== this.currentTab && this.mergeRequestTabs) {
      this.currentTab = action;
      if (this.setUrl) {
        this.setCurrentAction(action);
      }

      if (this.mergeRequestTabPanesAll) {
        this.mergeRequestTabPanesAll.forEach((el) => {
          const tabPane = el;
          tabPane.style.display = 'none';
        });
      }

      if (this.mergeRequestTabsAll) {
        this.mergeRequestTabsAll.forEach((el) => {
          el.classList.remove('active');
        });
      }

      const tabPane = this.mergeRequestTabPanes.querySelector(`#${action}`);
      if (tabPane) tabPane.style.display = 'block';
      const tab = this.mergeRequestTabs.querySelector(`.${action}-tab`);
      if (tab) tab.classList.add('active');

      if (shouldLoadPageBundle() && !this.loadedPages[action] && action in pageBundles) {
        toggleLoader(true);
        pageBundles[action]()
          .then(({ default: init }) => {
            toggleLoader(false);
            init();
            this.loadedPages[action] = true;
          })
          .catch(() => {
            toggleLoader(false);
            createAlert({ message: s__('MergeRequest|Failed to load the page') });
          });
      }

      this.expandSidebar?.forEach((el) =>
        el.classList.toggle('gl-display-none!', action !== 'show'),
      );

      if (action === 'commits') {
        if (!this.commitsLoaded) {
          this.loadCommits(href);
        }
        // this.hideSidebar();
        this.resetViewContainer();
        this.mergeRequestPipelinesTable = destroyPipelines(this.mergeRequestPipelinesTable);
      } else if (action === 'new') {
        this.expandView();
        this.resetViewContainer();
        this.mergeRequestPipelinesTable = destroyPipelines(this.mergeRequestPipelinesTable);
      } else if (this.isDiffAction(action)) {
        if (!shouldLoadPageBundle()) {
          /*
            for pages where we have not yet converted to the new vue
            implementation we load the diff tab content the old way,
            inserting html rendered by the backend.

            in practice, this only occurs when comparing commits in
            the new merge request form page.
          */
          this.loadDiff({ endpoint: href, strip: true });
        }
        // this.hideSidebar();
        this.expandViewContainer();
        this.mergeRequestPipelinesTable = destroyPipelines(this.mergeRequestPipelinesTable);
        this.commitsTab.classList.remove('active');
      } else if (action === 'pipelines') {
        // this.hideSidebar();
        this.resetViewContainer();
        this.mountPipelinesView();
      } else if (action === 'custom_page') {
        this.resetViewContainer();
      } else {
        const notesTab = this.mergeRequestTabs.querySelector('.notes-tab');
        const notesPane = this.mergeRequestTabPanes.querySelector('#notes');
        if (notesPane) {
          notesPane.style.display = 'block';
        }
        if (notesTab) {
          notesTab.classList.add('active');
        }

        // this.showSidebar();
        this.resetViewContainer();
        this.mergeRequestPipelinesTable = destroyPipelines(this.mergeRequestPipelinesTable);
      }

      renderGFM(document.querySelector('.detail-page-description'));

      if (shouldScroll) this.recallScroll(action);
    } else if (action === this.currentAction) {
      // ContentTop is used to handle anything at the top of the page before the main content
      const mainContentContainer = document.querySelector('.content-wrapper');
      const tabContentContainer = document.querySelector('.tab-content');

      if (mainContentContainer && tabContentContainer) {
        const mainContentTop = mainContentContainer.getBoundingClientRect().top;
        const tabContentTop = tabContentContainer.getBoundingClientRect().top;

        // 51px is the height of the navbar buttons, e.g. `Discussion | Commits | Changes`
        const scrollDestination = tabContentTop - mainContentTop - 51;

        // scrollBehavior is only available in browsers that support scrollToOptions
        if ('scrollBehavior' in document.documentElement.style && shouldScroll) {
          window.scrollTo({
            top: scrollDestination,
            behavior: 'smooth',
          });
        } else {
          window.scrollTo(0, scrollDestination);
        }
      }
    }

    this.eventHub.$emit('MergeRequestTabChange', action);
  }
}

const MergeRequestTabsToExport = gon?.features?.ffMrCustomPageTab
  ? ExtendedMergeRequestTabs
  : MergeRequestTabs;

export default MergeRequestTabsToExport;
