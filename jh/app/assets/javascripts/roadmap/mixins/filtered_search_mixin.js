import EEFilteredSearchMixin from 'ee/roadmap/mixins/filtered_search_mixin';
import axios from '~/lib/utils/axios_utils';
import MilestoneToken from '~/vue_shared/components/filtered_search_bar/tokens/milestone_token.vue';
// eslint-disable-next-line @jihu-fe/prefer-ee-modules
import { OPERATORS_IS } from '~/vue_shared/components/filtered_search_bar/constants';
import { TOKEN_TYPE_MILESTONE_GROUP, TOKEN_TITLE_MILESTONE_GROUP } from './constants';

export default {
  mixins: [EEFilteredSearchMixin],
  methods: {
    getFilteredSearchTokens({ supportsEpic = true } = {}) {
      const eeTokens = EEFilteredSearchMixin.methods.getFilteredSearchTokens.call(this, {
        supportsEpic,
      });

      if (!window.gon.epic_support_milestone_and_iteration) return eeTokens;

      return [
        ...eeTokens,
        {
          type: TOKEN_TYPE_MILESTONE_GROUP,
          icon: 'clock',
          title: TOKEN_TITLE_MILESTONE_GROUP,
          unique: true,
          symbol: '%',
          token: MilestoneToken,
          operators: OPERATORS_IS,
          defaultMilestones: [],
          fetchMilestones: (search = '') => {
            return axios.get(`${this.groupMilestonesPath}?only_group=true`).then(({ data }) => {
              if (search) {
                return {
                  data: data.filter((m) => m.title.toLowerCase().includes(search.toLowerCase())),
                };
              }
              return { data };
            });
          },
        },
      ];
    },
    getFilterParams(filters = []) {
      const filterParams = EEFilteredSearchMixin.methods.getFilterParams.call(this, filters);

      const groupMilestone = filters.find((f) => f.type === TOKEN_TYPE_MILESTONE_GROUP);
      if (groupMilestone) {
        filterParams.groupMilestoneTitle = groupMilestone.value.data;
      }

      return filterParams;
    },
  },
};
