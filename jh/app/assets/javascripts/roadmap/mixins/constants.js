import { s__ } from '~/locale';

export const TOKEN_TYPE_MILESTONE_GROUP = 'milestone-group';
export const TOKEN_TITLE_MILESTONE_GROUP = s__('JH|SearchToken|Milestone(Epic)');
