import Vue from 'vue';
import NewFields from 'jh/issues/components/new_fields.vue';
import { apolloProvider } from '~/graphql_shared/issuable_client';
import { WORKSPACE_PROJECT } from '~/issues/constants';

export function addNewFieldsOnForm(issuableType = WORKSPACE_PROJECT) {
  if (!(gon.jh_custom_labels_enabled && gon.jh_custom_labels?.issue?.length > 0)) {
    return;
  }

  const newFieldsRoot = document.querySelector('.js-customize-labels');
  const { fullPath, rowClass } = newFieldsRoot.dataset;

  // eslint-disable-next-line no-new
  new Vue({
    el: newFieldsRoot,
    apolloProvider,
    render(createElement) {
      return createElement(NewFields, {
        props: {
          issuableType,
          customLabels: gon.jh_custom_labels.issue,
          fullPath,
          rootClassName: rowClass,
        },
      });
    },
  });
}
