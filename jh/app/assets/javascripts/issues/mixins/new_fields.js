import { createAlert } from '~/alert';
import { getIdFromGraphQLId } from '~/graphql_shared/utils';
// eslint-disable-next-line @jihu-fe/prefer-ee-modules
import { workspaceLabelsQueries } from '~/sidebar/queries/constants';
import { sprintf } from '~/locale';
import { i18n, SCOPE_SEPARATOR } from '../constants';

export default {
  props: {
    fullPath: {
      type: String,
      required: true,
    },
    rootClassName: {
      type: String,
      required: false,
      default: '',
    },
  },
  apollo: {
    scopedLabels: {
      query() {
        return workspaceLabelsQueries[this.issuableType].query;
      },
      variables() {
        const queryVariables = {
          fullPath: this.fullPath,
        };

        return queryVariables;
      },
      update: (data) => data.workspace?.labels?.nodes || [],
      error() {
        createAlert({ message: i18n.FETCH_LABELS_ERROR_MESSAGE });
      },
      skip() {
        return !this.jhCustomizedLabelEnabled;
      },
    },
  },
  data() {
    return {
      selectedLabels: [],
      dropdownStates: [],
      scopedLabels: [],
    };
  },
  computed: {
    labelFetchingLoading() {
      return this.$apollo.queries.labels.loading;
    },
    jhCustomizedLabelEnabled() {
      return (
        gon.jh_custom_labels_enabled && gon.jh_custom_labels?.[this.customizeLabelType]?.length > 0
      );
    },
    customLabels() {
      return window.gon.jh_custom_labels?.[this.customizeLabelType] || [];
    },
    customLabelFormInvalid() {
      return this.selectedLabels
        .map(
          (selectedLabel, index) =>
            selectedLabel !== undefined || !this.customLabels[index]?.required,
        )
        .includes(false);
    },
  },
  methods: {
    validateForm(event) {
      if (this.customLabelFormInvalid) {
        event.preventDefault();
      }
    },
    findLabelsInScope(scope) {
      return this.scopedLabels
        .filter((label) => label.title.includes(`${scope}${SCOPE_SEPARATOR}`))
        .map(({ title, id }) => ({
          text: title.split(`${SCOPE_SEPARATOR}`).pop(),
          value: getIdFromGraphQLId(id),
        }));
    },
    findLabelTextById(id) {
      const labelFound = this.scopedLabels.find((label) => label.id === id);
      return labelFound ? labelFound.title.split(`${SCOPE_SEPARATOR}`).pop() : '';
    },
    toggleText(scope, index) {
      return this.selectedLabels[index] !== undefined
        ? this.findLabelTextById(this.selectedLabels[index])
        : this.headerText(scope);
    },
    toggleClass(index) {
      return this.dropdownStates[index]
        ? ''
        : 'gl-field-error-outline gl-field-error-outline gl-border-1!';
    },
    headerText(scope) {
      return sprintf(i18n.SELECT_SCOPE, {
        scope,
      });
    },
    groupLabel(customLabel) {
      return customLabel.scope + (customLabel.required ? ` ${i18n.REQUIRED}` : '');
    },
  },
  mounted() {
    this.selectedLabels = Array.from({ length: this.customLabels.length });
    this.dropdownStates = this.customLabels.map(() => true);
  },
};
