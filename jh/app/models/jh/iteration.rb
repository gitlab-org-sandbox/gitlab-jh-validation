# frozen_string_literal: true

module JH
  module Iteration
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      has_one :jh_iteration, class_name: 'Extend::Iteration', foreign_key: 'sprint_id'
      has_many :jh_epics, through: :jh_iteration, class_name: 'Extend::Epic', disable_joins: true
    end
  end
end
