# frozen_string_literal: true

module JH
  module Milestone
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      has_one :jh_milestone, class_name: 'Extend::Milestone'
      has_many :jh_epics, through: :jh_milestone, class_name: 'Extend::Epic', disable_joins: true

      scope :pluck_ids, -> { pluck(:id) }
      scope :for_groups, -> { where.not(group: nil) }
      scope :by_title, ->(title) { where(title: title) }
    end
  end
end
