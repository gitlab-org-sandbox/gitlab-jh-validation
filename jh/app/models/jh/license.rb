# frozen_string_literal: true

module JH
  module License
    class ::License
      extend ActiveSupport::Concern

      TEAM_PLAN = 'team'

      remove_const :EE_ALL_PLANS
      EE_ALL_PLANS = [TEAM_PLAN, STARTER_PLAN, PREMIUM_PLAN, ULTIMATE_PLAN].freeze

      def paid?
        [TEAM_PLAN, ::License::STARTER_PLAN, ::License::PREMIUM_PLAN, ::License::ULTIMATE_PLAN].include?(plan)
      end
    end
  end
end
