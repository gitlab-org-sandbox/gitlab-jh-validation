# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module Projects
    module Topic
      extend ActiveSupport::Concern

      prepended do
        include ContentValidateable
        validates :name, content_validation: true, if: :should_validate_content?
      end
    end
  end
end
