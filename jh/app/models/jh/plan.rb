# frozen_string_literal: true

module JH
  module Plan
    module ::EE
      module Plan
        extend ActiveSupport::Concern
        extend ::Gitlab::Utils::Override

        TEAM = 'team'

        temp_paid_hosted_plans = PAID_HOSTED_PLANS.dup
        remove_const :PAID_HOSTED_PLANS
        PAID_HOSTED_PLANS = (temp_paid_hosted_plans + [TEAM]).freeze

        temp_ee_all_plans = EE_ALL_PLANS.dup
        remove_const :EE_ALL_PLANS
        EE_ALL_PLANS = (temp_ee_all_plans + [TEAM]).freeze
      end
    end
  end
end
