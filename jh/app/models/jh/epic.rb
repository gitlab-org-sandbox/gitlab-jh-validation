# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module Epic
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      include ContentValidateable
      has_one :jh_epic, class_name: 'Extend::Epic'

      validates :title, :description, content_validation: true, if: :should_validate_content?

      scope :pluck_ids, -> { pluck(:id) }
      scope :by_ids, ->(ids) { where(id: ids) }
    end
  end
end
