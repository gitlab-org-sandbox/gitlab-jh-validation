# frozen_string_literal: true

module Extend
  class Iteration < JH::ApplicationRecord
    self.table_name = 'jh_sprint_extends'

    belongs_to :iteration, class_name: '::Iteration', inverse_of: :jh_iteration,
      foreign_key: 'sprint_id'
    has_many :jh_epics, class_name: 'Extend::Epic', inverse_of: :jh_iteration,
      foreign_key: 'sprint_id', primary_key: 'sprint_id'
  end
end
