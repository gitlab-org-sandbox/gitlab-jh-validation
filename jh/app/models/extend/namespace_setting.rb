# frozen_string_literal: true

module Extend
  class NamespaceSetting < JH::ApplicationRecord
    self.table_name = 'jh_namespace_setting_extends'

    attribute :mr_custom_page_title, default: ''
    attribute :mr_custom_page_url, default: ''

    validates :mr_custom_page_url, length: { maximum: 2048 },
      addressable_url: { ascii_only: true }, allow_blank: true
    belongs_to :namespace, class_name: '::Namespace', inverse_of: :jh_namespace_setting
  end
end
