# frozen_string_literal: true

module Extend
  class Milestone < JH::ApplicationRecord
    self.table_name = 'jh_milestone_extends'

    belongs_to :milestone, class_name: '::Milestone', inverse_of: :jh_milestone
    has_many :jh_epics, class_name: 'Extend::Epic', inverse_of: :jh_milestone, primary_key: 'milestone_id'
  end
end
