# frozen_string_literal: true

module Extend
  class Epic < JH::ApplicationRecord
    self.table_name = 'jh_epic_extends'

    belongs_to :epic, class_name: '::Epic', inverse_of: :jh_epic
    belongs_to :jh_milestone, class_name: 'Extend::Milestone', inverse_of: :jh_epics,
      foreign_key: 'milestone_id', primary_key: 'milestone_id'
    belongs_to :jh_iteration, class_name: 'Extend::Iteration', inverse_of: :jh_epics,
      foreign_key: 'sprint_id', primary_key: 'sprint_id'

    scope :pluck_epic_ids, -> { pluck(:epic_id) }
    scope :in_upstream_epics, ->(epic_ids) { where(epic_id: epic_ids) }
    scope :for_upstream_milestones, ->(milestone_ids) { where(milestone_id: milestone_ids) }
  end
end
