# frozen_string_literal: true

module JH
  module Epics
    module Findable
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      private

      override :by_milestone
      def by_milestone(items)
        return by_jh_group_milestone(items) if params[:group_milestone_title].present?

        super
      end

      def by_jh_group_milestone(items)
        items.by_ids(group_milestone_epic_ids(items))
      end

      def group_milestone_epic_ids(items)
        return [] unless filter_by_jh_group_milestone_allowed?

        Extend::Epic.in_upstream_epics(items.pluck_ids)
          .for_upstream_milestones(matched_group_milestone_ids)
          .pluck_epic_ids
      end

      def matched_group_milestone_ids
        ::Milestone.for_groups.by_title(params[:group_milestone_title]).pluck_ids
      end

      def filter_by_jh_group_milestone_allowed?
        JH::Gitlab.epic_support_milestone_and_iteration? &&
          params.group.licensed_feature_available?(:milestone_and_iteration_in_epic)
      end
    end
  end
end
