# frozen_string_literal: true

module JH
  module TrialHelper
    extend ::Gitlab::Utils::Override
    include ::Gitlab::Utils::StrongMemoize
    # override EE::TrialHelper method

    override :create_lead_form_data
    def create_lead_form_data
      return super unless ::Gitlab.com? && ::Gitlab.jh?

      {
        phone_number: ::Gitlab::CryptoHelper.aes256_gcm_decrypt(current_user.phone),
        country: 'CN'
      }.merge(super)
    end

    override :glm_params
    def glm_params
      super.merge(utm_params)
    end
    strong_memoize_attr :glm_params

    def utm_params
      params.permit(:utm_source, :utm_medium, :utm_keyword).to_h
    end
  end
end
