# frozen_string_literal: true

module JH
  module TreeHelper
    extend ::Gitlab::Utils::Override

    override :directory_download_links
    def directory_download_links(project, ref, archive_prefix)
      return if ::Gitlab::CurrentSettings.disable_download_button_enabled?

      super(project, ref, archive_prefix)
    end
  end
end
