# frozen_string_literal: true

module JH
  module EpicsHelper
    extend ::Gitlab::Utils::Override

    override :epic_show_app_data
    def epic_show_app_data(epic)
      {}.merge(super(epic))
    end
  end
end
