# frozen_string_literal: true

module JH
  module MergeRequestsHelper
    extend ::Gitlab::Utils::Override

    # rubocop: disable Style/ArgumentsForwarding
    override :tab_link_for
    def tab_link_for(merge_request, tab, options = {}, &block)
      return super(merge_request, tab, options, &block) unless tab == :custom_page

      data_attrs = {
        action: tab.to_s,
        target: "##{tab}",
        toggle: options.fetch(:force_link, false) ? '' : 'tabvue'
      }

      url = method(:custom_page_project_merge_request_path)
      link_to(url[merge_request.project, merge_request], data: data_attrs, &block)
    end
    # rubocop: enable Style/ArgumentsForwarding

    override :sticky_header_data
    def sticky_header_data(project, merge_request)
      return super unless custom_mr_page_enabled?(merge_request, project)

      data = super

      data[:tabs].push(['custom_page', merge_request.project.group.mr_custom_page_title,
        custom_page_project_merge_request_path(project, merge_request), nil])

      data
    end

    def custom_page_configured?(project)
      ::Feature.enabled?(:ff_mr_custom_page_tab, project.group) && ::Gitlab::Database.jh_database_configured?
    end

    def custom_mr_page_enabled?(merge_request, project)
      group = project.group
      return false unless custom_page_configured?(project) && group.present?

      mr_custom_page_url(merge_request).present? && group.mr_custom_page_title.present?
    end

    def mr_custom_page_url(merge_request)
      return unless merge_request.project.group&.mr_custom_page_url

      values = {
        '$MERGE_REQUEST_IID' => merge_request.iid,
        '$PROJECT_ID' => merge_request.project.id,
        '$USER_ID' => current_user&.id,
        '$USERNAME' => current_user&.username
      }

      merge_request.project.group.mr_custom_page_url.gsub(/\$\w+/) do |keyword|
        values[keyword] || keyword
      end
    end
  end
end
