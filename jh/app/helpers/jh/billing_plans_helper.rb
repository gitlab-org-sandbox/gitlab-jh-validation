# frozen_string_literal: true

module JH
  module BillingPlansHelper
    extend ::Gitlab::Utils::Override

    override :number_to_plan_currency
    def number_to_plan_currency(value)
      number_to_currency(value, unit: '¥', strip_insignificant_zeros: true, format: "%u%n")
    end

    override :billing_available_plans
    def billing_available_plans(plans_data, current_plan)
      filtered_data = super(plans_data, current_plan)

      team_plan = filtered_data.find { |plan| plan.code == 'team' }
      filtered_data = filtered_data.reject { |plan| plan.code == 'team' || plan.code == 'free' }
      filtered_data.unshift(team_plan) if team_plan

      filtered_data
    end

    def billing_plan_name(plan)
      plan_namespaces_map[plan]
    end

    override :plans_features
    def plans_features
      features = super
      features[:team] = [
        {
          title: s_('JH|BillingPlans|2000 CI/CD minutes per month')
        },
        {
          title: s_('JH|BillingPlans|Up to 10 users per top-level group')
        },
        {
          title: s_('JH|BillingPlans|AI empowered intelligent programming and DevOps')
        }
      ]

      features
    end

    override :plan_purchase_url
    def plan_purchase_url(group, plan)
      new_subscriptions_path(plan_id: plan.id, namespace_id: group.id, source: params[:source])
    end

    private

    def plan_namespaces_map
      {
        free: s_('JH|License|Free'),
        team: s_('JH|License|Team'),
        premium: s_('JH|License|Premium'),
        ultimate: s_('JH|License|Ultimate'),
        starter: s_('JH|License|Starter'),
        premium_trial: s_('JH|License|Premium Trial'),
        ultimate_trial: s_('JH|License|Ultimate Trial')
      }.with_indifferent_access.freeze
    end
  end
end
