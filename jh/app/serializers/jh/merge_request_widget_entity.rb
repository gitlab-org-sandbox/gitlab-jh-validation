# frozen_string_literal: true

module JH
  module MergeRequestWidgetEntity
    extend ActiveSupport::Concern

    prepended do
      expose :mr_custom_page_title, if: ->(_) { mr_custom_page_enable? } do |merge_request|
        merge_request.project.group&.mr_custom_page_title
      end

      expose :mr_custom_page_url, if: ->(_) { mr_custom_page_enable? } do |merge_request|
        mr_custom_page_url(merge_request)
      end

      private

      def mr_custom_page_enable?
        ::Feature.enabled?(:ff_mr_custom_page_tab, object.project.group) && ::Gitlab::Database.jh_database_configured?
      end

      def mr_custom_page_url(merge_request)
        return unless merge_request.project.group&.mr_custom_page_url

        values = {
          '$MERGE_REQUEST_IID' => merge_request.iid,
          '$PROJECT_ID' => merge_request.project.id,
          '$USER_ID' => current_user&.id,
          '$USERNAME' => current_user&.username
        }

        merge_request.project.group.mr_custom_page_url.gsub(/\$\w+/) do |keyword|
          values[keyword] || keyword
        end
      end
    end
  end
end
