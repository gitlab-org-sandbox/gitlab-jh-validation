# frozen_string_literal: true

module JH
  module Ci
    module BuildPolicy
      extend ActiveSupport::Concern

      prepended do
        condition(:troubleshoot_job_with_ai_authorized) do
          false
        end
      end
    end
  end
end
