# frozen_string_literal: true

module JH
  module GroupPolicy
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      condition(:performance_analytics_available, scope: :subject) do
        ::Feature.enabled?(:performance_analytics, @subject) &&
          @subject.licensed_feature_available?(:performance_analytics)
      end

      rule { (admin | reporter | auditor) & performance_analytics_available }
        .enable :read_performance_analytics_metrics
    end
  end
end
