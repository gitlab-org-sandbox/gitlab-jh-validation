# frozen_string_literal: true

module JH
  module Billing
    module PlanComponent
      extend ::Gitlab::Utils::Override

      override :annual_price_text
      def annual_price_text
        s_("JH|BillingPlans|Billed annually at %{price_per_year}") % { price_per_year: price_per_year }
      end

      override :trial?
      def trial?
        current_plan.code == ::Plan::PREMIUM_TRIAL
      end

      override :learn_more_url
      def learn_more_url
        "https://gitlab.cn/pricing/#{plan.code}"
      end

      override :currency_symbol
      def currency_symbol
        "¥"
      end

      override :plans_data
      def plans_data
        plans = {
          'free' => {
            features: [
              {
                title: s_("JH|BillingPlans|Bring your own JiHu GitLab CI runners")
              },
              {
                title: s_("JH|BillingPlans|5 users per top-level private group")
              },
              {
                title: s_("BillingPlans|400 CI/CD minutes per month")
              },
              {
                title: s_("JH|BillingPlans|5GB storage per project")
              }
            ]
          },
          "premium" => {
            features: [
              {
                title: s_("JH|BillingPlans|Everything from Free")
              },
              {
                title: s_("JH|BillingPlans|Efficient code review")
              },
              {
                title: s_("BillingPlans|Advanced CI/CD")
              },
              {
                title: s_("JH|BillingPlans|Enterprise agile management")
              },
              {
                title: s_("BillingPlans|Enterprise User and Incident Management")
              },
              # Removed for avoiding ambiguity
              # {
              #   title: s_("BillingPlans|50GB storage")
              # },
              {
                title: s_("BillingPlans|10,000 CI/CD minutes per month")
              },
              {
                title: s_("JH|BillingPlans|Professional technical support")
              },
              {
                title: s_("JH|BillingPlans|5GB storage per project")
              }
            ]
          },
          "ultimate" => {
            features: [
              {
                title: s_("JH|BillingPlans|Everything from Premium")
              },
              {
                title: s_("JH|BillingPlans|Advanced security testing")
              },
              {
                title: s_("BillingPlans|Dynamic Application Security Testing")
              },
              {
                title: s_("BillingPlans|Security Dashboards")
              },
              {
                title: s_("BillingPlans|Portfolio Management")
              },
              {
                title: s_("JH|BillingPlans|Value stream analysis")
              },
              {
                title: s_("BillingPlans|Free guest users")
              },
              {
                title: s_("BillingPlans|50,000 CI/CD minutes per month")
              },
              {
                title: s_("JH|BillingPlans|Professional technical support")
              },
              {
                title: s_("JH|BillingPlans|5GB storage per project")
              }
            ]
          }
        }
        super.tap do |data|
          data.each do |k, v|
            v.merge!(plans.fetch(k, {}))
          end
        end
      end
    end
  end
end
