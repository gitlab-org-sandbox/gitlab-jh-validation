# frozen_string_literal: true

module NewBilling
  class PlanComponent < ::Billing::PlanComponent
    def price_per_year_without_currency
      number_to_currency(plan.price_per_year, unit: '', strip_insignificant_zeros: true)
    end

    def plans_data
      plans = {
        "team" => {
          header_classes: "gl-bg-none",
          elevator_pitch: s_("JH|BillingPlans|Use JiHu GitLab for personal projects"),
          features_elevator_pitch: s_("JH|BillingPlans|Team features:"),
          features: [
            {
              title: s_("JH|BillingPlans|2000 CI/CD minutes per month")
            },
            {
              title: s_("JH|BillingPlans|10 users per top-level group")
            },
            {
              title: s_('JH|BillingPlans|5GB storage per project')
            },
            {
              title: s_('JH|BillingPlans|AI empowered intelligent programming and DevOps')
            }
          ],
          cta_text: s_("JH|BillingPlans|Upgrade to Team"),
          cta_category: {
            free: "primary",
            trial: 'secondary'
          },
          cta_data: {
            testid: "upgrade-to-team"
          }
        },
        "premium" => {
          features: [
            {
              title: s_("JH|BillingPlans|Everything from Team")
            },
            {
              title: s_("JH|BillingPlans|Efficient code review")
            },
            {
              title: s_("BillingPlans|Advanced CI/CD")
            },
            {
              title: s_("JH|BillingPlans|Enterprise agile management")
            },
            {
              title: s_("BillingPlans|Enterprise User and Incident Management")
            },
            {
              title: s_("BillingPlans|10,000 CI/CD minutes per month")
            },
            {
              title: s_("JH|BillingPlans|Professional technical support")
            },
            {
              title: s_("JH|BillingPlans|5GB storage per project")
            }
          ]
        },
        "ultimate" => {
          features: [
            {
              title: s_("JH|BillingPlans|Everything from Premium")
            },
            {
              title: s_("JH|BillingPlans|Advanced security testing")
            },
            {
              title: s_("BillingPlans|Dynamic Application Security Testing")
            },
            {
              title: s_("BillingPlans|Security Dashboards")
            },
            {
              title: s_("BillingPlans|Portfolio Management")
            },
            {
              title: s_("JH|BillingPlans|Value stream analysis")
            },
            {
              title: s_("BillingPlans|Free guest users")
            },
            {
              title: s_("BillingPlans|50,000 CI/CD minutes per month")
            },
            {
              title: s_("JH|BillingPlans|Professional technical support")
            },
            {
              title: s_("JH|BillingPlans|5GB storage per project")
            }
          ]
        }
      }

      plans.tap do |data|
        data.each do |plan_code, plan|
          plan.merge!(super.fetch(plan_code, {}))
        end
      end
    end
  end
end
