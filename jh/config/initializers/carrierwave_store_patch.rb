# frozen_string_literal: true

require "carrierwave/storage/fog"

module CarrierWave
  module Storage
    class Fog < Abstract
      class File
        alias_method :original_store, :store

        DIRECTORY_FOR_SECURITY_REVIEW = %w[@hashed design_management namespace personal_snippet].freeze

        def store(new_file)
          # This patch is used to solve the problem of Tencent COS skipping security review
          # Related issues: https://jihulab.com/gitlab-cn/internal/content-security-backend/-/issues/41
          #
          # The default behavior of CarrierWave is to upload first and then copy, which will cause COS
          # to skip the security review.
          # The behavior after overwriting is: no copy, just created directly.
          unless new_file.is_a?(self.class) &&
              ::Feature.enabled?(:ff_direct_upload_to_final_path) &&
              DIRECTORY_FOR_SECURITY_REVIEW.any? { |dir| path.start_with?(dir) }
            return original_store(new_file)
          end

          @content_type ||= new_file.content_type
          @file = directory.files.create({
            body: new_file.read,
            content_type: @content_type,
            key: path,
            public: @uploader.fog_public
          }.merge(@uploader.fog_attributes))
          true
        rescue StandardError => e
          Gitlab::AppLogger.error("Error storing file via direct upload: #{e.message}")
          original_store(new_file)
        end
      end
    end
  end
end
