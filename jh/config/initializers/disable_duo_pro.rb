# frozen_string_literal: true

# check https://jihulab.com/gitlab-cn/gitlab/-/issues/4109#note_4620652
# rubocop:disable Database/MultipleDatabases -- check features exists
if Rails.env.production? &&
    ::Gitlab::Database::Reflection.new(ActiveRecord::Base).exists? &&
    ActiveRecord::Base.connection.table_exists?('features') &&
    Feature.enabled?(:self_managed_code_suggestions)
  Feature.disable(:self_managed_code_suggestions)
end
# rubocop:enable Database/MultipleDatabases
