# frozen_string_literal: true

Devise.setup do |config|
  # inject cas3/dingtalk provider into devise omniauth providers in test env,
  # because upstream has removed all cas3/dingtalk config from the
  # gitlab.yml.example, so the feature test can't find the cas3/dingtalk button
  if Rails.env.test? && Gitlab::Auth.omniauth_enabled?
    cas3_provider = YAML.load_file(Rails.root.join('jh/spec/config/cas3.yml'))['test']['omniauth']['providers'][0]
    dingtalk_provider = { 'name' => 'dingtalk', :app_id => 'YOUR_APP_ID', :app_secret => 'YOUR_APP_SECRET' }
    Gitlab::OmniauthInitializer.new(config).execute([GitlabSettings::Options.build(cas3_provider), dingtalk_provider])
  end

  # override allow_unconfirmed_access_for
  break unless ::Gitlab.com? && ::Gitlab.jh?

  config.allow_unconfirmed_access_for = 60.days
end
