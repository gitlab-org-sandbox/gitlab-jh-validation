# frozen_string_literal: true

# check https://jihulab.com/gitlab-cn/gitlab/-/issues/4220
# rubocop:disable Database/MultipleDatabases -- check features exists
if Rails.env.production? &&
    ::Gitlab::Database::Reflection.new(ActiveRecord::Base).exists? &&
    ActiveRecord::Base.connection.table_exists?('features') &&
    ::Feature.enabled?(:ai_duo_chat_switch, type: :ops)
  Feature.disable(:ai_duo_chat_switch)
end
# rubocop:enable Database/MultipleDatabases
