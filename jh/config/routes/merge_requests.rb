# frozen_string_literal: true

resources :merge_requests, only: [:bulk_merge], constraints: { id: /\d+/ } do
  member do
    post :bulk_merge

    scope action: :show do
      get :custom_page, to: 'merge_requests#custom_page', defaults: { tab: 'custom_page' }
    end
  end
end
