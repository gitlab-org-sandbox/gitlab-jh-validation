# frozen_string_literal: true

resources :provinces, only: [:index]
