# frozen_string_literal: true

resources :performance_measurement, only: :index, controller: 'performance/performance_measurement'
