# frozen_string_literal: true

namespace :bundle_checksum do
  desc "Generates jh/Gemfile.checksum.jh"
  task :jh do
    require Rails.root.join('jh/vendor/gems/bundler-checksum/lib/bundler_checksum/command/jh.rb')

    BundlerChecksum::Command::Jh.execute
  end
end
