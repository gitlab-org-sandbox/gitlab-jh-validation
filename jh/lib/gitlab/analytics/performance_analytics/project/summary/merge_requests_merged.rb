# frozen_string_literal: true

module Gitlab
  module Analytics
    module PerformanceAnalytics
      module Project
        module Summary
          class MergeRequestsMerged < Summary::Base
            def data
              Event.for_merge_request.for_action(:merged).created_at(time_filter_range).for_project(project).count
            end
          end
        end
      end
    end
  end
end
