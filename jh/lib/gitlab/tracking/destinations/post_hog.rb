# frozen_string_literal: true

module Gitlab
  module Tracking
    module Destinations
      class PostHog < Base
        extend ::Gitlab::Utils::Override

        override :event
        def event(category, action, label: nil, property: nil, value: nil, context: nil)
          return unless enabled?

          event = "#{category}\##{action}"
          context.to_a.each do |item|
            item = item.to_json # rubocop: disable Gitlab/Json (item is a SnowplowTracker::SelfDescribingJson)
            distinct_id = item.dig(:data, :user_id) || SecureRandom.uuid
            properties = {
              category: category,
              action: action,
              label: label,
              property: property,
              value: value,
              context: item
            }
            tracker.capture({ distinct_id: distinct_id, event: event, properties: properties })
          end
        end

        def enabled?
          ::Gitlab.com? && api_key.present?
        end

        def hostname
          ENV['POSTHOG_API_HOST'] || "https://app.posthog.com"
        end

        # compatible with Snowplow
        def options(_group)
          {}
        end

        private

        def api_key
          ENV['POSTHOG_API_KEY']
        end

        def tracker
          @tracker ||= ::PostHog::Client.new({
            api_key: api_key,
            api_host: hostname,
            on_error: proc do |status, msg|
              Gitlab::ErrorTracking.track_and_raise_for_dev_exception(StandardError.new(msg), status: status)
            end
          })
        end
      end
    end
  end
end
