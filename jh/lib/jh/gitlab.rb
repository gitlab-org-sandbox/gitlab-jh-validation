# frozen_string_literal: true

module JH
  module Gitlab
    CN_URL = 'https://jihulab.com'

    def self.hk?
      ENV['SAAS_REGION'] == 'HK'
    end

    def self.com_except_hk?
      ::Gitlab::Saas.enabled? && !hk?
    end

    def self.epic_support_milestone_and_iteration?
      ::Gitlab::Database.jh_database_configured? &&
        !::Gitlab::Saas.enabled? &&
        ::Feature.enabled?(:epic_support_milestone_and_iteration)
    end
  end
end
