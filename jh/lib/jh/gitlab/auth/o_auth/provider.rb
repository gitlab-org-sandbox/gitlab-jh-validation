# frozen_string_literal: true

module JH
  module Gitlab
    module Auth
      module OAuth
        module Provider
          module ClassMethods
            LABELS = { "dingtalk" => "DingTalk" }.freeze

            def label_for(name)
              LABELS[name] || super(name)
            end
          end
        end
      end
    end
  end
end
