# frozen_string_literal: true

module JH
  module Gitlab
    module Auth
      module OAuth
        module AuthHash
          extend ActiveSupport::Concern
          extend ::Gitlab::Utils::Override

          # Avoid containing invalid characters, and conflict
          override :generate_temporarily_email
          def generate_temporarily_email(_username)
            "temp-email-for-oauth-#{SecureRandom.uuid}@gitlab.localhost"
          end
        end
      end
    end
  end
end
