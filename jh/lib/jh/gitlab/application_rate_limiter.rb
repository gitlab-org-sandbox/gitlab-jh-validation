# frozen_string_literal: true

module JH
  module Gitlab
    module ApplicationRateLimiter
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :rate_limits
        def rate_limits
          jh_rate_limits = {
            email_exists: { threshold: 20, interval: 1.minute },
            invitation_email: { threshold: 50, interval: 1.day }
          }

          super.merge(jh_rate_limits)
        end
      end
    end
  end
end
