# frozen_string_literal: true

module JH
  module API
    module Helpers
      extend ::Gitlab::Utils::Override

      override :require_pages_enabled!
      def require_pages_enabled!
        not_found! unless user_project.pages_available?
      end
    end
  end
end
