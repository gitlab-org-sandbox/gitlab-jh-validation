# frozen_string_literal: true

module JH
  module Sidebars
    module Groups
      module Menus
        module SettingsMenu
          extend ::Gitlab::Utils::Override

          private

          override :gitlab_duo_usage_menu_item
          def gitlab_duo_usage_menu_item
            # use NilMenuItem to hide gitlab duo usage
            ::Sidebars::NilMenuItem.new(item_id: :gitlab_duo_usage)
          end
        end
      end
    end
  end
end
