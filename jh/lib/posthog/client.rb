# frozen_string_literal: true

module Posthog
  class Client
    attr_reader :posthog_api_key, :posthog_url

    def initialize(posthog_api_key:, posthog_url:)
      @posthog_api_key = posthog_api_key
      @posthog_url = posthog_url
    end

    def request(method:, path:, query_body:)
      query_body[:headers] = {
        Authorization: "Bearer #{posthog_api_key}"
      }
      url = URI.join(posthog_url, path)

      case method
      when :get
        ::Gitlab::HTTP.get(url, query_body)
      when :post
        ::Gitlab::HTTP.post(url, query_body)
      else
        raise "invalid method #{method}"
      end
    end

    # @param [String] posthog_url, posthog_api_key, utm_source
    # @param [Integer] project_id, limit
    # @param [Date] date_from, date_to
    def request_persons_funnel(utm_source:, project_id:, limit:, date_from:, date_to:)
      path = "/api/projects/#{project_id}/persons/funnel/"
      query_body = ClientHelper.query_body(utm_source: utm_source, date_from: date_from, date_to: date_to, limit: limit)
      request(method: :get, path: path, query_body: { query: query_body })
    end
  end

  module ClientHelper
    def self.query_body(utm_source:, date_from:, date_to:, limit: 1000)
      {
        date_from: date_from.to_s,
        date_to: date_to.to_s,
        display: "FunnelViz",
        funnel_step: "3",
        funnel_viz_type: "steps",
        funnel_window_interval: 14,
        funnel_window_interval_unit: "day",
        insight: "FUNNELS",
        interval: "day",
        layout: "horizontal",
        limit: limit,
        smoothing_intervals: "1",
        events: ::Gitlab::Json.dump(query_events(utm_source))
      }.freeze
    end

    def self.query_events(utm_source)
      [
        {
          id: "$pageview",
          type: "events",
          order: 0,
          name: "$pageview",
          custom_name: nil,
          math: nil,
          math_property: nil,
          math_group_type_index: nil,
          properties: {
            type: "AND",
            values: [{
              key: "$current_url",
              value: [
                "https://jihulab.com/users/sign_up?utm_source=#{utm_source}"
              ],
              operator: "exact",
              type: "event"
            }]
          }
        },
        {
          id: "$pageview",
          type: "events",
          order: 1,
          name: "$pageview",
          custom_name: nil,
          math: nil,
          math_property: nil,
          math_group_type_index: nil,
          properties: {
            type: "AND", values: [{
              key: "$current_url",
              value: "https://jihulab.com/users/almost_there",
              operator: "icontains",
              type: "event"
            }]
          }
        },
        {
          id: "$pageview",
          type: "events",
          order: 2,
          name: "$pageview",
          custom_name: nil,
          math: nil,
          math_property: nil,
          math_group_type_index: nil,
          properties: {
            type: "AND", values: [{
              key: "$current_url",
              value: "jihulab.com/users/sign_up/welcome",
              operator: "icontains",
              type: "event"
            }, {
              key: "$current_url",
              value: "continuous_onboarding_getting_started",
              operator: "not_icontains",
              type: "event"
            }]
          }
        }
      ]
    end
  end
end
