# frozen_string_literal: true

module Gitee
  module Error
    Unauthorized = Class.new(StandardError)
  end
end
