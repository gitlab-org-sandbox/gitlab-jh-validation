# frozen_string_literal: true

PATTERNS = %w[
  %a.btn.btn-
  %button.btn.btn-
  .alert
  .alert-danger
  .alert-dismissible
  .alert-info
  .alert-link
  .alert-primary
  .alert-success
  .alert-warning
  .nav-tabs
  .toolbar-button-icon
  .tooltip
  .tooltip-inner
  <button
  <tabs
  bs-callout
  deprecated-modal
  has-tooltip
  has_tooltip
  initDeprecatedJQueryDropdown
  loading-button
  v-popover
  v-tooltip
  with_tooltip
].freeze

BLOCKING_PATTERNS = %w[
  pagination-button
  graphql_pagination
].freeze

def get_added_lines(files)
  lines = []
  files.each do |file|
    lines += helper.changed_lines(file).select { |line| %r{^[+]}.match?(line) }
  end
  lines
end

changed_vue_haml_files = helper.changed_files(/.vue$|.haml$/)

return if changed_vue_haml_files.empty?

changed_lines_in_mr = get_added_lines(changed_vue_haml_files)
deprecated_components_in_mr = PATTERNS.select { |pattern| changed_lines_in_mr.any? { |line| line[pattern] } }
blocking_components_in_mr = BLOCKING_PATTERNS.select { |pattern| changed_lines_in_mr.any? { |line| line[pattern] } }

return if (deprecated_components_in_mr + blocking_components_in_mr).empty?

markdown(<<~MARKDOWN)
  ## 已弃用的组件

MARKDOWN

PAJAMAS_URL = "<!-- https://design.gitlab.com/components/overview -->"

if blocking_components_in_mr.any?
  markdown(<<~MARKDOWN)
    下列组件已被迁移并弃用且不再使用，请使用 [Pajamas 设计系统](#{PAJAMAS_URL})替代。

    * #{blocking_components_in_mr.join("\n* ")}

  MARKDOWN

  raise "此合并请求包含已迁移并弃用的组件。" \
    "请使用 Pajamas 设计系统。"
end

if deprecated_components_in_mr.any?
  markdown(<<~MARKDOWN)
    下列组件正在迁移合并，请考虑使用 [Pajamas 设计系统](#{PAJAMAS_URL})替代。

    * #{deprecated_components_in_mr.join("\n* ")}

  MARKDOWN

  warn "此合并请求包含弃用组件。请考虑使用 Pajamas 设计系统。"
end
