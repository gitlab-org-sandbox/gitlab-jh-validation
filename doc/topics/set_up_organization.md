---
stage: none
group: unassigned
description: Users, groups, namespaces, SSH keys.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Manage your organization

Configure your organization and its users. Determine user roles
and give everyone access to the projects they need.

| | | |
|--|--|--|
| [**Tutorial: Set up your organization**](../tutorials/manage_user/index.md) **{chevron-right}**<br><br>Setup, configuration, onboarding, organization structure. | [**Namespaces**](../user/namespace/index.md) **{chevron-right}**<br><br>Organization, hierarchy, project grouping. | [**Members**](../user/project/members/index.md) **{chevron-right}**<br><br>User management, roles, permissions, access levels. |
| [**Organization** (in development)](../user/organization/index.md) **{chevron-right}**<br><br>Namespace hierarchy. | [**Groups**](../user/group/index.md) **{chevron-right}**<br><br>Project management, access control, client groups, team groups. | [**Sharing projects and groups**](../user/project/members/sharing_projects_groups.md) **{chevron-right}**<br><br>Invitations, group inheritance, project visibility. |
| [**Compliance**](../administration/compliance.md) **{chevron-right}**<br><br>Compliance center, audit events, security policies, compliance frameworks. | [**Enterprise users**](../user/enterprise_user/index.md) **{chevron-right}**<br><br>Domain verification, two-factor authentication, enterprise user management, SAML response. | [**Service accounts**](../user/profile/service_accounts.md) **{chevron-right}**<br><br>Machine user, rate limits, personal access tokens. |
| [**User account options**](../user/profile/index.md) **{chevron-right}**<br><br>Profile settings, preferences, authentication, notifications. | [**SSH keys**](../user/ssh.md) **{chevron-right}**<br><br>Authentication, permissions, key types, ownership. | [**GitLab.com settings**](../user/gitlab_com/index.md) **{chevron-right}**<br><br>Instance configurations. |
